// $Id$
// //////////////////////////////////////////////////////////////////////
//  Simple TriggerIn to queue a HardwareTriggeredRequest every time 
// that there is not one already busy.
//
//  Author: J.Petersen CERN / G.Crone UCL
//
// //////////////////////////////////////////////////////////////////////

#include "ROSIO/EmulatedHardwareTriggerIn.h"
#include "DFDebug/DFDebug.h"
#include "DFSubSystemItem/Config.h"  // for Config, ROS

namespace ROS {
   class IOManager;
   class TriggerIn;
}
namespace daq { namespace rc { class TransitionCmd; } }


using namespace ROS;
/*****************************************************************************************/
void EmulatedHardwareTriggerIn::setup(IOManager* iomanager, DFCountedPointer<Config> configuration)
/*****************************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSIO, 10, "EmulatedHardwareTriggerIn::setup entered");

  HardwareTriggerIn::setup(iomanager, configuration); 

}

/*****************************************************************************************/
void EmulatedHardwareTriggerIn::prepareForRun(const daq::rc::TransitionCmd& cmd)
/*****************************************************************************************/
{
   DEBUG_TEXT(DFDB_ROSIO, 10, "EmulatedHardwareTriggerIn::prepareForRun entered");
   m_busy=false;
   HardwareTriggerIn::prepareForRun(cmd);
}

/*****************************************************************************************/
void EmulatedHardwareTriggerIn::stopGathering(const daq::rc::TransitionCmd& cmd)
/*****************************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSIO, 10, "EmulatedHardwareTriggerIn::stopEB entered");

  m_busy=true;
  HardwareTriggerIn::stopGathering(cmd);
}

/*****************************************************************************************/
void EmulatedHardwareTriggerIn::clear()
/*****************************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSIO, 10, "EmulatedHardwareTriggerIn::clear entered");

  m_busy = false;

}

/*****************************************************************************************/
bool EmulatedHardwareTriggerIn::waitForTrigger()
/*****************************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSIO, 10, "EmulatedHardwareTriggerIn::waitForTrigger entered");

  DEBUG_TEXT(DFDB_ROSIO, 20, "EmulatedHardwareTriggerIn::waitForTrigger:  m_busy = " << m_busy);

  if (m_busy) {
    return false;
  }
  else {
     m_busy = true;
     return true;
  }
}

/** Shared library entry point */
extern "C" {
   extern TriggerIn* createEmulatedHardwareTriggerIn();
}
TriggerIn* createEmulatedHardwareTriggerIn()
{
   return (new EmulatedHardwareTriggerIn());
}
