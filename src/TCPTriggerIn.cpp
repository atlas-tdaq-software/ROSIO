// $Id$
// //////////////////////////////////////////////////////////////////////
//  
//
//  Author:  G.J.Crone / E.Pasqualucci
//
//  $Log$
//  Revision 1.6  2006/11/01 12:57:38  gcrone
//  Use new ROSErrorReporting macros instead of dfout
//
//  Revision 1.5  2006/03/09 19:04:35  gcrone
//  IOMPlugin now uses new FSM methods
//
//  Revision 1.4  2005/08/16 17:03:09  gcrone
//  Include <errno.h> and change debug level of message in waitForTrigger()
//  to 20
//
//  Revision 1.3  2005/08/12 16:00:09  jorgen
//   changed names of NW attributes; added a bit of debug
//
//  Revision 1.2  2005/08/09 17:01:21  gcrone
//  Bug fix: pass addresses of inputBuffer / outputBuffer to read / write
//
//  Revision 1.1  2005/08/08 14:40:26  gcrone
//  Added TCPTriggerIn
//
//
// //////////////////////////////////////////////////////////////////////
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <sys/time.h>
#include <errno.h>
#include <stdio.h>                           // for perror
#include <string.h>                          // for memcpy
#include <unistd.h>                          // for close, read, write
#include <iostream>                          // for ostringstream, basic_ost...
#include <string>                            // for operator<<, allocator

#include "DFSubSystemItem/Config.h"
#include "DFDebug/DFDebug.h"

#include "ROSIO/IOException.h"
#include "ROSIO/TCPTriggerIn.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "DFThreads/DFCountedPointer.h"      // for DFCountedPointer
#include "ROSIO/HardwareTriggerIn.h"         // for HardwareTriggerIn

namespace ROS {
   class IOManager;
   class TriggerIn;
}
namespace daq { namespace rc { class TransitionCmd; } }

using namespace ROS;
/*****************************************************************************************/
void TCPTriggerIn::setup(IOManager* iomanager, DFCountedPointer<Config> configuration)
/*****************************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSIO, 10, "TCPTriggerIn::setup entered");

  HardwareTriggerIn::setup(iomanager, configuration); 


  m_socket = -1;

  m_destinationNode    = configuration->getString ("TriggerSourceIPAddress");
  m_port = (unsigned short) configuration->getInt ("TriggerSourcePort");
  m_connectTimeout = configuration->getInt ("ConnectTimeout") * 1000000;
}


/***************************************/
void TCPTriggerIn::connect(const daq::rc::TransitionCmd&)
/***************************************/
{
   DEBUG_TEXT(DFDB_ROSIO, 10, "TCPTriggerIn::connect entered");

  /* open a socket and connect to the destination node */

   m_socket = ::socket (AF_INET, SOCK_STREAM, 0);
   if (m_socket < 0) {
      CREATE_ROS_EXCEPTION(tException, IOException, IOException::SOCKET_CREATE_ERROR, "");
      throw tException;
   }

   int status;
   int option = 1;
#if 0
   status=setsockopt (m_socket, SOL_SOCKET, SO_KEEPALIVE,
                      &option, sizeof (option));
   if(status != 0) {
      close (m_socket);
      CREATE_ROS_EXCEPTION(tException, IOException, IOException::SOCKET_OPTION_ERROR, "option KEEPALIVE");
      throw tException;
   }
#endif

   status=setsockopt (m_socket, SOL_SOCKET, SO_REUSEADDR,
                      &option, sizeof (option));
   if (status != 0) {
      close (m_socket);
      CREATE_ROS_EXCEPTION(tException, IOException, IOException::SOCKET_OPTION_ERROR, "option REUSEADDR");
      throw tException;
   }

   // Do we need to use a non-default buffer size???
   // There should only be 1 byte sent in each direction at a time
//    option = s_bufSize;
//    status=setsockopt (m_socket, SOL_SOCKET, SO_SNDBUF,
//                       &option, sizeof (option));
//    if (status != 0) {
//       close (m_socket);
//       IOException tException (IOException::SOCKET_OPTION_ERROR);
//       dfout << setexception (tException) << setseverity (DFError::FATAL)
//             << "File " << __FILE__ << " line " << __LINE__
//             << " : option SNDBUF" << endm;
//       throw tException;
//    }

//    status=setsockopt (m_socket, SOL_SOCKET, SO_RCVBUF,
//                       &option, sizeof (option));
//    if (status != 0) {
//       close (m_socket);
//       IOException tException (IOException::SOCKET_OPTION_ERROR);
//       dfout << setexception (tException) << setseverity (DFError::FATAL)
//             << "File " << __FILE__ << " line " << __LINE__
//             << " : option RCVBUF" << endm;
//       throw tException;
//   }

   /* Set it non-blocking */
   status = fcntl (m_socket, F_SETFL, O_NONBLOCK) ;
   if (status != 0) {
      close (m_socket);
      CREATE_ROS_EXCEPTION(tException, IOException, IOException::SOCKET_OPTION_ERROR,
                           "setting non-blocking (fcntl)");
      throw tException;
   }

   option = 1; /* 0 to enable Nagle algorithm */
   if (setsockopt (m_socket,
                   getprotobyname ("tcp")->p_proto,
                   TCP_NODELAY,
                   &option, sizeof (option))) {
      close (m_socket);
      CREATE_ROS_EXCEPTION(tException, IOException, IOException::SOCKET_OPTION_ERROR, "option NODELAY");
      throw tException;
   }

   struct hostent *hptr = gethostbyname (m_destinationNode.c_str ());
   if (hptr == 0) {
      close (m_socket);
      CREATE_ROS_EXCEPTION(tException, IOException, IOException::ADDRESS_TRANSLATION_ERROR,
                           m_destinationNode);
      throw tException;
   }

   struct sockaddr_in saddr;
   saddr.sin_family      = AF_INET;
   saddr.sin_port        = htons (m_port);
   memcpy (&saddr.sin_addr.s_addr, hptr->h_addr, hptr->h_length);

   struct timeval t0;
   gettimeofday (&t0, 0);
   int connectStatus=-1;
   int connectBadCount=0;
   while (connectStatus==-1) {
      connectStatus = ::connect (m_socket, (struct sockaddr *) &saddr,
                               sizeof (saddr));
      if (connectStatus==-1) {
         connectBadCount++;
         struct timeval t1;
         gettimeofday (&t1, 0);

         if ((t1.tv_sec * 1000000 + t1.tv_usec) - 
             (t0.tv_sec * 1000000 + t0.tv_usec) > m_connectTimeout) {
            perror ("connect error");
            close (m_socket);
            CREATE_ROS_EXCEPTION(tException, IOException, IOException::TCP_CONNECT_ERROR, "Cannot connect to "
                                 << m_destinationNode << " port " << m_port);
            throw tException;
         }
      }
  }

   std::cout << " # connect's with status -1 = " << connectBadCount << std::endl;

   DEBUG_TEXT(DFDB_ROSIO, 10, "TCPTriggerIn::connect finished");
}

/*****************************************************/
void TCPTriggerIn::disconnect(const daq::rc::TransitionCmd&)
/*****************************************************/
{
   if (m_socket >= 0) {
      close (m_socket);
      m_socket = -1;
   }
}

/*****************************************************************************************/
void TCPTriggerIn::clear()
/*****************************************************************************************/
{
   DEBUG_TEXT(DFDB_ROSIO, 10, "TCPTriggerIn::clear entered");

   char outputBuffer=1;
   int nwritten=write(m_socket, &outputBuffer, sizeof(outputBuffer));
   DEBUG_TEXT(DFDB_ROSIO, 5, "TCPTriggerIn::clear, wrote " << (int)outputBuffer);
   if (nwritten < 0) {
      CREATE_ROS_EXCEPTION(tException, IOException, IOException::TCP_SEND_ERROR,"Error sending to "
            << m_destinationNode);
      throw tException;
   }
}

/*****************************************************************************************/
bool TCPTriggerIn::waitForTrigger()
/*****************************************************************************************/
{
   DEBUG_TEXT(DFDB_ROSIO, 20, "TCPTriggerIn::waitForTrigger entered");

   char inputBuffer;
   int nread=read(m_socket, &inputBuffer, sizeof(inputBuffer));

   if (nread > 0) {
      DEBUG_TEXT(DFDB_ROSIO, 5, "TCPTriggerIn::waitForTrigger read char " << (int)inputBuffer);
      return true;
   }
   else {
      if ((errno == EAGAIN) || (errno == EINTR)) {
         return false;
      }
      else {
         CREATE_ROS_EXCEPTION(tException, IOException, IOException::TCP_SEND_ERROR,"Error reading from "
                              << m_destinationNode);
         throw tException;
      }
   }
}

/** Shared library entry point */
extern "C" {
   extern TriggerIn* createTCPTriggerIn();
}
TriggerIn* createTCPTriggerIn()
{
   return (new TCPTriggerIn());
}
