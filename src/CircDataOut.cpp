//
// //////////////////////////////////////////////////////////////////////
#include <iostream>
#include <cstring>                          // for memcpy
#include <string>                            // for allocator, operator<<

#ifdef YIELD
# include <sched.h>
#endif

#include "DFSubSystemItem/Config.h"
#include "ROSIO/CircDataOut.h"
#include "ROSBufferManagement/Buffer.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSIO/IOException.h"
#include "rcc_time_stamp/tstamp.h"
#include "DFThreads/DFCountedPointer.h"      // for DFCountedPointer
#include "ROSCore/DataOut.h"                 // for DataOut (ptr only), NodeID
#include "ROSInfo/CircDataOutInfo.h"         // for CircDataOutInfo
#include "ROSMemoryPool/MemoryPage.h"        // for MemoryPage
#include "circ/Circ.h"                       // for CircReserve, CircClose

class ISInfo;
namespace daq { namespace rc { class TransitionCmd; } }

using namespace ROS;

CircDataOut::CircDataOut()
{
   m_stats.fragmentsOutput = 0;
   m_stats.bufferFull = 0;

   m_fragNum = 0;
   m_outputCirc = 0;
}

CircDataOut::~CircDataOut() noexcept
{
//   unconfigure();
}

void CircDataOut::sendData(const Buffer* buffer, NodeID /*destination*/,
                           unsigned int /*transactionId*/,
                           unsigned int /*fragStatus*/)
{
   unsigned int nyield = 0;

   if (m_outputDelay > 0) {
      ts_wait (m_outputDelay, &nyield);
   }

   if (m_samplingGap && m_stats.fragmentsOutput%m_samplingGap == 0) {

      // This is where we should actually write out data

     char *ptr;
     unsigned int bytes = buffer->size();

     if ((ptr = CircReserve (m_outputCirc, m_fragNum, bytes)) ==
         (char *) -1) {
       ++m_stats.bufferFull;

       if (m_throwIfFull)
         return;

       while ((ptr = CircReserve (m_outputCirc, m_fragNum, bytes)) ==
              (char *) -1) {
         ++m_stats.bufferFull;
#ifdef YIELD
         sched_yield ();
#endif
       }
     }
 
     char *p=ptr;

     for (Buffer::page_iterator page=buffer->begin();
	  page!=buffer->end(); page++) {
       int s=(*page)->usedSize();
       memcpy (p, (*page)->address(), s);
       p+=s;
     }

     m_stats.dataOutput+=buffer->size()/(float)(1024*1024);
     ++m_stats.fragmentsOutput; ++m_fragNum;
     CircValidate (m_outputCirc, m_stats.fragmentsOutput, ptr, bytes);
   }
}

void CircDataOut::configure(const daq::rc::TransitionCmd&)
{
  /* connect to the circular buffer for writing */

  if ((m_outputCirc = CircOpen (0, (char *) m_outputCircName.c_str(),
                                m_bufSize)) < 0)
  {
     CREATE_ROS_EXCEPTION(tException, IOException, CIRC_OPEN_ERROR,
                          " name = " << m_outputCircName << ", size = " << m_bufSize);
     throw tException;
  }
}

void CircDataOut::unconfigure(const daq::rc::TransitionCmd&)
{
   if (m_outputCirc != 0){
     /* disconnect from the circular buffer */

     std::cout << "disconnect" << std::endl;

     CircClose (m_outputCirc);
     m_outputCirc=0;
   }
}

void CircDataOut::prepareForRun (const daq::rc::TransitionCmd&)
{
   m_stats.fragmentsOutput = 0;
   m_stats.bufferFull = 0;
   m_fragNum = 1;
}

void CircDataOut::setup(DFCountedPointer<Config> configuration)
{
   m_outputDelay=configuration->getInt("outputDelay");
   m_bufSize=configuration->getInt("bufSize");

   m_outputCircName=configuration->getString("outputCircName");

   if (m_outputCircName.size() == 0)
   {
     CREATE_ROS_EXCEPTION (tException, IOException, CIRC_OPEN_ERROR, "Zero-length buffer name");
     throw tException;
   }

   m_samplingGap=configuration->getInt("samplingGap");
   m_throwIfFull=configuration->getBool("throwIfFull");

   std::cout << "CircDataOut::"
        << " writing data to Circular Buffer " << m_outputCircName
        << " every " << m_samplingGap << " calls to sendData"
        << ", output delay " << m_outputDelay
        << std::endl;
}

ISInfo* CircDataOut::getISInfo()
{
   return &m_stats;
}

extern "C" {
   extern DataOut* createCircDataOut();
}

DataOut* createCircDataOut()
{
   return (new CircDataOut());
}

