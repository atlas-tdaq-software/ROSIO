// $Id$
// //////////////////////////////////////////////////////////////////////
//  
//
//  Author:  G.J.Crone
//
//  $Log$
//  Revision 1.4  2008/07/18 13:11:42  gcrone
//  Specify namespace std:: where necessary
//
//  Revision 1.3  2008/06/04 16:38:42  gcrone
//  Check for delayed execution
//
//  Revision 1.2  2008/01/28 18:38:23  gcrone
//  Use new getISInfo method, remove getInfo. Plus DataChannel interface is now in ROSCore
//
//  Revision 1.1  2005/04/28 20:55:29  gorini
//  Adapted MonitorinDataOut to new emon package. Moved Request implementation from ROSCore to here. Moved abstract classes from here to ROSCore to avoid cross dependencies between packages.
//
//  Revision 1.8  2004/03/24 14:07:26  gorini
//  Merged NewArchitecture branch.
//
//  Revision 1.7.2.2  2004/03/19 10:11:55  gorini
//  Made DataRequest more generic so that also RCDDataRequests can inherit from it.
//  buildROSFragment method has become purely virtual and is now called buildFragment.
//  Added ROSDataRequest that provides a specific implementation of buildFragment for the ROS requests.
//
//  Revision 1.7.2.1  2004/03/16 23:54:08  gorini
//  Adapted IOManager and Request classes to new ReadouModule/DataChannel architecture
//
//  Revision 1.7  2003/07/11 13:18:02  gorini
//  Added std:: for gcc32 compilation
//
//  Revision 1.6  2003/04/02 09:57:59  joos
//  Time-out added for Data Requests
//
//  Revision 1.5  2003/02/05 09:26:04  gcrone
//  Rearranged Request class hierarchy.  All data requests
//  L2,TestBeam... now subclassed from DataRequest which has a
//  buildFragment method.
//  Request::execute now returns bool for success/(temporary) failure. Now
//  retry data requests where Request::execute returns false;
//
//  Revision 1.4  2002/11/27 14:29:07  gcrone
//  Pass level1Ids vector to constructor by reference
//
//  Revision 1.3  2002/11/08 15:31:03  gcrone
//  New error/exception handling
//
//  Revision 1.2  2002/10/27 14:45:17  jorgen
//   added trace flag; more complete time stamping; catch EventFragmentException
//
//  Revision 1.1.1.1  2002/10/07 09:50:07  pyannis
//  Initial version of DataFlow ROS packages tree...
//
//  Revision 1.1.1.1  2002/08/23 13:36:54  gorini
//  First import
//
//  Revision 1.2  2002/08/01 14:15:55  gcrone
//  First 'working' version using ROS namespace and dynamically loaded libraries
//
//  Revision 1.1.1.1  2002/06/21 16:18:28  pyannis
//  Creation of the cmtros directory that will hold the new ROS software
//  managed by CMT.
//  Various packages are already installed, e.g. Core, BufferManagement, DFThreads.
//
//
// //////////////////////////////////////////////////////////////////////
#include <iostream>
#include <string>
#include <vector>
#include "rcc_time_stamp/tstamp.h"

#include "ROSIO/ReleaseRequest.h"
#include "ROSCore/Request.h"
#include "ROSCore/DataChannel.h"
#include "DFDebug/DFDebug.h"



using namespace ROS;


ReleaseRequest::ReleaseRequest(const std::vector<unsigned int>& level1Ids,
                               std::vector<DataChannel*> * dataChannels) :
  Request(dataChannels), m_level1Ids(level1Ids)
{
}


int ReleaseRequest::execute(void)
{
  //std::cout << "ReleaseRequest::execute   function entered" << std::endl;
   if (timeToExecute()) {
      TS_RECORD(TS_H1,2410);

#if 0
      std::cout << "Executing ReleaseRequest for " << m_level1Ids.size()
                << " Ids (";
      for (unsigned int loop=0; loop<m_level1Ids.size(); loop++) {
         std::cout << m_level1Ids[loop];
         if ((loop < m_level1Ids.size() -1)) {
            std::cout << ",";
         }
      }
      std::cout << ")\n";
#endif

      for (std::vector<DataChannel*>::iterator fm=m_dataChannels->begin();
           fm!=m_dataChannels->end(); fm++) {
         TS_RECORD(TS_H1,2420);
         (*fm)->releaseFragment(&m_level1Ids) ;
         TS_RECORD(TS_H1,2430);
      }

      TS_RECORD(TS_H1,2440);
      return(REQUEST_OK);
   }
   else {
      DEBUG_TEXT(DFDB_ROSCORE, 20, "ReleaseRequest for " << m_level1Ids[0] << ".." << m_level1Ids[m_level1Ids.size()-1] << " not old enough\n");
      return(REQUEST_NOTFOUND);
   }
}

std::string ReleaseRequest::what()
{
   std::string description="ReleaseRequest";
   return (description);
}

std::ostream& ReleaseRequest::put(std::ostream& stream) const
{
   stream << "ReleaseRequest for " << m_level1Ids.size() << " events\n";
   return stream;
}
