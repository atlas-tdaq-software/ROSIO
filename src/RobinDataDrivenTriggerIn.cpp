// $Id$
// //////////////////////////////////////////////////////////////////////
//   data driven trigger for the Robin based ROS
//
//   The trigger reads pairs of (first,last) L1ID from a queue connected to the
//   DDT UserAction that polls the Robin to retrieve the L1ID info from the Robin(s)
//   It then calls the trigger generator to generate requests for this 'bunch' of L1IDs
//
// Authors: G.Crone, M.Joos & J.Petersen ATLAS/TDAQ
//
// //////////////////////////////////////////////////////////////////////
#include <iostream>                     // for operator<<, endl, basic_ostream

#include "ROSIO/RobinDataDrivenTriggerIn.h"
#include "DFSubSystemItem/Config.h"
#include "DFDebug/DFDebug.h"
#include "ROSCore/TriggerInputQueue.h"  // for TriggerInputQueue, ROS
#include "DFThreads/DFCountedPointer.h"      // for DFCountedPointer
#include "ROSIO/EmulatedTriggerIn.h"         // for EmulatedTriggerIn, Emula...
class ISInfo;

namespace ROS {
   class IOManager;
   class TriggerIn;
}
namespace daq { namespace rc { class TransitionCmd; } }


using namespace ROS;

// will call the constructor of the EmulatedTriggerIn
/************************************/
RobinDataDrivenTriggerIn::RobinDataDrivenTriggerIn() :
   m_inputToTriggerQueue(0), m_runActive(false)
/************************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "RobinDataDrivenTriggerIn::constructor:  entered");
}

// will "call" the destructor of the EmulatedTriggerIn
/************************************/
RobinDataDrivenTriggerIn::~RobinDataDrivenTriggerIn() noexcept
/************************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "RobinDataDrivenTriggerIn::destructor:  entered");
}


/*****************************************************************************************/
void RobinDataDrivenTriggerIn::setup(IOManager* iomanager, DFCountedPointer<Config> configuration)
/*****************************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "RobinDataDrivenTriggerIn::setup:  entered");

  EmulatedTriggerIn::setup(iomanager,configuration);
  m_configuration=configuration;			// not (yet) used
}

/*********************************/
void RobinDataDrivenTriggerIn::configure(const daq::rc::TransitionCmd& cmd)
/*********************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "RobinDataDrivenTriggerIn::configure:  entered");
  m_inputToTriggerQueue = TriggerInputQueue::instance();
  std::cout << " RobinDataDrivenTriggerIn: connect, Q pointer = " << m_inputToTriggerQueue
            << std::endl;

  EmulatedTriggerIn::configure(cmd);
}


/***********************************/
void RobinDataDrivenTriggerIn::unconfigure(const daq::rc::TransitionCmd& cmd)
/***********************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "RobinDataDrivenTriggerIn::unconfigure:  entered");

  EmulatedTriggerIn::unconfigure(cmd);

  m_inputToTriggerQueue->destroy();   // detach from Q
}

void RobinDataDrivenTriggerIn::stopGathering(const daq::rc::TransitionCmd& /*cmd*/)
/***********************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "RobinDataDrivenTriggerIn::stopGathering:  entered");

//  EmulatedTriggerIn::stopGathering(cmd);

   m_runActive=false;
   m_inputToTriggerQueue->abort();
}


/***************************************************/
ISInfo* RobinDataDrivenTriggerIn::getISInfo()
/***************************************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "RobinDataDrivenTriggerIn::getISInfo:  entered");

  // just call getinfo() of EmulatedTriggerIn for now
  return EmulatedTriggerIn::getISInfo();
}


/***************************/
void RobinDataDrivenTriggerIn::run()
/***************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "RobinDataDrivenTriggerIn::run:  entered");
  m_runActive=true;

  while (m_runActive) {
    DEBUG_TEXT(DFDB_ROSTG, 20, "RobinDataDrivenTriggerIn::run: popping trigger Q");
    try {
       unsigned int first_level1Id = m_inputToTriggerQueue->pop();
       unsigned int last_level1Id = m_inputToTriggerQueue->pop();
       DEBUG_TEXT(DFDB_ROSTG, 10, "RobinDataDrivenTriggerIn::run: first L1ID = 0x" << std::hex << first_level1Id
                  << "  last L1ID = 0x" << last_level1Id << " popped from trigger Q");

       m_generator->generateL1idBunch(first_level1Id, last_level1Id) ;
    }
    catch (tbb::user_abort& abortException) {
       m_runActive=false;
    }
  }
}


/*******************************/
void RobinDataDrivenTriggerIn::cleanup()
/*******************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "RobinDataDrivenTriggerIn::cleanup:  entered");

  EmulatedTriggerIn::cleanup();

}

/** Shared library entry point */
extern "C" 
{
   extern TriggerIn* createRobinDataDrivenTriggerIn();
}
TriggerIn* createRobinDataDrivenTriggerIn()
{
   return (new RobinDataDrivenTriggerIn());
}
