// $Id$
// //////////////////////////////////////////////////////////////////////
//  
//
//  Author:  G.J.Crone, University College London
//
//  $Log$
//  Revision 1.2  2008/02/13 17:44:25  gcrone
//  Update to new Event Fragment API
//
//  Revision 1.1  2008/02/07 11:38:05  gcrone
//  New fragment building support
//
//
// //////////////////////////////////////////////////////////////////////

#include "ROSIO/ROSFragmentBuilder.h"
#include "ROSCore/Request.h"
#include "ROSEventFragment/EventFragment.h"
#include "ROSEventFragment/FullFragment.h"
#include "ROSEventFragment/ROBFragment.h"

using namespace ROS;

EventFragment* ROSFragmentBuilder::createFragment(unsigned int /*level1Id*/, unsigned int /*nChannels*/) {
   FullFragment *rosFragment=new FullFragment(s_pool,
                                              s_runNumber,
                                              s_rosId);
   return rosFragment;
}
void ROSFragmentBuilder::appendFragment(EventFragment* parent, EventFragment* child) {
   FullFragment* fullFragment=dynamic_cast<FullFragment*>(parent);
   ROBFragment* robFragment=dynamic_cast<ROBFragment*>(child);
   fullFragment->append(robFragment);
}
