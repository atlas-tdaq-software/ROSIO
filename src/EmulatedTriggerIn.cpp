// $Id$
// //////////////////////////////////////////////////////////////////////
//  Dummy TriggerIn for testing
//
// The trigger generation algorithm is : for EACH L1id it's decided
// whether to generate a  request ( ROI, EB or delete) or not
// This results in a (small) overhead per L1ID but the code is much simpler.
//
// Authors: Markus Joos & Jorgen Petersen ATLAS/TDAQ
//
// NB! it is assumed  that ROLs are numbered consecutively from
// firstROL to lastROL e.g. 1 to 8
//
// //////////////////////////////////////////////////////////////////////

#include <vector>
#include <iostream>

#include "rcc_time_stamp/tstamp.h"
#include "ROSIO/EmulatedTriggerIn.h"
#include "DFSubSystemItem/Config.h"
#include "DFDebug/DFDebug.h"

#include "ROSCore/FragmentRequest.h"
#include "ROSIO/ReleaseRequest.h"

#include "ROSIO/IOException.h"
#include "ROSCore/DataChannel.h"
#include "ROSCore/CoreException.h"
#include "ROSUtilities/ROSErrorReporting.h"

#include "DFThreads/DFCountedPointer.h"      // for DFCountedPointer
#include "DFThreads/DFFastQueue.h"           // for DFFastQueue
#include "DFThreads/DFInputQueue.h"          // for DFInputQueue<>::InStatis...
#include "DFThreads/DFOutputQueue.h"         // for DFOutputQueue<>::OutStat...
#include "ROSCore/IOManager.h"               // for IOManager
#include "ROSIO/ROSFragmentBuilder.h"        // for ROSFragmentBuilder
#include "ROSIO/TriggerGenerator.h"          // for TriggerGenerator
#include "ROSInfo/DcTriggerInInfo.h"         // for DcTriggerInInfo

class ISInfo;
namespace ROS { class TriggerIn; }
namespace daq { namespace rc { class TransitionCmd; } }

using namespace ROS;


/************************************/
EmulatedTriggerIn::EmulatedTriggerIn() : m_generator(0),
                                         m_ebQueue(0), m_releaseQueue(0),
                                         m_tsStart({0,0}), m_tsStop({0,0}),
                                         m_tsStopLast({0,0})
/************************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "EmulatedTriggerIn::constructor:  called");
  m_numberOfLevel1Last=0;

}

/************************************/
EmulatedTriggerIn::~EmulatedTriggerIn() noexcept
/************************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "EmulatedTriggerIn::destructor:  called");
}

/*****************************************************************************************/
void EmulatedTriggerIn::setup(IOManager* iomanager, DFCountedPointer<Config> configuration)
/*****************************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "EmulatedTriggerIn::setup:  called");
  initialise(iomanager);
  m_configuration=configuration;

}

/*********************************/
void EmulatedTriggerIn::configure(const daq::rc::TransitionCmd&)
/*********************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "EmulatedTriggerIn::configure:  called");
  char ebqueue[]="EBQueue";
  m_ebQueue=DFFastQueue<unsigned int>::Create(ebqueue,200);
  char relqueue[]="RELEASEQueue";
  m_releaseQueue=DFFastQueue<unsigned int>::Create(relqueue,200);

  m_generator = new Generator(m_configuration,m_ioManager,m_ebQueue,m_releaseQueue);
}


/***********************************/
void EmulatedTriggerIn::unconfigure(const daq::rc::TransitionCmd&)
/***********************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "EmulatedTriggerIn::unconfigure:  called");
  delete m_generator;
}


/***************************************/
void EmulatedTriggerIn::prepareForRun(const daq::rc::TransitionCmd&)
/***************************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "EmulatedTriggerIn::prepareForRun:  called");
  ts_clock(&m_tsStart);
  m_tsStopLast = m_tsStart;
  m_numberOfLevel1Last=0;
  m_generator->clearCounters();
  m_generator->resetQueues();
  m_generator->start();
  startExecution();
}


/***********************************/
void EmulatedTriggerIn::stopGathering(const daq::rc::TransitionCmd&)
/***********************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "EmulatedTriggerIn::stopEB:  called");
  //  m_generator->flushReleaseQueue();
  m_generator->stop();
  stopExecution();
}


ISInfo* EmulatedTriggerIn::getISInfo() {
   return m_generator->getISInfo();
}

/***************************/
void EmulatedTriggerIn::run()
/***************************/
{
  // The main action thread.  Generates L2 requests, EB requests and
  // release requests in proportion controlled by the configuration. 
  DEBUG_TEXT(DFDB_ROSTG, 15, "EmulatedTriggerIn::run:  called");
  ts_clock(&m_tsStart);
  m_tsStopLast = m_tsStart;
  m_generator->generate(0, 1) ;
}


/*******************************/
void EmulatedTriggerIn::cleanup()
/*******************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "EmulatedTriggerIn::cleanup:  called");
  m_generator->printStatistics();
}


/***********************************************************************/
EmulatedTriggerIn::Generator::Generator(DFCountedPointer<Config> config,
                  IOManager* ioManager,
                  DFFastQueue<unsigned int>* ebQueue,
                  DFFastQueue<unsigned int>* releaseQueue)
          : TriggerGenerator(config),
            m_ioManager(ioManager),
            m_ebQueue(ebQueue),
            m_releaseQueue(releaseQueue),
            m_releaseRequestsReceived(0),
            m_deltaNumberOfLostClears(0),
            m_clearLossThreshold(0),
            m_runActive(false)
/***********************************************************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "EmulatedTriggerIn::Generator constructor  called");

}

/***********************************************************************/
EmulatedTriggerIn::Generator::~Generator() {

  DEBUG_TEXT(DFDB_ROSTG, 15, "EmulatedTriggerIn::Generator destructor  called");

}

void EmulatedTriggerIn::Generator::start(){
   m_runActive=true;
}
void EmulatedTriggerIn::Generator::stop(){
   m_runActive=false;
}

/***********************************************************************/
void EmulatedTriggerIn::Generator::l2Request(const unsigned int level1Id,
                                  const vector<unsigned int> * rols,
                                  const unsigned int destination,
                                  const unsigned int transactionId)
/***********************************************************************/
{
   std::vector<DataChannel*> * dataChannels = new std::vector<DataChannel*>;
   dataChannels->reserve(rols->size());

   TS_RECORD(TS_H1,1132);
   for (std::vector<unsigned int>::const_iterator rol=rols->begin();
        rol!=rols->end(); rol++) {
      try {
         dataChannels->push_back(DataChannel::channel(*rol));
      }
      catch (CoreException &e) {
         if (e.getErrorId() == CoreException::UNDEFINEDID) {
            ENCAPSULATE_ROS_EXCEPTION(newExecption, IOException, IOException::BAD_DATA_REQUEST, e,
                                      " level1Id=" << level1Id 
                                      << " rolId=" << hex << *rol);
            throw newExecption;
         }
         else {
            throw e;
         }
      }
   }

   m_ioManager->queueRequest(new FragmentRequest(level1Id,
                                                 dataChannels,
                                                 true,
                                                 false,
                                                 false,
                                                 &m_builder,
                                                 destination,
                                                 transactionId,
                                                 m_ebQueue)
                             );
   m_stats.l2RequestsQueued++;
}

/*****************************************************************************************/
void EmulatedTriggerIn::Generator::releaseRequest(const vector<unsigned int>* level1Ids,
						  const unsigned int oldestLevel1Id)
/*****************************************************************************************/
{    
   m_releaseRequestsReceived++;
   m_ioManager->queueRequest(new ReleaseRequest(*level1Ids, DataChannel::channels()));
   m_stats.clearRequestsQueued++;

   m_stats.numberOfLevel1+=level1Ids->size();	// outside or inside loop?  FIXME
}

/*****************************************************************************************/
void EmulatedTriggerIn::Generator::ebRequest(const unsigned int l1Id,
                                  const unsigned int destination)
/*****************************************************************************************/
{
   FragmentRequest* req=new FragmentRequest(l1Id,
                                            DataChannel::channels(),
                                            false,
                                            true,
                                            false,
                                            &m_builder,
                                            destination,
                                            0,
                                            m_releaseQueue);
   m_ioManager->queueRequest(req);
   m_stats.ebRequestsQueued++;
}

/*****************************************************************************************/
void EmulatedTriggerIn::Generator::flushReleaseQueue(void)
/*****************************************************************************************/
{
  processReleaseQueue(true);
}

/*****************************************************************************************/
void EmulatedTriggerIn::Generator::clearCounters()
/*****************************************************************************************/
{
   m_stats.l2RequestsQueued=0;
   m_stats.clearRequestsQueued=0;
   m_stats.clearRequestsLost=0;
   m_stats.gcRequestsQueued=0;
   m_stats.numberOfLevel1=0;
   m_stats.ebRequestsQueued=0;
   m_releaseRequestsReceived=0;
}

void EmulatedTriggerIn::Generator::resetQueues()
{
   DEBUG_TEXT(DFDB_ROSTG, 10, "EmulatedTriggerIn::Generator::resetQueues Clearing " << m_ebQueue->numberOfElements()
             << " entries from EB queue, " << m_releaseQueue->numberOfElements() << " entries from release queue");

   std::vector<unsigned int>* ebLeftovers=m_ebQueue->reset();
   if (ebLeftovers!=0) {
      delete ebLeftovers;
   }
   std::vector<unsigned int>* releaseLeftovers=m_releaseQueue->reset();
   if (releaseLeftovers!=0) {
      delete releaseLeftovers;
   }
   set_level1Id(0);
}

/** Shared library entry point */
extern "C" 
{
   extern TriggerIn* createEmulatedTriggerIn();
}
TriggerIn* createEmulatedTriggerIn()
{
   return (new EmulatedTriggerIn());
}
