// $Id$
// //////////////////////////////////////////////////////////////////////
//  
//
//  Author:  G.J.Crone, University College London
//
//  $Log$
//  Revision 1.2  2008/02/15 17:30:24  gcrone
//  Remove spurious reference to ROSFragment.h
//
//  Revision 1.1  2008/02/13 17:44:25  gcrone
//  Update to new Event Fragment API
//
//
// //////////////////////////////////////////////////////////////////////

#include "ROSIO/FullFragmentBuilder.h"
#include "ROSCore/Request.h"
#include "ROSEventFragment/EventFragment.h"
#include "ROSEventFragment/FullFragment.h"

using namespace ROS;

EventFragment* FullFragmentBuilder::createFragment(unsigned int /*level1Id*/, unsigned int /*nChannels*/) {
   FullFragment *myFragment=new FullFragment(s_pool,
                                             s_runNumber,
                                             s_sdId);
   return myFragment;
}
void FullFragmentBuilder::appendFragment(EventFragment* parent, EventFragment* child) {
   FullFragment* fullFragment=dynamic_cast<FullFragment*>(parent);
   FullFragment* subFragment=dynamic_cast<FullFragment*>(child);
   fullFragment->append(subFragment);
}
