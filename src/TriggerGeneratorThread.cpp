// //////////////////////////////////////////////////////////////////////
// 
// Trigger generating thread
//
// //////////////////////////////////////////////////////////////////////

#include <vector>
#include <iostream>
#include <cstdlib>

#include "rcc_time_stamp/tstamp.h"

#include "ROSIO/TriggerGeneratorThread.h"
#include "DFSubSystemItem/Config.h"
#include "DFThreads/DFCountedPointer.h"    // for DFCountedPointer
#include "DFThreads/DFThread.h"            // for DFThread


using namespace ROS;

TriggerGeneratorThread::TriggerGeneratorThread(l2Request_callback l2Request,
					       ebRequest_callback ebRequest,
					       releaseRequest_callback releaseRequest) :
  m_l2Request(l2Request),
  m_ebRequest(ebRequest),
  m_releaseRequest(releaseRequest)
{
  std::cout << "TriggerGeneratorThread constructor\n";   
  m_numberOfL1ids = 0;
  m_numberOfL2A = 0;
  m_numberOfDeleteGroup = 0;

  for (int i  = 0; i < MAX_ROLS; i++) 
  {
    m_roihist[i] = 0;
    m_numberOfROI[i] = 0;
  }
}

void TriggerGeneratorThread::start()
{
   startExecution();
}
void TriggerGeneratorThread::configure(DFCountedPointer<Config> configuration)
{
   m_config=configuration;
}

/** The main action thread.  Generates L2 requests, EB requests and
 release requests in proportion controlled by the configuration.
*/
void TriggerGeneratorThread::run()
{
  int nextToBeReleased = 0;
  int level1Id = 0;
 
  int alea[ALEASIZE];
  int roip_i[MAX_ROLS];		// ROI % scaled up
  int l2ap_i;			// L2A % scaled up
  int rol, noRolsperROI, next;
  unsigned int nyield;
  bool l2ReqFlag;

  int deleteGrouping = m_config->getInt("deleteGrouping");
  if (deleteGrouping == 0)
    deleteGrouping = 10;

  EMDEBUG(" run: deletegrouping = " << deleteGrouping);

  std::vector<unsigned int> l1Ids;		// for deletes
  l1Ids.reserve(deleteGrouping);

  // assume ROLs in ROI consecutive
  int firstRolInRos = m_config->getInt("firstRol");
  int lastRolInRos = m_config->getInt("lastRol");

  m_numberOfRols = lastRolInRos-firstRolInRos+1 ;
  EMDEBUG(" run: ROLS : first, last, number  = " <<
           firstRolInRos << "  " << lastRolInRos << "  " << m_numberOfRols);

  std::vector<unsigned int> rols;
  rols.reserve(m_numberOfRols);	// zero elements, but memory reserved

  double l2RequestFraction = (double) m_config->getInt("l2RequestFraction");

  for(int i = 0; i < m_numberOfRols; i++)
  {
    roip_i[i] = (int) ((l2RequestFraction / 100.0) * (double)RAND_MAX);
    EMDEBUG(" run: rol = " << i << " ROI % scaled up = " << roip_i[i]);
    EMDEBUG(" run: RAND_MAX = " << RAND_MAX);
  }
   
  unsigned int ebRequestFraction = (m_config->getInt("ebRequestFraction"));
  l2ap_i = (int) ((ebRequestFraction / 100.0) * (double)RAND_MAX);
  EMDEBUG(" run: L2A % scaled up = " << l2ap_i);

  unsigned int inputDelay = m_config->getInt("inputDelay");

  unsigned int destination = m_config->getInt("destinationNodeId");

  std::cout << "TriggerGeneratorThread: L2Request fraction " << l2RequestFraction  
       << "% per Robin (" << m_numberOfRols << ")"  << std::endl;
  std::cout << " EBRequest fraction " << ebRequestFraction << "%" << std::endl;
  std::cout << " deletes grouped in " << deleteGrouping << " s" << std::endl;
  std::cout << " input delay " << inputDelay << " microseconds\n";

  srand(3);	// the seed, why not ..
  for(int loop = 0; loop < ALEASIZE; loop++)
  {
    alea[loop] = rand();
  }

  level1Id = 0;
  noRolsperROI = 0;
  next = 0;

  while (1) {		// loop over all L1id's

    TS_RECORD(TS_H1,1000);	// request handle start

    l2ReqFlag = false;
    for(rol = 0; rol < m_numberOfRols; rol++)
    {
      if (alea[next] <= roip_i[rol])
      {
        l2ReqFlag = true;
        EMDEBUG(" run: L1id =  " << level1Id<< "  rol = " << rol);	// the logical ROL
        rols.push_back(firstRolInRos + rol);				// the physical ROL
        m_numberOfROI[rol]++;		// count per ROL
        noRolsperROI++;
      }
      if (++next == ALEASIZE) next = 0;
    }
    m_roihist[noRolsperROI]++;			// each L1id?   FIXME

    if (l2ReqFlag)
    {
      //  emulate DC input
      nyield = 0;
      if (inputDelay > 0)
        ts_wait(inputDelay, &nyield);

      TS_RECORD(TS_H1,1110);
      EMDEBUG(" run: L1id =  " << level1Id << " ROI generated, ROL size =  " << rols.size());
      (*m_l2Request)(level1Id, &rols, destination);
      TS_RECORD(TS_H1,1190);

      noRolsperROI = 0;
      rols.clear();	// size = 0
      l2ReqFlag = false;

    }

    if (alea[next] <= l2ap_i)	// EB request
    {
      TS_RECORD(TS_H1,1200);

      m_numberOfL2A++;

    // emulate DC input
      nyield = 0;
      if (inputDelay > 0)
        ts_wait(inputDelay, &nyield);

      EMDEBUG(" run: L1id =  " << level1Id << " EB request ");
      (*m_ebRequest)(level1Id, destination);
      TS_RECORD(TS_H1,1290);

    }
    if (++next == ALEASIZE) next = 0;

    if ((level1Id % deleteGrouping) == 0 && level1Id != 0)	// delete request
    {

      TS_RECORD(TS_H1,1300);

  // emulate DC input
      nyield = 0;
      if (inputDelay > 0)
        ts_wait(inputDelay, &nyield);

      unsigned int l1Id = nextToBeReleased;
      for (int i = 0; i < deleteGrouping; i++)
      {
        l1Ids.push_back(l1Id);
        l1Id++;
      }

      TS_RECORD(TS_H1,1310);

      EMDEBUG(" run: L1id =  " << level1Id << " Delete Group request ");
      (*m_releaseRequest)(&l1Ids);
      nextToBeReleased = l1Id;
      l1Ids.clear();

      TS_RECORD(TS_H1,1390);
      m_numberOfDeleteGroup++;

      DFThread::cancellationPoint() ;	//  to reduce frequency
    }

    level1Id++;
    m_numberOfL1ids = level1Id;

    TS_RECORD(TS_H1,1490);

  }

}

void TriggerGeneratorThread::cleanup()
{

  std::cout << std::endl;
  std::cout << " TriggerGeneratorThread::cleanup " << std::endl;
  std::cout << " Number of events = " << m_numberOfL1ids + 1 << std::endl;

  for ( int rol = 0; rol <= m_numberOfRols; rol++)
  {
    std::cout << " Number of events with " << rol << " ROLs per ROI = " << m_roihist[rol] << std::endl;
  }
  for ( int rol = 0; rol < m_numberOfRols; rol++)
  {
    float reqPerRobin = 100.0 * ((float)m_numberOfROI[rol] / m_numberOfL1ids);
    std::cout << " Number of ROI requests for (logical) ROL " << rol << " = " <<
             m_numberOfROI[rol] << " in % = " << reqPerRobin << std::endl;
  }
  float L2APer = 100.0 * ((float)m_numberOfL2A /  m_numberOfL1ids);
  std::cout << " Number of events with L2A = " << m_numberOfL2A << " in % = " << L2APer << std::endl;
  std::cout << " Number of delete Groups   = " << m_numberOfDeleteGroup << std::endl;
  std::cout << std::endl;

}

void TriggerGeneratorThread::stop()
{
   stopExecution();
}
