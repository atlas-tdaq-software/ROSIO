// $Id$
// //////////////////////////////////////////////////////////////////////
//  
//
//  Author:  G.J.Crone, University College London
//
//  $Log$
//  Revision 1.2  2008/04/03 11:28:22  gcrone
//  Add closeFragment method to add ROD trailer
//
//  Revision 1.1  2008/02/15 17:29:39  gcrone
//  Adapt HardwareTriggerIn to new Fragment Rquest/Builder scheme.
//
//
// //////////////////////////////////////////////////////////////////////

#include "ROSIO/RODFragmentBuilder.h"
#include "ROSCore/Request.h"
#include "ROSEventFragment/EventFragment.h"
#include "ROSEventFragment/RODFragment.h"

using namespace ROS;

EventFragment* RODFragmentBuilder::createFragment(unsigned int level1Id, unsigned int nChannels) {
   RODFragment* rodFragment=new RODFragment(s_pool,
                                            s_rosId,
                                            level1Id,
                                            level1Id,  // Don't have a BCID
                                            0,
                                            0,
                                            nChannels,
                                            1,
                                            s_runNumber);
   return rodFragment;
}
void RODFragmentBuilder::appendFragment(EventFragment* parent, EventFragment* child) {
   RODFragment* rodFragment=dynamic_cast<RODFragment*>(parent);
   rodFragment->append(child);
}

void RODFragmentBuilder::closeFragment(EventFragment* fragment) {
   RODFragment* rodFragment=dynamic_cast<RODFragment*>(fragment);
   rodFragment->appendStatusAndTrailer();
}
