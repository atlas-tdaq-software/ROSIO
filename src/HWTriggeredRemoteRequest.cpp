// $Id$
// //////////////////////////////////////////////////////////////////////
//   Hardware Triggered Remote Request: Something that acts like a data
// Request but doesn't expect to get any data.
//
//  Author:  G.J.Crone
//
//  $Log$
//  Revision 1.5  2008/01/28 18:38:21  gcrone
//  Use new getISInfo method, remove getInfo. Plus DataChannel interface is now in ROSCore
//
//  Revision 1.4  2007/04/23 11:53:07  gcrone
//  Make all error messages use IOException
//
//  Revision 1.3  2006/11/29 16:58:15  gcrone
//  Remove obsolete references to DFOutputStream and DFError
//
//  Revision 1.2  2006/11/01 12:57:38  gcrone
//  Use new ROSErrorReporting macros instead of dfout
//
//  Revision 1.1  2005/08/04 17:34:06  gcrone
//  Remove obsolete Requests and add HWTriggeredRemoteRequest
//
//
// //////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>                            // for string, allocator
#include <vector>                            // for vector, vector<>::iterator
#include "DFSubSystemItem/Config.h"          // for ROS
#include "ROSCore/DataRequest.h"             // for DataRequest
#include "ROSCore/Request.h"                 // for Request::REQUEST_OK, Req...
#include "ers/ers.h"                         // for warning

#include "ROSCore/TriggerIn.h"
#include "ROSIO/HWTriggeredRemoteRequest.h"
#include "ROSIO/IOException.h"
#include "ROSEventFragment/EventFragment.h"
#include "ROSCore/DataChannel.h"
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"

using namespace ROS;

HWTriggeredRemoteRequest::HWTriggeredRemoteRequest(unsigned int level1Id,
                                 TriggerIn* trigger):
   DataRequest(level1Id,DataChannel::channels(),0),
   m_trigger(trigger)
{
   DEBUG_TEXT(DFDB_ROSIO, 20, "HWTriggeredRemoteRequest:: constructor entered level1Id=" << level1Id);
}

bool HWTriggeredRemoteRequest::buildFragment()
{
   return true;
}

/** The action routine for the Request.  Here we loop over the data
    channels calling requestFragment and getFragment but we don't
    expect to recieve any actual data fragments.  If we do we just
    delete them immediately.
*/
int HWTriggeredRemoteRequest::execute(void)
{
   DEBUG_TEXT(DFDB_ROSIO, 20, "HWTriggeredRemoteRequest::execute entered");
   bool retired = checkAge();

   if (retired)
   {
      std::cout << "HWTriggeredRemoteRequest::execute  TIME OUT !!!!" << std::endl;
      return(REQUEST_TIMEOUT);
   }

   //This code is to record the last runs through this function with 
   //the ring buffer mode of the rcc_time_stamp package
   //unsigned int tsdata = (m_level1Id * 100) + 2000000000;
   //TS_RECORD(TS_H5,(tsdata+1));

   int channel=0;
   // Pre-request "data" from all DataChannels involved
   for (vector<DataChannel *>::iterator dc=m_dataChannels->begin ();
	dc != m_dataChannels->end (); dc++)
   {
     m_ticket[channel] = (*dc)->requestFragment (m_level1Id);   
     channel++ ;
   }

   // Now really get the data
   channel=0;
   for (vector<DataChannel *>::iterator dc=m_dataChannels->begin();
        dc != m_dataChannels->end (); dc++)
   {
      EventFragment* fragment = ((*dc)->getFragment (m_ticket[channel])) ;

      if (fragment != 0) 
      {
         CREATE_ROS_EXCEPTION(ex, IOException, UNEXPECTED_DATA, "in HWTriggeredRemoteRequest");
         ers::warning(ex);
         delete fragment;
      }
      channel++;
   }

   m_trigger->clear();
   DEBUG_TEXT(DFDB_ROSIO, 20, "HWTriggeredRemoteRequest::execute done");
   return (REQUEST_OK);
}

string HWTriggeredRemoteRequest::what()
{
   string description="HWTriggeredRemote Request";
   return (description);
}

::ostream& HWTriggeredRemoteRequest::put(::ostream& stream) const
{
   stream << "HWTriggeredRemote Request for event " << m_level1Id ;
   return stream;
}
