#include <string.h>
#include <iomanip>

#include "ROSIO/MEtFragmentBuilder.h"
#include "ROSIO/MEtException.h"

#include "ROSMemoryPool/MemoryPool_malloc.h"
#include "ROSEventFragment/ROBFragment.h"
#include "DFThreads/DFFastMutex.h"

#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"

#include "ROSCore/DataChannel.h"

using namespace ROS;

MEtFragment::MEtFragment(MemoryPool* pool, unsigned int sourceId)
   : m_firstAppend(true), m_sourceId(sourceId),
     m_ready(false), m_needHeader(true) {
   DEBUG_TEXT(DFDB_ROSIO, 10, "MEtFragment constructor called");

   m_buffer=new Buffer(pool);

#if 0
   m_writePage=pool->getPage();
   m_writePtr=static_cast<u_int *> (m_writePage->address());
#else
   m_writePage=pool->getPage();
   m_buffer->append(*m_writePage);
   m_writePage->free();

   //   m_buffer->reserve(pool->pageSize()); // Empty front page!
   int headerSize=sizeof(ROBFragment::ROBHeader)-ROBFragment::s_nStatusElements*sizeof(u_int)+MEtFragment::s_nStatusElements*sizeof(u_int);
   headerSize+=sizeof(RODFragment::RODHeader);
   // Add extra sizeof(u_int) for robCount word
   m_writePtr=static_cast<u_int *> (m_buffer->reserve(headerSize+sizeof(u_int)));

   //   std::cout << ",  free pages=" << pool->numberOfFreePages() << std::endl;
#endif
   m_header=(ROBFragment::ROBHeader*) m_writePtr;
   m_header->generic.headerSize=(sizeof(ROBFragment::ROBHeader)/sizeof(u_int))-ROBFragment::s_nStatusElements+MEtFragment::s_nStatusElements;
   m_writePtr+=m_header->generic.headerSize;
   m_rodHeader=(RODFragment::RODHeader*) m_writePtr;
   m_writePtr+=sizeof(RODFragment::RODHeader)/sizeof(u_int);

   m_robCount=m_writePtr++;   // Initialise upper bits of robCount with words/rob
   if (sourceId>>16==0x007d) {
      *m_robCount=10<<16;     // 10 for LAr
   }
   else {
      *m_robCount=16<<16;     // 16 for Tile
   }

   m_header->statusElement[0]=0;
   m_header->statusElement[1]=0;
   m_status=&m_header->statusElement[0];
   m_header->generic.startOfHeaderMarker=s_robMarker;

   m_header->generic.totalFragmentsize=m_header->generic.headerSize+sizeof(RODFragment::RODHeader)/sizeof(u_int)+1;
   m_header->generic.formatVersionNumber=s_formatVersionNumber;
   m_header->generic.numberOfStatusElements=MEtFragment::s_nStatusElements;
   m_header->generic.sourceIdentifier=sourceId;
   m_rodHeader->startOfHeaderMarker=s_rodMarker;
   m_rodHeader->headerSize=sizeof(RODFragment::RODHeader) / sizeof (u_int);

   DEBUG_TEXT(DFDB_ROSIO, 10, "MEtFragment constructor finished");
}

MEtFragment::~MEtFragment() {
   delete m_buffer;
}

u_int MEtFragment::size() {
   return m_header->generic.totalFragmentsize;
}
u_int MEtFragment::level1Id() {
   return m_rodHeader->level1Id;
}
u_int MEtFragment::runNumber() {
   return 0;
}
u_int MEtFragment::status() {
   return *m_status;
}
void MEtFragment::setStatus(u_int word) {
   *m_status=word;
}
bool MEtFragment::fragmentReady() {
   return m_ready;
}
Buffer* MEtFragment::buffer() const {
   return m_buffer;
}
void MEtFragment::print(std::ostream& out) const {
   out << "MEtFragment";
}

bool MEtFragment::processFeb(unsigned int* dataPtr) {
   unsigned int offset=dataPtr[3]>>16;
   //   std::cout << "offset=" << offset << std::endl;
   if (offset!=0) {
      unsigned int nSamples=dataPtr[7]&0xffff;
      //      std::cout << "number of samples=" << nSamples << std::endl;
      if (nSamples==5) {
         offset+=75;
      }
      else if (nSamples==7) {
         offset+=76;
      }
      else {
         // Bad number of samples
         CREATE_ROS_EXCEPTION(nsampException, MEtException, MEtException::BAD_NSAMPLES, " " <<nSamples);
         ers::error(nsampException);
         *m_status|=0x00020008;
         return false;
      }

      DEBUG_TEXT(DFDB_ROSIO, 20, "Adding FEB block  id=" << dataPtr[1]
                 << ",  xenergy=" << dataPtr[offset]
                 << ",  yenergy=" << dataPtr[offset+1]
                 << ",  zenergy=" << dataPtr[offset+2]
                 << ",  summed=" << dataPtr[offset+3]
                 << std::dec);
      *m_writePtr++=dataPtr[1];         // FEB id
      *m_writePtr++=dataPtr[offset];    // X energy
      *m_writePtr++=dataPtr[offset+1];  // Y energy
      *m_writePtr++=dataPtr[offset+2];  // Z energy
      *m_writePtr++=dataPtr[offset+3];  // Summed energy
      m_header->generic.totalFragmentsize+=5;
      return true;
   }
   else {
      CREATE_ROS_EXCEPTION(lengthException, MEtException, MEtException::BAD_LENGTH, "offset=0  FEBID 0x"<<HEX(dataPtr[1]));
      ers::warning(lengthException);
      *m_status|=0x00040008;
      return false;
   }
}

void MEtFragment::addEmptyBlock() {
   int blockSize=4;
   if (m_sourceId>>16==0x007d) {
      blockSize=5;
   }
   for (int word=0; word<blockSize; word++) {
      *m_writePtr++=0;
   }
   m_header->generic.totalFragmentsize+=blockSize;
}

void MEtFragment::append(ROBFragment* robFragment) {
   DEBUG_TEXT(DFDB_ROSIO, 10, "MEtFragment::append() called");
   Buffer::page_iterator bufPage=robFragment->buffer()->begin();
   unsigned int* dataPtr=(unsigned int*) (*bufPage)->address();
   ROBFragment::ROBHeader* robHeader=(ROBFragment::ROBHeader*) dataPtr;
   dataPtr+=robHeader->generic.headerSize;
   RODFragment::RODHeader* rodHeader=(RODFragment::RODHeader*) dataPtr;
   dataPtr+=rodHeader->headerSize;
   if (m_firstAppend) {
      m_firstAppend=false;

      m_rodHeader->sourceIdentifier=(rodHeader->sourceIdentifier&0xffff)|(m_sourceId&0xffff0000);
      DEBUG_TEXT(DFDB_ROSIO, 20, "input rodHeader->sourceIdentifier="
                 << std::hex << rodHeader->sourceIdentifier
                 << " modified rodHeader->sourceIdentifier="
                 << m_rodHeader->sourceIdentifier
                 << std::dec);
      m_rodHeader->runNumber=rodHeader->runNumber;
      m_rodHeader->level1Id=rodHeader->level1Id;
   }

   // Make sure that the rest of the header is copied from a real ROB
   // fragment and not one invented by the robin
   if (m_needHeader) {
      if (robHeader->statusElement[0]==0) {
         m_needHeader=false;
      }
      m_rodHeader->formatVersionNumber=rodHeader->formatVersionNumber;
      m_rodHeader->bunchCrossingId=rodHeader->bunchCrossingId;
      m_rodHeader->level1TriggerType=rodHeader->level1TriggerType;
      m_rodHeader->detectorEventType=rodHeader->detectorEventType;
      DEBUG_TEXT(DFDB_ROSIO, 20, "MEtFragment::append() copied ROD header" << std::hex
                 <<"  formatVersionNumber=" << m_rodHeader->formatVersionNumber
                 <<", bunchCrossingId=" << m_rodHeader->bunchCrossingId
                 <<", detectorEventType=" << m_rodHeader->detectorEventType
                 <<std::dec);
   }

   if (m_sourceId>>16==0x007d) {
      // LAr

      m_writePtr=static_cast<u_int *> (m_buffer->reserve(10*sizeof(u_int)));

      bool headerOK=false;
      if (robHeader->statusElement[0]!=0) {
         DEBUG_TEXT(DFDB_ROSIO, 20, "MEtFragment::append() input robHeader has status=" << std::hex << robHeader->statusElement[0] << std::dec);
         *m_status|=robHeader->statusElement[0];
      }
      else {
         if (rodHeader->detectorEventType==4) {
            headerOK=true;
         }
         else {
            CREATE_ROS_EXCEPTION(etypeException, MEtException, MEtException::BAD_TYPE, " " <<std::hex << rodHeader->detectorEventType << std::dec);
            ers::error(etypeException);
            *m_status|=0x00010008;
         }
      }

      if (headerOK) {
         int offset=0;
         for (int block=0;block<2;block++) {
            bool febOK=false;
            if (robHeader->generic.totalFragmentsize > robHeader->generic.headerSize + rodHeader->headerSize + offset) {
               febOK=processFeb(dataPtr);
            }
            if (!febOK) {
               addEmptyBlock();
            }
            offset=dataPtr[0]+7;
            dataPtr+=offset;
         }
      }
      else {
         addEmptyBlock();
         addEmptyBlock();
      }
   }
   else {
      // Tile

      m_writePtr=static_cast<u_int *> (m_buffer->reserve(16*sizeof(u_int)));

      int blockInserted=0;
      unsigned int totalDataSize=0;
      unsigned int rodDataSize=robHeader->generic.totalFragmentsize-(robHeader->generic.headerSize+rodHeader->headerSize);
      if (robHeader->statusElement[0]!=0) {
         DEBUG_TEXT(DFDB_ROSIO, 20, "MEtFragment::append() input robHeader has status=" << std::hex << robHeader->statusElement[0] << std::dec);
         *m_status|=robHeader->statusElement[0];
      }
      else {
         if (rodHeader->detectorEventType!=1) { // Physics event=1 for Tile
            CREATE_ROS_EXCEPTION(etypeException, MEtException, MEtException::BAD_TYPE, " " <<std::hex << rodHeader->detectorEventType << std::dec);
            ers::error(etypeException);
            *m_status|=0x00010008;
         }
         else {
            int block=0;
            while (dataPtr[0]==0xff1234ff && totalDataSize<rodDataSize) {
               unsigned int blockSize=dataPtr[1];
               unsigned int blockType=dataPtr[2]&0x000f0000;
               DEBUG_TEXT (DFDB_ROSIO, 15, "block " << block << ", type " <<std::hex << blockType <<std::dec << ", size " << blockSize);
               if (blockType==0x00040000 && blockSize==54) {
#if DEBUG_LEVEL>0
                  if ((DF::GlobalDebugSettings::packageId()==DFDB_ROSIO) || (DF::GlobalDebugSettings::packageId()==0)) {
                     if (DF::GlobalDebugSettings::traceLevel() >= 20) {
                        std::cout << std::hex << std::setfill('0');
                        for (int index=0; index<54; index++) {
                           std::cout << "   " << std::setw(8) << dataPtr[index];
                           if (index%8==7) std::cout << std::endl;
                        }
                        std::cout << std::setfill(' ') << std::dec << std::endl;
                     }
                  }
#endif
                  unsigned int drawerId=dataPtr[2];
                  DEBUG_TEXT (DFDB_ROSIO, 15, "Found energy sums " << std::hex
                              << "  Drawer=" << drawerId
                              << "  Et=" << dataPtr[51]
                              << "  EZ=" << dataPtr[52]
                              << "  Esum=" << dataPtr[53]
                              << std::dec);
                  *m_writePtr++=drawerId;
                  *m_writePtr++=dataPtr[51];
                  *m_writePtr++=dataPtr[52];
                  *m_writePtr++=dataPtr[53];
                  m_header->generic.totalFragmentsize+=4;
                  blockInserted++;
               }
               totalDataSize+=blockSize;
               if (totalDataSize<rodDataSize) {
                  dataPtr+=blockSize;
               }
               else {
                  CREATE_ROS_EXCEPTION(lengthException, MEtException, MEtException::BAD_LENGTH, "sub block length " << blockSize << ", total length " << totalDataSize << " exceeds ROD data length " << rodDataSize);
                  ers::error(lengthException);
                  *m_status|=0x00040008;
               }
               block++;
            }
         }
      }
      // Pad any missing blocks with 0
      for (int block=blockInserted; block<4; block++) {
         addEmptyBlock();
      }
   }
   (*m_robCount)++;
   DEBUG_TEXT(DFDB_ROSIO, 10, "MEtFragment::append() completed");
}


void MEtFragment::close() {
   DEBUG_TEXT(DFDB_ROSIO, 10, "MEtFragment::close() called");
   m_writePtr=static_cast<u_int *> (m_buffer->reserve(4*sizeof(u_int)));
   *m_writePtr++=*m_status;
   *m_writePtr++=1;  // status elements
   *m_writePtr++=m_header->generic.totalFragmentsize-m_header->generic.headerSize-m_rodHeader->headerSize;
   *m_writePtr++=1;  // status block follows data
   m_header->generic.totalFragmentsize+=4;

#if 0
   m_writePage->reserve(sizeof(unsigned int)*m_header->generic.totalFragmentsize);
   m_buffer->append(*m_writePage);
#endif

#if DEBUG_LEVEL>0
   if ((DF::GlobalDebugSettings::packageId()==DFDB_ROSIO) || (DF::GlobalDebugSettings::packageId()==0)) {
      if (DF::GlobalDebugSettings::traceLevel() >= 20) {
         m_buffer->print();
         std::cout << std::hex << std::setfill('0');
         //         unsigned int* tPtr=(unsigned int*)m_writePage->address();
         unsigned int* tPtr=(unsigned int*)m_header;
         for (unsigned int element=0; element<m_header->generic.totalFragmentsize; element++) {
            if (element%8==0) {
               std::cout << std::endl;
            }
            std:: cout << " " << std::setw(8) << *tPtr++;
         }
         std::cout << std::setfill(' ') << std::dec << std::endl;
      }
   }
#endif

#if 0
   m_writePage->free();
#endif

   m_ready=true;
   DEBUG_TEXT(DFDB_ROSIO, 10, "MEtFragment::close() finished");
}

MEtFragmentBuilder::MEtFragmentBuilder(unsigned int source) : m_sourceId(source) {
   m_requestMutex=DFFastMutex::Create((char *) "REQUESTMUTEX");

   int wordsPerChannel=16;     // 16 for Tile
   if (source>>16==0x007d) {
      wordsPerChannel=10;     // 10 for LAr
   }
   int headerSize=sizeof(ROBFragment::ROBHeader)-ROBFragment::s_nStatusElements*sizeof(u_int)+MEtFragment::s_nStatusElements*sizeof(u_int);
   headerSize+=sizeof(RODFragment::RODHeader);

   int nchannels=DataChannel::channels()->size();
   unsigned int fragSize=headerSize+(nchannels*wordsPerChannel*sizeof(u_int))+4*sizeof(u_int);
   if (fragSize > s_pool->pageSize()) {
      CREATE_ROS_EXCEPTION(sizeException, MEtException, MEtException::PAGE_SIZE, " page size " << s_pool->pageSize() << " words should be at least " << fragSize << " to avoid using multiple pages");
      ers::warning(sizeException);
   }
}

EventFragment* MEtFragmentBuilder::createFragment(unsigned int /*level1Id*/,
                                                  unsigned int /*nChannels*/) {
   DEBUG_TEXT(DFDB_ROSIO, 10, "MEtFragmentBuilder::createFragment() called");
   MEtFragment* fragment=new MEtFragment(s_pool,m_sourceId);
   DEBUG_TEXT(DFDB_ROSIO, 10, "MEtFragmentBuilder::createFragment() finished");
   return fragment;
}


void MEtFragmentBuilder::appendFragment(EventFragment* parent,
                                        EventFragment* child) {
   MEtFragment* metFragment=dynamic_cast<MEtFragment*>(parent);
   ROBFragment* robFragment=dynamic_cast<ROBFragment*>(child);

   metFragment->append(robFragment);
}


void MEtFragmentBuilder::closeFragment(EventFragment* parent) {
   MEtFragment* frag=dynamic_cast<MEtFragment*>(parent);
   frag->close();
}
