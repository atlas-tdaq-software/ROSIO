// $Id$
// //////////////////////////////////////////////////////////////////////
//  Receives messages from the SWRobin thread and produced Accept requests
//  For test beam & ROD Crate DAQ 
//
//  Author:  J.O.Petersen CERN
//
// //////////////////////////////////////////////////////////////////////

#include <vector>
#include <iostream>
#include <sys/types.h>                       // for u_int
#include <string>                            // for operator==, basic_string

#include "ROSIO/DataDrivenTriggerIn.h"
#include "ROSIO/IOException.h"

#include "ROSCore/IOManager.h"               // for IOManager
#include "ROSCore/TriggerInputQueue.h"       // for TriggerInputQueue
#include "ROSCore/DataChannel.h"
#include "ROSCore/FragmentRequest.h"
#include "ROSIO/ROSFragmentBuilder.h"
#include "ROSIO/FullFragmentBuilder.h"

#include "DFThreads/DFCountedPointer.h"      // for DFCountedPointer
#include "DFThreads/DFThread.h"              // for DFThread

#include "ROSUtilities/ROSErrorReporting.h"

#include "DFSubSystemItem/Config.h"
#include "DFDebug/DFDebug.h"

namespace ROS { class TriggerIn; }
namespace daq { namespace rc { class TransitionCmd; } }

using namespace ROS;

DataDrivenTriggerIn::DataDrivenTriggerIn() : 
   m_builder(0), m_numberOfRequests(0), m_inputToTriggerQueue(0),
   m_singleFragmentMode(false), m_runActive(false) {
   cout << "DataDrivenTriggerIn constructor\n";   
}

DataDrivenTriggerIn::~DataDrivenTriggerIn() noexcept {
}

void DataDrivenTriggerIn::prepareForRun(const daq::rc::TransitionCmd&)
{

   cout << "DataDrivenTriggerIn start " << endl;
   m_runActive=true;
   startExecution();
}

void DataDrivenTriggerIn::setup(IOManager* iomanager,
                                DFCountedPointer<Config> configuration)
{
   initialise(iomanager);
   m_config=configuration;

   m_singleFragmentMode = m_config->getBool("SingleFragmentMode");
   std::cout << "DataDrivenTriggerIn::setup: Single Fragment Mode: " << m_singleFragmentMode << std::endl;

   std::string requestName=m_config->getString("RequestType");
   if (requestName=="AllRequestWithRelease") {
      m_builder=new ROSFragmentBuilder;
   }
   else if (requestName=="EBRequestWithRelease") {
      m_builder=new FullFragmentBuilder;
   }
   else {
      CREATE_ROS_EXCEPTION (tException, IOException, UNKNOWN_REQ, requestName);
      throw tException;
   }
}

/** The main action thread.  Generates  EB requests 
*/
void DataDrivenTriggerIn::run()
{

//   unsigned int destination = m_config->getInt("destinationNodeId");
   unsigned int destination = 12345;	// Dummy - FIXME

   m_numberOfRequests = 0;
   while (m_runActive) {
     DFThread::cancellationPoint() ;	// trigger gets killed with Q possibly non-empty

     DEBUG_TEXT(DFDB_QUEUE, 20, "DataDrivenTriggerIn::run: popping trigger Q");
     try {
        int level1Id = m_inputToTriggerQueue->pop();
        DEBUG_TEXT(DFDB_QUEUE, 20, "DataDrivenTriggerIn::run: L1ID = " << level1Id << " popped from trigger Q");

        FragmentRequest* request;
        if (m_singleFragmentMode == false) {
           request=new FragmentRequest(level1Id,
                                       DataChannel::channels(),
                                       false,
                                       true,
                                       true,
                                       m_builder,
                                       destination);
        }
        else {
           u_int channel = m_inputToTriggerQueue->pop();
           DEBUG_TEXT(DFDB_QUEUE, 20, "DataDrivenTriggerIn::run: channel = " << channel << " popped from trigger Q");
           std::vector <DataChannel*>* singleChannel = new std::vector<DataChannel*>;
           std::vector < DataChannel *>* seqDataChannels = DataChannel::channels();	// pointer to vector of pointers to THE data channels
           singleChannel->push_back((*seqDataChannels)[channel]);
           DEBUG_TEXT(DFDB_QUEUE, 20, "DataDrivenTriggerIn::run: pointer to channel = " << (*seqDataChannels)[channel]);

           request=new FragmentRequest(level1Id,
                                       singleChannel,
                                       true,		// delete channel vector
                                       true,
                                       true,
                                       m_builder,
                                       destination);
        }
        m_ioManager->queueRequest(request);
     }
     catch (tbb::user_abort& abortException) {
        m_runActive=false;
     }

   }
   std::cout << "DataDrivenTriggerIn run exiting...\n";
}

void DataDrivenTriggerIn::cleanup()
{
  cout << "---- DataDrivenTriggerIn::cleanup ----" << endl ;
}

void DataDrivenTriggerIn::stopGathering(const daq::rc::TransitionCmd&)
{
   m_runActive=false;
   m_inputToTriggerQueue->abort();
//   stopExecution();
//   this->DFThread::waitForCondition(DFThread::TERMINATED, 10);  // test FIXME

   cout << "DataDrivenTriggerIn stop " << endl;
}

void DataDrivenTriggerIn::configure(const daq::rc::TransitionCmd&)
{
   m_inputToTriggerQueue=TriggerInputQueue::instance();
//    cout << " DataDrivenTriggerIn: connect, Q pointer = " << m_inputToTriggerQueue
//         << " size = " << m_inputToTriggerQueue->size() << endl;
}
void DataDrivenTriggerIn::unconfigure(const daq::rc::TransitionCmd&)
{
  m_inputToTriggerQueue->destroy();	// detach from Q
}


/** Shared library entry point */
extern "C" {
   extern TriggerIn* createDataDrivenTriggerIn();
}
TriggerIn* createDataDrivenTriggerIn()
{
   return (new DataDrivenTriggerIn());
}
