//*****************************************************************************/
// file: EMonDataOut.cpp
// desc: DataOut using ROS Monitoring Handler, e.g. for ROD monitoring
// auth: 05/03/03 R. Spiwoks
//*****************************************************************************/

// $Id$

#include <sys/types.h>
#include <string.h>                      // for memcpy, size_t
#include <unistd.h>                      // for usleep
#include <string>                        // for allocator, string

#include "DFThreads/DFCountedPointer.h"  // for DFCountedPointer
#include "DFThreads/DFFastMutex.h"       // for DFFastMutex
#include "ROSCore/DataOut.h"             // for DataOut, DataOut::Type, Data...
#include "ROSInfo/DataOutInfo.h"         // for DataOutInfo
#include "ROSMemoryPool/MemoryPage.h"    // for MemoryPage
#include "emon/EventChannel.h"           // for EventChannel
#include "emon/EventSampler.h"           // for EventSampler
#include "emon/PullSampling.h"           // for PullSampling
#include "emon/SamplingAddress.h"        // for SamplingAddress
#include "emon/SelectionCriteria.h"      // for SelectionCriteria
#include "ipc/partition.h"               // for IPCPartition

#include "DFSubSystemItem/Config.h"
#include "DFDebug/DFDebug.h"
#include "ROSBufferManagement/Buffer.h"

#include "ROSIO/EMonDataOut.h"

class ISInfo;

using namespace ROS;

EMonDataOut::Sampling::Sampling(const emon::SelectionCriteria &sc, 
                                DFFastMutex* mutex,
                                char** buffer,
                                size_t* sizePtr): m_selectionCriteria(sc),
                                                  m_mutex(mutex),
                                                  m_buffer(buffer),
                                                  m_sizePtr(sizePtr) {
   DEBUG_TEXT(DFDB_ROSIO, 10, "EMonDataOut::Sampling constructor entered");
}

//******************************
EMonDataOut::Sampling::~Sampling() {
   DEBUG_TEXT(DFDB_ROSIO, 10, "EMonDataOut::Sampling destructor entered");
}


//******************************
void EMonDataOut::Sampling::sampleEvent(emon::EventChannel& channel) {
//******************************
   DEBUG_TEXT(DFDB_ROSIO, 15, "EMonDataOut::SampleEvent entered");
   bool sampled=false;
   int count=0;
   while (!sampled && count++< 1000) {
      if (m_mutex->trylock()) {
         if (*m_sizePtr) {
            DEBUG_TEXT(DFDB_ROSIO, 20, "EMonDataOut::Sampling::run()  pushing event");
            channel.pushEvent((unsigned int*) *m_buffer, (*m_sizePtr)/sizeof(unsigned int));
            *m_sizePtr=0;
            sampled=true;
         }
         else {
            DEBUG_TEXT(DFDB_ROSIO, 20, "EMonDataOut::SampleEvent  No event to push");
         }
         m_mutex->unlock();
      }
      else {
         DEBUG_TEXT(DFDB_ROSIO, 20, "EMonDataOut::SampleEvent  Mutex busy");
      }
      if (!sampled) {
         usleep(1);
      }
   }
   DEBUG_TEXT(DFDB_ROSIO, 15, "EMonDataOut::SampleEvent finished count="<<count);
}



//------------------------------------------------------------------------------

EMonDataOut::SamplingFactory::SamplingFactory(DFFastMutex* mutex,
                                              const DataOut::Type type, EMonDataOut* dataOut,
                                              char** buffer, size_t* size) :
   m_mutex(mutex), m_type(type), m_criteria(0),
   m_buffer(buffer), m_sizePtr(size) {
}


EMonDataOut::SamplingFactory::~SamplingFactory() {
   if (m_criteria!=0){
      delete m_criteria;
   }
}

emon::PullSampling* EMonDataOut::SamplingFactory::startSampling(const emon::SelectionCriteria & criteria)
   /* throw (emon::BadCriteria, emon::NoResources) */ 
{
   DEBUG_TEXT(DFDB_ROSIO, 10, "EMonDataOut::SamplingFactory creating new PushSampling object");

   if (m_type==DataOut::SAMPLED) {
      if (m_criteria!=0){
         delete m_criteria;
      }
      m_criteria=new emon::SelectionCriteria(criteria);
      DataOut::selectionCriteria(m_criteria);
   }
   Sampling* sampling=new Sampling(criteria, m_mutex, m_buffer, m_sizePtr);
   return sampling;
}


//------------------------------------------------------------------------------

EMonDataOut::EMonDataOut(const Type myType) 
         : DataOut(myType) {
 
   DEBUG_TEXT(DFDB_ROSIO, 10, "EMonDataOut constructor entered");

   m_type=myType;

   m_sampling=0;
   m_samplingFactory=0;
   m_eventSampler=0;

   m_accessMutex=DFFastMutex::Create();

   m_scalingFactor=1;

   // reset statistics
   m_stats.fragmentsOutput = 0;
   m_stats.dataOutput = 0;

   m_sendCalled = 0;

   m_dataStorage=0;
   m_dataStorageSize=0;
   m_bufferedEventSize=0;

   DEBUG_TEXT(DFDB_ROSIO, 10, "EMonDataOut constructor complete");
}

//------------------------------------------------------------------------------

EMonDataOut::~EMonDataOut() noexcept { 
   DEBUG_TEXT(DFDB_ROSIO, 10, "EMonDataOut destructor entered");
#if 0
   m_accessMutex->lock();

   DEBUG_TEXT(DFDB_ROSIO, 10, "EMonDataOut destructor acquired mutex");
#endif

   if (m_eventSampler!=0) {
      delete m_eventSampler;
   }
   DEBUG_TEXT(DFDB_ROSIO, 10, "EMonDataOut deleted eventSampler");


   if (m_dataStorage!=0) {
      delete [] m_dataStorage;
   }
   m_accessMutex->destroy();
   DEBUG_TEXT(DFDB_ROSIO, 10, "EMonDataOut destructor finished");
}


void EMonDataOut::setup(DFCountedPointer<Config> configuration) {
   DEBUG_TEXT(DFDB_ROSIO, 10, "EMonDataOut::setup entered");

   bool interactive=configuration->getBool("interactive");

   if (!interactive) {
      std::string partitionName=configuration->getString("partition");
      IPCPartition partition(partitionName);

      std::string key;
      std::string value;
      if (configuration->isKey("key") && configuration->isKey("value")) {
         key=configuration->getString("key");
         value=configuration->getString("value");
      }
      else {
         key="ReadoutApplication";
         value=configuration->getString("appName");
      }

      DEBUG_TEXT(DFDB_ROSIO, 15, "EmonDataOut::setup setting address to key="
                 << key << "  value=" << value);

      emon::SamplingAddress address(key, value);
      m_samplingFactory=new SamplingFactory(m_accessMutex, m_type, this, &m_dataStorage, &m_bufferedEventSize);
      m_eventSampler=new emon::EventSampler(partition,
                                            address,
                                            m_samplingFactory,
                                            1);
   }
   DEBUG_TEXT(DFDB_ROSIO, 10, "EMonDataOut::setup complete");
}
//------------------------------------------------------------------------------

void EMonDataOut::copyBuffer(const Buffer* buffer) {
   int	size;    
   int index = 0;
   if (m_dataStorageSize < buffer->size()) {
      if (m_dataStorage!=0) {
         delete[] m_dataStorage;
      }
      m_dataStorage = new char[buffer->size()]; 
      m_dataStorageSize = buffer->size();
   }

   for (Buffer::page_iterator page=buffer->begin(); page!=buffer->end(); page++) {
      size = (*page)->usedSize();
      memcpy(m_dataStorage+index, (*page)->address(), size);
      index+=size;
   }
   m_bufferedEventSize=buffer->size();
   m_stats.dataOutput += (float)buffer->size()/(float)(1024*1024);
   m_stats.fragmentsOutput++;
   DEBUG_TEXT(DFDB_ROSIO, 15, 
              "EMonDataOut::copyBuffer fragments copied = " << m_stats.fragmentsOutput
              << ",  send called = " << m_sendCalled);

}


void EMonDataOut::sendData(const Buffer* buffer,
                                 NodeID /*dst*/, u_int /*tid*/,
                                 unsigned int /*status*/) {

   DEBUG_TEXT(DFDB_ROSIO, 15, "EMonDataOut::sendData, send_called = " << m_sendCalled);

   m_sendCalled++;
   if (m_accessMutex->trylock()) {
      if ((m_bufferedEventSize==0 && m_scalingFactor > 0 && (m_sendCalled%m_scalingFactor) == 0) ||
          (m_type == DEBUGGING)) {
         copyBuffer(buffer);
      }
      m_accessMutex->unlock();
   }
   else {
      DEBUG_TEXT(DFDB_ROSIO, 20, "EMonDataOut::sendData failed to get mutex");
   }
}

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------


ISInfo* EMonDataOut::getISInfo() {
   return &m_stats;
}

//------------------------------------------------------------------------------
// shared library entry point

extern "C" {
extern DataOut* createEMonDataOut(const DataOut::Type* myType);
}

DataOut* createEMonDataOut(const DataOut::Type* myType) {
    return (new EMonDataOut(*myType));
}
