static char Usage[] = "\
Usage: testOutCirc [-t] <destination> <port> <event-lenght> <repeat-count>\
";
#define USAGE printf("%s\n",Usage), exit(1)

#include <stdio.h>
#include <sched.h>


#include "ROSEventFragment/HWFragment.h"
#include "ROSBufferManagement/Buffer.h"
#include "ROSMemoryPool/MemoryPool_malloc.h"
#include "DFSubSystemItem/Config.h"
#include "ROSIO/TCPDataOut.h"

using namespace RCD;
using namespace ROS;

/***************************************************************************/
int main (int argc, char *argv[])
/*---------------------------
*/
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
{
  char *destination = 0;
  int evtsize, repeat, i;
  int bufsize = 64*1024;
  int length, npages, pageSize = 8192;
  unsigned short port = 0;
  int np = 0;

  bool novip = false;

  MemoryPool *memoryPool;

  /* Get parameters form the command line */
  if (argc < 5)
    USAGE;

  if (argv[1][0] == '-') {
    if (argv[1][1] == 't') {
      novip = true; np++;
    }
    else
      USAGE;
  }

  if (argc != np + 5)
    USAGE;

  destination       = argv[np+1];
  port              = (unsigned short) atoi (argv[np+2]);
  evtsize           = atoi (argv[np+3]);
  repeat            = atoi (argv[np+4]);

  npages = evtsize / pageSize + 1;
  //length = (evtsize % sizeof (int) == 0 ? 0 : 1)+ evtsize / sizeof (int);
  length = evtsize;

  memoryPool = new MemoryPool_malloc (npages, pageSize);
  EventFragment *fragment = new HWFragment (memoryPool, length);

  /* Open the circular buffer */

  TCPDataOut *tcp = new TCPDataOut ();

  DFCountedPointer<Config> conf = Config::New ();

  conf->set ("outputDelay", 0);
  conf->set ("TCPbufSize", bufsize);
  conf->set ("destinationNode", destination);
  conf->set ("destinationPort", (int) port);
  conf->set ("samplingGap", 1);
  conf->set ("throwIfUnavailable", novip);

  tcp->setup (conf);
  tcp->configure ();

  for (i = 0; i < repeat; i++) {
    tcp->sendData (fragment->buffer(), 0);
    sched_yield ();
  }

  DFCountedPointer<Config> outconf = tcp->getInfo ();

  std::cout << "testOutTCP : sent " << outconf->getInt ("FragmentsSent") <<
          " fragments" << std::endl;
  std::cout << "testOutTCP : sent " << outconf->getInt ("BytesSent") <<
          " bytes" << std::endl;
  std::cout << "testOutTCP : found " << outconf->getInt ("Timeouts") <<
          " timeouts" << std::endl;

  delete tcp;
  delete fragment;
  delete memoryPool;

  return 0;
}
