// $Id$
// //////////////////////////////////////////////////////////////////////
//  
//
//  Author:  G.J.Crone, University College London
//
//  $Log$
//  Revision 1.64  2008/11/17 16:39:56  gcrone
//  Add --wait option
//
//  Revision 1.63  2008/11/14 14:49:29  gcrone
//  Don't try to configure multicast group unless both ROSTestDC and ROS
//  have ReceiveMulticast set to true.
//
//  Revision 1.62  2008/11/04 13:57:03  gcrone
//  Bug fix
//
//  Revision 1.61  2008/10/16 08:06:48  gcrone
//  Adapt for new multicast group configuration.
//
//  Revision 1.60  2008/09/30 10:16:57  gcrone
//  Use uint_32 instead of unsigned long for roiDistribution vector.
//
//  Revision 1.59  2008/05/20 13:21:27  gcrone
//  Trap TERM signal and shutdown nicely
//
//  Revision 1.58  2008/04/08 16:54:35  gcrone
//  Fix memory leak, delete myhdr at end of getLvl1id.
//
//  Revision 1.57  2008/03/27 07:46:01  gcrone
//  Update to new ReadoutModule schema
//
//  Revision 1.56  2008/03/18 17:52:20  gcrone
//  Bug fix: Should only skip 4 words of ROD header to get to the L1ID, not 5.
//
//  Revision 1.55  2008/03/18 12:28:23  gcrone
//  Get SyncDeletesWithEBRequests flag from database.
//
//  Revision 1.54  2008/03/10 09:14:26  gcrone
//  Remove redundant/obsolete include
//
//  Revision 1.53  2008/02/28 18:17:55  gcrone
//  Update to new dcmessages
//
//  Revision 1.52  2008/02/26 18:12:42  gcrone
//  Remove test_response option as this needs updating for event format 4.
//  Also, tidy up a bit, remove some spurious includes and make sure std
//  namespace specified where appropriate.
//
//  Revision 1.51  2008/02/13 17:47:00  gcrone
//  Don't expect ROSHeader on data coming back
//
//  Revision 1.50  2007/05/10 14:55:43  gcrone
//  Make sure TransportManager initialises the transport for the multicast group
//
//  Revision 1.49  2007/04/19 12:42:32  gcrone
//  Our dal class is now TESTNODEID_ROSTestDC to get an appropriat node ID.  Also, get TraceLevel and TracePackage from our own db object instead of the ROS
//
//  Revision 1.48  2007/04/18 07:07:50  gcrone
//  TraceLevel and TracePackage start with upper case characters now!!
//
//  Revision 1.47  2007/04/16 12:03:08  gcrone
//  Use new ROSRequest instead of ROSRequestMessage.  Start to get node IDs using get_node_id() dal method.
//
//  Revision 1.46  2007/03/30 09:09:15  gcrone
//  Fix up getting trace level and package from ROS's ReadoutConfig.  Maybe
//  we should actually have our own attributes in the database for this.
//
//  Revision 1.45  2007/03/28 09:48:30  gcrone
//  Adapt to new df schema
//
//  Revision 1.44  2007/03/20 10:38:10  gcrone
//  Update for new dcmessages implementation
//
//  Revision 1.43  2006/11/01 12:56:52  gcrone
//  Comment out unsed args to avoid compiler warnings
//
//  Revision 1.42  2006/03/23 17:08:40  gcrone
//  Change and document default value for number of outstanding requests.
//
//  Revision 1.41  2006/03/22 17:48:56  gcrone
//  Check Module class name for preloaded, not channel.
//
//  Revision 1.40  2006/03/15 14:57:08  gcrone
//  Catch and print ConfigExceptions.
//
//  Revision 1.39  2006/03/15 13:20:20  gcrone
//  Construct channel Ids that include the detector Id.
//
//  Revision 1.38  2006/03/11 10:28:23  gcrone
//  use daq::Application
//
//  Revision 1.37  2006/01/17 15:21:14  gcrone
//  Removed redundant include of oksconfig/OksConfiguration.h
//
//  Revision 1.36  2005/12/15 16:33:21  gcrone
//  Update usage text.
//
//  Revision 1.35  2005/12/13 16:04:58  gcrone
//  Tidy up option processing and extend usage text
//
//  Revision 1.34  2005/12/06 18:21:13  jorgen
//   fix CTL+\ problem by blocking SIGQUIT
//
//  Revision 1.33  2005/12/01 15:33:11  gcrone
//  Get TestGarbageCollection flag from database.
//  Tidy some output statements.
//
//  Revision 1.32  2005/12/01 13:16:55  jorgen
//   add emulation & testing of garbage collection
//
//  Revision 1.31  2005/11/23 17:50:46  gcrone
//  Change end of run to wait for outstanding requests. Plus config changes for new df schema
//
//  Revision 1.30  2005/11/16 19:15:51  gcrone
//  Use get_belongs_to() instead of belongs_to() and check that our
//  ConnectsToROS relationship returns non-zero.
//
//  Revision 1.29  2005/11/11 17:26:00  gcrone
//  Remove references to TrafficType.
//
//  Revision 1.28  2005/11/09 16:15:40  gcrone
//  Add clear sequence count.
//
//  Revision 1.27  2005/11/03 16:48:55  gcrone
//  Updated config item names to be as given in the database so that ROSDBConfig doesn't need to translate
//
//  Revision 1.26  2005/10/27 15:35:05  gcrone
//  Addd option to configure ROSTestDC from database
//
//  Revision 1.25  2005/10/27 08:34:16  jorgen
//   initialise L1ID in trigger generator via set_level1Id method
//
//  Revision 1.24  2005/10/25 13:15:33  gcrone
//  Kludge a 0 into oldest L1ID field of DFM_Clear_Msg constructor for now.
//
//  Revision 1.23  2005/10/18 17:53:37  gcrone
//  Fix memory leak,  delete myhdr in getLvl1id() and check_response()
//
//  Revision 1.22  2005/10/03 16:02:59  mlevine
//
//  Removed  ROSTestDC::formatted_rod_print()
//  Changed  check_response() to conform to EF Ver 3.0
//  Print EF version used
//  Removed erroneous pointer manipulations committed previously (event was truncated)
//  printout of ROB/ROD now inline in  check_response()
//  Verified working
//
//  Revision 1.21  2005/08/25 17:01:15  gcrone
//  Allow running for infinite number of events if number given is 0.
//  Move delete of 1st reply to before 2nd receive!
//  Remove do {...}while(0);
//
//  Revision 1.20  2005/08/25 11:45:12  gcrone
//  Rename m_tot_instances to m_totalInstances and fix "totalInstances"
//  flag fom 1 to 0 in option structure.
//
//  Revision 1.19  2005/08/24 12:57:34  gcrone
//  New queing interface
//
//  Revision 1.18  2005/08/23 16:17:31  gcrone
//  Make sure getLvl1id() is called for ALL replies received.
//
//  Revision 1.17  2005/08/22 08:04:43  jorgen
//   remove SYNC option
//
//  Revision 1.16  2005/08/19 16:01:59  jorgen
//   add time stamping & DEBUG_TEXT
//
//  Revision 1.15  2005/08/17 15:24:13  jorgen
//   hack to ROSTestDC for speed-up
//
//  Revision 1.14  2005/08/05 11:54:39  unel
//  fogotten debug removed from ROSTestDc
//
//  Revision 1.13  2005/08/03 17:52:37  unel
//  sync in Trigger Generator using queues
//
//  Revision 1.12  2005/07/21 20:40:44  unel
//  sync for ROSTestDC
//
//  Revision 1.11  2005/07/16 13:01:37  unel
//  ROB printout converted to ef3.0
//
//  Revision 1.10  2005/07/15 07:20:23  unel
//  start for new eformat
//
//  Revision 1.9  2005/01/14 13:40:22  joos
//  Adaptations to new API in DFSubSystemItem/Config.cpp
//
//  Revision 1.8  2005/01/11 15:20:40  gcrone
//  Update DF_ALLOCATOR stuff so that it compiles
//
//  Revision 1.7  2004/12/17 10:23:21  unel
//  added DOC directory
//  imported MLevine's ROSTestDC
//
//  Revision 1.6.2 2004/11/11 mlevine
//  using Event Format Library to navigate event structure and to detect errors
//  add flag --p to trigger the printout of the EFL and ROD payload
//
//  Revision 1.6.1 2004/10/27 mlevine
//  added formatted printing of events received, --c specifies cookie (length of response payload)
//
//  Revision 1.6  2004/10/19 13:08:12  glehmann
//  added missing libraries to test programs
//
//  Revision 1.5  2004/08/25 08:42:13  joos
//  globally: code added to catch a division by zero and debug output improved
//
//  Revision 1.4  2004/07/26 17:33:37  jorgen
//  Moved ROSTestDC configuration from triggerIn.dat  and dataOut.dat to dedicated ROSTestDC.dat file
//
//  Revision 1.3  2004/07/26 15:35:39  jorgen
//   fix problems around data channels
//
//  Revision 1.2  2004/07/12 15:03:04  joos
//  L1ID reset at start; memory leak fixed
//
//  Revision 1.1.1.1  2004/02/05 18:31:17  akazarov
//  imported from nightly 05.02.2004
//
//  Revision 1.11  2004/01/26 14:31:00  gcrone
//  Remove ends from end of ostringstream writes.
//
//  Revision 1.10  2004/01/19 10:08:10  joos
//  DFDebug introduced
//
//  Revision 1.9  2003/12/03 15:09:36  gcrone
//  Config constructors now protected, use static method New to enforce use of DFCountedPointer
//
//  Revision 1.8  2003/11/11 10:56:07  gcrone
//  Use DFCountedPointer<Config> in place of Config*.
//
//  Revision 1.7  2003/10/16 16:59:14  jorgen
//   sigquit cleaning
//
//  Revision 1.6  2003/10/15 18:28:15  gorini
//  Added ctrl-\ handler to ROSTestDC.... it prints stats
//
//  Revision 1.5  2003/03/11 13:55:37  gorini
//  Minor bug fix
//
//  Revision 1.4  2003/03/10 15:04:44  gorini
//  Added --instance(-i) commandline argument. It is needed to run multiple instances of the test program on different machines. The --instance value must correspond to a remoteNode as specified in the ROS config files.
//
//  Revision 1.3  2003/02/27 16:36:42  joos
//  bug fixed
//
//  Revision 1.2  2003/02/26 20:42:13  gorini
//  New version of ROSTestDC program with a more realistic trigger generation
//
//  Revision 1.1  2003/02/25 14:17:42  gcrone
//  Added test program ROSTestDC
//
//
// //////////////////////////////////////////////////////////////////////

#include <sstream>
#include <string>
#include <iostream>
#include <fstream>

#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>

#include <time.h>
#include <sys/time.h>
#include <getopt.h>

#include "DFDebug/DFDebug.h"

#include "ROSIO/IOException.h"
#include "ROSIO/TransportManager.h"
#include "DFSubSystemItem/Config.h"
#include "DFSubSystemItem/ConfigException.h"
#include "rcc_time_stamp/tstamp.h"

#include "msg/Port.h"

#include "eformat/eformat.h"
#include "dcmessages/eutil.h"
#include "dcmessages/DFM_Clear_Msg.h"
#include "dcmessages/DataRequest.h"
#include "dcmessages/Messages.h"


// Include files for the database
#include "config/Configuration.h"
#include "dal/util.h"
#include "dal/Partition.h"
#include "dal/Component.h"
#include "dal/Detector.h"
#include "DFdal/TESTNODEID_ROSTestDC.h"
#include "DFdal/ROS.h"
#include "DFdal/ReadoutConfiguration.h"
#include "DFdal/ReadoutModule.h"
#include "DFdal/InputChannel.h"
#include "DFdal/DFParameters.h"
//Message Passing stuff
#include "msgconf/MessageConfiguration.h"


#include "ROSTestDC.h"

#include "ipc/core.h"

using daq::Application;
using namespace eformat;
using namespace ROS;

//helper::Version my_version;


ROSTestDC::ROSTestDC(DFCountedPointer<Config> configuration,
                     int numberOfOutstandingRequests,
		     int instance, 
		     int totalInstances,
		     int cookie,
		     bool printData,
		     bool testResponse)
   : ROSTest(configuration, numberOfOutstandingRequests, printData),
     m_testResponse(testResponse),
     m_cookie(cookie)  {

   m_instance=instance;
   set_level1Id(instance);		// FIXME
   m_totalInstances=totalInstances;

   m_localHostId=configuration->getInt("instanceNodeId", instance);


   std::cout << " Cookie=" << m_cookie << std::endl;
   std::cout << " RT:m_localHostId = " << m_localHostId << std::endl;

   std::string localHost=configuration->getString("instanceAddress", instance);

   std::cout << " RT:localHost = " << localHost << std::endl;

   m_remoteNodeId=configuration->getInt("ROSNodeId");
   std::string remoteNode=configuration->getString("ROSAddress");

   std::cout << " RT:m_remoteNodeId = " << m_remoteNodeId << std::endl;
   std::cout << " RT:remoteNode = " << remoteNode << std::endl;

   std::string multicastAddress = configuration->getString("multicastAddress");
   std::cout << " RT:multicastAddress = " << multicastAddress << std::endl;
   int multicastId = configuration->getInt("multicastId");
   std::cout << " RT:multicastId = " << multicastId << std::endl;
   TransportManager* transportManager=TransportManager::create();
   transportManager->initialise(localHost,multicastAddress);
   if (multicastAddress != "") {
      transportManager->addGroup("ROS", multicastId, multicastAddress);
   }
   transportManager->addNode(localHost, m_localHostId);
   transportManager->addNode(remoteNode, m_remoteNodeId, "ROS");
   transportManager->configure(m_localHostId);

   m_remotePort=MessagePassing::Port::find(m_remoteNodeId);
   if (m_remotePort==0) {
      std::cerr << "Failed to find remote Port!\n" ;
      exit (EXIT_FAILURE);
   }

   if (multicastAddress != "") {
      m_rosPort=MessagePassing::Port::find(multicastId);
   }
   else {
      m_rosPort=m_remotePort;
   }
   std::cout << "m_remotePort: " << m_remotePort <<std::endl;
   std::cout << "m_rosPort: " << m_rosPort <<std::endl;


   int metPercent=configuration->getInt("metPercent");
   m_metInterval=RAND_MAX*((float)metPercent/100.0);


   if (m_metInterval!=0) {
      unsigned int detectorId=configuration->getUInt("detectorId");
      if ((detectorId&0x00f0)==0x40) {
         m_metID=0x007d;
         std::cout << "LAr ROS - will configure for MEt hack\n";
      }
      else   if ((detectorId&0x00f0)==0x50) {
         m_metID=0x007e;
         std::cout << "Tile ROS - will configure for MEt hack\n";
      }
      else {
         m_metID=0x007f;
         std::cout << "=====================================================\n"
                   << "#  Detector id " << std::hex << detectorId << std::dec << " does not support mET requests #\n"
                   << "#  This should not be expected to work              #\n"
                   << "=====================================================\n";
      }
   }
   //   std::cout <<std::endl <<"===========  using Event Format Library  " << my_version.human()<< "  =========" <<std::endl;
}

/***************************************************************************/
void ROSTestDC::l2Request(const unsigned int level1Id,
                          const std::vector<unsigned int> * rols,
                          const unsigned int /*destination*/,
                          const unsigned int transactionId)
   /***************************************************************************/
{
   dcmessages::DataRequest request(level1Id);

   m_l2RequestsGenerated++;
   if (m_metInterval!=0 && rand()<m_metInterval) {
      m_metRequestsGenerated++;

      unsigned int rolId=*(rols->begin());
      //      std::cout << "level1Id " << level1Id << std::hex << ", raw rolId="<<rolId;
      rolId=(rolId&0x0000ffff)|(m_metID<<16);
      //std::cout << ",  cooked rolId="<<rolId << std::dec << std::endl;
      request.push_back(rolId);
   }
   else {
      for (std::vector<unsigned int>::const_iterator it=rols->begin();
           it!=rols->end();it++) {
         request.push_back(*it);
      }
   }
   
   request.send(ROS_REQ, m_remotePort, transactionId, m_cookie);
}

/***************************************************************************/
void ROSTestDC::ebRequest(const unsigned int level1Id,
                          const unsigned int /*destination*/) {
   /***************************************************************************/
   dcmessages::DataRequest request(level1Id);
   request.send(SFI_DATAREQUEST, m_remotePort, 0, m_cookie);
}

/***************************************************************************/
void ROSTestDC::releaseRequest(const std::vector<unsigned int>* level1Ids,
			       const unsigned int oldestL1id) {
   /***************************************************************************/

   std::vector<unsigned int>* l1Ids = const_cast<std::vector<unsigned int>* >(level1Ids);
   DEBUG_TEXT(DFDB_ROSIO, 10, "releaseRequest: oldestL1id =  " << oldestL1id);
   dcmessages::DFM_Clear_Msg message(*l1Ids, oldestL1id, m_clearSequence);
   message.send(m_rosPort);
   m_clearSequence++;
}


/***************************************************************************/
uint32_t ROSTestDC::getLvl1id(MessagePassing::Buffer* reply) 
   /***************************************************************************/
{
   using namespace MessagePassing;
   using namespace MessageInput;
   using namespace eformat;
   MessageHeader*  myhdr = new MessageInput::MessageHeader(reply);
   if (!myhdr->valid()) {
      std::cerr << "=============== Generic Header INVALID ================ \n";
      delete myhdr;
      return (0);
   }

   MessagePassing::Buffer::iterator itbuf = reply->begin();
   itbuf += MessageInput::MessageHeader::SIZE;
   uint32_t thisL1id;

   uint32_t startOfHeaderMarker;
   uint32_t totalSize;
   uint32_t genericHeaderSize;
   itbuf >> startOfHeaderMarker >> totalSize >> genericHeaderSize;

#if 0
   std::cout << std::endl;
   std::cout << "startOfHeaderMarker " << std::hex << startOfHeaderMarker << std::dec << std::endl;
   std::cout << "totalSize " << totalSize << std::dec << std::endl;
   std::cout << "genericHeaderSize " << genericHeaderSize << std::dec << std::endl;
#endif

   if ( startOfHeaderMarker != eformat::ROB ) { 
      std::cout << "NOT a ROB fragment" << std::hex << startOfHeaderMarker << "!="<< eformat::ROB <<std::dec << std::endl; 
      delete myhdr;
      return (0);
   }

   // Skip over the rest of the ROB header to get to a ROD header
   for (unsigned int iw=0; iw<genericHeaderSize-3; iw++) {
      uint32_t word;
      itbuf >> word;
      //     std::cout << "header word " << iw << ": " << std::hex << word << std::dec << std::endl;
   }
   itbuf >> startOfHeaderMarker;
   // Skip over words in the ROD header until we get to the L1 ID
   for (unsigned int iw=0; iw<4; iw++) {
      uint32_t word;
      itbuf >> word;
      //     std::cout << "header word " << iw << ": " << std::hex << word << std::dec << std::endl;
   }
   itbuf >> thisL1id;

#if 0
   std::cout << "startOfHeaderMarker " << std::hex << startOfHeaderMarker << std::dec << std::endl;
   std::cout << "Level 1 ID " << thisL1id << std::dec << std::endl;
   std::cout << std::endl;
#endif

   DEBUG_TEXT(DFDB_ROSIO, 10, "getLvl1id: l1id received: " << thisL1id << "  Tid = " << myhdr->xid());
   // put the received L1ID into the appropriate Q.
   if (myhdr->xid() == 0) { 
      // this is an answer to an EB request 
      DEBUG_TEXT(DFDB_ROSIO, 10, "getLvl1id: put L1id " << thisL1id <<
                 " on the Delete Queue of size " << releaseQueueSize());
      releaseQueuePush(thisL1id);
   }
   else {
      // this is an answer to an L2 request 
      DEBUG_TEXT(DFDB_ROSIO, 10, "getLvl1id: put L1id " << thisL1id <<
                 " on the EB  Queue of size " <<  ebQueueSize());
      ebQueuePush(thisL1id);
   } 
   delete myhdr;
   return(thisL1id);
}

/***************************************************************************/
void ROSTestDC::check_response(MessagePassing::Buffer* reply) 
/***************************************************************************/
{
   using namespace MessagePassing;
   using namespace MessageInput;
   using namespace eformat;
   MessageHeader*  myhdr = new MessageInput::MessageHeader(reply);

   if (myhdr->valid()) {
      std::cout << std::hex << std::setfill('0');
      if (myhdr->cookie()!=0) {
         std::cout << "\nFragment status " << myhdr->cookie() << " received";
      }
      MessagePassing::Buffer::iterator itbuf = reply->begin();
      itbuf += MessageInput::MessageHeader::SIZE;

      const unsigned int bufferSize=17000;
      unsigned int buf[bufferSize]; // long enough for full UDP response
      unsigned int replyLength=reply->size()/4;
      if (replyLength > bufferSize) {
         replyLength=bufferSize;
      }


      if (m_printData) {
         std::cout << "\nGeneric header:"
                   << "\n==============="
                   << "\n    msg_typ: 0x" << std::setw(4) << myhdr->type()
                   << "\n    src_id: 0x" << std::setw(4) << myhdr->source()
                   << "\n    size: 0x" << std::setw(8) << myhdr->size()
                   << "\n    timestamp: 0x" << std::setw(8) << myhdr->timestamp()
                   << "\n    transaction id: 0x" << std::setw(8) << myhdr->xid()
                   << "\n    cookie: 0x" << std::setw(8) << myhdr->cookie()
                   << "\nData:"
                   << "\n=====";
            //                   << std::endl << std::dec << std::setfill(' ');
      }
      for (unsigned int idx=0; idx <replyLength; idx++) {
         itbuf >> buf[idx]; 
         if (m_printData) {
            if (idx%8==0) {
               std::cout << std::endl;
            }
            std::cout << " "  << std::setw(8) << buf[idx];
         }
      }

      if (m_printData) {
         std::cout << std::endl;
      }

      std::cout << "Check_response:: ";
      const eformat::ROBFragment<const unsigned int*> fragment(buf);
      try {
         std::cout << std::hex << " ROBFragment size :" << fragment.fragment_size_word();
         std::cout << " source :" << fragment.source_id();
         if (fragment.check()) {
            std::cout << " ROBFragment OK ";
         }
      }
      catch (std::exception& ex) {
         std::cout << "\nException caught: " << ex.what() << std::endl;
         perror("Exception caused by: ");
      }
      std::cout << std::dec << std::setfill(' ') << std::endl << std::endl;

   }
   else {
      std::cout << "=============== Generic Header INVALID ================ \n";
   }

   delete myhdr;
}

//==========================

/***************************************************************************/
float ROSTestDC::run(int maxRequests)
/***************************************************************************/
{
   struct timeval startTime;
   float elapsedTime=0.0;
   struct timezone dummy;
   gettimeofday(&startTime,&dummy);
   sigset_t backslashSet;
   sigset_t oldSet;
   sigset_t currentSet;

   sigemptyset(&backslashSet);
   sigaddset(&backslashSet, SIGQUIT);
   sigprocmask(SIG_BLOCK, &backslashSet, &oldSet);
   DEBUG_TEXT(DFDB_ROSIO, 10, "run: after sigprocmask, old set = " << oldSet.__val[0]);	// not quite standard ..
   sigprocmask(SIG_SETMASK, NULL, &currentSet);
   DEBUG_TEXT(DFDB_ROSIO, 10, "run: after sigprocmask, current set = " << currentSet.__val[0]);

   if (m_numberOfOutstanding > 0) {
      // Prime system with m_numberOfOutstanding requests
      int sent = generate(m_numberOfOutstanding, 0);
      if (sent == 0) {
         std::cout << "No triggers have been sent." <<std::endl;
         std::cout << "Most likely the trigger disabled itself to avoid a division by zero." <<std::endl;
         std::cout << "Use the debug version to confirm" <<std::endl;
      }

      // Now receive replies and generate requests to maintain number of outstanding requests
      int numberReceived=0;
      while (((maxRequests == 0) || (numberReceived < maxRequests)) && !s_sigtermFlag) { 
         MessagePassing::Buffer* reply=MessagePassing::Port::receive();

         // only listen to SIGQUIT in the following lines ...
         sigprocmask(SIG_UNBLOCK,&backslashSet,&oldSet);
         DEBUG_TEXT(DFDB_ROSIO, 10, "run: after UNBLOCK, old set = " << oldSet.__val[0]);
         if (s_sigquitFlag) {
            struct timezone dummy;
            struct timeval endTime;
            gettimeofday(&endTime,&dummy);
            float elapsed=(endTime.tv_sec-startTime.tv_sec) +
               (endTime.tv_usec-startTime.tv_usec)/1000000.0;

            std::cout <<std::endl << " Trigger statistics" <<std::endl ; 
            printStatistics(elapsed);
            s_sigquitFlag=false;
         }
         sigaddset(&backslashSet, SIGQUIT);	// block again
         sigprocmask(SIG_BLOCK, &backslashSet, &oldSet);
         DEBUG_TEXT(DFDB_ROSIO, 10, "run: after BLOCK, old set = " << oldSet.__val[0]);

         if (reply != 0) {
            numberReceived++;

            // switch to avoid testing correctness of response (printing is also suppressed)
            if (m_testResponse) {
               check_response(reply); 	
            }

            // Process received L1ID  -- Ignore the name of the method!!
            getLvl1id(reply);
            delete reply;

            if (((maxRequests == 0) || (numberReceived < maxRequests)) && !s_sigtermFlag) {
               // send another request to maintain m_numberOfOutstanding requests
               sent = generate(1, 0);
               //	  std::cout << "sent2 = " << sent <<std::endl;
               for (int loop = 1; loop < sent; loop++) {
                  reply=MessagePassing::Port::receive();
                  if (reply != 0) {
                     // Process received L1ID  -- Ignore the name of the method!!
                     getLvl1id(reply);
                     numberReceived++;
                     delete reply;
                  }
                  else {
                     std::cerr << "Port::receive 1  returned 0\n";
                  }
               }
            }
         }
         else {
            std::cerr << "Port::receive 2 returned 0\n";
         }
      }
      struct timeval endTime;
      gettimeofday(&endTime,&dummy);
      elapsedTime=(endTime.tv_sec-startTime.tv_sec) +
         (endTime.tv_usec-startTime.tv_usec)/1000000.0;

      // Now we've received the required number of responses but we
      // should still have m_numberOfOutstanding requests outstanding
      // so listen for those and throw them away so that we end
      // cleanly.
      for (int nExtras=0; nExtras<m_numberOfOutstanding-1; nExtras++) {
         MessagePassing::Buffer* extraReply=MessagePassing::Port::receive(1000000);
         if (extraReply==0) {
            // Give up, probably timed out
            std::cerr << "Only managed to collect " << nExtras << " of the outstanding events in tidy up\n";
            break;
         }
         delete extraReply;
      }
   }
   else {
      generate(maxRequests, 0);
   }

   if (m_metInterval!=0) {
      std::cout << std::dec
                << "\t=========================================================\n"
                << "\t#  " << m_l2RequestsGenerated << " L2 request generated of which " << m_metRequestsGenerated << " were for MEt #\n"
                << "\t=========================================================\n";
   }

   return elapsedTime;
}

void loadConfig(std::string name, DFCountedPointer<Config> configuration)
{
   std::string configDir;
   char* configDirfromEnvironment=getenv("DAQ_CONFIG_DIR");
   if (configDirfromEnvironment == 0) {
      std::cout << "DAQ_CONFIG_DIR is not set. I don't know where to get the configuration!" <<std::endl;
      exit (EXIT_FAILURE);

      // configDir=".";
   }
   else {
      configDir=configDirfromEnvironment;
   }
   std::string fileName=configDir + "/" + name;
   std::ifstream configFile(fileName.c_str());
   if (!configFile) {
      std::cerr << "cannot open " << fileName
                << " iostream has no error reporting so can't tell you why!\n";
      exit (EXIT_FAILURE);
   }

   const int lineSize = 1024;
   char buf[lineSize];
   
   while(configFile.getline(buf, lineSize)) {
      if (buf[0] == '#') { // skip comment lines
         continue;
      }
      std::string line(buf);
      //    std::cout << "line is: " << line <<std::endl;
      int equalsPosition=line.find('=');
      std::string key(line, 0, equalsPosition);
      //std::cout << "key is: " << key <<std::endl;
      line.erase(0, equalsPosition+1);
      std::string value(line);
      //std::cout << "value is: " << value <<std::endl;
      configuration->set(key, value);
   }
     
}

int main (int argc, char*argv[])
{
   const int defaultOutstanding=100;
   int numberOfOutstandingRequests=defaultOutstanding;
   const int defaultRequests=5000;
   int totalNumberOfRequests=defaultRequests;
   int optionIndex=0;
   int option=0;
   int cookie=0;
   bool testResponse=false;
   int instance=0;
   int totalInstances=1;
   int printData = 0;
   bool useConfigFiles=true;
   std::string myName;
   int delay=0;
   int metPercent=0;

   struct option longOptions[] = {
      {"outstanding", 1, 0, 'o'},
      {"number", 1, 0, 'n'},
      {"instance", 1, 0, 'i'},
      {"totalInstances", 1, 0, 'T'},
      {"cookie",  1,  0, 'c'},
      {"metPercent",  1,  0, 'm'},
      {"test_response",  0,  0, 't'},
      {"print",  0,  0, 'p'},
      {"database", 0, 0, 'd'},
      {"name", 1, 0, 'N'},
      {"wait", 1, 0, 'w'},
      {"help", 0, 0, 'h'},
      {0, 0, 0, 0}
   };

   while (option != -1) {
      option=getopt_long (argc, argv, "o:n:i:T:c:m:N:pdtw:h", longOptions, &optionIndex);
      switch (option) {
      case 'o':
         if (isdigit(optarg[0])) {
            numberOfOutstandingRequests=strtol(optarg, 0, 10);
         }
         break;
      case 'n':
         if (isdigit(optarg[0])) {
            totalNumberOfRequests=strtol(optarg, 0, 10);
         }
         break;
      case 'i':
         if (isdigit(optarg[0])) {
            instance=strtol(optarg, 0, 10);
         }
	 break;
      case 'T':
         if (isdigit(optarg[0])) {
            totalInstances=strtol(optarg, 0, 10);
         }
	 break;
      case 'c':
         if (isdigit(optarg[0])) {
            cookie=strtol(optarg, 0, 10);
         }
         break;
      case 'N':
         myName=optarg;
         break;
      case 'd':
         useConfigFiles=false;
         break;
      case 'm':
         if (isdigit(optarg[0])) {
            metPercent=strtol(optarg, 0, 10);
         }
         break;
      case 't':
         testResponse=true;
         break;
      case 'p':
         printData=true;
         break;
      case 'w':
         if (isdigit(optarg[0])) {
            delay=strtol(optarg, 0, 10);
         }
         break;
      case 'h':
      case ':':
      case '?':
         std::cerr << "Usage: " << argv[0]
                   << " [--number=n] [--outstanding=o] [--instance=i] [--totalInstances=T] [--cookie=c] [--metPercent=m] [--test_response] [--name=N] [--database] [--print] [--wait=w] [--help]\n"
                   << "  where:\n"
                   << "\t<n> is the number of data requests that must complete before we exit (0=infinite) default=" << defaultRequests << "\n"
                   << "\t<o> is the number of outstanding requests in the system default=" << defaultOutstanding << "\n"
                   << "\t<i> is the number of this instance in the system\n"
                   << "\t<T> is the total number of instances of " << argv[0] << " in the system\n"
                   << "\t<c> is the cookie to be set in the DC request headers\n"
                   << "\t<m> is the % of L2 requests that are Missing ET requests\n"
                   << "\t<N> is the name of this application instance in the OKS database\n"
                   << "\t  --database causes the application to configure itself from the OKS database (rather than simple config files)\n"
                   << "\t  --test_response causes  a check of the correctness of the response ** NOT IMPLEMENTED IN THIS RELEASE **\n"
                   << "\t  --print causes detailed formatted dump of received event fragments ** NOT IMPLEMENTED IN THIS RELEASE **\n"
                   << "\t  --wait w seconds before starting\n"
                   << "\t  --help print this help text and exit\n";
         exit (0);
      }
   }

   if (printData && !testResponse) {
      std::cerr << "--print only works when --test_response  is selected!" <<std::endl;
      exit(0);
   }

   TS_OPEN(100000, TS_H1);     // for (optional:TSTAMP) time stamping
   TS_START(TS_H1);
   TS_RECORD(TS_H1,1);

   DFCountedPointer<Config> config=Config::New();

   if (useConfigFiles) {
      loadConfig("ROSTestDC.dat", config);
   }
   else {
      IPCCore::init(argc,argv);

      // Get config from OKS database
      Configuration confDB("");
      if(!confDB.loaded()) {
         std::cerr << "ROSTestDC: cannot load database <" << confDB.get_impl_spec() << ">" <<std::endl;
         return 0;
      }

      const daq::core::Partition* dbPartition;
      std::string any;
      if(!(dbPartition = daq::core::get_partition( confDB, any))) {
         std::cerr << "ROSTestDC: TDAQ_PARTITION not set or Partition object not found in the DB" <<std::endl;
         return 0;
      }

      const daq::df::TESTNODEID_ROSTestDC* dbApp= confDB.get<daq::df::TESTNODEID_ROSTestDC>(myName);
      if (dbApp==0) {
         std::cerr << "ROSTestDC: Cannot find " << myName << " in database" <<std::endl;
         return 0;
      }

      // Ask that all $variables in the DB are automatically substituted
      confDB.register_converter(new daq::core::SubstituteVariables(confDB, *dbPartition));

      const daq::df::ROS* myROS=dbApp->get_ConnectsToROS();
      if (myROS==0) {
         std::cerr << "Failed to find ConnectsToROS relationship in database\n";
         exit(EXIT_FAILURE);
      }


      MessageConfiguration msgConf;
      //      unsigned int myNodeId = Application::string2id(dbApp->UID());
      std::vector<daq::core::AppConfig> appOut;
      std::set<std::string> appTypes;
      appTypes.insert("DFMessagePassingNode");
      unsigned int myNodeId=0;
      unsigned int rosNodeId=0;
      dbPartition->get_all_applications(appOut, confDB, &appTypes, NULL, NULL);

      for(unsigned int it = 0; it < appOut.size(); it++) {
         std::cout << "appOut[" << it << "] id=" <<appOut[it].get_app_id() << std::endl;
         if (appOut[it].get_app_id() == dbApp->UID()) {
            std::cout << "Found my entry, getting id\n";
            myNodeId = appOut[it].get_node_id();
            break;
         }
      }

      if (myNodeId==0) {
         std::cerr << "Failed to get my node ID, found " << appOut.size() << " message passing apps \n";
         exit(EXIT_FAILURE);
      }

      const daq::df::DFParameters *df_params = confDB.cast<daq::df::DFParameters>(dbPartition->get_DataFlowParameters());
      if(!df_params) {
         std::cerr << "ROSTestDC: Cannot initialize the Message Passing configuration: DataFlowParameters not linked to the Partition in the Database"
                   <<std::endl;
         exit(EXIT_FAILURE);
      }


      msgConf.clear_configuration();
      bool result = msgConf.init_configuration(myNodeId, confDB);
      if (!result) {
         std::cerr << "ROSTestDC: Cannot  initialize the Message Passing configuration" <<std::endl;
         exit(EXIT_FAILURE);
      }

      // Set up local address info
      config->set("instanceNodeId", myNodeId, instance);
      const MessagePassing::Node* myNode = msgConf.find_node(myNodeId);
      std::ostringstream myAddr;
      for(MessagePassing::AddressList::const_iterator it=myNode->begin(); 
	  it !=myNode->end(); ++it) {
         myAddr << (*it).address() << ";;" ;
      }
      config->set("instanceAddress", myAddr.str(), instance);

      // Set up address of ROS we're gonna talk to
      for(unsigned int it = 0; it < appOut.size(); it++) {
         if (appOut[it].get_app_id() == myROS->UID()) {
            rosNodeId = appOut[it].get_node_id();
            break;
         }
      }

      if (rosNodeId==0) {
         std::cerr << "Failed to get ROS node ID\n";
         exit(EXIT_FAILURE);
      }
      std::cout << "ROS node ID is " << rosNodeId << " (" << std::hex << rosNodeId << std::dec << ")\n";

      config->set("ROSNodeId",rosNodeId);
      const MessagePassing::Node* rosNode = msgConf.find_node(rosNodeId);
      std::ostringstream rosAddr;
      for(MessagePassing::AddressList::const_iterator it=rosNode->begin(); 
	  it !=rosNode->end(); ++it) {
         rosAddr << (*it).address() << ";;" ;
      }
      config->set("ROSAddress", rosAddr.str());

      if (myROS->get_ReceiveMulticast() && dbApp->get_ReceiveMulticast()) {
         // Setup multicast address of ROS group
         std::string multicastAddress= df_params->get_MulticastAddress();
         config->set("multicastAddress", multicastAddress);
         msgConf.create_by_group("ROS"); 
         msgConf.create_group("ROS");
         config->set("multicastId", msgConf.find_group("ROS")->id());
      }
      else {
         std::cout << "NOT ";
         config->set("multicastAddress", "");
         config->set("multicastId", 0);
      }
      std::cout << "using multicast for clears\n";

      unsigned int detectorId=myROS->get_Detector()->get_LogicalId();
      // Work out what channels are active in the ROS we're gonna talk to
      unsigned int nChannels=0;
      std::vector<const daq::core::Component*> disabledComponents = dbPartition->get_Disabled();

      for (std::vector<const daq::core::ResourceBase*>::const_iterator moduleIter = myROS->get_Contains().begin(); 
           moduleIter != myROS->get_Contains().end(); moduleIter++) {
         const daq::df::ReadoutModule*  roModule=
            confDB.cast<daq::df::ReadoutModule, daq::core::ResourceBase> (*moduleIter);
         if (roModule==0) {
            std::cerr << "ReadoutApplication " << myROS->UID()
                      << " Contains relationship to something (" << (*moduleIter)->class_name() <<
               ") that is not a ReadoutModule\n";
            continue;
         }

         const daq::core::ResourceSet*  resources=
            confDB.cast<daq::core::ResourceSet, daq::core::ResourceBase> (*moduleIter);
         if (resources==0) {
            std::cerr << "ReadoutApplication " << myROS->UID()
                      << " has a ReadoutModule that is not a ResourceSet\n";
            continue;
         }
         for (std::vector<const daq::core::ResourceBase*>::const_iterator channelIter = resources->get_Contains().begin(); 
              channelIter != resources->get_Contains().end(); channelIter++) {
            const daq::df::InputChannel* channelPtr=
               confDB.cast<daq::df::InputChannel, daq::core::ResourceBase> (*channelIter);
            if (channelPtr==0) {
               std::cerr << "ReadoutApplication " << myROS->UID()
                         << " Contains relationship to something (" << (*channelIter)->class_name() <<
                  ") that is not an InputChannel\n";
               continue;
            }
            else {
               if (!(*channelIter)->disabled(*dbPartition)) {
                  unsigned int channelId=channelPtr->get_Id();
                  if ((*moduleIter)->class_name() != "PreloadedReadoutModule") {
                     // fold in detector ID from ROS
                     channelId=(channelId & 0x0000ffff) | (detectorId << 16);
                  }

                  config->set("channelId", channelId, nChannels);
                  nChannels++;
               }
            }
         }
      }
      config->set("numberOfChannels", nChannels);

      // Load the ROI distribution array
      const std::vector<uint32_t> roiDistribution=dbApp->get_ROIDistribution();
      for (unsigned int loop=0; loop<roiDistribution.size(); loop++) {
         config->set("probRols", roiDistribution[loop], loop+1);
      }


      config->set("maxRolsInRoI", roiDistribution.size());
      // and the other generation parameters
      config->set("L2RequestFraction", dbApp->get_L2RequestFraction());
      config->set("EBRequestFraction", dbApp->get_EBRequestFraction());
      config->set("DeleteGrouping", dbApp->get_DeleteGrouping());

      config->set("tracingLevel", dbApp->get_TraceLevel());
      config->set("tracingPackage", dbApp->get_TracePackage());

      config->set("SyncDeletesWithEBRequests", dbApp->get_SyncDeletesWithEBRequests());

      // Garbage collection flag for TriggerGenerator
      config->set("TestGarbageCollection", dbApp->get_TestGarbageCollection());

      config->set("metPercent", metPercent);
      config->set("detectorId", detectorId);
      config->dump();  // DEBUG
   }

   //For compatibility reasons! (TriggerGenerator wants them)
   config->set("InputDelay",0);
   config->set("destinationNodeId",0);

   int dblevel = config->getInt("tracingLevel");
   int dbpackage = config->getInt("tracingPackage");
   DF::GlobalDebugSettings::setup(dblevel, dbpackage);

   try {
      ROSTestDC test(config, numberOfOutstandingRequests, instance, totalInstances, cookie, printData, testResponse);

      if (delay!=0) {
         sleep (delay);
      }

      float elapsed=test.run(totalNumberOfRequests);

      TS_SAVE(TS_H1, "ROS_timing");
      TS_CLOSE(TS_H1);
  
      std::cout <<std::endl << " Trigger statistics" <<std::endl ; 
      test.printStatistics(elapsed);
   }
   catch (ConfigException& error) {
      std::cout << "Exception: " << error.what() << std::endl;
      exit(EXIT_FAILURE);
   }
}
