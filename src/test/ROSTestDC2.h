// -*- c++ -*-
// $Id: ROSTestDC.h,v 1.17 2008/02/26 18:14:02 gcrone Exp $ 
//  modified mlevine
//
// $Log: ROSTestDC.h,v $
// Revision 1.17  2008/02/26 18:14:02  gcrone
// Use std:: namespace qualifier where needed.
//
// Revision 1.16  2006/08/18 11:27:10  gcrone
// Put in ROS namespace
//
// Revision 1.15  2005/12/13 16:04:58  gcrone
// Tidy up option processing and extend usage text
//
// Revision 1.14  2005/12/01 13:16:55  jorgen
//  add emulation & testing of garbage collection
//
// Revision 1.13  2005/11/23 17:50:46  gcrone
// Change end of run to wait for outstanding requests. Plus config changes for new df schema
//
// Revision 1.12  2005/11/09 16:15:40  gcrone
// Add clear sequence count.
//
// Revision 1.11  2005/08/24 12:57:34  gcrone
// New queing interface
//
//

#ifndef DCROSTEST_H
#define DCROSTEST_H
#include <string>
#include <vector>
#include <queue>
#include <list>
#include "msg/Types.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define PARAM_EXCHANGE_PORT_NUMBER 7375
#define RUN_CONTROLPORT_NUMBER 7374

namespace MessagePassing {
   class Port;
}

namespace ROS {
	class Config;

static char stateText[][64]=
{ 
	"NO_REQ",
	"L2_REQ_TO_BE_SENT",
	"L2_REQ_HAS_BEEN_SENT",
	"L2_REQ_HAS_BEEN_RESENT",
	"L2_REQ_WITH_ASSOCIATED_EB_REQ_TO_BE_SENT",
	"L2_REQ_HAS_BEEN_SENT_TO_BE_FOLLOWED_BY_EB_REQ",
	"L2_REQ_HAS_BEEN_RESENT_TO_BE_FOLLOWED_BY_EB_REQ",
	"L2_REQ_RECEIVED_ASSOCIATED_EB_REQ_TO_BE_SENT",
	"L2_REQ_RECEIVED_ASSOCIATED_EB_REQ_HAS_BEEN_SENT",
	"L2_REQ_RECEIVED_ASSOCIATED_EB_REQ_HAS_BEEN_RESENT",
	"EB_REQ_TO_BE_SENT",
	"EB_REQ_HAS_BEEN_SENT",
	"EB_REQ_HAS_BEEN_RESENT"
};

typedef enum
{
	NO_REQ = 0, // no L2 or EB request
	L2_REQ_TO_BE_SENT,
	L2_REQ_HAS_BEEN_SENT,
	L2_REQ_HAS_BEEN_RESENT,
	L2_REQ_WITH_ASSOCIATED_EB_REQ_TO_BE_SENT,
	L2_REQ_HAS_BEEN_SENT_TO_BE_FOLLOWED_BY_EB_REQ,
	L2_REQ_HAS_BEEN_RESENT_TO_BE_FOLLOWED_BY_EB_REQ,
	L2_REQ_RECEIVED_ASSOCIATED_EB_REQ_TO_BE_SENT,
	L2_REQ_RECEIVED_ASSOCIATED_EB_REQ_HAS_BEEN_SENT,
	L2_REQ_RECEIVED_ASSOCIATED_EB_REQ_HAS_BEEN_RESENT,
	EB_REQ_TO_BE_SENT,
	EB_REQ_HAS_BEEN_SENT,
	EB_REQ_HAS_BEEN_RESENT
} eReqTypeState;

// maximum number of instances
#define MAX_INSTANCES 8 
	
struct sExchangeBuf
{
	float l2frac;
	float ebfrac; 
	unsigned int deleteGroupSize;
	float avROLsPerRoI;
	unsigned int printInterval;
	int maxNumberOfOutputsPerRun;
	float targetL1Freq;
    float percentOfEtmiss;
};


class SockIO
{
	public:
		SockIO(const char *serverName, int nClients, int numberOfPort);
		~SockIO() {close(socketRef);};
		bool InitClient();
		bool InitServer();
		void GetParams(struct sExchangeBuf *theBuf);
		void SendParams(struct sExchangeBuf *theBuf, unsigned int fragSize);
		
		int portNumber;
		struct sockaddr_in theSock;
		int socketRef;
		int acceptSocketRef[MAX_INSTANCES];
		int numberOfClients;

};

class ROSTestDC
{
   private:
      std::string m_localHost;
      MessagePassing::NodeID m_localHostId;
      std::string m_remoteNodeAddress;
      MessagePassing::NodeID m_remoteNodeId;
      MessagePassing::Port* m_remotePort;
      MessagePassing::Port* m_rosPort;
      int m_numberOfOutstanding;
      int m_cookie;
      bool m_printData;
      bool m_testResponse;
      std::queue<unsigned int>  m_ebQueue;
      std::queue<unsigned int>  m_releaseQueue;
      unsigned int m_clearSequence;
      int m_metInterval;
      unsigned int m_l2RequestsGenerated;
      unsigned int m_metID;
      unsigned int m_metRequestsGenerated;

	  //Constants
	  static const int c_aleasize = 500000;
	  static const int c_maxRols = 20;
	  static const int c_maxRolsPerL2PU = 12;	// maximum nuber of different source id's
	  static const int c_maxEBbins = 220;		// goes with EB request Q size !!
	  static const int c_maxCLEARbins = 220;

		unsigned int numberOfGoodEBResponses, numberOfGoodL2Responses;
		unsigned int printInterval;
		unsigned int numberOfEBOutstanding;
		unsigned int numberOfL2Outstanding;
		unsigned int numberOfEBReReqsOutstanding;
		unsigned int numberOfL2ReReqsOutstanding;

#ifdef GORDON
     std::map<unsigned int, unsigned int> m_completeEBFragmentsPerROB;
     std::map<unsigned int, unsigned int> m_incompleteEBFragmentsPerROB;
     std::map<unsigned int, unsigned int> m_completeL2FragmentsPerROB;
     std::map<unsigned int, unsigned int> m_incompleteL2FragmentsPerROB;
#else
		std::map<unsigned int, unsigned int> sourceIdMap;
		unsigned int completeEBFragmentsPerROB[c_maxRolsPerL2PU];
	   unsigned int inCompleteEBFragmentsPerROB[c_maxRolsPerL2PU];
	   unsigned int completeL2FragmentsPerROB[c_maxRolsPerL2PU];
	   unsigned int inCompleteL2FragmentsPerROB[c_maxRolsPerL2PU];
       unsigned int numberOfEtmissFragsReceived;
#endif
		unsigned int lastIdToBeCleared;

		static const unsigned int c_ereqtypestate_size = 0x200000;
		static const unsigned int c_ereqtypestate_mask = c_ereqtypestate_size - 1;
		
		eReqTypeState *reqTypeState;
		bool *clearThisIdOnlyFlag;
		unsigned int *m_ebInterval;
		unsigned int *m_l2Interval;
		unsigned int *reRequestCount;
		unsigned int *releaseCount;
		static const unsigned int reRequestLimit = 20;
		SockIO *paramExchangeSocket;
		const char *controllerName;
		bool socketioFlag;
		bool masterFlag;
		bool slaveFlag;
		float lastL1Freq;
		float l2OrEBStepSize;
		int upCount, downCount;
		float stepSizeStopValue;
		float targetL1Freq;
        float percentOfEtmiss;
        float m_metProbability;
		float avROLsPerRoI;
		struct sExchangeBuf theBuf;
  public:
      ROSTestDC(DFCountedPointer<Config> configuration,
                int numberOfOutstanding,
                int instance=0, 
                int tot_instances=1,
                int cookie=0,
                bool print=false, bool testResponse=false, 
					 int waitUntilStartupTime = 1,
					 bool masterFlag = false, bool slaveFlag = false, bool multiReadoutAppsFlag = false,
					 const char *masterName = NULL);
   ~ROSTestDC();

      float run(int maxRequests);
      int generate(int nRequests = 0, int mode = 0);  //Generate next trigger
      // generate a bunch of consecutive L1IDs
      int generateL1idBunch(unsigned int firstL1id, unsigned int lastL1id);
      void printStatistics(float elapsedSeconds=0.,std::ostream &output=std::cout);  // Get operational statistics 
		unsigned int lastid;
      void init(bool firstInitFlag);
      void syncParams();
	   void set_level1Id(int l1id);
		void setTargetL1Freq(float t) {targetL1Freq = t;};
		void setStepSizeStopValue(float s) {stepSizeStopValue = s;};
		void setL2OrEBStepsize(float s) {l2OrEBStepSize = s;};
		void setMaxNumberOfOutputsPerRun(int m) {maxNumberOfOutputsPerRun = m;};
		void setWaitingTime(float w) {waitingTime = w;};
		void setPrintInterval(unsigned int p) {printInterval = p;};
		void setAvROLsPerRoI(float a) {avROLsPerRoI = a;};
		void setDoNotIterateFlag(bool x) {doNotIterateFlag = x;};
		void setIterateOnEBFracFlag(bool e) {iterateOnEBFracFlag = e;};
        void setPercentofEtmiss(float p) {percentOfEtmiss = p;};

		unsigned int m_deleteGrouping;

      unsigned int m_instance;
      unsigned int m_totalInstances;

      bool m_testGarbageCollection;
      bool m_syncDeletesWithEBRequests;

      std::queue<unsigned int> m_queue_ebrequest_ids;
      std::queue<unsigned int> m_queue_all_ids;
      std::queue<unsigned int> m_queue_ebfinished_ids;


   protected:
     void l2Request(const unsigned int level1Id,
                             const std::vector<unsigned int> * rols,
                             const unsigned int destination,
                             const unsigned int transactionId=1);
      void ebRequest(const unsigned int l1Id,
                             const unsigned int destination);
      void releaseRequest(const std::vector<unsigned int>* level1Ids,
                                  const unsigned int oldestL1id);    

      void ebQueuePush(unsigned int lvl1id);
      unsigned int  ebQueuePop(void);
      int  ebQueueSize(void);
      bool ebQueueEmpty(void);

      void releaseQueuePush(unsigned int lvl1id);
      unsigned int  releaseQueuePop(void);
      int  releaseQueueSize(void);
      bool releaseQueueEmpty(void);
      void processReleaseQueue(bool flush);

      //void formatted_rod_print(int *, const eformat::write::RODHeader&);
      void printEvent(MessagePassing::Buffer* reply);
	   void check_response(MessagePassing::Buffer* );
      bool getLvl1id(MessagePassing::Buffer* );
		bool condition();
		
	 int generateRequestsForOneL1ID(unsigned int l1Id);
    int noRolsInThisRoI(unsigned int);
    void generateL2Request(unsigned int l1Id);
    void generateEBRequest(unsigned int l1Id);
    void generateReleaseRequest(unsigned int oldestLevelId);
	 void checkHistory(unsigned int);
 
    // "static" loop variables
    int m_next;
    int m_transactionId;
    unsigned int m_level1Id;
   
    //Statistical counters
    unsigned int m_numberOfL1ids;
    unsigned int m_rollovers;
    unsigned int m_numberOfL2A ;
    unsigned int m_numberOfDeleteGroup;
    int m_numberOfROI[c_maxRols];    // # ROIs per ROL
    unsigned int m_totalNumberOfROI;
    unsigned int m_numberOfL2req;
    int m_roihist[c_maxRols];        // # ROLs per ROI
    int m_maxRolsInRoI;
	 unsigned int maxFragSize;
	 float firstElapsedSeconds, lastElapsedSeconds;
	 unsigned int firstNumberOfL1Ids ,lastNumberOfL1Ids;
	 unsigned int firstNumberOfL2req, lastNumberOfL2req;
    unsigned int firstNumberOfL2A,lastNumberOfL2A;
    unsigned int firstNumberOfEtmissreq,lastNumberOfEtmissreq;
    unsigned int firstNumberOfdeleteGroup,lastNumberOfdeleteGroup;
	 
	 bool stopFlag;
	 bool integrateModeFlag;
	 bool l2ReqfracEqualsOneStopFlag;
	 bool doNotIterateFlag;
	 bool iterateOnEBFracFlag;
	 bool multiReadoutAppsFlag;
	 int numberOfStatOutputPerRun;
	 int maxNumberOfOutputsPerRun;
	 
	 struct timespec waitTime;
	 struct timeval stopTime;
	 float waitingTime;

    int ranseed;
	 double m_alea[c_aleasize];
	 double m_l2ap_i;	// average number of L1As between successive L2Accepts
	 double m_l2req_interval; // average number of L1As between successive L2 requests
	 unsigned int last_m_numberOfL1ids;

    bool m_disabled;
    int m_numberOfChannels;
    std::vector<int> m_channelIds;
    std::vector<unsigned int> m_l1Ids;
    std::vector<unsigned int> m_rols;
    std::vector<unsigned int> m_l2puGroup;
    std::vector<double> m_l2puLimit;
    unsigned int m_destination;
    unsigned int m_inputDelay;
	 float l2RequestFraction;
	 float ebRequestFraction;

    std::vector<int> m_qEBhist;
    std::vector<int> m_qCLEARhist;

    // for garbage collection
    std::set<unsigned int> m_l1IdsInProcess;		// set of L1IDs busy with level2 and EB
    unsigned int m_oldestL1id;			// TG will never request L1IDs older(less) than this
    static const int c_maxINPROCESSbins = 200;
    std::vector<int> m_setINPROCESShist;
	};
}
#endif

