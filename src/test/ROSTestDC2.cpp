// $Id: ROSTestDC.cpp,v 1.59 2008/05/20 13:21:27 gcrone Exp $
// //////////////////////////////////////////////////////////////////////
//  
//
//  Author:  G.J.Crone, University College London
//
//  $Log: ROSTestDC.cpp,v $
//  Revision 1.64  2008/11/17 16:39:56  gcrone
//  Add --wait option
//
//  Revision 1.63  2008/11/14 14:49:29  gcrone
//  Don't try to configure multicast group unless both ROSTestDC and ROS
//  have ReceiveMulticast set to true.
//
//  Revision 1.62  2008/11/04 13:57:03  gcrone
//  Bug fix
//
//  Revision 1.61  2008/10/16 08:06:48  gcrone
//  Adapt for new multicast group configuration.
//
//  Revision 1.60  2008/09/30 10:16:57  gcrone
//  Use uint_32 instead of unsigned long for roiDistribution vector.
//
//  Revision 1.59  2008/05/20 13:21:27  gcrone
//  Trap TERM signal and shutdown nicely
//
//  Revision 1.58  2008/04/08 16:54:35  gcrone
//  Fix memory leak, delete myhdr at end of getLvl1id.
//
//  Revision 1.57  2008/03/27 07:46:01  gcrone
//  Update to new ReadoutModule schema
//
//  Revision 1.56  2008/03/18 17:52:20  gcrone
//  Bug fix: Should only skip 4 words of ROD header to get to the L1ID, not 5.
//
//  Revision 1.55  2008/03/18 12:28:23  gcrone
//  Get SyncDeletesWithEBRequests flag from database.
//
//  Revision 1.54  2008/03/10 09:14:26  gcrone
//  Remove redundant/obsolete include
//
//  Revision 1.53  2008/02/28 18:17:55  gcrone
//  Update to new dcmessages
//
//  Revision 1.52  2008/02/26 18:12:42  gcrone
//  Remove test_response option as this needs updating for event format 4.
//  Also, tidy up a bit, remove some spurious includes and make sure std
//  namespace specified where appropriate.
//
//  Revision 1.51  2008/02/13 17:47:00  gcrone
//  Don't expect ROSHeader on data coming back
//
//  Revision 1.50  2007/05/10 14:55:43  gcrone
//  Make sure TransportManager initialises the transport for the multicast group
//
//  Revision 1.49  2007/04/19 12:42:32  gcrone
//  Our dal class is now TESTNODEID_ROSTestDC to get an appropriat node ID.  Also, get TraceLevel and TracePackage from our own db object instead of the ROS
//
//  Revision 1.48  2007/04/18 07:07:50  gcrone
//  TraceLevel and TracePackage start with upper case characters now!!
//
//  Revision 1.47  2007/04/16 12:03:08  gcrone
//  Use new ROSRequest instead of ROSRequestMessage.  Start to get node IDs using get_node_id() dal method.
//
//  Revision 1.46  2007/03/30 09:09:15  gcrone
//  Fix up getting trace level and package from ROS's ReadoutConfig.  Maybe
//  we should actually have our own attributes in the database for this.
//
//  Revision 1.45  2007/03/28 09:48:30  gcrone
//  Adapt to new df schema
//
//  Revision 1.44  2007/03/20 10:38:10  gcrone
//  Update for new dcmessages implementation
//
//  Revision 1.43  2006/11/01 12:56:52  gcrone
//  Comment out unsed args to avoid compiler warnings
//
//  Revision 1.42  2006/03/23 17:08:40  gcrone
//  Change and document default value for number of outstanding requests.
//
//  Revision 1.41  2006/03/22 17:48:56  gcrone
//  Check Module class name for preloaded, not channel.
//
//  Revision 1.40  2006/03/15 14:57:08  gcrone
//  Catch and print ConfigExceptions.
//
//  Revision 1.39  2006/03/15 13:20:20  gcrone
//  Construct channel Ids that include the detector Id.
//
//  Revision 1.38  2006/03/11 10:28:23  gcrone
//  use daq::Application
//
//  Revision 1.37  2006/01/17 15:21:14  gcrone
//  Removed redundant include of oksconfig/OksConfiguration.h
//
//  Revision 1.36  2005/12/15 16:33:21  gcrone
//  Update usage text.
//
//  Revision 1.35  2005/12/13 16:04:58  gcrone
//  Tidy up option processing and extend usage text
//
//  Revision 1.34  2005/12/06 18:21:13  jorgen
//   fix CTL+\ problem by blocking SIGQUIT
//
//  Revision 1.33  2005/12/01 15:33:11  gcrone
//  Get TestGarbageCollection flag from database.
//  Tidy some output statements.
//
//  Revision 1.32  2005/12/01 13:16:55  jorgen
//   add emulation & testing of garbage collection
//
//  Revision 1.31  2005/11/23 17:50:46  gcrone
//  Change end of run to wait for outstanding requests. Plus config changes for new df schema
//
//  Revision 1.30  2005/11/16 19:15:51  gcrone
//  Use get_belongs_to() instead of belongs_to() and check that our
//  ConnectsToROS relationship returns non-zero.
//
//  Revision 1.29  2005/11/11 17:26:00  gcrone
//  Remove references to TrafficType.
//
//  Revision 1.28  2005/11/09 16:15:40  gcrone
//  Add clear sequence count.
//
//  Revision 1.27  2005/11/03 16:48:55  gcrone
//  Updated config item names to be as given in the database so that ROSDBConfig doesn't need to translate
//
//  Revision 1.26  2005/10/27 15:35:05  gcrone
//  Addd option to configure ROSTestDC from database
//
//  Revision 1.25  2005/10/27 08:34:16  jorgen
//   initialise L1ID in trigger generator via set_level1Id method
//
//  Revision 1.24  2005/10/25 13:15:33  gcrone
//  Kludge a 0 into oldest L1ID field of DFM_Clear_Msg constructor for now.
//
//  Revision 1.23  2005/10/18 17:53:37  gcrone
//  Fix memory leak,  delete myhdr in getLvl1id() and check_response()
//
//  Revision 1.22  2005/10/03 16:02:59  mlevine
//
//  Removed  ROSTestDC::formatted_rod_print()
//  Changed  check_response() to conform to EF Ver 3.0
//  Print EF version used
//  Removed erroneous pointer manipulations committed previously (event was truncated)
//  printout of ROB/ROD now inline in  check_response()
//  Verified working
//
//  Revision 1.21  2005/08/25 17:01:15  gcrone
//  Allow running for infinite number of events if number given is 0.
//  Move delete of 1st reply to before 2nd receive!
//  Remove do {...}while(0);
//
//  Revision 1.20  2005/08/25 11:45:12  gcrone
//  Rename m_tot_instances to m_totalInstances and fix "totalInstances"
//  flag fom 1 to 0 in option structure.
//
//  Revision 1.19  2005/08/24 12:57:34  gcrone
//  New queing interface
//
//  Revision 1.18  2005/08/23 16:17:31  gcrone
//  Make sure getLvl1id() is called for ALL replies received.
//
//  Revision 1.17  2005/08/22 08:04:43  jorgen
//   remove SYNC option
//
//  Revision 1.16  2005/08/19 16:01:59  jorgen
//   add time stamping & DEBUG_TEXT
//
//  Revision 1.15  2005/08/17 15:24:13  jorgen
//   hack to ROSTestDC for speed-up
//
//  Revision 1.14  2005/08/05 11:54:39  unel
//  fogotten debug removed from ROSTestDc
//
//  Revision 1.13  2005/08/03 17:52:37  unel
//  sync in Trigger Generator using queues
//
//  Revision 1.12  2005/07/21 20:40:44  unel
//  sync for ROSTestDC
//
//  Revision 1.11  2005/07/16 13:01:37  unel
//  ROB printout converted to ef3.0
//
//  Revision 1.10  2005/07/15 07:20:23  unel
//  start for new eformat
//
//  Revision 1.9  2005/01/14 13:40:22  joos
//  Adaptations to new API in DFSubSystemItem/Config.cpp
//
//  Revision 1.8  2005/01/11 15:20:40  gcrone
//  Update DF_ALLOCATOR stuff so that it compiles
//
//  Revision 1.7  2004/12/17 10:23:21  unel
//  added DOC directory
//  imported MLevine's ROSTestDC
//
//  Revision 1.6.2 2004/11/11 mlevine
//  using Event Format Library to navigate event structure and to detect errors
//  add flag --p to trigger the printout of the EFL and ROD payload
//
//  Revision 1.6.1 2004/10/27 mlevine
//  added formatted printing of events received, --c specifies cookie (length of response payload)
//
//  Revision 1.6  2004/10/19 13:08:12  glehmann
//  added missing libraries to test programs
//
//  Revision 1.5  2004/08/25 08:42:13  joos
//  globally: code added to catch a division by zero and debug output improved
//
//  Revision 1.4  2004/07/26 17:33:37  jorgen
//  Moved ROSTestDC configuration from triggerIn.dat  and dataOut.dat to dedicated ROSTestDC.dat file
//
//  Revision 1.3  2004/07/26 15:35:39  jorgen
//   fix problems around data channels
//
//  Revision 1.2  2004/07/12 15:03:04  joos
//  L1ID reset at start; memory leak fixed
//
//  Revision 1.1.1.1  2004/02/05 18:31:17  akazarov
//  imported from nightly 05.02.2004
//
//  Revision 1.11  2004/01/26 14:31:00  gcrone
//  Remove ends from end of ostringstream writes.
//
//  Revision 1.10  2004/01/19 10:08:10  joos
//  DFDebug introduced
//
//  Revision 1.9  2003/12/03 15:09:36  gcrone
//  Config constructors now protected, use static method New to enforce use of DFCountedPointer
//
//  Revision 1.8  2003/11/11 10:56:07  gcrone
//  Use DFCountedPointer<Config> in place of Config*.
//
//  Revision 1.7  2003/10/16 16:59:14  jorgen
//   sigquit cleaning
//
//  Revision 1.6  2003/10/15 18:28:15  gorini
//  Added ctrl-\ handler to ROSTestDC.... it prints stats
//
//  Revision 1.5  2003/03/11 13:55:37  gorini
//  Minor bug fix
//
//  Revision 1.4  2003/03/10 15:04:44  gorini
//  Added --instance(-i) commandline argument. It is needed to run multiple instances of the test program on different machines. The --instance value must correspond to a remoteNode as specified in the ROS config files.
//
//  Revision 1.3  2003/02/27 16:36:42  joos
//  bug fixed
//
//  Revision 1.2  2003/02/26 20:42:13  gorini
//  New version of ROSTestDC program with a more realistic trigger generation
//
//  Revision 1.1  2003/02/25 14:17:42  gcrone
//  Added test program ROSTestDC
//
//
// //////////////////////////////////////////////////////////////////////

#include <sstream>
#include <string>
#include <iostream>
#include <fstream>

#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <stdio.h>

#include <time.h>
#include <sys/time.h>
#include <getopt.h>
#include <netdb.h>
#include <errno.h>

#include "DFDebug/DFDebug.h"

#include "ROSIO/IOException.h"
#include "ROSIO/TransportManager.h"
#include "DFSubSystemItem/Config.h"
#include "DFSubSystemItem/ConfigException.h"
#include "rcc_time_stamp/tstamp.h"

#include "msg/Port.h"

#include "eformat/eformat.h"
#include "dcmessages/eutil.h"
#include "dcmessages/DFM_Clear_Msg.h"
#include "dcmessages/DataRequest.h"
#include "dcmessages/Messages.h"


// Include files for the database
#include "config/Configuration.h"
#include "dal/util.h"
#include "dal/Partition.h"
#include "dal/Component.h"
#include "dal/Detector.h"
#include "DFdal/TESTNODEID_ROSTestDC.h"
#ifdef TDAQ191
#include "DFdal/DCMulticastGroup.h"
#endif
#include "DFdal/ROS.h"
#include "DFdal/ReadoutConfiguration.h"
#include "DFdal/ReadoutModule.h"
#include "DFdal/InputChannel.h"
#ifndef TDAQ191
#include "DFdal/DFParameters.h"
#endif
//Message Passing stuff
#include "msgconf/MessageConfiguration.h"
#ifndef TDAQ191
#include "rcc_time_stamp/tstamp.h"
#include "ROSIO/IOException.h"
#include "DFDebug/DFDebug.h"
#include "DFSubSystemItem/Config.h"
#include "ROSUtilities/ROSErrorReporting.h"
#endif

#include "ROSTestDC2.h"

#include "ipc/core.h"

using daq::Application;
using namespace eformat;
using namespace ROS;

/* Common Block Declaration for random function */

struct ranmarstruct {
    double u[97], c, cd, cm;
    int i97, j97;
} raset1_1;

int ranseed;

/********************************************************************************/
// rmarin, ranmar and ran: Marsaglia-Zaman random number generation
// Implementation by F. James, with f2c translated from Frtran into C
// See: F. James, "A reveiew of pseudorandom number generators", 
// Comp. Phys. Comm. 60 (1990) 329 - 344
// CERNLIB V116: RANMAR.F
// http://http://cernlib.web.cern.ch/cernlib/download/2006_source/src/packlib/kernlib/kerngen/tcgen/
// CPC Program library: PSEUDORAN, catologue number: ABTK_v1_0
/********************************************************************************/


/********************************************************************************/
int rmarin(int *ij, int *kl)
/********************************************************************************/
{

	static int i, j, k, l, m;
	static double s, t;
	static int ii, jj;
	static int ij1, kl1;

	ij1 = *ij;
	kl1 = *kl;

	i = ij1 / 177 % 177 + 2;
	j = ij1 % 177 + 2;
	k = kl1 / 169 % 178 + 1;
	l = kl1 % 169;
	for (ii = 1; ii <= 97; ++ii)
	{
		s = 0.f;
		t = .5f;
		for (jj = 1; jj <= 24; ++jj) 
		{
			m = i * j % 179 * k % 179;
			i = j;
			j = k;
			k = m;
			l = (l * 53 + 1) % 169;
			if (l * m % 64 >= 32)
			{
				s += t;
			}
			t *= .5f;
		}
		raset1_1.u[ii - 1] = s;
	 }
	 raset1_1.c = .021602869033813477f;
	 raset1_1.cd = .45623308420181274f;
	 raset1_1.cm = .99999982118606567f;
	 raset1_1.i97 = 97;
	 raset1_1.j97 = 33;
	 return 0;
}


/********************************************************************************/
int ranmar(double *rvec, int *ilen)
/********************************************************************************/
{
/* Universal random number generator proposed by Marsaglia and Zaman
 * in report FSU-SCRI-87-50

 * Slightly modified by F. James, 1988, to generate a vector
 * of pseudorandom numbers rvec of length ilen
 * and making the common block include everything needed to
 * specify completely the state of the generator.
 * see also subroutine "rmarin"

 * rvec	=> double array, array where random numbers will be stored.
 * ilen	<= int, length of array rvec.
 */
	int i__1;
	static int ivec;
	static double uni;

	--rvec;

	i__1 = *ilen;
	for (ivec = 1; ivec <= i__1; ++ivec)
	{
		uni = raset1_1.u[raset1_1.i97 - 1] - raset1_1.u[raset1_1.j97 - 1];
		if (uni < 0.f)
		{
			uni += 1.f;
		}
		raset1_1.u[raset1_1.i97 - 1] = uni;
		--raset1_1.i97;
		if (raset1_1.i97 == 0)
		{
			raset1_1.i97 = 97;
		}
		--raset1_1.j97;
		if (raset1_1.j97 == 0)
		{
			raset1_1.j97 = 97;
		}
		raset1_1.c -= raset1_1.cd;
		if (raset1_1.c < 0.f)
		{
			raset1_1.c += raset1_1.cm;
		}
		uni -= raset1_1.c;
		if (uni < 0.f)
		{
			uni += 1.f;
		}
		rvec[ivec] = uni;
	}
	return 0;
}

/********************************************************************************/
double ran(int *ised)
/********************************************************************************/
{
/* Random number generator interface to ranmar etc.
 * the seed is only used at the very first call and determines
 * which sequence is chosen from ranmar. 
 *
 * ised	<= int, initial seed selector for ranmar.
 */

	static bool frs = true;
	static int nrn = 0;
	double ret_val;
	static int c__100 = 100;

	static int i1, i2;
	static double rn[100];

	--nrn;
	if (nrn <= 0)
	{
		if (frs)
		{
			if (*ised != 0)
			{
				i1 = *ised % 31329;
				i2 = *ised / 31328 % 30082;
				rmarin(&i1, &i2);
			}
			frs = false;
		}
		ranmar(rn, &c__100);
		nrn = 100;
		ret_val = rn[99];
	}
	else
	{
		ret_val = rn[nrn - 1];
	}
	return ret_val;
}

/********************************************************************************/
double expran(double averageInterval)
/********************************************************************************/
{
		double rnum, rexp;
		
		rnum = ran(&ranseed);
		if (rnum < 0.0000001f) rnum = 0.0000001f;
		rexp = -log(rnum) * (double) averageInterval;
		return (rexp);
}

/********************************************************************************/
double truncAverage(double av0, double f)
/********************************************************************************/
{
/* Calculate average of truncated exponential distribution
 * use case: limiting time interval between successive events
 *
 * Input: av0: average of untruncated distribution
 *        f:  distribution is truncated at f * average
 * Minimum time interval = 1.0
 *
 * The exponential distribution with an average av0 is: av0 * exp(-t/av0)
 * This gives the probability to have an interval with length between t and t+dt.
 * The average interval is given by the integral of av0 * t * exp(-t/av0)
 * This is av0 for maximum t equal to infinity.
 * For a maximum T and a mimimum 1 the average  becomes:
 * (av0 + T - (av0 + 1.0) * exp((T-1.0)/av0) / (1.0 - exp((T-1.0)/av0))
 * These results are obtained by calculating the integral, but with a correction
 * factor equal to the integral of av0 * exp(-t/av0) from t=1 to t=f*average
 * so that the probability distribution used for computing the average of t
 * is  normalised.
 *
 * This function determines with an iterative procedure the parameters of
 * an exponential distribution truncated at a maximum of t=f*average and a
 * minimum of t = 1.0 , which has the same average as the full distribution.
 * With x = av0 / newAverage and T = f * av0, the integral becomes:
 * newAverage * (1.0 + f*x - (1.0 + 1.0/newAverage) * exp (f*x - 1.0/newAverage) ) / (1.0 - exp (f*x - 1.0/newAverage) ) 
 * This should be equal to av0 = x * newAverage
 * Therefore, if 1.0/newAverage is replaced by x/av0, this equation is obtained:
 * x - (1.0 + f*x - (1.0 + x/av0) * exp (f*x -x/av0) )/ (1.0 - exp (f*x -x/av0) ) = 0 is iteratively solved.
 *
 * NB: f > about 2.08, otherwise there is no solution
 */
	double avCalculated, x, diff, newAverage;
	double aStep = -0.2;

	if (f < 2.08)
	{
		std::cout << "truncAverage: f " << f << "< 2.08 -> no solution" << std::endl; 
		exit(0);
	}
	
 	avCalculated = av0 * (1.0 - f / (exp(f) - 1.0 ) );
	x = avCalculated / av0; // start value
	while ( fabs(aStep) > 1.0e-14)
	{
		diff = x -  (1.0 + f*x - (1.0 + x/av0) * exp (f*x -x/av0) )/ (1.0 - exp (f*x -x/av0) );
		if (diff == 0.0)
		{
			break;
		}
		if (diff < 0.0)
		{
			if (aStep < 0.0) aStep = -0.5 * aStep;
		}
		else
		{
			if (aStep > 0.0) aStep = -0.5 * aStep;
		}
		x = x + aStep;
	}
	newAverage = av0 / x;
	std::cout << "newAverage " << newAverage << std::endl;
	return newAverage;
}

//helper::Version my_version;

// a global ..
bool sigquitFlag = false;
bool sigtermFlag = false;
u_long crctab16[65536];


/********************************************************************************/
SockIO::SockIO(const char *serverName, int nClients, int numberOfPort)
/********************************************************************************/
{
	portNumber = numberOfPort;
	theSock.sin_family = AF_INET;
	theSock.sin_port = htons(portNumber);
	if (serverName == NULL)
	{
		theSock.sin_addr.s_addr = htonl(INADDR_ANY);
	}
	else
	{
		struct hostent *hp = gethostbyname(serverName);
		memcpy( (char *) &theSock.sin_addr,hp->h_addr,hp->h_length);
	}
	socketRef = socket( AF_INET, SOCK_STREAM, 0 );
	if ( socketRef < 0 )
	{
		std::cerr << "socket call failed" << std::endl;
		exit(1);
	}
	if (serverName == 0)
	{
		std::cout << "Socket created for master" << std::endl;
	}
	else
	{
		std::cout << "Socket created for slave, master = " << serverName << std::endl;
	}
	numberOfClients = nClients;
}

/********************************************************************************/
bool SockIO::InitClient()
/********************************************************************************/
{
	int connectResult = connect(socketRef, (struct sockaddr*) &theSock, sizeof(theSock) );
	if (connectResult)
	{
		std::cerr << "connect call failed, errno = " << errno << std::endl;
		perror("connect call error");
		return false;
	}
	return true;
}

/********************************************************************************/
bool SockIO::InitServer()
/********************************************************************************/
{
	int bindResult = bind(socketRef, (struct sockaddr*) &theSock, sizeof(theSock) );
	if (bindResult < 0)
	{
		std::cerr << "bind call failed, errno = " << errno << std::endl;
		perror("bind call error");
		return false;
	}
	int listenResult = listen(socketRef, 5 );
	if (listenResult)
	{
		std::cerr << "listen call failed, errno = " << errno << std::endl;
		perror("listen call error");
		return false;
	}

	for (int i=0;i<numberOfClients;i++)
	{
		acceptSocketRef[i] = accept(socketRef, NULL, NULL );
		if (acceptSocketRef[i] < 0)
		{
			std::cerr << "accept call failed, errno = " << errno << std::endl;
			perror("accept call error");
			exit(1);
		}
		else
		{
			std::cout << "acceptSocketRef " << acceptSocketRef[i] << " for client " << i << std::endl;
		}
	}
	return true;
}

/********************************************************************************/
void SockIO::GetParams(struct sExchangeBuf *theBuf)
/********************************************************************************/
{
	int rc;
	
	char *p = (char *) theBuf;
	int nBytes =  sizeof(struct sExchangeBuf);

	while (nBytes>0) 
	{
		rc = recv(socketRef, p, nBytes, 0 );
		if (rc <= 0)
		{
			std::cerr << "recv call failed" << std::endl;
			exit(1);
		}
		p = p + rc;
		nBytes = nBytes - rc;
	}

	std::cout << "Received via socket: l2frac " << theBuf->l2frac << " ebfrac " << theBuf->ebfrac 
				 << " deleteGroupSize " << theBuf->deleteGroupSize << " avROLsPerRoI " 
				 << theBuf->avROLsPerRoI << " printInterval " << theBuf->printInterval
				 << " maxNumberOfOutputsPerRun " << theBuf->maxNumberOfOutputsPerRun 
                 << " target L1 frequency " << theBuf->targetL1Freq
                 << " Etmiss fraction " << theBuf->percentOfEtmiss << std::endl;
}

/********************************************************************************/
void SockIO::SendParams(struct sExchangeBuf *theBuf, unsigned int fragSize)
/********************************************************************************/
{
	int rc;
	char *p = (char *) theBuf;
	
	for (int i=0; i<numberOfClients; i++)
	{
		int nBytes =  sizeof(struct sExchangeBuf);
		while (nBytes>0) 
		{
			rc = send(acceptSocketRef[i], (char *) theBuf, sizeof(struct sExchangeBuf), 0 );
			if (rc <= 0 )
			{
				std::cerr << "send call failed" << std::endl;
				exit(1);
			}
			p = p + rc;
			nBytes = nBytes - rc;
		}
		std::cout << "Sent via socket " << acceptSocketRef[i] << " : l2frac " << theBuf->l2frac 
					 << " ebfrac " << theBuf->ebfrac << " deleteGroupSize " << theBuf->deleteGroupSize
					 << " avROLsPerRoI " << theBuf->avROLsPerRoI 
					 << " printInterval " << theBuf->printInterval
					 << " maxNumberOfOutputsPerRun " << theBuf->maxNumberOfOutputsPerRun 
                     << " target L1 frequency " << theBuf->targetL1Freq
                     << " Etmiss fraction " << theBuf->percentOfEtmiss << std::endl
					 << " NB: size = " << fragSize << std::endl;
	}
}


/********************************************************************************/
ROSTestDC::ROSTestDC(DFCountedPointer<Config> configuration,
                     int numberOfOutstandingRequests,
							int instance, 
							int totalInstances,
							int cookie,
							bool printData,
							bool testResponse,
							int waitUntilStartupTime,
							bool theMasterFlag, bool theSlaveFlag, bool theMultiReadoutAppsFlag,
							const char *theName)
/********************************************************************************/
{
  m_printData = printData;
  m_testResponse = testResponse;
  DEBUG_TEXT(DFDB_ROSTG, 15, "ROSTestDC: embedded constructor of TriggerGenerator called:  called");

  m_instance = 0;		// may be overwritten by ROSTestDC  FIXME
  m_totalInstances = 1;

  m_disabled=false;

  m_l2RequestsGenerated=0;
  m_metRequestsGenerated = 0;

  socketioFlag = theMasterFlag || theSlaveFlag;
  masterFlag = theMasterFlag;
  slaveFlag = theSlaveFlag;
  multiReadoutAppsFlag = theMultiReadoutAppsFlag;
  if (masterFlag)
  {
		controllerName = theName;
  }
  else
  {
		controllerName = NULL;
  }
  
  // garbage collection
  m_testGarbageCollection = false;
  m_oldestL1id = 0;

  m_next = 0;
  m_transactionId = 1;

  m_qEBhist.reserve(c_maxEBbins);
  for (int i = 0; i < c_maxEBbins; i++) {
    m_qEBhist.push_back(0);
  }

  m_qCLEARhist.reserve(c_maxCLEARbins);
  for (int i = 0; i < c_maxCLEARbins; i++) {
    m_qCLEARhist.push_back(0);
  }

  m_setINPROCESShist.reserve(c_maxINPROCESSbins);
  for (int i = 0; i < c_maxINPROCESSbins; i++) {
    m_setINPROCESShist.push_back(0);
  }

  //Delete grouping
  m_deleteGrouping = configuration->getInt("DeleteGrouping");
  DEBUG_TEXT(DFDB_ROSTG, 20, "TriggerGenerator: constructor:  delete grouping = " << m_deleteGrouping);

  //Reserve space for the vector of l1Ids for deletes
  m_l1Ids.reserve(m_deleteGrouping);

  //Max size of ROI requests
  m_maxRolsInRoI = configuration->getInt("maxRolsInRoI");

  //Number of ROLs
  m_numberOfChannels =  configuration->getInt("numberOfChannels");
  if (m_numberOfChannels==0) {
// This is the new code  
     CREATE_ROS_EXCEPTION (tException, IOException, IOException::DIVBY0,
                           "Number of channels is zero. All triggers will be disabled. ");
     ers::error( tException );

// Instead of this one
//    IOException tException (IOException::DIVBY0);
//    dfout << setexception(tException) << "\nNumber of channels is zero. All triggers will be disabled" << endm;


    m_disabled=true; 
  }
  for (int i=0; i<m_numberOfChannels; i++) 
  {
    m_channelIds.push_back(configuration->getInt("channelId", i));
    DEBUG_TEXT(DFDB_ROSTG, 20, "TriggerGenerator: constructor:  found channel # " << m_channelIds[i]);
  }

  //Reserve space for vector of rols for L2 requests
  m_rols.reserve(m_numberOfChannels);  

  //Probability of an L2PU request
  l2RequestFraction = configuration->getFloat("L2RequestFraction");
   
  //Probability of an EB request
  ebRequestFraction = (configuration->getFloat("EBRequestFraction"));
  m_l2ap_i = 100.0 / ebRequestFraction;
  DEBUG_TEXT(DFDB_ROSTG, 20, "TriggerGenerator: constructor:   L2A % scaled up = " << m_l2ap_i);
  DEBUG_TEXT(DFDB_ROSTG, 20, "TriggerGenerator: constructor:   RAND_MAX = " << RAND_MAX);

  //Parameters for DC simulation
  m_inputDelay = configuration->getInt("InputDelay");
  m_destination = configuration->getInt("destinationNodeId");
  
  //Parameters to control the number of ROLs involved in a L2PU request    
  m_l2puGroup.reserve(m_maxRolsInRoI + 1);
  for(int i = 0; i< m_maxRolsInRoI; i++) 
  {
    m_l2puGroup[i + 1] = configuration->getInt("probRols", i + 1);
  }

// NIKHEF parameter ..

  m_syncDeletesWithEBRequests = configuration->getBool("SyncDeletesWithEBRequests");
  DEBUG_TEXT(DFDB_ROSTG, 20, "TriggerGenerator: constructor: m_syncDeletesWithEBRequests = " << m_syncDeletesWithEBRequests);

// garbage collection parameters
  m_testGarbageCollection = configuration->getBool("TestGarbageCollection");

  //Configuration dump
  std::cout << "TriggerGenerator: L2Request fraction " << l2RequestFraction <<
               "% per Robin (" << std::dec << m_numberOfChannels << ")"  << std::endl;
  std::cout << " EBRequest fraction " << ebRequestFraction << "%" << std::endl;
  std::cout << " deletes grouped in " << std::dec << m_deleteGrouping << " s" << std::endl;
  std::cout << " input delay " << std::dec << m_inputDelay << " microseconds" << std::endl;
  for(int i=0; i< m_maxRolsInRoI; i++) 
  {
    std::cout << "TriggerGenerator: m_l2puGroup[" << i+1 << "] = " << m_l2puGroup[i+1] << std::endl;
  }

  std::cout << "TriggerGenerator: m_testGarbageCollection   = " << m_testGarbageCollection << std::endl;

// === end of code from contructor TriggerGenerator


   m_numberOfOutstanding=numberOfOutstandingRequests;
   
	if (multiReadoutAppsFlag == false)
	{
		m_instance = instance;
		set_level1Id(instance);		// FIXME
		m_totalInstances = totalInstances;
	}
	else
	{
		m_instance = 1;
		set_level1Id(m_instance);		// FIXME
		m_totalInstances = 1;
	}
	std::cout << "total instances = " << totalInstances << " instance " << instance << "m_totalInstances " << m_totalInstances << " m_instance " << m_instance << std::endl;
   m_cookie = cookie;
   m_localHostId=configuration->getInt("instanceNodeId", instance);

   m_clearSequence=0;

   std::cout << " Max. outstanding = " << m_numberOfOutstanding << std::endl;
   std::cout << " RT:m_localHostId = " << m_localHostId << std::endl;

   std::string localHost=configuration->getString("instanceAddress", instance);

   std::cout << " RT:localHost = " << localHost << std::endl;

   m_remoteNodeId=configuration->getInt("ROSNodeId");
   std::string remoteNode=configuration->getString("ROSAddress");

   std::cout << " RT:m_remoteNodeId = " << m_remoteNodeId << std::endl;
   std::cout << " RT:remoteNode = " << remoteNode << std::endl;

   std::string multicastAddress = configuration->getString("multicastAddress");
   std::cout << " RT:multicastAddress = " << multicastAddress << std::endl;
   int multicastId = configuration->getInt("multicastId");
   std::cout << " RT:multicastId = " << multicastId << std::endl;
   TransportManager* transportManager=TransportManager::create();
   transportManager->initialise(localHost,multicastAddress);
#ifndef TDAQ191
	if (multicastAddress != "")
#endif
	{
	  transportManager->addGroup("ROS", multicastId, multicastAddress);
	}
   transportManager->addNode(localHost, m_localHostId);
   transportManager->addNode(remoteNode, m_remoteNodeId, "ROS");
   transportManager->configure(m_localHostId);

	m_remotePort=MessagePassing::Port::find(m_remoteNodeId);
	if (m_remotePort==0) 
	{
	  std::cerr << "Failed to find remote Port!\n" ;
	  exit (EXIT_FAILURE);
	}

#ifndef TDAQ191
	if (multicastAddress != "")
	{
	  m_rosPort=MessagePassing::Port::find(multicastId);
	  std::cout << "Multicasting, separate port for sending clears\n" ;
	}
	else
	{
	  m_rosPort = m_remotePort;
	  std::cout << "No multicasting, use port for sending requests also for sending clears\n" ;
	}
   if (m_rosPort==0) {
      std::cerr << "Failed to find remote Port for sending deletes!\n" ;
      exit (EXIT_FAILURE);
   }
#else
   m_rosPort=MessagePassing::Port::find(multicastId);
#endif
   std::cout << "m_remotePort: " << m_remotePort <<std::endl;
   std::cout << "m_rosPort: " << m_rosPort <<std::endl;


   int metPercent=configuration->getInt("metPercent");
   m_metInterval=RAND_MAX*((float)metPercent/100.0);

   if (m_metInterval!=0) {
      unsigned int detectorId=configuration->getUInt("detectorId");
      if ((detectorId&0x00f0)==0x40) {
         m_metID=0x007d;
         std::cout << "LAr ROS - will configure for MEt hack\n";
      }
      else   if ((detectorId&0x00f0)==0x50) {
         m_metID=0x007e;
         std::cout << "Tile ROS - will configure for MEt hack\n";
      }
      else {
         m_metID=0x007f;
         std::cout << "=====================================================\n"
                   << "#  Detector id " << std::hex << detectorId << std::dec << " does not support mET requests #\n"
                   << "#  This should not be expected to work              #\n"
                   << "=====================================================\n";
      }
   }
   
   reqTypeState = new eReqTypeState[c_ereqtypestate_size];
   clearThisIdOnlyFlag = new bool[c_ereqtypestate_size];
   reRequestCount = new unsigned int[c_ereqtypestate_size];
   releaseCount = new unsigned int[c_ereqtypestate_size];
	
	lastIdToBeCleared = m_instance;
	maxFragSize = 0;
	

	// random numbers
	ranseed = 93634 + m_instance;
	std::cout << "ranseed " << ranseed << std::endl;
	ran(&ranseed);

	for(int loop = 0; loop < c_aleasize; loop++)
	{
		m_alea[loop] = ran(&ranseed);
	}

	std::cout << "m_l2ap_i " << m_l2ap_i << " Instances " << m_totalInstances << " m_instance " << m_instance << std::endl;

   //   std::cout <<std::endl <<"===========  using Event Format Library  " << my_version.human()<< "  =========" <<std::endl;
	
	m_ebInterval = new unsigned int[c_ereqtypestate_size];
   m_l2Interval = new unsigned int[c_ereqtypestate_size];
	
	stopFlag = false;
	numberOfStatOutputPerRun = 0;
	lastid = m_instance;
	m_numberOfL1ids = 0;
	
	if (masterFlag || slaveFlag)
	{
		int i;
		int numberOfPort = PARAM_EXCHANGE_PORT_NUMBER;
		if (masterFlag)
		{
			paramExchangeSocket = new SockIO(NULL, totalInstances - 1, numberOfPort);
			// the server must first start to listen
			for (i=0;i<4;i++)
			{
				if (paramExchangeSocket->InitServer())
				{
					std::cerr << "Initialized server, port number = " << numberOfPort << std::endl;
					break;
				}
				else
				{
					// something wrong, get rid of socket, wait some time and try again
					std::cerr << "Could not initialize server for port number = " << numberOfPort << std::endl;
					waitTime.tv_sec = 1;
					waitTime.tv_nsec = 0;
					delete paramExchangeSocket;
					nanosleep(&waitTime, NULL);
					numberOfPort++;
					paramExchangeSocket = new SockIO(NULL, totalInstances - 1, numberOfPort);
					nanosleep(&waitTime, NULL);
				}
			}
			if (i==4)
			{
				exit(0);
			}
		}
		else
		{
			paramExchangeSocket = new SockIO(theName, totalInstances - 1, numberOfPort);
			waitTime.tv_sec = 5;
			waitTime.tv_nsec = 0;
			nanosleep(&waitTime, NULL);
			for (i=0;i<4;i++)
			{
				if (paramExchangeSocket->InitClient())
				{
					std::cerr << "Initialized client, port number = " << numberOfPort << std::endl;
					break;
				}
				else
				{
					// something wrong, get rid of socket, wait some time and try again
					std::cerr << "Could not initialize client for port number = " << numberOfPort << std::endl;
					waitTime.tv_sec = 1;
					waitTime.tv_nsec = 0;
					delete paramExchangeSocket;
					nanosleep(&waitTime, NULL);
					numberOfPort++;
					paramExchangeSocket = new SockIO(theName, totalInstances - 1, numberOfPort);
					nanosleep(&waitTime, NULL);
				}
			}
			if (i==4)
			{
				exit(0);
			}
		}
	}
	else
	{
		// safety
		paramExchangeSocket = NULL;
	}
	waitTime.tv_sec = waitUntilStartupTime;
   waitTime.tv_nsec = 0;
   nanosleep(&waitTime, NULL);
	
	lastL1Freq = 0.0;
	upCount = 0;
	downCount = 0;

	l2ReqfracEqualsOneStopFlag = false;
	integrateModeFlag = true;
}

ROSTestDC::~ROSTestDC() {
   delete [] reqTypeState;
   delete [] clearThisIdOnlyFlag;
   delete [] reRequestCount;
   delete [] releaseCount;

   delete [] m_ebInterval;
   delete [] m_l2Interval;
}

/**************************************************************/
void ROSTestDC::syncParams()
/**************************************************************/
{
	// synchronize parameters
	if (socketioFlag)
	{
		if (masterFlag)
		{
			theBuf.l2frac = l2RequestFraction;
			theBuf.ebfrac = ebRequestFraction;
			theBuf.deleteGroupSize = m_deleteGrouping;
			theBuf.avROLsPerRoI = avROLsPerRoI;
			theBuf.printInterval = printInterval;
			theBuf.maxNumberOfOutputsPerRun = maxNumberOfOutputsPerRun;
			theBuf.targetL1Freq = targetL1Freq;
         theBuf.percentOfEtmiss = percentOfEtmiss;
			paramExchangeSocket->SendParams(&theBuf, maxFragSize);
		}
		else
		{
			paramExchangeSocket->GetParams(&theBuf);
			l2RequestFraction = theBuf.l2frac;
			ebRequestFraction = theBuf.ebfrac;
			m_deleteGrouping = theBuf.deleteGroupSize;
			avROLsPerRoI = theBuf.avROLsPerRoI;
			printInterval = theBuf.printInterval;
			maxNumberOfOutputsPerRun = theBuf.maxNumberOfOutputsPerRun;
			if (multiReadoutAppsFlag == true)
			{
				maxNumberOfOutputsPerRun++;
			}
			targetL1Freq = theBuf.targetL1Freq;
         percentOfEtmiss = theBuf.percentOfEtmiss;
		}
	}
}


/**************************************************************/
void ROSTestDC::init(bool firstInitFlag)
/**************************************************************/
{
	double nextEbReqId, nextL2ReqId;
	unsigned int nextEbReqIdInt, nextL2ReqIdInt;
	unsigned int currentEbReqIdInt, currentL2ReqIdInt;
	double maxIntervalLength;
	double maxIntervalFactor = 2.5;
	double interval;
	double pmin[c_maxRols+1], pmax[c_maxRols+1];
	double intervalEBSum, intervalEBSumInt;
	double intervalL2Sum = 0.0;
	double intervalL2SumInt = 0.0;
	int nEBInterval = 0;
	int nL2Interval = 0;
	unsigned int i;
	double modified_m_l2ap_i = 0.0; // assign a value to prevent a warning from the compiler
	double modified_m_l2req_interval = 0.0; // assign a value to prevent a warning from the compiler
	float lowestValForExpRan = 1.25;
	
  for (int j=0;j<c_maxRols;j++) 
  {
    m_roihist[j] = 0;
    m_numberOfROI[j] = 0;
  }

  if (m_maxRolsInRoI > c_maxRols)
  {
    std::cout << "m_maxRolsInRoI is larger than " << std::dec << c_maxRolsPerL2PU << std::endl;

    CREATE_ROS_EXCEPTION (tException, IOException, IOException::TGMAX_ROLS2, "");
    ers::error( tException );

  }
 
  int sum = 0;
  for(int loop = 1; loop < (m_maxRolsInRoI + 1); loop++)
  {
    sum += m_l2puGroup[loop];
    DEBUG_TEXT(DFDB_ROSTG, 20, "TriggerGenerator: constructor:   sum = " << sum);
  }
  
  if (sum != 100 && m_maxRolsInRoI)
  {
    std::cout << "The sum of the probuXXRols parameters is " << sum << " instead of 100" << std::endl;
    CREATE_ROS_EXCEPTION(tException, IOException, IOException::TGSUM,
                         "The sum of the probuXXRols parameters is " << sum << " instead of 100");
    throw tException;
  }
  
  //Compute some constants for the creation of L2PU requests
  float meanNumberOfRolsPerL2Request = 0.0;
  if ( avROLsPerRoI < 0.0)
  {
	  for(int loop = 1; loop < (m_maxRolsInRoI + 1); loop++)
	  {
		 meanNumberOfRolsPerL2Request += (float)m_l2puGroup[loop] / 100.0 * loop;
	  }
	  if (meanNumberOfRolsPerL2Request == 0)
	  {
		  CREATE_ROS_EXCEPTION(tException, IOException, IOException::DIVBY0,
									 "\n meanNumberOfRolsPerL2Request is zero. All triggers will be disabled");
		 ers::error(tException);
		 m_disabled=true; 
	  }
	}
	else
	{
		meanNumberOfRolsPerL2Request = avROLsPerRoI;
	}

  float l2FractionPerRos = (float)m_numberOfChannels * l2RequestFraction / meanNumberOfRolsPerL2Request;
  if (l2FractionPerRos > 100.) 
  {
    std::cout << "l2RequestFraction            = " << l2RequestFraction << std::endl;
	 l2RequestFraction = (100.0 * meanNumberOfRolsPerL2Request / ( (float) m_numberOfChannels));
    std::cout << "The fraction of ROI requests per ROS exceeds 100%! : therefore set l2RequestFraction to: " << l2RequestFraction << std::endl ;
    std::cout << "l2FractionPerRos             = " << l2FractionPerRos << std::endl;
    std::cout << "m_numberOfChannels           = " << std::dec << m_numberOfChannels << std::endl;
    std::cout << "meanNumberOfRolsPerL2Request = " << meanNumberOfRolsPerL2Request << std::endl;
    // CREATE_ROS_EXCEPTION (tException, IOException, IOException::TGL2F, "");
    // throw tException;  
	 l2ReqfracEqualsOneStopFlag = true;
  }
	if (l2FractionPerRos > 0.0001)
	{
		m_l2req_interval = 100.0 / l2FractionPerRos;
	}
	else
	{
		m_l2req_interval = -1.0;
	}
	
/* No longer needed ??
  //float absoluteL2puGroup[m_maxRolsInRoI + 1];
  float absoluteL2puGroup[c_maxRols + 1];
  absoluteL2puGroup[0] = 100.0;
  for(int loop = 1; loop < (m_maxRolsInRoI + 1); loop++)
  {
    absoluteL2puGroup[loop] = m_l2puGroup[loop] / 100.0 * l2FractionPerRos;
    absoluteL2puGroup[0] -= absoluteL2puGroup[loop];
    std::cout << "absoluteL2puGroup[0] = " << absoluteL2puGroup[0] << " loop " << loop << " absoluteL2puGroup " << absoluteL2puGroup[loop] << std::endl;
  }

  m_l2puLimit.reserve(m_maxRolsInRoI);  
  float integratedL2puGroup = 0.;
  for(int loop = 0; loop < m_maxRolsInRoI; loop++)
  {
    integratedL2puGroup += absoluteL2puGroup[loop];
	 m_l2puLimit[loop] = integratedL2puGroup / 100.0;
    std::cout << "integratedL2puGroup = " << integratedL2puGroup << " loop " << loop << " m_l2puLimit " << m_l2puLimit[loop] << std::endl;
  }
*/

	m_rollovers = 0;
   last_m_numberOfL1ids = m_numberOfL1ids;
	m_numberOfL2A = 0;
	m_totalNumberOfROI=0;
	m_numberOfL2req=0;
	m_numberOfDeleteGroup = 0;
	
  lastElapsedSeconds = 0.0;
  lastNumberOfL1Ids = 0;
  lastNumberOfL2req = 0;
  lastNumberOfL2A = 0;
  lastNumberOfdeleteGroup = 0;
	
  firstElapsedSeconds = 0.0;
  firstNumberOfL1Ids = 0;
  firstNumberOfL2req = 0;
  firstNumberOfL2A = 0;
  firstNumberOfdeleteGroup = 0;

	for (unsigned int i=0;i<c_ereqtypestate_size;i++)
	{
		clearThisIdOnlyFlag[i] = false;
		reRequestCount[i] = 0;
		releaseCount[i] = 0;
	}

	// initialize statistics counters
#ifdef GORDON
	for (int i=0;i<m_numberOfChannels;i++) 
	{
	  unsigned int chan=m_channelIds[i];
	  m_completeEBFragmentsPerROB[chan] = 0;
	  m_incompleteEBFragmentsPerROB[chan] = 0;
	  m_completeL2FragmentsPerROB[chan] = 0;
	  m_incompleteL2FragmentsPerROB[chan] = 0;
	}
#else
	for (unsigned int i=0;i< (unsigned int) m_numberOfChannels;i++) 
	{
		unsigned int chan=m_channelIds[i];
		sourceIdMap[chan] = i;
		std::cout << "Map source id " << chan << " onto " << i << std::endl;
	}
	for (int i=0;i<c_maxRolsPerL2PU;i++)
	{
		completeEBFragmentsPerROB[i] = 0;
		inCompleteEBFragmentsPerROB[i] = 0;
		completeL2FragmentsPerROB[i] = 0;
		inCompleteL2FragmentsPerROB[i] = 0;
	}
    numberOfEtmissFragsReceived = 0;
#endif
	
	numberOfGoodEBResponses = 0;
	numberOfGoodL2Responses = 0;
	numberOfEBOutstanding = 0;
	numberOfL2Outstanding = 0;
	numberOfL2ReReqsOutstanding = 0;
	numberOfEBReReqsOutstanding = 0;

	// EB requests
	m_l2ap_i = 100.0 / ebRequestFraction;
	maxIntervalLength = m_l2ap_i * maxIntervalFactor;
	if (m_l2ap_i > lowestValForExpRan)
	{
		modified_m_l2ap_i = truncAverage(m_l2ap_i, maxIntervalFactor);
		std::cout << "New m_l2ap_i " << modified_m_l2ap_i << " max interval length " << maxIntervalLength << std::endl;
		nextEbReqId = expran(modified_m_l2ap_i);
	}
	else
	{
		nextEbReqId = m_l2ap_i;
	}
	interval = nextEbReqId;
	intervalEBSum = nextEbReqId;
	nextEbReqIdInt = (int) (nextEbReqId + 0.5);
	// nextEbReqIdInt = m_totalInstances * nextEbReqIdInt + m_instance;  // must be a multiple of m_totalInstances with offset m_instance
	intervalEBSumInt = (double) nextEbReqIdInt;
	// std::cout << nextEbReqIdInt << " " << nextEbReqId << std::endl;
	for (unsigned int loop = 0; loop < c_ereqtypestate_size; loop++)
	{
		if (loop == nextEbReqIdInt)
		{
			m_ebInterval[loop] = 1;
			currentEbReqIdInt = nextEbReqIdInt;
			if (m_l2ap_i > lowestValForExpRan)
			{
				do
				{
					interval = expran(modified_m_l2ap_i);
				}
				while ((interval > maxIntervalLength) || (interval <= 1.0)); 
			}
			nextEbReqId = nextEbReqId + interval;
			intervalEBSum = intervalEBSum + interval;
			nEBInterval++;
			nextEbReqIdInt = (int) (nextEbReqId + 0.5);
			intervalEBSumInt = intervalEBSumInt + (double) (nextEbReqIdInt - currentEbReqIdInt);
			// std::cout << nextEbReqIdInt << " " << nextEbReqId << std::endl;
		}
		else
		{
			m_ebInterval[loop] = 0;
		}
	}

	// setting up pmin and pmax, print only once 
	{
		if (avROLsPerRoI < 0.0)
		{
			pmin[1] = 0.0;
			for (int loop=1; loop <= m_maxRolsInRoI;loop++)
			{
				pmax[loop] = pmin[loop] + m_l2puGroup[loop] / 100.0;
				pmin[loop+1] = pmax[loop];
				if (firstInitFlag)
				{
					std::cout << loop << " pmin " << pmin[loop] << " pmax " << pmax[loop] << std::endl;
				}
			}
		}
		else
		{
			float lowVal = (float) ((int) (avROLsPerRoI + 0.00001) );
			
			if (fabs(avROLsPerRoI-lowVal) < 0.00001)
			{
				// integer value of average number of ROLs per RoI
				int nAv = (int) lowVal;
				pmin[1] = 0.0;
				for (int loop=1; loop <= c_maxRols;loop++)
				{
					if (loop == nAv)
					{
						pmax[loop] = pmin[loop] + 1.0;
					}
					else
					{
						pmax[loop] = pmin[loop];
					}
					pmin[loop+1] = pmax[loop];
					if ( (pmin[loop] < 0.99999) && firstInitFlag)
					{
						std::cout << loop << " pmin " << pmin[loop] << " pmax " << pmax[loop] << std::endl;
					}
				}
			}
			else
			{
				// we need two values 
				int nLow = (int) lowVal;
				int nHigh = nLow + 1;
				float pLow = (float) nHigh - avROLsPerRoI;
				float pHigh = avROLsPerRoI - (float) nLow;
				pmin[1] = 0.0;
				for (int loop=1; loop <= c_maxRols;loop++)
				{
					if (loop == nLow)
					{
						pmax[loop] = pmin[loop] + pLow;
					}
					else
					{
						if (loop == nHigh)
						{
							pmax[loop] = pmin[loop] + pHigh;
						}
						else
						{
							pmax[loop] = pmin[loop];
						}
					}
					pmin[loop+1] = pmax[loop];
					if ( (pmin[loop] < 0.99999) && firstInitFlag)
					{
						std::cout << loop << " pmin " << pmin[loop] << " pmax " << pmax[loop] << std::endl;
					}
				}
			}
		}
	}
	
	if (m_l2req_interval > 0.0)
	{
		// L2 requests and number of ROLs
		std::cout << "Original m_l2req_interval " << m_l2req_interval << std::endl;
		maxIntervalLength = m_l2req_interval * maxIntervalFactor;
		if (m_l2req_interval > lowestValForExpRan)
		{
			modified_m_l2req_interval = truncAverage(m_l2req_interval, maxIntervalFactor);
			std::cout << "New m_l2req_interval " << modified_m_l2req_interval << " max interval length " << maxIntervalLength << std::endl;
			nextL2ReqId = expran(modified_m_l2req_interval);
		}
		else
		{
			if (m_l2req_interval <= 1.0)
			{
				m_l2req_interval = 1.00001; // somewhat larger than 1.0 to prevent problems due to rounding errors
				std::cout << "Set m_l2req_interval to " << m_l2req_interval << std::endl;
				l2RequestFraction = 100.0 * meanNumberOfRolsPerL2Request / ( (float) m_numberOfChannels);
				l2ReqfracEqualsOneStopFlag = true;
			}
			else
			{
				std::cout << "Kept m_l2req_interval at " << m_l2req_interval << std::endl;
			}
			nextL2ReqId = m_l2req_interval;
		}
		interval = nextL2ReqId;
		intervalL2Sum = nextL2ReqId;
		nextL2ReqIdInt = (int) (nextL2ReqId + 0.5);
		intervalL2SumInt = (double) nextL2ReqIdInt;
		// std::cout << nextL2ReqIdInt << " " << nextL2ReqId << std::endl;
		for(unsigned int loop = 0; loop < c_ereqtypestate_size; loop++)
		{
			int nrols = 0;
			if (loop == nextL2ReqIdInt)
			{
				// determine number of ROLs
				while (nrols == 0)
				{
					double p = ran(&ranseed);
					for (int i=1;i<= c_maxRols ;i++)
					{
						if ( (p >= pmin[i]) && p < pmax[i])
						{
							nrols = i;
							break;
						}
					}
				}
				m_l2Interval[loop] = nrols;
				currentL2ReqIdInt = nextL2ReqIdInt;
				if (m_l2req_interval > lowestValForExpRan)
				{
					do
					{
						interval = expran(modified_m_l2req_interval);
					}
					while ((interval > maxIntervalLength) || (interval <= 1.0));
				}
				nextL2ReqId = nextL2ReqId + interval;
				intervalL2Sum = intervalL2Sum + interval;
				nL2Interval++;
				nextL2ReqIdInt = (int) (nextL2ReqId + 0.5);
				// nextL2ReqIdInt = m_totalInstances * nextL2ReqIdInt + m_instance;  // must be a multiple of m_totalInstances with offset m_instance
				intervalL2SumInt = intervalL2SumInt + (double) (nextL2ReqIdInt - currentL2ReqIdInt);
				 // std::cout << nextL2ReqIdInt << " " << nextL2ReqId << " nrols " << nrols << std::endl;
			}
			else
			{
				m_l2Interval[loop] = 0;
			}
		}
	}
	else
	{
		// no L2 requests
		for(unsigned int loop = 0; loop < c_ereqtypestate_size; loop++)
		{
			m_l2Interval[loop] = 0;
		}
	}
	std::cout << "EB average " << intervalEBSum / nEBInterval << std::endl;
	std::cout << "EB average int " << intervalEBSumInt / nEBInterval << std::endl;
	std::cout << "L2 average " << intervalL2Sum / nL2Interval << std::endl;
	std::cout << "L2 average int " << intervalL2SumInt / nL2Interval << std::endl;

	for (i=0;i<c_ereqtypestate_size;i++)
	{
		if (m_ebInterval[i] > 0)
		{
			if (m_l2Interval[i] > 0)
			{
				reqTypeState[i] = L2_REQ_WITH_ASSOCIATED_EB_REQ_TO_BE_SENT;
			}
			else
			{
				reqTypeState[i] = EB_REQ_TO_BE_SENT;
			}
		}
		else
		{
			if (m_l2Interval[i] > 0)
			{
				reqTypeState[i] = L2_REQ_TO_BE_SENT;
			}
			else
			{
				reqTypeState[i] = NO_REQ;
			}
		}
	}

    m_metProbability = percentOfEtmiss  / 100.0;
    std::cout << "MEtProb " << m_metProbability << " " << percentOfEtmiss << std::endl;
}


/***************************************************************************/
void ROSTestDC::l2Request(const unsigned int level1Id,
                          const std::vector<unsigned int> * rols,
                          const unsigned int /*destination*/,
                          const unsigned int transactionId)
/***************************************************************************/
{
   eReqTypeState *state;
   float p = ran(&ranseed);
   dcmessages::DataRequest request(level1Id);

   // std::cout << "L2 request for " << level1Id << " pEtmiss " << p << " m_metProbability " << m_metProbability << std::endl;
   m_l2RequestsGenerated++;
#ifdef GORDOETMISS
     if (m_metInterval!=0 && rand()<m_metInterval) {
#else
     if (p < m_metProbability) {
#endif
         m_metRequestsGenerated++;

      unsigned int rolId=*(rols->begin());

      //      std::cout << "level1Id " << level1Id << std::hex << ", raw rolId="<<rolId;
      rolId=(rolId&0x0000ffff)|(m_metID<<16);
      //std::cout << ",  cooked rolId="<<rolId << std::dec << std::endl;
      request.push_back(rolId);
   }
   else {
      for (std::vector<unsigned int>::const_iterator it=rols->begin();it!=rols->end();it++) {
         request.push_back(*it);
      }
   }
   request.send(ROS_REQ, m_remotePort, transactionId, m_cookie);
	numberOfL2Outstanding++;
	unsigned int level1IdDivInstances = level1Id / m_totalInstances;
	state = &reqTypeState[level1IdDivInstances & c_ereqtypestate_mask];
	switch (*state)
	{
		case L2_REQ_TO_BE_SENT:
			*state = L2_REQ_HAS_BEEN_SENT;
			break;
		case L2_REQ_WITH_ASSOCIATED_EB_REQ_TO_BE_SENT:
			*state = L2_REQ_HAS_BEEN_SENT_TO_BE_FOLLOWED_BY_EB_REQ;
			break;
		default:
			std::cout << "Wrong state " << ROS::stateText[*state] << " in ROSTestDC::l2Request for id " << level1Id << std::endl;
			break;
	}
	
}

/***************************************************************************/
void ROSTestDC::ebRequest(const unsigned int level1Id,
                          const unsigned int /*destination*/)
/***************************************************************************/
{
	  eReqTypeState *state;
	dcmessages::DataRequest request(level1Id);
        request.send(SFI_DATAREQUEST, m_remotePort, 0, m_cookie);
	
	// std::cout << "EB request for " << level1Id << std::endl;
	numberOfEBOutstanding++;
	unsigned int level1IdDivInstances = level1Id / m_totalInstances;
	state = &reqTypeState[level1IdDivInstances & c_ereqtypestate_mask];
	switch (*state)
	{
		case EB_REQ_TO_BE_SENT:
			*state = EB_REQ_HAS_BEEN_SENT;
			break;
		case NO_REQ:
		case L2_REQ_TO_BE_SENT:
		case L2_REQ_HAS_BEEN_SENT:
		case L2_REQ_HAS_BEEN_RESENT:
		case EB_REQ_HAS_BEEN_SENT:
		case EB_REQ_HAS_BEEN_RESENT:
			std::cout << "Wrong state " << ROS::stateText[*state] << " in ROSTestDC::ebRequest for id " << level1Id << std::endl;
			break;
		default:
			// do nothing
			break;
	}
	
}

/***************************************************************************/
void ROSTestDC::releaseRequest(const std::vector<unsigned int>* level1Ids,
			       const unsigned int oldestL1id)
/***************************************************************************/
{
   std::vector<unsigned int>* l1Ids = const_cast<std::vector<unsigned int>* >(level1Ids);
   DEBUG_TEXT(DFDB_ROSIO, 10, "releaseRequest: oldestL1id =  " << oldestL1id);
   dcmessages::DFM_Clear_Msg message(*l1Ids, oldestL1id, m_clearSequence);
#ifdef TDAQ191
   message.send(m_remotePort);
#else
	message.send(m_rosPort);
#endif
   m_clearSequence++;
	// std::cout << "releaseRequest: oldestL1id =   " << oldestL1id << " vector starts with: " << l1Ids->front() << " vector stops with: " << l1Ids->back() <<std::endl;
}

/*******************************************/
void ROSTestDC::ebQueuePush(unsigned int lvl1id) {
   /*******************************************/
   m_ebQueue.push(lvl1id);
}

/*******************************************/
unsigned int ROSTestDC::ebQueuePop(void) {
   /*******************************************/
   unsigned int ret = m_ebQueue.front();
   m_ebQueue.pop();
   return ret;
}

/*******************************************/
int ROSTestDC::ebQueueSize(void) {
   /*******************************************/
   return m_ebQueue.size();
}

/*******************************************/
bool ROSTestDC::ebQueueEmpty(void) {
   /*******************************************/
   return  m_ebQueue.empty();
}


/*******************************************/
void ROSTestDC::releaseQueuePush(unsigned int lvl1id)
{
/*******************************************/
   m_releaseQueue.push(lvl1id);
	lvl1id = lvl1id / m_totalInstances;
	releaseCount[lvl1id & c_ereqtypestate_mask]++;
	// std::cout << "** Release id " << lvl1id*m_totalInstances+m_instance << " count " << releaseCount[lvl1id & c_ereqtypestate_mask] << " " << releaseCount[0] << std::endl;

}

/*******************************************/
unsigned int ROSTestDC::releaseQueuePop(void) {
   /*******************************************/
   int ret = m_releaseQueue.front();
   m_releaseQueue.pop();
   return ret;
}

/*******************************************/
int ROSTestDC::releaseQueueSize(void) {
   /*******************************************/
   return m_releaseQueue.size();
}

/*******************************************/
bool ROSTestDC::releaseQueueEmpty(void) {
   /*******************************************/
   return  m_releaseQueue.empty();
}

/*******************************************/
bool ROSTestDC::condition(void) {
/*******************************************/
	if ( (numberOfL2ReReqsOutstanding + numberOfEBReReqsOutstanding) > 0)
	{
		/* std::cout << "Condition 1 FALSE, numberOfEBReReqsOutstanding " << numberOfEBReReqsOutstanding << \
		 " numberOfL2ReReqsOutstanding " << numberOfL2ReReqsOutstanding << " numberOfEBOutstanding " \
		<< numberOfEBOutstanding << " numberOfL2Outstanding " << numberOfL2Outstanding << std::endl; */
		return false;
	}
	if ( (numberOfEBOutstanding + numberOfL2Outstanding) >= (unsigned int) m_numberOfOutstanding)
	{
		/* std::cout << "Condition 2 FALSE, numberOfEBReReqsOutstanding " << numberOfEBReReqsOutstanding << \
		   " numberOfL2ReReqsOutstanding " << numberOfL2ReReqsOutstanding << " numberOfEBOutstanding " \
		   << numberOfEBOutstanding << " numberOfL2Outstanding " << numberOfL2Outstanding << std::endl; */
		return false;
	}
	if (stopFlag)
	{
		return false;
	}
	return true;
}

/**************************************************************/
int ROSTestDC::generate(int nRequests, int mode)
/**************************************************************/
{
  //Definition of mode:
  //0 = continue with old values (e.g. L1ID)
  //1 = re-start from zero
  
  TS_RECORD(TS_H1, 1000);	// request handle start
  DEBUG_TEXT(DFDB_ROSTG, 15, "TriggerGenerator: generate:  called");
 
  if(m_disabled) {
    DEBUG_TEXT(DFDB_ROSTG, 20, "TriggerGenerator: generate:  m_disabled -> nothing to do -> return(0)");
    return(0);
  }
 
  if (mode==1) {
    DEBUG_TEXT(DFDB_ROSTG, 20, "TriggerGenerator: generate:  mode = 1: Restarting from L1ID = 0");
    m_next = 0;
    m_level1Id = m_instance;
    m_transactionId = 1;
  }
    
  bool forever = false;
  if (nRequests == 0) forever = true;
  int requestsGenerated = 0;
  // std::cout << "generate " << nRequests << std::endl;
  while ((forever || (requestsGenerated < nRequests)) && condition()) { 

 	 requestsGenerated += generateRequestsForOneL1ID(m_level1Id);

    // make sure we can run w/ multiple instances of ROSTestDC and *not* use the same events.
    m_level1Id+=m_totalInstances;
	 // m_level1Id = m_level1Id & 0xffffff;
	 if (m_level1Id < m_totalInstances)
	 {
		// rollover, need to reset start id
		// m_level1Id = m_instance;
		m_rollovers++;
		std::cout << "Number of rollovers = " << m_rollovers << std::endl;
	 }
	 m_numberOfL1ids = (m_level1Id-m_instance)/m_totalInstances;
    // m_numberOfL1ids = (m_rollovers * 0xffffff + m_level1Id - m_instance) / m_totalInstances ;
    
//    TS_RECORD(TS_H1, 1490);  
  }
 // std::cout << " generated 1 - " << requestsGenerated  << std::endl;
  pthread_testcancel(); //MJ: how would the loop be terminated if forever==true?
  DEBUG_TEXT(DFDB_ROSTG, 20, "TriggerGenerator: generate:  # data requests generated  = " << requestsGenerated);
  // std::cout << " generated  2 - " << requestsGenerated  << std::endl;
  return(requestsGenerated);
}


/****************************************************************************************/
int ROSTestDC::generateL1idBunch(unsigned int firstL1id, unsigned int lastL1id)
/****************************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "TriggerGenerator: generateL1idBunch:  entered");
 
  int requestsGenerated = 0;

  for (unsigned l1id = firstL1id; l1id <=lastL1id; l1id++) {
    requestsGenerated += generateRequestsForOneL1ID(l1id);
    m_numberOfL1ids++;
  }

  pthread_testcancel(); //MJ: how would the loop be terminated if forever==true?
  DEBUG_TEXT(DFDB_ROSTG, 20, "generateL1idBunch:  # data requests generated  = " << requestsGenerated);
  return(requestsGenerated);
}

/******************************************************/
void ROSTestDC::set_level1Id(int) {
/******************************************************/
  m_level1Id = m_instance;
}


/*******************************************************************/
int ROSTestDC::generateRequestsForOneL1ID(unsigned int l1id) 
/*******************************************************************/
{
  int requestsGenerated = 0;

  TS_RECORD(TS_H1, 1100);

  int rolsInThisRoI = noRolsInThisRoI(l1id);
  DEBUG_TEXT(DFDB_ROSTG, 15, "generateRequestForOneL1ID: L1id = 0x" << std::hex << l1id
                             << " # Rols in ROI = " << std::dec << rolsInThisRoI);
    
//    TS_RECORD(TS_H1,1110);
  bool L1id_L2ed = false;
  if (rolsInThisRoI > 0) {
    m_numberOfL2req++;
	generateL2Request(l1id);
    requestsGenerated++;
    L1id_L2ed=true;

    if (m_testGarbageCollection) {
      m_l1IdsInProcess.insert(l1id);	// L1ID "busy" with Level2
      DEBUG_TEXT(DFDB_ROSTG, 20, "TriggerGenerator: generate: inserted L1id = " <<
                                 l1id << " in busy set " << " of size " << m_l1IdsInProcess.size());
   
      // update  set size histogram
      int setIPsize = m_l1IdsInProcess.size();
      if (setIPsize < (c_maxINPROCESSbins -1)) {
        m_setINPROCESShist[setIPsize]++;
      }
      else {				// overflow
        m_setINPROCESShist[c_maxINPROCESSbins -1]++;
      }
    }
  }

//    TS_RECORD(TS_H1, 1190);
  // we are done with this L1ID, if not sent to ROS for q request we put it into a Q.

  if (!L1id_L2ed) {
    DEBUG_TEXT(DFDB_ROSTG, 20, "generateRequestForOneL1ID:  if no l2Request, l1id = " <<
                               std::hex << l1id << " on EB Q of size " << std::dec << ebQueueSize());
    ebQueuePush(l1id);
  }

  //EB Requests ---------------------------------------

  // handle ONE L1id from the EB Q
  // the L1ids "without" L2 request will push ONE request onto the EB Q (code above)
  // and remove ONE immediately afterwards: NOT necessarily the same ONE if Q is non-empty!!
  // If L1ids are pushed onto the Q from getLvlId(), the Q size may increase.
  // If (several) L2 requests are generated, the Q will be emptied since NO request is put
  // on the Q in the trigger generator (code above); still, if the Q is non-empty, we go
  // through the code below and remove one L1id

  if ( ! ebQueueEmpty() ) 
  {
//      TS_RECORD(TS_H1, 1200);

    // update EB Q size histogram
    int qEBsize = ebQueueSize();
    if (qEBsize < (c_maxEBbins -1)) {
      m_qEBhist[qEBsize]++;
    }
    else {				// overflow
      m_qEBhist[c_maxEBbins -1]++;
    }

    unsigned int level1IdEB = ebQueuePop(); // get L1 id to decide on its fate.

    DEBUG_TEXT(DFDB_ROSTG, 20, "generateRequestForOneL1ID:  EB request, popped l1id = " <<
                               std::hex << level1IdEB << " from EB Q of size " << std::dec << ebQueueSize());

    bool L1id_EBed = false;
	 unsigned int level1IdEBDivInstances = level1IdEB / m_totalInstances;
    if (m_ebInterval[level1IdEBDivInstances & c_ereqtypestate_mask] == 1) 
    {   
//	TS_RECORD(TS_H1, 1210);
      m_numberOfL2A++;
      generateEBRequest(level1IdEB);
//	TS_RECORD(TS_H1, 1290);
      L1id_EBed=true;
      requestsGenerated++;

      if (m_testGarbageCollection) {
        m_l1IdsInProcess.insert(level1IdEB);	// L1ID "busy" with EB
        DEBUG_TEXT(DFDB_ROSTG, 20, "TriggerGenerator: generate: inserted L1id = " <<
                                   level1IdEB << " in busy set " << " of size " << m_l1IdsInProcess.size());
   
        // update  set size histogram
        int setIPsize = m_l1IdsInProcess.size();
        if (setIPsize < (c_maxINPROCESSbins -1)) {
          m_setINPROCESShist[setIPsize]++;
        }
        else {				// overflow
          m_setINPROCESShist[c_maxINPROCESSbins -1]++;
        }
      }

    }

    if (!m_syncDeletesWithEBRequests) {
      if (!L1id_EBed) {
        DEBUG_TEXT(DFDB_ROSTG, 20, "generateRequestForOneL1ID: if no EB request, l1id = "
                                   << std::hex << level1IdEB << " on CLEAR Q of size "
                                   << std::dec << releaseQueueSize());
        releaseQueuePush(level1IdEB);
      }
    }
  } 
  // end-of-EB-queue -------------------
  //    

  processReleaseQueue(false) ;		// empty the release Q

  return requestsGenerated;

}

/******************************************************/
void ROSTestDC::processReleaseQueue(bool flush) 
/******************************************************/
{
  // the CLEAR Queue may grow quite large: in EB mode, EB requests will be generated while the 
  // release Q is empty. The request Q may be filled: <~ 100 requests
  // The request handlers then start processing the requests and pushing CLEAR L1ids on the
  // release sync Q. The trigger generator (thread) may only resume when the watermark is hit:
  // 10 elements left. We may expect the Q to grow to ~ 90 elements.
  // careful to keep request queue to 100 as ALWAYS ..

  if (m_deleteGrouping && !releaseQueueEmpty() ) {	// possibly several delete groups
    while  ( (m_l1Ids.size() < m_deleteGrouping) && ( !releaseQueueEmpty() ) )	// up to one delete group
    {

      // update CLEAR Q size histogram
      int qCLEARsize = releaseQueueSize();
      if (qCLEARsize < (c_maxCLEARbins -1)) {
        m_qCLEARhist[qCLEARsize]++;
      }
      else {				// overflow
        m_qCLEARhist[c_maxCLEARbins -1]++;
      }

      int level1Id = releaseQueuePop();

      if (m_testGarbageCollection) {
        m_l1IdsInProcess.erase(level1Id);
        DEBUG_TEXT(DFDB_ROSTG, 20, "processReleaseQueue: erased L1ID " << level1Id <<
                                   " from L1ID busy set of size " << m_l1IdsInProcess.size());
      }

      m_l1Ids.push_back(level1Id);
      DEBUG_TEXT(DFDB_ROSTG, 20, "processReleaseQueue: The L1ID to be deleted is:"
                                  << level1Id << " queue size = " <<  m_l1Ids.size() );
    } 
 
    if ( m_l1Ids.size() == m_deleteGrouping ) {

      m_numberOfDeleteGroup++;		// # delete groups: NOT # release requests

//        TS_RECORD(TS_H1, 1300);
      if (m_testGarbageCollection) {
        std::set<unsigned int>::iterator it_minL1id = min_element(m_l1IdsInProcess.begin(), m_l1IdsInProcess.end());
        m_oldestL1id = *it_minL1id;
        DEBUG_TEXT(DFDB_ROSTG, 10, "processReleaseQueue: m_oldestL1id = " << std::dec << m_oldestL1id);
      }
      else {
        m_oldestL1id = 0;
      }

      generateReleaseRequest(m_oldestL1id);
      //requestsGenerated += m_deleteGrouping;   //You need this line if you want to run with deletes only

//        TS_RECORD(TS_H1, 1390);

      m_l1Ids.clear();		// forget the accumulated ones
    }
  }

  // queue is now empty but L1ID vector may not be.
  if (flush) {
    DEBUG_TEXT(DFDB_ROSTG, 20, "processReleaseQueue: flush L1ID vector of size " << std::dec << m_l1Ids.size());
    if (m_l1Ids.size() > 0) {
      generateReleaseRequest(m_oldestL1id);
      m_l1Ids.clear();		// forget the accumulated ones
      m_numberOfDeleteGroup++;
//        TS_RECORD(TS_H1, 1390);
      //requestsGenerated += m_deleteGrouping;   //You need this line if you want to run with deletes only
    }
  }
}

/***************************************************************************/
bool ROSTestDC::getLvl1id(MessagePassing::Buffer* reply) 
/***************************************************************************/
{
   using namespace MessagePassing;
   using namespace MessageInput;
   using namespace eformat;
	
	eReqTypeState *state;
	int numberOfWordsNotYetLookedAt;
	bool resendRequestFlag;
    int sourceIds[c_maxRolsPerL2PU];
    int subdetId;
    int numberOfSourceIds = 0;

   MessageHeader*  myhdr = new MessageInput::MessageHeader(reply);
   if (!myhdr->valid()) {
      std::cerr << "=============== Generic Header INVALID ================ \n";
      return (0);
   }
  
   MessagePassing::Buffer::iterator itbuf = reply->begin();
   itbuf += MessageInput::MessageHeader::SIZE;
   uint32_t receivedL1idDivInstancesMasked, receivedL1id;

   uint32_t startOfHeaderMarker;
   uint32_t totalSize;
   uint32_t genericHeaderSize;
	uint32_t statusOfTheROBIN;
	uint32_t formatROBFragmentVersion;
	uint32_t sourceId;
	uint32_t numberOfStatusElements;
	unsigned int theId, theIdDivInstancesMasked = 0;
   
  itbuf >> startOfHeaderMarker;	

   if ( startOfHeaderMarker != eformat::ROB )
	{ 
      std::cout << "NOT a ROB fragment " << std::hex << startOfHeaderMarker << "!="<< eformat::ROB <<std::dec << std::endl; 
      delete myhdr;
      return (0);
   }

	// check on completeness of event, otherwise resubmit request
	numberOfWordsNotYetLookedAt = ( (reply->size() - MessageInput::MessageHeader::SIZE) / 4);
	resendRequestFlag = false;
	while (true)
	{
		unsigned int rolNumber;

		itbuf >> totalSize >> genericHeaderSize;
		itbuf >> formatROBFragmentVersion >> sourceId >> numberOfStatusElements;
		rolNumber = sourceIdMap[sourceId];
		if (rolNumber >= (unsigned int) c_maxRolsPerL2PU)
		{
			std::cerr << "Error, source id " << sourceId << " mapped to invalid number: " << rolNumber << " --> Exit" << std::endl;
			exit(0);
		}

		if (numberOfSourceIds >= c_maxRolsPerL2PU)
		{
			std::cerr << "numberOfSourceIds too large: " << numberOfSourceIds << " --> exit" << std::endl;
			exit(0);
		}
		sourceIds[numberOfSourceIds] = sourceId;
        subdetId = (sourceId & 0xff0000) >> 16;
        if (subdetId == 0x7d || subdetId == 0x7e)
        {
            numberOfEtmissFragsReceived++;
        }
		numberOfSourceIds++;
		if ( totalSize > maxFragSize)
		{
			maxFragSize = totalSize;
		}

		// itbuf += 16; // 4 * 4
		itbuf >> statusOfTheROBIN;
		itbuf += 4 * (genericHeaderSize - 2);
		itbuf >> receivedL1id;
		// std::cout << "At beginning of loop in getLvl1id for id " << receivedL1id << " and number of words " << numberOfWordsNotYetLookedAt << std::endl;
		if (statusOfTheROBIN == 0)
		{
			if (myhdr->xid() == 0)
			{
#ifdef GORDON
				 m_completeEBFragmentsPerROB[sourceId]++;
#else
				 completeEBFragmentsPerROB[rolNumber]++;
#endif
			}
			else
			{
#ifdef GORDON
				 m_completeL2FragmentsPerROB[sourceId]++;
#else
				 completeL2FragmentsPerROB[rolNumber]++;
#endif
			}
		}
		else
		{
			if (myhdr->xid() == 0)
			{
#ifdef GORDON
				 m_inCompleteEBFragmentsPerROB[sourceId]++;
#else
				 inCompleteEBFragmentsPerROB[rolNumber]++;
#endif
                 std::cout << "EB: resend required for id " << receivedL1id << " sourceId " << sourceId << std::endl;
			}
			else
			{
#ifdef GORDON
				 m_inCompleteL2FragmentsPerROB[sourceId]++;
#else
				 inCompleteL2FragmentsPerROB[rolNumber]++;
#endif
                 std::cout << "L2: resend required for id " << receivedL1id << " sourceId " << sourceId << std::endl;
			}
			resendRequestFlag = true;
			/*  std::cout << "resend required for id " << receivedL1id << " sourceId " << sourceId << std::endl;
			std::cout << "status " << std::hex << statusOfTheROBIN << std::dec << std::endl;
			std::cout << "totalSize " << totalSize << std::endl;
			std::cout << "receivedL1id " << receivedL1id << std::endl;
			std::cout << "numberOfWordsNotYetLookedAt " << numberOfWordsNotYetLookedAt << std::endl; */
			// break;
		}
		// OK, skip to next fragment, assuming that ROD header size is always 9 words
		numberOfWordsNotYetLookedAt -= totalSize;
		if (numberOfWordsNotYetLookedAt <= 0)
		{
			break;
		}
		itbuf += 4 * (totalSize - genericHeaderSize - 5);
	}
	// std::cout << "End of loop in getLvl1id for id " << receivedL1id << " and number of words " << numberOfWordsNotYetLookedAt << std::endl;

	DEBUG_TEXT(DFDB_ROSIO, 10, "getLvl1id: l1id received: " << receivedL1id << "  Tid = " << myhdr->xid());
   // put the received L1ID into the appropriate Q.
	receivedL1idDivInstancesMasked = receivedL1id / m_totalInstances;
	receivedL1idDivInstancesMasked = receivedL1idDivInstancesMasked & c_ereqtypestate_mask;
   state = &reqTypeState[receivedL1idDivInstancesMasked];
	if (myhdr->xid() == 0)
	{ 
      // this is an answer to an EB request 
      DEBUG_TEXT(DFDB_ROSIO, 10, "getLvl1id: put L1id " << receivedL1id <<
                 " on the Delete Queue of size " << releaseQueueSize());
		// std::cout << "--> Received EB response for id " << receivedL1id << " resendRequestFlag " << resendRequestFlag << std::endl;
		if (resendRequestFlag )
		{
			switch (*state)
			{
				case L2_REQ_RECEIVED_ASSOCIATED_EB_REQ_HAS_BEEN_RESENT:
					numberOfEBReReqsOutstanding--;
					break;
				case L2_REQ_RECEIVED_ASSOCIATED_EB_REQ_HAS_BEEN_SENT:
					*state = L2_REQ_RECEIVED_ASSOCIATED_EB_REQ_HAS_BEEN_RESENT;
					break;
				case EB_REQ_HAS_BEEN_RESENT:
					numberOfEBReReqsOutstanding--;
					break;
				case EB_REQ_HAS_BEEN_SENT:
					*state = EB_REQ_HAS_BEEN_RESENT;
					break;
				default:
					std::cout << "Wrong state " << ROS::stateText[*state] << " in ROSTestDC::getLvl1id for resending EB request, id = " << receivedL1id << std::endl;
					break;
			}
			// first try to transfer releases if any can be sent 
			processReleaseQueue(false);
			// std::cout << "--> generate additional EB request for id = " << receivedL1id << " reRequestCount = " << reRequestCount[receivedL1idDivInstancesMasked] << std::endl;
			{
				if (reRequestCount[receivedL1idDivInstancesMasked] > reRequestLimit)
				{
					// doesn't make sense to rerequest this again -> exit)
                    // std::cout << "EB: reRequestCount for " <<  receivedL1id << " > " << reRequestLimit << ", lastIdToBeCleared = " << lastIdToBeCleared << std::endl;
					checkHistory(receivedL1id);
					// give up for this one
					reRequestCount[receivedL1idDivInstancesMasked] = 0;
					switch (*state)
					{
						case L2_REQ_RECEIVED_ASSOCIATED_EB_REQ_HAS_BEEN_RESENT:
							*state = L2_REQ_WITH_ASSOCIATED_EB_REQ_TO_BE_SENT;
							break;
						case EB_REQ_HAS_BEEN_RESENT:
							*state = EB_REQ_TO_BE_SENT;
							break;
						default:
							std::cout << "Erroneous state --> exit " << std::endl;
							exit(0);
							break;
					}
				}
				else
				{
					reRequestCount[receivedL1idDivInstancesMasked]++;
					waitTime.tv_sec = 0;
					waitTime.tv_nsec = reRequestCount[receivedL1idDivInstancesMasked] * 2000000; // wait per rerequest 2 ms  
					nanosleep(&waitTime, NULL);
					dcmessages::DataRequest request(receivedL1id);
					request.send(SFI_DATAREQUEST, m_remotePort, 0);
					numberOfEBReReqsOutstanding++;
				}
			}
		}
		else
		{
			switch (*state)
			{
				case L2_REQ_RECEIVED_ASSOCIATED_EB_REQ_HAS_BEEN_RESENT:
					numberOfEBReReqsOutstanding--;
					// fall through
				case L2_REQ_RECEIVED_ASSOCIATED_EB_REQ_HAS_BEEN_SENT:
					*state = L2_REQ_WITH_ASSOCIATED_EB_REQ_TO_BE_SENT;
					releaseQueuePush(receivedL1id); // clear this id
                    // std::cout << "EB req with associated L2 received with id " << receivedL1id << " and cleared" << std::endl;
					numberOfEBOutstanding--;
					reRequestCount[receivedL1idDivInstancesMasked] = 0;
					break;
				case EB_REQ_HAS_BEEN_RESENT:
					// fall through
					numberOfEBReReqsOutstanding--;
				case EB_REQ_HAS_BEEN_SENT:
					*state = EB_REQ_TO_BE_SENT;
					reRequestCount[receivedL1idDivInstancesMasked] = 0;
					if (!clearThisIdOnlyFlag[receivedL1idDivInstancesMasked])
					{
						theId = lastIdToBeCleared;
						theIdDivInstancesMasked = theId / m_totalInstances;
						theIdDivInstancesMasked = theIdDivInstancesMasked & c_ereqtypestate_mask;
						while (theId != receivedL1id)
						{
							if (reqTypeState[theIdDivInstancesMasked] == NO_REQ)
							{
								releaseQueuePush(theId);
								// std::cout << "EB req received with id " << thisL1id << " clear id " << theId << std::endl;
							}
							else
							{
								// std::cout << "EB req received with id " << receivedL1id << " did not clear id " << theId << " as state is " << reqTypeState[receivedL1idDivInstancesMasked] << std::endl;
								clearThisIdOnlyFlag[theIdDivInstancesMasked] = true;
							}
							theId = theId + m_totalInstances;
							theIdDivInstancesMasked = theId / m_totalInstances;
							theIdDivInstancesMasked = theIdDivInstancesMasked & c_ereqtypestate_mask;
						}
						lastIdToBeCleared = theId + m_totalInstances;
					}
					releaseQueuePush(receivedL1id); // this id can always be cleared
					clearThisIdOnlyFlag[receivedL1idDivInstancesMasked] = false;
					// std::cout << "EB req received with id " << receivedL1idDivInstancesMasked << " clear id " << receivedL1id << std::endl;
					numberOfEBOutstanding--;
					break;
				default:
					std::cout << "Wrong state " << ROS::stateText[*state] << " in ROSTestDC::getLvl1id for receiving EB response, id = " << receivedL1id << std::endl;
					break;
			}
			processReleaseQueue(false);
		}
	}
   else
	{
      // this is an answer to an L2 request 
        // std::cout << "--> Received L2 response for id " << receivedL1id << std::endl;
		if (resendRequestFlag)
		{
			switch (*state)
			{
				case L2_REQ_HAS_BEEN_RESENT_TO_BE_FOLLOWED_BY_EB_REQ:
					numberOfL2ReReqsOutstanding--;
					break;
				case L2_REQ_HAS_BEEN_SENT_TO_BE_FOLLOWED_BY_EB_REQ:
					*state = L2_REQ_HAS_BEEN_RESENT_TO_BE_FOLLOWED_BY_EB_REQ;
					break;
				case L2_REQ_HAS_BEEN_RESENT:
					numberOfL2ReReqsOutstanding--;
					break;
				case L2_REQ_HAS_BEEN_SENT:
					*state = L2_REQ_HAS_BEEN_RESENT;
					break;
				default:
					std::cout << "Wrong state " << ROS::stateText[*state] << " in ROSTestDC::getLvl1id for resending L2 request, id = " << receivedL1id << std::endl;
					break;
			}
			// also L2 requests are  resubmitted
			{
                std::cout << "--> generate additional L2 request for id = " << receivedL1id << " reRequestCount = " << reRequestCount[receivedL1idDivInstancesMasked] << std::endl;
				if (reRequestCount[receivedL1idDivInstancesMasked] > reRequestLimit)
				{
					// doesn't make sense to rerequest this again -> exit)
					std::cout << "L2: reRequestCount for " <<  receivedL1id << " > " << reRequestLimit << ", lastIdToBeCleared = " << lastIdToBeCleared << std::endl;
					checkHistory(receivedL1id);
					// give up for this one
					reRequestCount[receivedL1idDivInstancesMasked] = 0;
					switch (*state)
					{
						case L2_REQ_HAS_BEEN_RESENT_TO_BE_FOLLOWED_BY_EB_REQ:
							*state = L2_REQ_WITH_ASSOCIATED_EB_REQ_TO_BE_SENT;
							break;
						case L2_REQ_HAS_BEEN_RESENT:
							*state = L2_REQ_TO_BE_SENT;
							break;
						default:
							std::cout << "Erroneous state --> exit " << std::endl;
							exit(0);
							break;
					}
				}
				else
				{
					dcmessages::DataRequest request(receivedL1id);

					reRequestCount[receivedL1idDivInstancesMasked]++;
					waitTime.tv_sec = 0;
					waitTime.tv_nsec = reRequestCount[receivedL1idDivInstancesMasked] * 2000000; // wait per rerequest 2 ms  
					nanosleep(&waitTime, NULL);
					for (int i=0;i<numberOfSourceIds;i++)
					{
							request.push_back(sourceIds[i]);
							// std::cout << "L2 response not OK, id = " << thisL1id << " source " << sourceIds[i] << std::endl;
					}
					m_transactionId++;
					request.send(ROS_REQ, m_remotePort, m_transactionId);
					numberOfL2ReReqsOutstanding++;
				}
			}
		}
		else
		{
			switch (*state)
			{
				case L2_REQ_HAS_BEEN_RESENT_TO_BE_FOLLOWED_BY_EB_REQ:
					numberOfL2ReReqsOutstanding--;
					// fall through
				case L2_REQ_HAS_BEEN_SENT_TO_BE_FOLLOWED_BY_EB_REQ:
					*state = L2_REQ_RECEIVED_ASSOCIATED_EB_REQ_TO_BE_SENT;
					reRequestCount[receivedL1idDivInstancesMasked] = 0;
					numberOfL2Outstanding--;
					// std::cout << "--> generate new EB request after receiving L2 response for id = " << thisL1id << std::endl;
					{
						dcmessages::DataRequest request(receivedL1id);
						request.send(SFI_DATAREQUEST, m_remotePort, 0);
						numberOfEBOutstanding++;
						*state = L2_REQ_RECEIVED_ASSOCIATED_EB_REQ_HAS_BEEN_SENT;
						m_numberOfL2A++;
					}
					break;
				case L2_REQ_HAS_BEEN_RESENT:
					numberOfL2ReReqsOutstanding--;
					// fall through
				case L2_REQ_HAS_BEEN_SENT:
					*state = L2_REQ_TO_BE_SENT;
					if (!clearThisIdOnlyFlag[receivedL1idDivInstancesMasked])
					{
						theId = lastIdToBeCleared;
						theIdDivInstancesMasked = theId / m_totalInstances;
						theIdDivInstancesMasked = theIdDivInstancesMasked & c_ereqtypestate_mask;
						while (theId != receivedL1id)
						{
							if (reqTypeState[theIdDivInstancesMasked] == NO_REQ)
							{
								releaseQueuePush(theId);
								// std::cout << "L2 req received with id " << thisL1id << " clear id " << theId << std::endl;
							}
							else
							{
								// std::cout << "L2 req received with id " << thisL1id << " did not clear id " << theId << " as state is " << reqTypeState[theIdDivInstancesMasked] << std::endl;
								clearThisIdOnlyFlag[theIdDivInstancesMasked] = true;
							}
							theId = theId + m_totalInstances;
							theIdDivInstancesMasked = theId / m_totalInstances;
							theIdDivInstancesMasked = theIdDivInstancesMasked & c_ereqtypestate_mask;
						}
						lastIdToBeCleared = theId + m_totalInstances;
					}
					releaseQueuePush(receivedL1id); // this id can always be cleared
					reRequestCount[receivedL1idDivInstancesMasked] = 0;
					clearThisIdOnlyFlag[receivedL1idDivInstancesMasked] = false;
					// std::cout << "L2 req received with id " << thisL1id << " clear id " << thisL1id << std::endl;
					numberOfL2Outstanding--;
					break;
				default:
					std::cout << "Wrong state " << ROS::stateText[*state] << " in ROSTestDC::getLvl1id for receiving EB response, id = " << receivedL1id << std::endl;
					break;
			}
			processReleaseQueue(false);
		}
   } 
   delete myhdr;
   return(true);
}

/***************************************************************************/
int ROSTestDC::noRolsInThisRoI(unsigned int l1Id)
/***************************************************************************/
{
  int rolsInThisRoI;
  unsigned int l1idDivInstances;
  
  l1idDivInstances = l1Id / m_totalInstances;
  rolsInThisRoI = m_l2Interval[l1idDivInstances & c_ereqtypestate_mask];

  if (l1Id != (lastid + m_totalInstances) )
  {
		if (l1Id >= m_totalInstances)
		{
			std::cout << "l1id " << l1Id << " ne lastid-" << m_totalInstances << " " << lastid << std::endl;
		}
  }
  lastid = l1Id;
  
  if (rolsInThisRoI > 0)
  {
	  int rol = (int) (m_alea[m_next] * m_numberOfChannels);
	  if (rol == m_numberOfChannels) rol = 0;
	  if (++m_next == c_aleasize) m_next = 0;
	  for(int loop2 = 0; loop2 < rolsInThisRoI; loop2++)
	  {
		 DEBUG_TEXT(DFDB_ROSTG, 20, "noRolsInThisRoI:  L2 request on (logical) rol = " << rol
											 << " rol ID = " << m_channelIds[loop2]);
		 m_rols.push_back(m_channelIds[rol]);  // the Channel ID
		 m_numberOfROI[rol]++;		        // count per ROL
		 m_totalNumberOfROI++;
		 rol++;
		 if (rol == m_numberOfChannels) rol = 0;
	  }
	}
  m_roihist[rolsInThisRoI]++;
  return rolsInThisRoI;
}


/***************************************************************************/
void ROSTestDC::printEvent(MessagePassing::Buffer* reply) 
/***************************************************************************/
{
   using namespace MessagePassing;
   using namespace MessageInput;
   using namespace eformat;
   MessageHeader*  myhdr = new MessageInput::MessageHeader(reply);
   uint32_t numberOfFragmentsInMessage;
   size_t sizeOfBuffer, numberOfWordsNotYetLookedAt;
   bool errorFlag =  false;
   uint32_t lastExtendedLVL1ID = 0;   

   MessagePassing::Buffer::iterator itbuf = reply->begin();
    // step over message header
   itbuf += MessageInput::MessageHeader::SIZE;

   sizeOfBuffer = reply->size();
   numberOfWordsNotYetLookedAt =  (reply->size() - MessageInput::MessageHeader::SIZE) / 4;
   std::cout << std::endl;
   std::cout << "Initially in buffer " << sizeOfBuffer << " bytes" << std::dec << std::endl;
   std::cout << "msg_typ " << std::hex << myhdr->type() << std::dec << std::endl;
   std::cout << "src_id " << std::hex << myhdr->source() << std::dec << std::endl;
   std::cout << "size " << myhdr->size() << std::dec << std::endl;
   std::cout << "timestamp " << std::hex << myhdr->timestamp() << std::dec << std::endl;
   std::cout << "transaction id " << std::hex << myhdr->xid() << std::dec << std::endl;
   std::cout << "cookie " << std::hex << myhdr->cookie() << std::dec << std::endl;
   
   // loop over all ROB fragments in Buffer
    numberOfFragmentsInMessage = 0;
   do
   {
	   int32_t totalSize, genericHeaderSize, sizeOfRODHeader, numberOfStatusElements;
	   uint32_t  startOfROBHeaderMarker, formatROBFragmentVersion, sourceId;
	   uint32_t  theWord, checkSumType, statusOfTheROBIN;
	   uint32_t  startofRODHeaderMarker, formatRODFragmentVersion, sourceIdROD, runNumber, extendedLVL1ID;
	   uint32_t  bunchCrossingId, firstLevelTriggerType, detectorEventType;
	   uint32_t  theCRCword, calculatedCRC;
	   uint32_t  lastWordPrecedingTrailer, numberOfRODStatusElements, numberOfDataElements, statusBlockPosition;
	   uint32_t  crc1, crc2;
	   
	   // get ROB header
	   itbuf >> startOfROBHeaderMarker >> totalSize >> genericHeaderSize >> formatROBFragmentVersion >> sourceId >> numberOfStatusElements;

	   if ( startOfROBHeaderMarker != eformat::ROB )
	   { 
		  std::cout << "NOT a ROB fragment " << std::hex << startOfROBHeaderMarker << " != "<< eformat::ROB <<std::dec << std::endl; 
		  break;
	   }
	   
	   std::cout << std::endl;
       std::cout << "startOfROBHeaderMarker 0x" << std::hex << startOfROBHeaderMarker << std::dec << std::endl;
	   std::cout << "totalSize " << totalSize << std::dec << std::endl;
	   std::cout << "genericHeaderSize " << genericHeaderSize << std::dec << std::endl;
       std::cout << "formatROBFragmentVersion 0x" << std::hex << formatROBFragmentVersion << std::dec << std::endl;
       std::cout << "sourceId 0x" << std::hex << sourceId << std::dec << std::endl;
	   std::cout << "numberOfStatusElements " << numberOfStatusElements << std::dec << std::endl;
       
	   itbuf >> statusOfTheROBIN;
       std::cout << "statusOfTheROBIN: 0x" << std::hex << statusOfTheROBIN << std::dec << std::endl;
	   for (int iw=1; iw < numberOfStatusElements; iw++)
	   {
		   itbuf >> theWord;
           std::cout << "Status word 0x" << iw << ": " << std::hex << theWord << std::dec << std::endl;
	   }
	   itbuf >> checkSumType;
	   std::cout << "checkSumType " << checkSumType << std::dec << std::endl;
	   if (checkSumType != 1)
	   {
          std::cout << "Checksum not type 1 but " << std::hex << checkSumType << std::dec << std::endl;
	   }
	   // start CRC calculation
	   crc1 = 0xffff;
	   crc2 = 0xffff;
	   
	   // get ROD header
	   itbuf >> startofRODHeaderMarker >> sizeOfRODHeader >> formatRODFragmentVersion >> sourceIdROD >> runNumber >> extendedLVL1ID;
	   itbuf >> bunchCrossingId >> firstLevelTriggerType >> detectorEventType;
	   
	   if (numberOfFragmentsInMessage > 0)
	   {
	      if (extendedLVL1ID != lastExtendedLVL1ID)
	      {
		     std::cout << "ERROR: same event, two fragments with different extendedLVL1ID: " << extendedLVL1ID << ", "<< lastExtendedLVL1ID << std::endl;
		     errorFlag = true; 
	      }
	   }
	   lastExtendedLVL1ID = extendedLVL1ID;
	   lastExtendedLVL1ID = extendedLVL1ID;

	   crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (startofRODHeaderMarker >> 16)];
	   crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (startofRODHeaderMarker & 0xffff)];
	   crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (sizeOfRODHeader >> 16)];
	   crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (sizeOfRODHeader & 0xffff)];
	   crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (formatRODFragmentVersion >> 16)];
	   crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (formatRODFragmentVersion & 0xffff)];
	   crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (sourceIdROD >> 16)];
	   crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (sourceIdROD & 0xffff)];
	   crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (runNumber >> 16)];
	   crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (runNumber & 0xffff)];
	   crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (extendedLVL1ID >> 16)];
	   crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (extendedLVL1ID & 0xffff)];
	   crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (bunchCrossingId >> 16)];
	   crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (bunchCrossingId & 0xffff)];
	   crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (firstLevelTriggerType >> 16)];
	   crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (firstLevelTriggerType & 0xffff)];
	   crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (detectorEventType >> 16)];
	   crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (detectorEventType & 0xffff)];

       std::cout << "startofRODHeaderMarker 0x" << std::hex << startofRODHeaderMarker << std::dec << std::endl;
	   std::cout << "sizeOfRODHeader " << sizeOfRODHeader << std::dec << std::endl;
       std::cout << "formatRODFragmentVersion 0x" << std::hex << formatRODFragmentVersion << std::dec << std::endl;
       std::cout << "sourceIdROD 0x" << std::hex << sourceIdROD << std::dec << std::endl;
	   std::cout << "runNumber " << runNumber << std::dec << std::endl;
       std::cout << "extendedLVL1ID 0x" << std::hex << extendedLVL1ID << " " << std::dec << extendedLVL1ID << std::endl;
       std::cout << "bunchCrossingId 0x" << std::hex << bunchCrossingId << std::dec << std::endl;
       std::cout << "firstLevelTriggerType 0x" << std::hex << firstLevelTriggerType << std::dec << std::endl;
       std::cout << "detectorEventType 0x" << std::hex << detectorEventType << std::dec << std::endl;

	   if (statusOfTheROBIN == 0)
	   {
		   for (int iw=0; iw < (totalSize - genericHeaderSize - sizeOfRODHeader - 5) ; iw++)
		   {
			  itbuf >> theWord;
			  crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (theWord >> 16)];
			  crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (theWord & 0xffff)];
			  std::cout << "Data word " << std::hex << iw << ": " << theWord << std::dec << std::endl;
		   }
		   itbuf >> lastWordPrecedingTrailer >> numberOfRODStatusElements >> numberOfDataElements >> statusBlockPosition;
		   std::cout << "lastWordPrecedingTrailer " << lastWordPrecedingTrailer << std::dec << std::endl;
		   std::cout << "numberOfRODStatusElements " << numberOfRODStatusElements << std::dec << std::endl;
		   std::cout << "numberOfDataElements " << numberOfDataElements << std::dec << std::endl;
		   std::cout << "statusBlockPosition " << statusBlockPosition << std::dec << std::endl;
		   
		   crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (lastWordPrecedingTrailer >> 16)];
		   crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (lastWordPrecedingTrailer & 0xffff)];
		   crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (numberOfRODStatusElements >> 16)];
		   crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (numberOfRODStatusElements & 0xffff)];
		   crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (numberOfDataElements >> 16)];
		   crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (numberOfDataElements & 0xffff)];
		   crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (statusBlockPosition >> 16)];
		   crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (statusBlockPosition & 0xffff)];
		   
		   crc1 &= 0xffff;
		   crc2 &= 0xffff;
		   calculatedCRC = (crc1 << 16) + crc2;
		   std::cout << "calculatedCRC " <<  std::hex << calculatedCRC << std::dec << std::endl;

           if ((statusOfTheROBIN == 0) && (checkSumType == 1))
		   {
			  itbuf >> theCRCword;
              std::cout << "theCRCword 0x" << std::hex << theCRCword << std::dec << std::endl;
			  if (calculatedCRC != theCRCword)
			  {
				  std::cout << "ERROR: calculated CRC != CRC" << std::endl;
				  errorFlag = true;
			  }
		   }
	   }
	   else
	   {
		   for (int iw=0; iw < (totalSize - genericHeaderSize - sizeOfRODHeader) ; iw++)
		   {
			  itbuf >> theWord;
			  std::cout << "Data word " << std::hex << iw << ": " << theWord << std::dec << std::endl;
		   }
	   }
	   numberOfFragmentsInMessage++;
	   numberOfWordsNotYetLookedAt = numberOfWordsNotYetLookedAt - totalSize;
	}
	while (numberOfWordsNotYetLookedAt > 0 );
	std::cout << "numberOfFragmentsInMessage " << numberOfFragmentsInMessage << std::dec << std::endl;
    std::cout << "numberOfWordsNotYetLookedAt " << numberOfWordsNotYetLookedAt << std::dec << std::endl;
	if (errorFlag)
	{
		 std::cout << "ERROR flag raised -> stop" << std::endl;
		 exit(0);
	}
	delete myhdr;
}

/***************************************************************************/
void ROSTestDC::check_response(MessagePassing::Buffer* reply) 
/***************************************************************************/
{
   using namespace MessagePassing;
   using namespace MessageInput;
   using namespace eformat;
   MessageHeader*  myhdr = new MessageInput::MessageHeader(reply);
   uint32_t numberOfFragmentsInMessage;
   size_t numberOfWordsNotYetLookedAt;
   bool errorFlag =  false;
   uint32_t lastExtendedLVL1ID = 0;

   if (myhdr->valid())
   {
      if (m_printData)
	  {
	     printEvent(reply);
		 delete myhdr;
		 return;
      }
   }
   else
   {
      std::cout << "=============== Generic Header INVALID ================> STOP " << std::endl;
	  exit(0);
      return;
   }
   MessagePassing::Buffer::iterator itbuf = reply->begin();
    // step over message header
   itbuf += MessageInput::MessageHeader::SIZE;

   numberOfWordsNotYetLookedAt =  (reply->size() - MessageInput::MessageHeader::SIZE) / 4;
 
   // loop over all ROB fragments in Buffer
   numberOfFragmentsInMessage = 0;
   do
   {
		int32_t totalSize, genericHeaderSize, sizeOfRODHeader, numberOfStatusElements;
		uint32_t  startOfROBHeaderMarker, formatROBFragmentVersion, sourceId;
		uint32_t  theWord, checkSumType, statusOfTheROBIN;
		uint32_t  startofRODHeaderMarker, formatRODFragmentVersion, sourceIdROD, runNumber, extendedLVL1ID;
		uint32_t  bunchCrossingId, firstLevelTriggerType, detectorEventType;
		uint32_t  theCRCword, calculatedCRC;
		uint32_t  lastWordPrecedingTrailer, numberOfRODStatusElements, numberOfDataElements, statusBlockPosition;
		uint32_t  crc1, crc2;

		// get ROB header
		itbuf >> startOfROBHeaderMarker >> totalSize >> genericHeaderSize >> formatROBFragmentVersion >> sourceId >> numberOfStatusElements;

		if ( startOfROBHeaderMarker != eformat::ROB )
		{ 
			std::cout << "NOT a ROB fragment " << std::hex << startOfROBHeaderMarker << " != "<< eformat::ROB <<std::dec << std::endl;
			errorFlag = true;
			break;
		}
		if (sourceId > 11) // as sourceId is unsigned, ther is no need to check on sourceID < 0
		{
			std::cout << "Invalid source id" << sourceId << std::endl;
			errorFlag = true;
			break;
		}

		itbuf >> statusOfTheROBIN;
	   for (int iw=1; iw < numberOfStatusElements; iw++)
	   {
		  itbuf >> theWord;
	   }

	   itbuf >> checkSumType;
	   if (checkSumType != 1)
	   {
		  std::cout << "Checksum not  type 1 but " << std::hex << checkSumType << std::dec << std::endl; 
          if (checkSumType != 0)
          {
            errorFlag = true;
          }
	   }
	   // start CRC calculation
	   crc1 = 0xffff;
	   crc2 = 0xffff;
	   
	   // get ROD header
	   itbuf >> startofRODHeaderMarker >> sizeOfRODHeader >> formatRODFragmentVersion >> sourceIdROD >> runNumber >> extendedLVL1ID;
	   itbuf >> bunchCrossingId >> firstLevelTriggerType >> detectorEventType;
	   
	   if (numberOfFragmentsInMessage > 0)
	   {
	      if (extendedLVL1ID != lastExtendedLVL1ID)
	      {
		     std::cout << "ERROR: same event, two fragments with different extendedLVL1ID: " << extendedLVL1ID << ", "<< lastExtendedLVL1ID << std::endl;
		     errorFlag = true; 
	      }
	   }
	   lastExtendedLVL1ID = extendedLVL1ID;

	   crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (startofRODHeaderMarker >> 16)];
	   crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (startofRODHeaderMarker & 0xffff)];
	   crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (sizeOfRODHeader >> 16)];
	   crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (sizeOfRODHeader & 0xffff)];
	   crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (formatRODFragmentVersion >> 16)];
	   crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (formatRODFragmentVersion & 0xffff)];
	   crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (sourceIdROD >> 16)];
	   crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (sourceIdROD & 0xffff)];
	   crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (runNumber >> 16)];
	   crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (runNumber & 0xffff)];
	   crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (extendedLVL1ID >> 16)];
	   crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (extendedLVL1ID & 0xffff)];
	   crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (bunchCrossingId >> 16)];
	   crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (bunchCrossingId & 0xffff)];
	   crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (firstLevelTriggerType >> 16)];
	   crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (firstLevelTriggerType & 0xffff)];
	   crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (detectorEventType >> 16)];
	   crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (detectorEventType & 0xffff)];
	   if (statusOfTheROBIN == 0)
	   {
 		   for (int iw=0; iw < (totalSize - genericHeaderSize - sizeOfRODHeader - 5) ; iw++)
		   {
			  itbuf >> theWord;
			  crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (theWord >> 16)];
			  crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (theWord & 0xffff)];
		   }
			itbuf >> lastWordPrecedingTrailer >> numberOfRODStatusElements >> numberOfDataElements >> statusBlockPosition;
		   
		   crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (lastWordPrecedingTrailer >> 16)];
		   crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (lastWordPrecedingTrailer & 0xffff)];
		   crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (numberOfRODStatusElements >> 16)];
		   crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (numberOfRODStatusElements & 0xffff)];
		   crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (numberOfDataElements >> 16)];
		   crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (numberOfDataElements & 0xffff)];
		   crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (statusBlockPosition >> 16)];
		   crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (statusBlockPosition & 0xffff)];
		   
		   crc1 &= 0xffff;
		   crc2 &= 0xffff;
		   calculatedCRC = (crc1 << 16) + crc2;

		   itbuf >> theCRCword;
		   if (calculatedCRC != theCRCword)
		   {
			  std::cout << "ERROR: calculated CRC != CRC" << std::endl;
			  errorFlag = true;
		   }
	   }
	   else
	   {
		   // skip to next event
		   for (int iw=0; iw < (totalSize - genericHeaderSize - sizeOfRODHeader) ; iw++)
		   {
			  itbuf >> theWord;
		   }
		}
	   numberOfFragmentsInMessage++;
	   numberOfWordsNotYetLookedAt = numberOfWordsNotYetLookedAt - totalSize;
	}
	while (numberOfWordsNotYetLookedAt > 0 );
	if (errorFlag)
	{
		 std::cout << "ERROR flag raised -> stop" << std::endl;
		 printEvent(reply);
		 exit(0);
	}

   delete myhdr;
}

//==========================

/***************************************************************************/
float ROSTestDC::run(int maxRequests)
/***************************************************************************/
{
   struct timeval startTime;
   struct timeval endTime;
   float elapsedTime=0.0;
   struct timezone dummy;
   gettimeofday(&startTime,&dummy);
   sigset_t backslashSet;
   sigset_t oldSet;
   sigset_t currentSet;
	unsigned int lastPrintThreshold;
	unsigned int printReferenceMask;
	bool syncNotYetDoneFlag = true;

   sigemptyset(&backslashSet);
   sigaddset(&backslashSet, SIGQUIT);
   sigprocmask(SIG_BLOCK, &backslashSet, &oldSet);
   DEBUG_TEXT(DFDB_ROSIO, 10, "run: after sigprocmask, old set = " << oldSet.__val[0]);	// not quite standard ..
   sigprocmask(SIG_SETMASK, NULL, &currentSet);
   DEBUG_TEXT(DFDB_ROSIO, 10, "run: after sigprocmask, current set = " << currentSet.__val[0]);

   if (m_numberOfOutstanding > 0) {
      // Prime system with m_numberOfOutstanding requests
      // int sent = generate(1, 0);
		int sent = generate(m_numberOfOutstanding, 0);
      if (sent == 0) {
         std::cout << "No triggers have been sent." <<std::endl;
         std::cout << "Most likely the trigger disabled itself to avoid a division by zero." <<std::endl;
         std::cout << "Use the debug version to confirm" <<std::endl;
      }

      // Now receive replies and generate requests to maintain number of outstanding requests
      int numberReceived=0;

		// setup regular output of statistics
		lastPrintThreshold = 0;
		printReferenceMask = (printInterval >> 1) ;
		while ( (printReferenceMask & 0x1) == 0)
		{
			printReferenceMask |= (printReferenceMask >> 1);
		}
		printReferenceMask = ~printReferenceMask;
		
      while (((maxRequests == 0) || (numberReceived < maxRequests)) && !sigtermFlag)
		{ 
			MessagePassing::Buffer* reply=MessagePassing::Port::receive(100000);

         // only listen to SIGQUIT in the following lines ...
         sigprocmask(SIG_UNBLOCK,&backslashSet,&oldSet);
         DEBUG_TEXT(DFDB_ROSIO, 10, "run: after UNBLOCK, old set = " << oldSet.__val[0]);
         if (sigquitFlag) {
            gettimeofday(&endTime,&dummy);
            float elapsed=(endTime.tv_sec-startTime.tv_sec) +
               (endTime.tv_usec-startTime.tv_usec)/1000000.0;

            std::cout <<std::endl << " Trigger statistics" <<std::endl ; 
            printStatistics(elapsed);
            sigquitFlag=false;
         }
         sigaddset(&backslashSet, SIGQUIT);	// block again
         sigprocmask(SIG_BLOCK, &backslashSet, &oldSet);
         DEBUG_TEXT(DFDB_ROSIO, 10, "run: after BLOCK, old set = " << oldSet.__val[0]);

         if (reply != 0)
			{
            numberReceived++;

            // switch to avoid testing correctness of response (printing is also suppressed)
            if (m_testResponse) {
               check_response(reply); 	
            }

            // Process received L1ID  -- Ignore the name of the method!!
 				getLvl1id(reply);
            delete reply;

 				if (!stopFlag)
				{
					sent = generate(m_numberOfOutstanding, 0);
				}
			}
         else 
			{
				if (stopFlag)
				{
					gettimeofday(&endTime,&dummy);
					float timeDiff = (endTime.tv_sec-stopTime.tv_sec) + (endTime.tv_usec-stopTime.tv_usec)/1000000.0;
					// synchronize parameters
					if (socketioFlag && syncNotYetDoneFlag)
					{
						if (masterFlag)
						{
							bool exitFlag = false;
							bool iterationEndFlag = false;
							// set new values of parameters
							if ( (l2OrEBStepSize > stepSizeStopValue) && (l2ReqfracEqualsOneStopFlag == false) && (fabs(lastL1Freq-targetL1Freq) > 0.2) && (doNotIterateFlag == false) )
							{
								if (lastL1Freq > targetL1Freq)
								{
									if (downCount > 0)
									{
										l2OrEBStepSize = 0.5 * l2OrEBStepSize;
										downCount = 0;
									}
									if (iterateOnEBFracFlag)
									{
										ebRequestFraction = ebRequestFraction + l2OrEBStepSize;
										if (ebRequestFraction > 99.9999)
										{
											std::cout << "new ebRequestFraction > 100 -> STOP" << std::endl;
											// use original value for output of results
											ebRequestFraction = ebRequestFraction - l2OrEBStepSize;
											exitFlag = true;
										}
									}
									else
									{
										l2RequestFraction = l2RequestFraction + l2OrEBStepSize;
										if (l2RequestFraction > 99.9999)
										{
											std::cout << "new l2RequestFraction > 100 -> STOP" << std::endl;
											//use original value for output of results
											l2RequestFraction = l2RequestFraction - l2OrEBStepSize;
											exitFlag = true;
										}
									}
									upCount++;
								}
								else
								{
									if (upCount > 0)
									{
										l2OrEBStepSize = 0.5 * l2OrEBStepSize;
										upCount = 0;
									}
									if (iterateOnEBFracFlag)
									{
										ebRequestFraction = ebRequestFraction - l2OrEBStepSize;
										if (ebRequestFraction < 0.00001)
										{
											std::cout << "new ebRequestFraction < 0 -> STOP" << std::endl;
											// need to use original value for output of results
											ebRequestFraction = ebRequestFraction + l2OrEBStepSize;
											exitFlag = true;
										}
										
									}
									else
									{
										l2RequestFraction = l2RequestFraction - l2OrEBStepSize;
										if (l2RequestFraction < 0.00001)
										{
											std::cout << "new l2RequestFraction < 0 -> STOP" << std::endl;
											// need to use original value for output of results
											l2RequestFraction = l2RequestFraction + l2OrEBStepSize;
											exitFlag = true;
										}
									}
									downCount++;
								}
							}
							else
							{
								iterationEndFlag = true;
								if (l2ReqfracEqualsOneStopFlag)
								{
									exitFlag = true;
								}
							}
							if (exitFlag || iterationEndFlag)
							{
								// time to stop, get time
								gettimeofday(&endTime,&dummy);
								float elapsed=(endTime.tv_sec-startTime.tv_sec) +
									(endTime.tv_usec-startTime.tv_usec)/1000000.0;
								float avROLS = 0.0;
								float sumRoihist = 0.0;
								
								if (l2RequestFraction > 0.0001)
								{
									for (int i=1; i<= m_numberOfChannels; i++)
									{
										avROLS = avROLS + ( (float) m_roihist[i]) * (float) i;
										sumRoihist = sumRoihist + (float) m_roihist[i];
									}
									avROLS = avROLS / sumRoihist;
								}
								else
								{
									avROLS = 0;
								}
								std::cout << "STOP last stepSize = " << l2OrEBStepSize << " stepSizeStopValue = " << stepSizeStopValue << std::endl;
                                std::cout << "STOP fragSize l2frac (%) - ebfrac (%) - deleteGrouping - avROLS - lastL1Freq(kHz) - l2rate (kHz) - ebrate(kHz)  Etmissfrac Etmissrate"  << std::endl;
								if (iterationEndFlag)
								{
									std::cout << "Final ";
								}
								else
								{
									std::cout << "Final(Run-Halted) ";
								}
								std::cout << maxFragSize << " " << l2RequestFraction << " " << ebRequestFraction << " " 
											 << m_deleteGrouping << " " << avROLS << " " << lastL1Freq << " ";
                                float l2ReqRate = lastL1Freq * l2RequestFraction * ( (float) m_numberOfChannels) / (100.0 * avROLS);
                                if (l2RequestFraction > 0.0001)
								{			 
                                    std::cout << l2ReqRate << " ";
								}
								else
								{
									std::cout << " 0 ";
								}
                                float etMissFraction;
                                if (m_l2RequestsGenerated > 0)
                                {
                                     etMissFraction = (float) m_metRequestsGenerated / (float) m_l2RequestsGenerated;
                                }
                                else
                                {
                                    etMissFraction = 0.0;
                                }
                                float etMissRate = l2ReqRate * etMissFraction;
                                std::cout << lastL1Freq * ebRequestFraction / 100.0 << " " << etMissFraction * 100.0 << " " << etMissRate << std::endl;
								std::cout << "NB: l2rate and ebrate as calculated from fractions" << std::endl;
								// tell run control
								// port number to be taken from command line, needs still to be implemented
								SockIO *runControlSock = new SockIO(controllerName, 1, 7374);
								runControlSock->InitClient();
								// program going to be closed
								delete runControlSock;
								delete paramExchangeSocket;
								return elapsed;
							}
							// setup communication buffer and send to slaves
							theBuf.l2frac = l2RequestFraction;
							theBuf.ebfrac = ebRequestFraction;
							theBuf.deleteGroupSize = m_deleteGrouping;
							theBuf.avROLsPerRoI = avROLsPerRoI;
							theBuf.printInterval = printInterval;
							theBuf.maxNumberOfOutputsPerRun = maxNumberOfOutputsPerRun;
							theBuf.targetL1Freq = targetL1Freq;
                            theBuf.percentOfEtmiss = percentOfEtmiss;
							paramExchangeSocket->SendParams(&theBuf, maxFragSize);
						}
						else
						{
							paramExchangeSocket->GetParams(&theBuf);
							l2RequestFraction = theBuf.l2frac;
							ebRequestFraction = theBuf.ebfrac;
							m_deleteGrouping = theBuf.deleteGroupSize;
							avROLsPerRoI = theBuf.avROLsPerRoI;
							printInterval = theBuf.printInterval;
							maxNumberOfOutputsPerRun = theBuf.maxNumberOfOutputsPerRun;
                            percentOfEtmiss = theBuf.percentOfEtmiss;
							targetL1Freq = theBuf.targetL1Freq;
						}
						syncNotYetDoneFlag = false;
					}
					if ( timeDiff > waitingTime)
					{
						std::cout << std::endl << "---> Init again and start new run ... L1 freq was " << lastL1Freq << " kHz" << std::endl << std::endl;
						if (!socketioFlag)
						{
							// unsynchronized setting of parameters
							// l2RequestFraction = l2RequestFraction + l2OrEBStepSize;
							std::cout << "No master or slave flag seen: request fractions are not modified" << std::endl; 
						}
						init(false);
						stopFlag = false;
						syncNotYetDoneFlag = true;
						gettimeofday(&startTime,&dummy);
						numberOfStatOutputPerRun = 0;
						sent = generate(m_numberOfOutstanding, 0);
					}
				}
				else
				{
				  std::cerr << "Port::receive 2 returned 0\n";
				}
			}
			
#define PRINT_STATISTICS		
	
#ifdef PRINT_STATISTICS
			// if ( ( (m_level1Id + (m_rollovers << 24) )  & printReferenceMask) != lastPrintThreshold)
			if ( (m_level1Id & printReferenceMask) != lastPrintThreshold)
			{
				struct timezone dummy;
				struct timeval endTime;
				gettimeofday(&endTime,&dummy);
				float elapsed=(endTime.tv_sec-startTime.tv_sec) +
					(endTime.tv_usec-startTime.tv_usec)/1000000.0;

				std::cout << std::endl << " Trigger statistics" << std::endl ; 
				printStatistics(elapsed);
#ifdef GORDON
           std::cout << std::endl << "completeEBFragments, inCompleteEBFragments, completeL2Fragments, incompleteL2FragmentsPerROB" << std::endl;
           for (int i=0;i<m_numberOfChannels;i++) {
              unsigned int chan=m_channelIds[i];
              std::cout << m_completeEBFragmentsPerROB[chan] << "  " << m_incompleteEBFragmentsPerROB[chan] << "  " << m_completeL2FragmentsPerROB[chan] << "  " << m_incompleteL2FragmentsPerROB[chan] << std::endl;
#else
                std::cout << std::endl << "completeEBFragments - inCompleteEBFragments - completeL2Fragments - inCompleteL2FragmentsPerROB - EtmissFragments (for ROL 0)" << std::endl;
				for (int i=0;i<c_maxRolsPerL2PU;i++)
				{
                    std::cout << completeEBFragmentsPerROB[i] << "  " << inCompleteEBFragmentsPerROB[i] << "  " << completeL2FragmentsPerROB[i] << "  " << inCompleteL2FragmentsPerROB[i];
                    if (i==0)
                    {
                        std::cout << " " << numberOfEtmissFragsReceived;
                    }
                    std::cout << std::endl;
				}
#endif
				std::cout <<  std::endl << "numberOfEBOutstanding " << numberOfEBOutstanding << std::endl;
				std::cout << "numberOfL2Outstanding " << numberOfL2Outstanding << std::endl;
				lastPrintThreshold = lastPrintThreshold + printInterval;
			}
#endif

      }
      struct timeval endTime;
      gettimeofday(&endTime,&dummy);
      elapsedTime=(endTime.tv_sec-startTime.tv_sec) +
         (endTime.tv_usec-startTime.tv_usec)/1000000.0;

      // Now we've received the required number of responses but we
      // should still have m_numberOfOutstanding requests outstanding
      // so listen for those and throw them away so that we end
      // cleanly.
      for (int nExtras=0; nExtras<m_numberOfOutstanding-1; nExtras++) {
         MessagePassing::Buffer* extraReply=MessagePassing::Port::receive(1000000);
         if (extraReply==0) {
            // Give up, probably timed out
            std::cerr << "Only managed to collect " << nExtras << " of the outstanding events in tidy up\n";
            break;
         }
         delete extraReply;
      }
   }
   else {
      generate(maxRequests, 0);
   }
   return elapsedTime;
}

void loadConfig(std::string name, DFCountedPointer<Config> configuration)
{
   std::string configDir;
   char* configDirfromEnvironment=getenv("DAQ_CONFIG_DIR");
   if (configDirfromEnvironment == 0) {
      std::cout << "DAQ_CONFIG_DIR is not set. I don't know where to get the configuration!" <<std::endl;
      exit (EXIT_FAILURE);

      // configDir=".";
   }
   else {
      configDir=configDirfromEnvironment;
   }
   std::string fileName=configDir + "/" + name;
   std::ifstream configFile(fileName.c_str());
   if (!configFile) {
      std::cerr << "cannot open " << fileName
                << " iostream has no error reporting so can't tell you why!\n";
      exit (EXIT_FAILURE);
   }

   const int lineSize = 1024;
   char buf[lineSize];
   
   while(configFile.getline(buf, lineSize)) {
      if (buf[0] == '#') { // skip comment lines
         continue;
      }
      std::string line(buf);
      //    std::cout << "line is: " << line <<std::endl;
      int equalsPosition=line.find('=');
      std::string key(line, 0, equalsPosition);
      //std::cout << "key is: " << key <<std::endl;
      line.erase(0, equalsPosition+1);
      std::string value(line);
      //std::cout << "value is: " << value <<std::endl;
      configuration->set(key, value);
   }
     
}

/*************************************************************/
void ROSTestDC::generateL2Request(unsigned int l1id) 
/*************************************************************/
{
  // emulate DC input
  if (m_inputDelay > 0) 
  {
    unsigned int nyield = 0;
    ts_wait(m_inputDelay, &nyield);
  }
 
     
//      TS_RECORD(TS_H1,1150);
  DEBUG_TEXT(DFDB_ROSTG, 15, "generateL2Request:  L2 request for L1id = 0x" << std::hex <<
                              l1id << " generated, ROL size = " << std::dec << m_rols.size());
  m_transactionId++;

  l2Request(l1id, &m_rols, m_destination, m_transactionId);
//      TS_RECORD(TS_H1, 1160);
      
  m_rols.clear();	// size = 0

}

/****************************************************************/
void ROSTestDC::generateEBRequest(unsigned int level1Id) 
/****************************************************************/
{
  // emulate DC input
  if (m_inputDelay > 0) 
  {
    unsigned int nyield = 0;
    ts_wait(m_inputDelay, &nyield);
  }

  DEBUG_TEXT(DFDB_ROSTG, 15, "generateEBRequest:  EB request generated for L1id = 0x"
                             << std::hex << level1Id );
  ebRequest(level1Id, m_destination);

}

/*****************************************************************************/
void ROSTestDC::generateReleaseRequest(unsigned int oldestLevelId) 
/*****************************************************************************/
{
  // emulate DC input
  if (m_inputDelay > 0) 
  {
    unsigned int nyield = 0;
    ts_wait(m_inputDelay, &nyield);
  }

//        TS_RECORD(TS_H1, 1310);
  DEBUG_TEXT(DFDB_ROSTG, 15, "generateReleaseRequest:  release request to be sent, last L1id  = 0x"
                             << std::hex << m_l1Ids[m_l1Ids.size()-1]
                             << " oldest L1ID = " << oldestLevelId);
  releaseRequest(&m_l1Ids, oldestLevelId);
}

/*****************************************************************************/
void ROSTestDC::checkHistory(unsigned int id) 
/*****************************************************************************/
{
	unsigned int i;
	unsigned int releaseCount0;
	bool notOKFlag = false;
	
	std::cout << "START checkHistory,  m_instance = " << m_instance << " m_totalInstances = " << m_totalInstances << std::endl;

	id = id / m_totalInstances;
	id = id & c_ereqtypestate_mask;
	releaseCount0 = releaseCount[0];
	for (i=0;i<id;i++)
	{
		switch (reqTypeState[i])
		{
			case NO_REQ:
				if (m_ebInterval[i] != 0)
				{
					std::cout << "checkHistory, reqTypeStat == NO_REQ: m_ebInterval != 0 for id " << i << std::endl;
					notOKFlag = true;
				}
				if (m_l2Interval[i] != 0)
				{
					std::cout << "checkHistory, reqTypeStat == NO_REQ: m_l2Interval != 0 for id " << i << std::endl;
					notOKFlag = true;
				}
				if (reRequestCount[i] != 0)
				{
					std::cout << "checkHistory, reqTypeStat == NO_REQ: reRequestCount != 0 for id " << i << std::endl;
					notOKFlag = true;
				}
				if (releaseCount[i] != releaseCount0)
				{
					std::cout << "checkHistory, reqTypeStat == NO_REQ: releaseCount != " << releaseCount0 << " for id " << i << " count is " << releaseCount[i] << std::endl;
					notOKFlag = true;
				}
				break;
			case L2_REQ_TO_BE_SENT:
				if (m_ebInterval[i] != 0)
				{
					std::cout << "checkHistory, reqTypeStat == L2_REQ_TO_BE_SENT: m_ebInterval != 0 for id " << i << std::endl;
					notOKFlag = true;
				}
				if (m_l2Interval[i] == 0)
				{
					std::cout << "checkHistory, reqTypeStat == L2_REQ_TO_BE_SENT: m_l2Interval == 0 for id " << i << std::endl;
					notOKFlag = true;
				}
				if (releaseCount[i] != releaseCount0)
				{
					std::cout << "checkHistory, reqTypeStat == L2_REQ_TO_BE_SENT: releaseCount != " << releaseCount0 << " for id " << i << " count is " << releaseCount[i] << std::endl;
					notOKFlag = true;
				}
				break;
			case L2_REQ_WITH_ASSOCIATED_EB_REQ_TO_BE_SENT:
				if (m_ebInterval[i] == 0)
				{
					std::cout << "checkHistory, reqTypeStat == L2_REQ_WITH_ASSOCIATED_EB_REQ_TO_BE_SENT: m_ebInterval == 0 for id " << i << std::endl;
					notOKFlag = true;
				}
				if (m_l2Interval[i] == 0)
				{
					std::cout << "checkHistory, reqTypeStat == L2_REQ_WITH_ASSOCIATED_EB_REQ_TO_BE_SENT: m_l2Interval == 0 for id " << i << std::endl;
					notOKFlag = true;
				}
				if (releaseCount[i] != releaseCount0)
				{
					std::cout << "checkHistory, reqTypeStat == L2_REQ_WITH_ASSOCIATED_EB_REQ_TO_BE_SENT:  releaseCount != " << releaseCount0 << " for id " << i << " count is " << releaseCount[i] << std::endl;
					notOKFlag = true;
				}
				break;
			case EB_REQ_TO_BE_SENT:
				if (m_ebInterval[i] == 0)
				{
					std::cout << "checkHistory, reqTypeStat == EB_REQ_TO_BE_SENT: m_ebInterval == 0 for id " << i << std::endl;
					notOKFlag = true;
				}
				if (m_l2Interval[i] != 0)
				{
					std::cout << "checkHistory, reqTypeStat == EB_REQ_TO_BE_SENT: m_l2Interval != 0 for id " << i << std::endl;
					notOKFlag = true;
				}
				if (releaseCount[i] != releaseCount0)
				{
					std::cout << "checkHistory, reqTypeStat == EB_REQ_TO_BE_SENT: reRequestCount != releaseCount0 for id " << i << " count is " << releaseCount[i] << std::endl;
					notOKFlag = true;
				}
				break;
			default:
				std::cout << "id " << i*m_totalInstances + m_instance << " m_ebInterval " << m_ebInterval[i] << " m_l2Interval " << m_l2Interval[i] \
				<< " reRequestCount " << reRequestCount[i] << " releaseCount " << releaseCount[i] << " state " << reqTypeState[i] << " = " << stateText[reqTypeState[i]] << std::endl;
				break;
		}
	}
	
	if (notOKFlag)
	{
		// std::cout << "TRY TO CONTINUE !!!" << std::endl;
		// std::cout << "TRY TO CONTINUE !!!" << std::endl;
		// std::cout << "TRY TO CONTINUE !!!" << std::endl;

		// Tell server that we stop
		if (masterFlag)
		{
			SockIO *runControlSock = new SockIO(controllerName, 1, 7374);
			runControlSock->InitClient();
			// program going to be closed
			delete runControlSock;
			delete paramExchangeSocket;
		}
		if (slaveFlag)
		{
			delete paramExchangeSocket;
		}
		std::cout << "ABORTING due to problem found" << std::endl;
		exit(0);
	}
}

// #define HISTOGRAMS

/********************************************************************************/
void ROSTestDC::printStatistics(float elapsedSeconds,std::ostream &output) 
/********************************************************************************/
{

#ifdef HISTOGRAMS
  output << std::endl;
  output << " EB Queue histogram " << std::endl;
  output << " Q size  # elements " << std::endl;
  int nEBgroup = 5;					// ad hoc
  for (int i=0; i<(c_maxEBbins/nEBgroup); i++) {
    int bin_contents = 0;
    for (int j=0; j<nEBgroup; j++) {
      bin_contents+= m_qEBhist[nEBgroup*i+j];
    }
    output << std::setw(7) << std::dec << nEBgroup*i << std::setw(12) << bin_contents << std::endl;
  }
  output << std::endl;

  output << " CLEAR Queue histogram " << std::endl;
  output << " Q size  # elements " << std::endl;
  int nCLEARgroup = 5;
  for (int i=0; i<(c_maxCLEARbins/nCLEARgroup); i++) {
    int bin_contents = 0;
    for (int j=0; j<nCLEARgroup; j++) {
      bin_contents+= m_qCLEARhist[nCLEARgroup*i+j];
    }
    output << std::setw(7) << std::dec << nCLEARgroup*i << std::setw(12) << bin_contents << std::endl;
  }
  output << std::endl;

  if (m_testGarbageCollection) {
    output << " In Process set size histogram " << std::endl;
    output << " set size  # elements " << std::endl;
    int nIPgroup = 10;
    for (int i=0; i<(c_maxINPROCESSbins/nIPgroup); i++) {
      int bin_contents = 0;
      for (int j=0; j<nIPgroup; j++) {
        bin_contents+= m_setINPROCESShist[nIPgroup*i+j];
      }
      output << std::setw(9) << std::dec << nIPgroup*i << std::setw(12) << bin_contents << std::endl;
    }
    output << std::endl;
  }
#endif
  unsigned int nEvents = m_numberOfL1ids - last_m_numberOfL1ids;
  
  output << " Number of events = " << std::dec << nEvents << std::endl;

  for (int rol = 0; rol <= m_numberOfChannels; rol++) {
    output << " Number of events with " << rol << " ROLs per ROI = " << m_roihist[rol] << std::endl;
  }
  
  for (int rol = 0; rol < m_numberOfChannels; rol++) {
    float reqPerRobin = 100.0 * ((float) m_numberOfROI[rol] ) / ( (float) nEvents);
    output << " Number of ROI requests for ROL " << rol << " with ID " 
	   << m_channelIds[rol] << " = " 
	   << m_numberOfROI[rol] << " in % = " << reqPerRobin << std::endl;
  }
  output << " Number of Etmiss requests " << m_metRequestsGenerated << std::endl;
  float L2APer = 100.0 * ((float)m_numberOfL2A /  ( (float) nEvents));
  output << " Number of events with L2A = " << m_numberOfL2A << " in % = " << L2APer << std::endl;
  output << " Number of delete Groups   = " << m_numberOfDeleteGroup << std::endl;
  output << std::endl;

  output << " Last L1 ID generated  = " << m_level1Id << std::endl;
  output << std::endl;

  if (elapsedSeconds > 0.) 
  {
    output << "--------------------------------------------" << std::endl ;
    output << "Elapsed time (sec): " << elapsedSeconds << std::endl; 
    output << "L1 rate (kHz): " << (float) nEvents/(1000.*elapsedSeconds) << std::endl ;
    output << "Total data requests rate (kHz): "
           << (m_numberOfL2req+m_numberOfL2A)/(1000.*elapsedSeconds) << std::endl ;
    output << "L2 requests rate (kHz):     " << m_numberOfL2req/(1000.*elapsedSeconds) << std::endl ;
    output << "EB requests rate (kHz):     " << m_numberOfL2A/(1000.*elapsedSeconds) << std::endl ;
    output << "Etmiss request rate (kHz):  " << m_metRequestsGenerated/(1000.*elapsedSeconds) << std::endl ;
    output << "Delete requests rate (kHz): "
           << m_numberOfDeleteGroup/(1000.*elapsedSeconds) << std::endl ;
     output << "Next line: Measured L1 rate - Measured L2 fraction (%) - Measured EB fraction (%) - Fragment size - Measured Etmissfraction (%)" << std::endl;
     output << "----> IntegratedAll     " << ((float) nEvents)/(1000.*elapsedSeconds)
            << " " << 100.0* ( (float) m_numberOfL2req) / ( (float) nEvents) << " "
            << L2APer  << " " << maxFragSize << (float) m_metRequestsGenerated / (float) nEvents << std::endl;
	 if (firstNumberOfL1Ids == 0)
	 {
		firstElapsedSeconds = elapsedSeconds;
		firstNumberOfL1Ids = m_numberOfL1ids;
		firstNumberOfL2req = m_numberOfL2req;
		firstNumberOfL2A = m_numberOfL2A;
        firstNumberOfEtmissreq = m_metRequestsGenerated;
		firstNumberOfdeleteGroup = m_numberOfDeleteGroup;

		lastElapsedSeconds = elapsedSeconds;
		lastNumberOfL1Ids = m_numberOfL1ids;
		lastNumberOfL2req = m_numberOfL2req;
		lastNumberOfL2A = m_numberOfL2A;
        lastNumberOfEtmissreq = m_metRequestsGenerated;
        lastNumberOfdeleteGroup = m_numberOfDeleteGroup;
	 }
	 else
	 {
		float diffElapsedSeconds = 1000. * (float) (elapsedSeconds - firstElapsedSeconds);
		float diffL1Ids = (float) (m_numberOfL1ids - firstNumberOfL1Ids);
		float diffL2reqs = (float) (m_numberOfL2req - firstNumberOfL2req);
		float diffL2As = (float) (m_numberOfL2A - firstNumberOfL2A);
        float diffEtmissreqs = (float) (m_metRequestsGenerated - firstNumberOfEtmissreq);
	 
		output << std::endl << "------> Without first interval " << std::endl << std::endl;
		output << "Elapsed time (sec): " << (elapsedSeconds - firstElapsedSeconds) << std::endl; 
		output << "L1 rate (kHz): " << diffL1Ids/diffElapsedSeconds << std::endl ;
        output << "Total data request rate (kHz): "<< (diffL2reqs+diffL2As)/diffElapsedSeconds << std::endl ;
        output << "L2 request rate (kHz):     " << diffL2reqs/diffElapsedSeconds << std::endl ;
        output << "Etmiss request rate (kHz): " << diffEtmissreqs/diffElapsedSeconds << std::endl ;
        output << "EB request rate (kHz):     " << diffL2As/diffElapsedSeconds << std::endl ;
        output << "Delete request rate (kHz): "
			  << ( (float) (m_numberOfDeleteGroup-firstNumberOfdeleteGroup) )/diffElapsedSeconds << std::endl ;
        output << "Next line: Measured L1 rate - Measured L2 fraction (%) - Measured EB fraction (%)- Fragment size - Measured Etmissfraction (%)" << std::endl;
        output << "----> IntegratedNoFirst " << diffL1Ids/diffElapsedSeconds << " " << 100.0*(diffL2reqs/diffL1Ids)
               << " " << 100.0*(diffL2As/diffL1Ids) << " " << maxFragSize
               << " " << 100.0*(diffEtmissreqs/diffL1Ids) << std::endl;
		if (integrateModeFlag)
		{
			lastL1Freq = ( (float) m_totalInstances) * diffL1Ids/diffElapsedSeconds;
		}

		diffElapsedSeconds = 1000. * (float) (elapsedSeconds - lastElapsedSeconds);
		diffL1Ids = (float) (m_numberOfL1ids - lastNumberOfL1Ids);
		diffL2reqs = (float) (m_numberOfL2req - lastNumberOfL2req);
		diffL2As = (float) (m_numberOfL2A - lastNumberOfL2A);
        diffEtmissreqs = (float) (m_metRequestsGenerated - lastNumberOfEtmissreq);

		output << std::endl << "------> Only this interval " << std::endl << std::endl;
		output << "Elapsed time (sec): " << (elapsedSeconds - lastElapsedSeconds) << std::endl; 
		output << "L1 rate (kHz): " << diffL1Ids/diffElapsedSeconds << std::endl ;
        output << "Total data request rate (kHz): "<< (diffL2reqs+diffL2As)/diffElapsedSeconds << std::endl ;
        output << "L2 request rate (kHz):     " << diffL2reqs/diffElapsedSeconds << std::endl ;
        output << "Etmiss request rate (kHz): " << diffEtmissreqs/diffElapsedSeconds << std::endl ;
        output << "EB request rate (kHz):     " << diffL2As/diffElapsedSeconds << std::endl ;
        output << "Delete request rate (kHz): "
			  << ( (float) (m_numberOfDeleteGroup-lastNumberOfdeleteGroup) )/diffElapsedSeconds << std::endl ;
        output << "Next line: Measured L1 rate - Measured L2 fraction (%) - Measured EB fraction (%)- Fragment size - Measured Etmissfraction (%)" << std::endl;
        output << "----> IntegratedSingle  " << diffL1Ids/diffElapsedSeconds << " " << 100.0*(diffL2reqs/diffL1Ids)
               << " " << 100.0*(diffL2As/diffL1Ids) << " " << maxFragSize
               << " " << 100.0*(diffEtmissreqs/diffL1Ids) << std::endl;
        if (!integrateModeFlag)
		{
			lastL1Freq = ( (float) m_totalInstances) * diffL1Ids/diffElapsedSeconds;
		}

		 numberOfStatOutputPerRun++;
		 if ( (numberOfStatOutputPerRun >= maxNumberOfOutputsPerRun) && (masterFlag || slaveFlag) )
		 {
			 // only stop a run if this is a master or slave instance
			 struct timezone dummy;
			 gettimeofday(&stopTime,&dummy);
			 stopFlag = true;
			 numberOfStatOutputPerRun = 0;
		 }
		lastElapsedSeconds = elapsedSeconds;
		lastNumberOfL1Ids = m_numberOfL1ids;
		lastNumberOfL2req = m_numberOfL2req;
		lastNumberOfL2A = m_numberOfL2A;
		lastNumberOfdeleteGroup = m_numberOfDeleteGroup;
	 }
  }
}


/*****************************/
void sigquit_handler(int)
/*****************************/
{
   sigquitFlag = true;
   std::cout << " CTL\\ handler: sigquitFlag = " << sigquitFlag << std::endl;
}

/*****************************/
void sigterm_handler(int)
/*****************************/
{
   sigtermFlag = true;
   std::cout << " TERM handler: trapped TERM signal\n";
}

int main (int argc, char*argv[])
{
   struct sigaction saint;
   const int defaultOutstanding=100;
   int numberOfOutstandingRequests=defaultOutstanding;
   const int defaultRequests=5000;
   int totalNumberOfRequests=defaultRequests;
   int optionIndex=0;
   int option=0;
   int cookie=0;
   bool testResponse=false;
   int instance=0;
   int totalInstances=1;
   int printData = 0;
   bool useConfigFiles=true;
   std::string myName;
   int waitUntilStartupTime = 1;
	bool masterFlag = false;
	bool slaveFlag = false;
	bool doNotIterateFlag = false;
	bool iterateOnEBFracFlag = false;
	bool multiReadoutAppsFlag = false;
   std::string masterName;
   std::string controllerName;


	// default values
	int nOutputsPerRun = 3; // 3 times statistics output per run
	float timePerRun = 60.0; // in seconds
	float targetL1Freq = 100.0;
    float percentOfEtmiss = 0.0;
	// unsigned int printinterval = 0x200000;
	unsigned int p = 0;
	unsigned int printinterval  = 0x1000; // minimum value
	float waitingTime = 5.0; // wait 5 seconds before starting new run
	float l2OrEBStepSize = 0.5;
	float stepSizeStopValue = 0.005;
	float avROLsPerRoI = -1.0;
   int metPercent=0;

   struct option longOptions[] = {
      {"outstanding", 1, 0, 'o'},
      {"number", 1, 0, 'n'},
      {"instance", 1, 0, 'i'},
      {"totalInstances", 1, 0, 'T'},
      {"cookie",  1,  0, 'c'},
      {"metPercent",  1,  0, 'm'},
      {"test_response",  0,  0, 't'},
      {"print",  0,  0, 'p'},
      {"database", 0, 0, 'd'},
      {"name", 1, 0, 'N'},
      {"Master", 0, 0, 'M'},
      {"Server", 1, 0, 'S'},
      {"L1freq", 1, 0, 'L'},
      {"timePerRun", 1, 0, 'R'},
      {"OutputsPerRun", 1, 0, 'O'},
      {"l2OrEBStepSize", 1, 0, 'l'},
      {"stepSizeStopValue", 1, 0, 'V'},
      {"avROLsPerRoI", 1, 0, 'a'},
      {"doNotIterate", 0, 0, 'x'},
      {"iterateOnEBFrac", 0, 0, 'e'},
      {"help", 0, 0, 'h'},
      {0, 0, 0, 0}
   };
   
   while (option != -1) {
      option=getopt_long (argc, argv, "w:o:n:i:T:c:m:N:S:s:L:R:O:l:V:a:M:pdthexq", longOptions, &optionIndex);
      switch (option) {
      case 'w':
         if (isdigit(optarg[0])) {
            waitUntilStartupTime=strtol(optarg, 0, 10);
         }
         break;
      case 'o':
         if (isdigit(optarg[0])) {
            numberOfOutstandingRequests=strtol(optarg, 0, 10);
         }
         break;
      case 'n':
         if (isdigit(optarg[0])) {
            totalNumberOfRequests=strtol(optarg, 0, 10);
         }
         break;
      case 'i':
         if (isdigit(optarg[0])) {
            instance=strtol(optarg, 0, 10);
         }
			break;
      case 'T':
         if (isdigit(optarg[0])) {
            totalInstances=strtol(optarg, 0, 10);
         }
			break;
      case 'c':
         if (isdigit(optarg[0])) {
            cookie=strtol(optarg, 0, 10);
         }
         break;
      case 'N':
         myName=optarg;
         break;
       case 'M':
         masterFlag = true;
         controllerName=optarg;
         break;
     case 'S':
         masterName=optarg;
			slaveFlag = true;
         break;
     case 's':
         if (isdigit(optarg[0])) {
            p=strtol(optarg, 0, 10);
			}
         break;
      case 'd':
         useConfigFiles=false;
         break;
      case 'm':
         if (isdigit(optarg[0])) {
            metPercent=strtol(optarg, 0, 10);
            percentOfEtmiss = strtof(optarg, NULL);
         }
         break;
      case 't':
         testResponse=true;
         break;
      case 'p':
         printData=true;
         break;
		case 'L':
        if (isdigit(optarg[0]))
            targetL1Freq=strtof(optarg, NULL);
			break;
		case 'O':
        if (isdigit(optarg[0]))
            nOutputsPerRun=strtol(optarg, 0, 10);
			break;
		case 'R':
        if (isdigit(optarg[0]))
            timePerRun=strtof(optarg, NULL);
			break;
		case 'l':
        if (isdigit(optarg[0]))
            l2OrEBStepSize=strtof(optarg, NULL);
			break;
		case 'V':
        if (isdigit(optarg[0]))
            stepSizeStopValue=strtof(optarg, NULL);
			break;
		case 'a':
        if (isdigit(optarg[0]))
            avROLsPerRoI=strtof(optarg, NULL);
			break;
		case 'x':
			doNotIterateFlag = true;
			break;
		case 'e':
			iterateOnEBFracFlag = true;
			break;
		case 'q':
			multiReadoutAppsFlag = true;
			break;
      case 'h':
      case ':':
      case '?':
         std::cerr << "Usage: " << argv[0]
                   << " [--number=n] [--outstanding=o] [--instance=i] [--totalInstances=T] [--cookie=c] [--waitUntilStartup=w]"
						 << " [--test_response] [--name=N] [--Master=M] [--Server=S] [--database] [--statInterval=s] [--print] [--help]"
						 << " [--L1freq=L] [--timePerRun=R] [--OutputsPerRun=O] [--l2OrEBStepSize=l] [--stepSizeStopValue=V] [--avROLsPerRoI=a]"
						 << " [--doNotIterate=x] [--iterateOnEBFrac=e] [--multiReadoutApps=q]\n"
                   << "  where:\n"
                   << "\t<n> is the number of data requests that must complete before we exit (0=infinite) default=" << defaultRequests << "\n"
                   << "\t<o> is the number of outstanding requests in the system default=" << defaultOutstanding << "\n"
                   << "\t<i> is the number of this instance in the system\n"
                   << "\t<T> is the total number of instances of " << argv[0] << " in the system\n"
                   << "\t<c> is the cookie to be set in the DC request headers\n"
                   << "\t<N> is the name of this application instance in the OKS database\n"
                   << "\t<m> is the percentage of L2 requests converted to special Missing ET requests\n"
                   << "\t<M> is the name of the computer on which the control script is running\n"
                   << "\t<S> is the name of the computer on which the master instance is running\n"
                   << "\t<s> after s triggers information on requests sent and received is output\n"
                   << "\t<w> is the time in seconds before program starts to send requests (default 1 second)\n"
                   << "\t<L> is the target L1 frequency  (default 100 kHz)\n"
                   << "\t<R> is the time per single run with fixed request fractions (default 60 seconds)\n"
                   << "\t<l> is the start step size for changes in the L2 or EB request fraction (default 0.5)\n"
						 << "\t<V> is the smallest step size for changes in the L2 or EB request fraction (default 0.005)\n"
						 << "\t<a> is the average number of ROLs per RoI (default taken from configuration database)\n"
                   << "\t  --database causes the application to configure itself from the OKS database (rather than simple config files)\n"
                   << "\t  --test_response causes  a check of the correctness of the response \n"
                   << "\t  --print causes detailed formatted dump of received event fragments \n"
                   << "\t  --Master causes the instance to function as master for other instances \n"
						 << "\t  --doNotIterate causes always an exit after one cycle \n"
						 << "\t  --iterateOnEBFrag causes the EB fraction to be modified instead of L2 fraction \n"
						 << "\t  --multiReadoutApps option for more than 1 independent ReadoutApp  \n"
                   << "\t  --help print this help text and exit\n"
						 << "NB: --number=x can also be specified as -n x, etc." << std::endl;
						 
         exit (0);
      }
   }

   if (printData && !testResponse) {
      std::cerr << "--print only works when --test_response  is selected!" <<std::endl;
      exit(0);
   }
   
	if (totalInstances > MAX_INSTANCES)
	{
      std::cerr << "number if instances " << totalInstances << " >= MAX_INSTANCES (defined in RSTestDC.h" << std::endl;
      exit(0);
   }
		
	
   // Install signal handler for SIGQUIT
   sigemptyset(&saint.sa_mask);
   saint.sa_flags   = 0;
   saint.sa_handler = sigquit_handler;

   if (sigaction(SIGQUIT, &saint, NULL) < 0) {
      std::cout << "main: sigaction() FAILED with ";
      if (errno == EFAULT) std::cout << "EFAULT" << std::endl;
      if (errno == EINVAL) std::cout << "EINVAL" << std::endl;
      exit(0);
   }

   // Install signal handler for SIGTERM
   sigemptyset(&saint.sa_mask);
   saint.sa_flags   = 0;
   saint.sa_handler = sigterm_handler;

   if (sigaction(SIGTERM, &saint, NULL) < 0) {
      std::cout << "main: sigaction() FAILED with ";
      if (errno == EFAULT) std::cout << "EFAULT" << std::endl;
      if (errno == EINVAL) std::cout << "EINVAL" << std::endl;
      exit(0);
   }

   TS_OPEN(100000, TS_H1);     // for (optional:TSTAMP) time stamping
   TS_START(TS_H1);
   TS_RECORD(TS_H1,1);

   DFCountedPointer<Config> config=Config::New();

   if (useConfigFiles) {
      loadConfig("ROSTestDC.dat", config);
   }
   else {
      IPCCore::init(argc,argv);

      // Get config from OKS database
      Configuration confDB("");
      if(!confDB.loaded()) {
         std::cerr << "ROSTestDC: cannot load database <" << confDB.get_impl_spec() << ">" <<std::endl;
         return 0;
      }

      const daq::core::Partition* dbPartition;
      std::string any;
      if(!(dbPartition = daq::core::get_partition( confDB, any))) {
         std::cerr << "ROSTestDC: TDAQ_PARTITION not set or Partition object not found in the DB" <<std::endl;
         return 0;
      }

      const daq::df::TESTNODEID_ROSTestDC* dbApp= confDB.get<daq::df::TESTNODEID_ROSTestDC>(myName);
      if (dbApp==0) {
         std::cerr << "ROSTestDC: Cannot find " << myName << " in database" <<std::endl;
         return 0;
      }

      // Ask that all $variables in the DB are automatically substituted
      confDB.register_converter(new daq::core::SubstituteVariables(confDB, *dbPartition));

      const daq::df::ROS* myROS=dbApp->get_ConnectsToROS();
      if (myROS==0) {
         std::cerr << "Failed to find ConnectsToROS relationship in database\n";
         exit(EXIT_FAILURE);
      }


      MessageConfiguration msgConf;
      //      unsigned int myNodeId = Application::string2id(dbApp->UID());
      std::vector<daq::core::AppConfig> appOut;
      std::set<std::string> appTypes;
      appTypes.insert("DFMessagePassingNode");
      unsigned int myNodeId=0;
      unsigned int rosNodeId=0;
      dbPartition->get_all_applications(appOut, confDB, &appTypes, NULL, NULL);

      for(unsigned int it = 0; it < appOut.size(); it++) {
         std::cout << "appOut[" << it << "] id=" <<appOut[it].get_app_id() << std::endl;
         if (appOut[it].get_app_id() == dbApp->UID()) {
            std::cout << "Found my entry, getting id\n";
            myNodeId = appOut[it].get_node_id();
            break;
         }
      }

      if (myNodeId==0) {
         std::cerr << "Failed to get my node ID, found " << appOut.size() << " message passing apps \n";
         exit(EXIT_FAILURE);
      }

#ifdef TDAQ191
      msgConf.clear_configuration();
      if(dbPartition->get_DataFlowParameters()) {
         bool result = msgConf.init_configuration(myNodeId, confDB);
         if (!result) {
            std::cerr << "ROSTestDC: Cannot  initialize the Message Passing configuration" <<std::endl;
            exit(EXIT_FAILURE);
         }
      }
      else {
         std::cerr << "ROSTestDC: Cannot initialize the Message Passing configuration: DataFlowParameters not linked to the Partition in the Database"
                   <<std::endl;
         exit(EXIT_FAILURE);
      }
#else
	  const daq::df::DFParameters *df_params = confDB.cast<daq::df::DFParameters>(dbPartition->get_DataFlowParameters());
	  if(!df_params) 
	  {
		  std::cerr << "ROSTestDC: Cannot initialize the Message Passing configuration: DataFlowParameters not linked to the Partition in the Database"
						<<std::endl;
		  exit(EXIT_FAILURE);
	  }


	  msgConf.clear_configuration();
	  bool result = msgConf.init_configuration(myNodeId, confDB);
	  if (!result)
	  {
		  std::cerr << "ROSTestDC: Cannot  initialize the Message Passing configuration" <<std::endl;
		  exit(EXIT_FAILURE);
	  }
#endif

      // Set up local address info
      config->set("instanceNodeId", myNodeId, instance);
      const MessagePassing::Node* myNode = msgConf.find_node(myNodeId);
      std::ostringstream myAddr;
      for(MessagePassing::AddressList::const_iterator it=myNode->begin(); 
	  it !=myNode->end(); ++it) {
         myAddr << (*it).address() << ";;" ;
      }
      config->set("instanceAddress", myAddr.str(), instance);

      // Set up address of ROS we're gonna talk to
      for(unsigned int it = 0; it < appOut.size(); it++) {
         if (appOut[it].get_app_id() == myROS->UID()) {
            rosNodeId = appOut[it].get_node_id();
            break;
         }
      }

      if (rosNodeId==0) {
         std::cerr << "Failed to get ROS node ID\n";
         exit(EXIT_FAILURE);
      }
      std::cout << "ROS node ID is " << rosNodeId << " (" << std::hex << rosNodeId << std::dec << ")\n";

      config->set("ROSNodeId",rosNodeId);
      const MessagePassing::Node* rosNode = msgConf.find_node(rosNodeId);
      std::ostringstream rosAddr;
      for(MessagePassing::AddressList::const_iterator it=rosNode->begin(); 
	  it !=rosNode->end(); ++it) {
         rosAddr << (*it).address() << ";;" ;
      }
      config->set("ROSAddress", rosAddr.str());

#ifdef TDAQ191
      // Setup multcast address of ROS group
      if(myROS->get_belongs_to().size()) {
         std::string multicastAddress=myROS->get_belongs_to()[0]->get_MulticastAddress();
         config->set("multicastAddress", multicastAddress);
         config->set("multicastId", msgConf.find_group("ROS")->id());
      }
#else
     if (myROS->get_ReceiveMulticast() && dbApp->get_ReceiveMulticast())
	  {
        // Setup multicast address of ROS group
        std::string multicastAddress= df_params->get_MulticastAddress();
        config->set("multicastAddress", multicastAddress);
        msgConf.create_by_group("ROS"); 
        msgConf.create_group("ROS");
        config->set("multicastId", msgConf.find_group("ROS")->id());
     }
     else 
	  {
        std::cout << "NOT ";
        config->set("multicastAddress", "");
        config->set("multicastId", 0);
     }
     std::cout << "using multicast for clears\n";
#endif

      unsigned int detectorId=myROS->get_Detector()->get_LogicalId();
      // Work out what channels are active in the ROS we're gonna talk to
      unsigned int nChannels=0;
      std::vector<const daq::core::Component*> disabledComponents = dbPartition->get_Disabled();

      for (std::vector<const daq::core::ResourceBase*>::const_iterator moduleIter = myROS->get_Contains().begin(); 
           moduleIter != myROS->get_Contains().end(); moduleIter++) {
         const daq::df::ReadoutModule*  roModule=
            confDB.cast<daq::df::ReadoutModule, daq::core::ResourceBase> (*moduleIter);
         if (roModule==0) {
            std::cerr << "ReadoutApplication " << myROS->UID()
                      << " Contains relationship to something (" << (*moduleIter)->class_name() <<
               ") that is not a ReadoutModule\n";
            continue;
         }

         const daq::core::ResourceSet*  resources=
            confDB.cast<daq::core::ResourceSet, daq::core::ResourceBase> (*moduleIter);
         if (resources==0) {
            std::cerr << "ReadoutApplication " << myROS->UID()
                      << " has a ReadoutModule that is not a ResourceSet\n";
            continue;
         }
         for (std::vector<const daq::core::ResourceBase*>::const_iterator channelIter = resources->get_Contains().begin(); 
              channelIter != resources->get_Contains().end(); channelIter++) {
            const daq::df::InputChannel* channelPtr=
               confDB.cast<daq::df::InputChannel, daq::core::ResourceBase> (*channelIter);
            if (channelPtr==0) {
               std::cerr << "ReadoutApplication " << myROS->UID()
                         << " Contains relationship to something (" << (*channelIter)->class_name() <<
                  ") that is not an InputChannel\n";
               continue;
            }
            else {
               if (!(*channelIter)->disabled(*dbPartition)) {
                  unsigned int channelId=channelPtr->get_Id();
                  if ((*moduleIter)->class_name() != "PreloadedReadoutModule") {
                     // fold in detector ID from ROS
                     channelId=(channelId & 0x0000ffff) | (detectorId << 16);
                  }

                  config->set("channelId", channelId, nChannels);
                  nChannels++;
               }
            }
         }
      }
      config->set("numberOfChannels", nChannels);

      // Load the ROI distribution array
#ifdef TDAQ191
      const std::vector<unsigned long int> roiDistribution=dbApp->get_ROIDistribution();
#else
     const std::vector<uint32_t> roiDistribution=dbApp->get_ROIDistribution();
#endif
      for (unsigned int loop=0; loop<roiDistribution.size(); loop++) {
         config->set("probRols", roiDistribution[loop], loop+1);
      }


      config->set("maxRolsInRoI", roiDistribution.size());
      // and the other generation parameters
      config->set("L2RequestFraction", dbApp->get_L2RequestFraction());
      config->set("EBRequestFraction", dbApp->get_EBRequestFraction());
      config->set("DeleteGrouping", dbApp->get_DeleteGrouping());

      config->set("tracingLevel", dbApp->get_TraceLevel());
      config->set("tracingPackage", dbApp->get_TracePackage());

      config->set("SyncDeletesWithEBRequests", dbApp->get_SyncDeletesWithEBRequests());

      // Garbage collection flag for TriggerGenerator
      config->set("TestGarbageCollection", dbApp->get_TestGarbageCollection());

      config->set("metPercent", metPercent);
      config->set("detectorId", detectorId);
      config->dump();  // DEBUG
   }

   //For compatibility reasons! (TriggerGenerator wants them)
   config->set("InputDelay",0);
   config->set("destinationNodeId",0);

   int dblevel = config->getInt("tracingLevel");
   int dbpackage = config->getInt("tracingPackage");
   DF::GlobalDebugSettings::setup(dblevel, dbpackage);
   
   //compute CRC table
   {
      const u_long crchighbit = (u_long) (1 << 15);
      const u_long polynom =  0x1021;
      for (int i = 0; i < 65536; i++) 
      {
	     u_long crc = i;

		 for (int j = 0; j < 16; j++) 
		 {
            u_long bit = crc & crchighbit;
            crc <<= 1;
            if (bit) 
             crc ^= polynom;
          }			
         crctab16[i] = crc & 0xffff; 
       }
	}

   try {
      std::cout << "Instantiating ROSTestDC, totalInstances " << totalInstances << " instance " << instance << std::endl;
		ROSTestDC *test;
		if (masterFlag)
		{
			// master
			test = new ROSTestDC(config, numberOfOutstandingRequests, instance, totalInstances, cookie, printData, testResponse,  waitUntilStartupTime,  masterFlag, slaveFlag, multiReadoutAppsFlag, controllerName.c_str());
		}
		else
		{
			if (slaveFlag)
			{
				// slave
				test = new ROSTestDC(config, numberOfOutstandingRequests, instance, totalInstances, cookie, printData, testResponse,  waitUntilStartupTime, masterFlag, slaveFlag, multiReadoutAppsFlag, masterName.c_str());
			}
			else
			{
				test = new ROSTestDC(config, numberOfOutstandingRequests, instance, totalInstances, cookie, printData, testResponse,  waitUntilStartupTime);
			}
		}

		if (p == 0)
		{
			// not specified on command line
			p = (unsigned int) (targetL1Freq * 1000.0 * timePerRun / ( (float) (nOutputsPerRun + 1) ) );
		}
		// printinterval needs to be equal to power of 2 in view of possible roll-over of event id's
		while (printinterval < p)
		{
			printinterval = printinterval << 1;
		}

		test->setMaxNumberOfOutputsPerRun(nOutputsPerRun);
		test->setPrintInterval(printinterval);
		test->setWaitingTime(waitingTime);
		test->setL2OrEBStepsize(l2OrEBStepSize);
		test->setStepSizeStopValue(stepSizeStopValue);
		test->setTargetL1Freq(targetL1Freq);
		test->setAvROLsPerRoI(avROLsPerRoI);
		test->setDoNotIterateFlag(doNotIterateFlag);
		test->setIterateOnEBFracFlag(iterateOnEBFracFlag);
        test->setPercentofEtmiss(percentOfEtmiss);
		test->syncParams();
		std::cout << "Max number of outputs per run: " << nOutputsPerRun << std::endl;
		std::cout << "Printinterval, hex: " << std::hex << printinterval << " dec: " << std::dec << printinterval << std::endl;
		std::cout << "L2 step size: " << l2OrEBStepSize << std::endl;
		std::cout << "L2 step size stop: " << stepSizeStopValue << std::endl;
		std::cout << "Target L1 Freq: " << targetL1Freq << std::endl;

		test->init(true);
      // float elapsed=test->run(totalNumberOfRequests);
		test->run(totalNumberOfRequests);

      TS_SAVE(TS_H1, "ROS_timing");
      TS_CLOSE(TS_H1);
  
      std::cout << std::endl << "Program finished" << std::endl;
		// std::cout <<std::endl << " Trigger statistics" <<std::endl ; 
      // test->printStatistics(elapsed);
   }
   catch (ConfigException& error) {
      std::cout << "Exception: " << error.what() << std::endl;
      exit(EXIT_FAILURE);
   }
}
