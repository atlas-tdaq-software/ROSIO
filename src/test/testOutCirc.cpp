static char Usage[] = "\
Usage: testOutCirc [-t] <key-name> <buf-length> <event-lenght> <repeat-count>\
";
#define USAGE printf("%s\n",Usage), exit(1)

#include <stdio.h>
#include <sched.h>
#include <signal.h>

#include "ROSEventFragment/HWFragment.h"
#include "ROSBufferManagement/Buffer.h"
#include "ROSMemoryPool/MemoryPool_malloc.h"
#include "DFSubSystemItem/Config.h"
#include "ROSIO/CircDataOut.h"
#include "ROSInfo/CircDataOutInfo.h"

using namespace RCD;
using namespace ROS;

bool stop = false;

void testOutCirc_sighandler (int sig)
{
  printf ("Got signal %d, cleaning up...\n", sig);
  stop = true;
}

/***************************************************************************/
int main (int argc, char *argv[])
/*---------------------------
*/
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
{
  char *key;
  int evtsize, repeat, i;
  int length, npages, pageSize = 8192;
  int bufsize = 0;
  int np = 0;

  bool novip = false;

  MemoryPool *memoryPool;

  signal (SIGINT,  testOutCirc_sighandler);
  signal (SIGQUIT, testOutCirc_sighandler);
  signal (SIGTERM, testOutCirc_sighandler);

  /* Get parameters form the command line */

  if (argc < 5)
    USAGE;

  if (argv[1][0] == '-') {
    if (argv[1][1] == 't') {
      novip = true; np++;
    }
    else
      USAGE;
  }

  if (argc != np + 5)
    USAGE;

  key     = argv[np+1];
  bufsize = atoi (argv[np+2]);
  evtsize = atoi (argv[np+3]);
  repeat  = atoi (argv[np+4]);

  npages = evtsize / pageSize + 1;
  length = evtsize;

  memoryPool = new MemoryPool_malloc (npages, pageSize);
  EventFragment *fragment = new HWFragment (memoryPool, length);

  /* Open the circular buffer */

  CircDataOut *circ = new CircDataOut ();

  DFCountedPointer<Config> conf = Config::New ();

  conf->set ("outputDelay", 0);
  conf->set ("bufSize", bufsize);
  conf->set ("outputCircName", key);
  conf->set ("samplingGap", 1);
  conf->set ("throwIfFull", novip);

  circ->setup (conf);
  circ->configure ();
  circ->prepareForRun ();

  for (i = 1; i != repeat; i++)
  {
    if (stop)
      break;

    circ->sendData (fragment->buffer(), 0);
    sched_yield ();
  }

  ROS::CircDataOutInfo *outinfo = dynamic_cast <ROS::CircDataOutInfo *> (circ->getISInfo ());

  unsigned int fragmentsOutput = outinfo->fragmentsOutput;
  unsigned int bufferFull = outinfo->bufferFull;
  float dataOutput = outinfo->dataOutput;

  std::cout << "testOutCirc : sent " << fragmentsOutput <<
          " fragments" << std::endl;
  std::cout << "testOutCirc : full " << bufferFull << std::endl;
  std::cout << "testOutCirc : sent " << dataOutput <<
          " MB" << std::endl;

  circ->stopEB ();
  circ->unconfigure ();

  delete circ;
  delete fragment;
  delete memoryPool;

  return 0;
}
