// -*- c++ -*-
// $Id$ 

#ifndef DCROSTEST_H
#define DCROSTEST_H
#include <vector>
#include <queue>

#include "ROSIO/TriggerGenerator.h"
#include "DFThreads/DFCountedPointer.h"

namespace ROS {
   class Config;

   class TriggerGeneratorTest : public ROS::TriggerGenerator {
   private:
      std::queue<unsigned int>  m_ebQueue;
      std::queue<unsigned int>  m_releaseQueue;
      unsigned int m_l2Requests;
      unsigned int m_ebRequests;
      unsigned int m_relRequests;
   public:
      TriggerGeneratorTest(DFCountedPointer<Config> triggerInconfiguration);
      void run(int maxRequests);
      unsigned int l2Requests() const { return m_l2Requests;}
      unsigned int ebRequests() const { return m_ebRequests;}
      unsigned int relRequests() const { return m_relRequests;}
   protected:
      virtual void l2Request(const unsigned int level1Id, const std::vector<unsigned int> * rols, const unsigned int destination, const unsigned int transactionId=1);
      virtual void ebRequest(const unsigned int l1Id, const unsigned int destination);
      virtual void releaseRequest(const std::vector<unsigned int>* level1Ids,
                                  const unsigned int oldestL1id);    


      virtual void ebQueuePush(unsigned int lvl1id);
      virtual unsigned int  ebQueuePop(void);
      virtual int  ebQueueSize(void);
      virtual bool ebQueueEmpty(void);

      virtual void releaseQueuePush(unsigned int lvl1id);
      virtual unsigned int  releaseQueuePop(void);
      virtual int  releaseQueueSize(void);
      virtual bool releaseQueueEmpty(void);

   };
}
#endif
