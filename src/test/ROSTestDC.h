// -*- c++ -*-
//

#ifndef ROSTESTDC_H
#define ROSTESTDC_H
#include <string>
#include <vector>
#include <queue>
#include "msg/Types.h"

#define SYNC_TRIGGER
#include "ROSIO/ROSTest.h"

namespace MessagePassing {
   class Port;
}

namespace ROS {
   class Config;

   class ROSTestDC : public ROSTest {
   private:
      std::string m_localHost;
      MessagePassing::NodeID m_localHostId;
      std::string m_remoteNodeAddress;
      MessagePassing::NodeID m_remoteNodeId;
      MessagePassing::Port* m_remotePort;
      MessagePassing::Port* m_rosPort;
      int m_numberOfOutstanding;
      bool m_testResponse;
      int m_cookie;
   public:
      ROSTestDC(DFCountedPointer<Config> configuration,
                int numberOfOutstanding,
                int instance=0, 
                int tot_instances=1,
                int cookie=0,
                bool print=false, bool testResponse=false);
      float run(int maxRequests);
   protected:
      virtual void l2Request(const unsigned int level1Id,
                             const std::vector<unsigned int> * rols,
                             const unsigned int destination,
                             const unsigned int transactionId=1);
      virtual void ebRequest(const unsigned int l1Id,
                             const unsigned int destination);
      virtual void releaseRequest(const std::vector<unsigned int>* level1Ids,
                                  const unsigned int oldestL1id);    

      //virtual void formatted_rod_print(int *, const eformat::write::RODHeader&);
      virtual void check_response(MessagePassing::Buffer* );
      virtual uint32_t getLvl1id(MessagePassing::Buffer* );
   };
}
#endif
