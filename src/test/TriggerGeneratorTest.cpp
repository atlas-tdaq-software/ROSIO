#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctype.h>
#include <string>
#include <sys/time.h> 
#include <getopt.h>
#include "TriggerGeneratorTest.h"
#include "DFSubSystemItem/Config.h"

using namespace ROS;


TriggerGeneratorTest::TriggerGeneratorTest(DFCountedPointer<Config> triggeInConfiguration)
  : TriggerGenerator(triggeInConfiguration)
{
   std::cout << "TriggerGeneratorTest constructor called" << std::endl;
}


void TriggerGeneratorTest::l2Request(const unsigned int /*level1Id*/,
                                     const std::vector<unsigned int> * /*rols*/,
	       const unsigned int /*destination*/,
	       const unsigned int /*transactionId*/)
{
//  std::cout << "TriggerGeneratorTest::l2Request  called" << std::endl;
}

void TriggerGeneratorTest::ebRequest(const unsigned int /*level1Id*/,
	       const unsigned int /*destination*/) 
{
//  std::cout << "TriggerGeneratorTest::ebRequest  called" << std::endl;
}

void TriggerGeneratorTest::releaseRequest(const std::vector<unsigned int>* /*level1Ids*/,
					  const unsigned int /*oldestL1id*/)
{
//  std::cout << "TriggerGeneratorTest::releaseRequest  called" << std::endl;
}


/*******************************************/
void TriggerGeneratorTest::ebQueuePush(unsigned int lvl1id) {
/*******************************************/
  m_ebQueue.push(lvl1id);
}

/*******************************************/
unsigned int TriggerGeneratorTest::ebQueuePop(void) {
/*******************************************/
  int ret = m_ebQueue.front();
  m_ebQueue.pop();
  return ret;
}

/*******************************************/
int TriggerGeneratorTest::ebQueueSize(void) {
/*******************************************/
  return m_ebQueue.size();
}

/*******************************************/
bool TriggerGeneratorTest::ebQueueEmpty(void) {
/*******************************************/
  return  m_ebQueue.empty();
}


/*******************************************/
void TriggerGeneratorTest::releaseQueuePush(unsigned int lvl1id) {
/*******************************************/
  m_releaseQueue.push(lvl1id);
}

/*******************************************/
unsigned int TriggerGeneratorTest::releaseQueuePop(void) {
/*******************************************/
  int ret = m_releaseQueue.front();
  m_releaseQueue.pop();
  return ret;
}

/*******************************************/
int TriggerGeneratorTest::releaseQueueSize(void) {
/*******************************************/
  return m_releaseQueue.size();
}

/*******************************************/
bool TriggerGeneratorTest::releaseQueueEmpty(void) {
/*******************************************/
  return  m_releaseQueue.empty();
}


void TriggerGeneratorTest::run(int maxRequests)
{
  int sent = generate(maxRequests, 0);
  if (sent == 0)
    std::cout << "no triggers have been sent. Most likely the trigger disabled itself to avoid a division by zero. Use the debug version to confirm" << std::endl;

}


void loadConfig(std::string name, DFCountedPointer<Config> configuration)
{
  std::string configDir;
  char* configDirfromEnvironment = getenv("DAQ_CONFIG_DIR");
  if (configDirfromEnvironment == 0) 
  {
    std::cout << "DAQ_CONFIG_DIR is not set. I don't know where to get the configuration!" << std::endl;
    exit (EXIT_FAILURE);
  }
  else
     configDir=configDirfromEnvironment;

  std::string fileName = configDir + "/" + name;
  std::ifstream configFile(fileName.c_str());
  if (!configFile) 
  {
    std::cerr << "cannot open " << fileName << " iostream has no error reporting so can't tell you why!" << std::endl;
    exit (EXIT_FAILURE);
  }

  const int lineSize = 1024;
  char buf[lineSize];

  while(configFile.getline(buf, lineSize)) 
  {
    if (buf[0] == '#') // skip comment lines
      continue;
    
    std::string line(buf);
    int equalsPosition = line.find('=');
    std::string key(line, 0, equalsPosition);
    line.erase(0, equalsPosition+1);
    std::string value(line);
    configuration->set(key, value);
  }
}


int main (int argc, char*argv[])
{
   struct option longOptions[] = 
   {
      {"outstanding", 1, 0, 'o'},
      {"number", 1, 0, 'n'},
      {0, 0, 0, 0}
   };

   int numberOfOutstandingRequests = 2;
   int totalNumberOfRequests = 5000;
   int optionIndex = 0;
   int option = 0;
   while (option != -1) 
   {
      option=getopt_long (argc, argv, "o:n:", longOptions, &optionIndex);
      switch (option) 
      {
      case 'o':
         if (isdigit(optarg[0])) 
            numberOfOutstandingRequests=strtol(optarg, 0, 10);
         break;
      case 'n':
         if (isdigit(optarg[0])) 
            totalNumberOfRequests=strtol(optarg, 0, 10);
         break;

      case ':':
      case '?':
         std::cerr << "Usage: " << argv[0]
              << " [--number=n] [--outstanding=m]\n"
              << "where <n> is the number of requests to generate\n"
              << "and m is the number of outstanding requests in the system\n";
         exit (0);
      }
   }

   DFCountedPointer<Config> triggerInConfig = Config::New();
   loadConfig("triggerInConfig.dat", triggerInConfig);

   TriggerGeneratorTest test(triggerInConfig);
   std::cout << "1" << std::endl;
   struct timeval startTime;
   struct timezone dummy;
   gettimeofday(&startTime,&dummy);

   test.run(totalNumberOfRequests);

   struct timeval endTime;
   gettimeofday(&endTime,&dummy);
   float elapsed = (endTime.tv_sec - startTime.tv_sec) + (endTime.tv_usec - startTime.tv_usec) / 1000000.0;
   float rate = (totalNumberOfRequests / 1000.0) / elapsed;
   std::cout << "Elapsed time: " << elapsed << "s  rate: " << rate << "kHz\n";
   std::cout << std::endl << "Emulated trigger statistics" << std::endl ; 
   test.printStatistics();
}
