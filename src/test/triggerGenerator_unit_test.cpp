#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctype.h>
#include <string>
#include <sys/time.h> 
#include <getopt.h>

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE ROS::TriggerGeneratorUnitTest
#include <boost/test/unit_test.hpp>

#include "TriggerGeneratorTest.h"
#include "DFSubSystemItem/Config.h"

using namespace ROS;


TriggerGeneratorTest::TriggerGeneratorTest(DFCountedPointer<Config> triggeInConfiguration)
   : TriggerGenerator(triggeInConfiguration),m_l2Requests(0),m_ebRequests(0)
{
   std::cout << "TriggerGeneratorTest constructor called" << std::endl;
}


void TriggerGeneratorTest::l2Request(const unsigned int /*level1Id*/,
                                     const std::vector<unsigned int> * /*rols*/,
	       const unsigned int /*destination*/,
	       const unsigned int /*transactionId*/)
{
//  std::cout << "TriggerGeneratorTest::l2Request  called" << std::endl;
   m_l2Requests++;
}

void TriggerGeneratorTest::ebRequest(const unsigned int /*level1Id*/,
	       const unsigned int /*destination*/) 
{
//  std::cout << "TriggerGeneratorTest::ebRequest  called" << std::endl;
   m_ebRequests++;
}

void TriggerGeneratorTest::releaseRequest(const std::vector<unsigned int>* /*level1Ids*/,
					  const unsigned int /*oldestL1id*/)
{
//  std::cout << "TriggerGeneratorTest::releaseRequest  called" << std::endl;
   m_relRequests++;
}


/*******************************************/
void TriggerGeneratorTest::ebQueuePush(unsigned int lvl1id) {
/*******************************************/
  m_ebQueue.push(lvl1id);
}

/*******************************************/
unsigned int TriggerGeneratorTest::ebQueuePop(void) {
/*******************************************/
  int ret = m_ebQueue.front();
  m_ebQueue.pop();
  return ret;
}

/*******************************************/
int TriggerGeneratorTest::ebQueueSize(void) {
/*******************************************/
  return m_ebQueue.size();
}

/*******************************************/
bool TriggerGeneratorTest::ebQueueEmpty(void) {
/*******************************************/
  return  m_ebQueue.empty();
}


/*******************************************/
void TriggerGeneratorTest::releaseQueuePush(unsigned int lvl1id) {
/*******************************************/
  m_releaseQueue.push(lvl1id);
}

/*******************************************/
unsigned int TriggerGeneratorTest::releaseQueuePop(void) {
/*******************************************/
  int ret = m_releaseQueue.front();
  m_releaseQueue.pop();
  return ret;
}

/*******************************************/
int TriggerGeneratorTest::releaseQueueSize(void) {
/*******************************************/
  return m_releaseQueue.size();
}

/*******************************************/
bool TriggerGeneratorTest::releaseQueueEmpty(void) {
/*******************************************/
  return  m_releaseQueue.empty();
}


void TriggerGeneratorTest::run(int maxRequests) {
}



BOOST_AUTO_TEST_CASE(TriggerGeneratorUnitTest){
   DFCountedPointer<Config> triggerInConfig = Config::New();
   triggerInConfig->set("DeleteGrouping",100);
   triggerInConfig->set("maxRolsInRoI",1);
   triggerInConfig->set("numberOfChannels",1);
   triggerInConfig->set("channelId#0",1);
   triggerInConfig->set("EBRequestFraction",100);
   triggerInConfig->set("InputDelay",0);
   triggerInConfig->set("destinationNodeId",0);
   triggerInConfig->set("probRols#1",100);
   triggerInConfig->set("SyncDeletesWithEBRequests",0);
   triggerInConfig->set("TestGarbageCollection",0);

   const int NEVENTS=1000000;
   std::vector<float> l2Fraction={0.0, 10.0,20.0,25.0};
   for (auto frac : l2Fraction) {
      triggerInConfig->set("L2RequestFraction",frac);
      auto test= new TriggerGeneratorTest(triggerInConfig);
      int sent=test->generateL1idBunch(1,NEVENTS);
      int ebRequests=test->ebRequests();
      int l2Requests=test->l2Requests();
      int totalRequests=ebRequests+l2Requests;
      BOOST_CHECK(totalRequests==sent);
      std::cout << "ebRequests=" << ebRequests
                << ", l2Requests=" << l2Requests
                << ", total=" << totalRequests << std::endl;
      BOOST_CHECK(totalRequests==NEVENTS);
      float l2FracGot=100.0*(float)l2Requests/(float)totalRequests;
      std::cout << "l2 fraction requested " << l2FracGot
                << ", l2 fraction wanted " << frac
                << ",  diff " << abs(l2FracGot-frac)
                << std::endl;
      BOOST_CHECK(abs(l2FracGot-frac)<1.0);
      delete test;
   }
}
