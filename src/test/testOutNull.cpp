static char Usage[] = "\
Usage: testOutNull <event-lenght> <repeat-count>\
";
#define USAGE printf("%s\n",Usage), exit(1)

#include <stdio.h>
#include <sched.h>

#include "ROSModules/RCDDummyModule.h"
#include "ROSEventFragment/HWFragment.h"
#include "ROSBufferManagement/Buffer.h"
#include "ROSMemoryPool/MemoryPool_malloc.h"
#include "DFSubSystemItem/Config.h"
#include "ROSIO/RCDNullDataOut.h"

using namespace RCD;
using namespace ROS;

/***************************************************************************/
int main (int argc, char *argv[])
/*---------------------------
*/
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
{
  int evtsize, repeat, i;
  int length, npages, pageSize = 8192;

  MemoryPool *memoryPool;

  /* Get parameters form the command line */
  if (argc != 3)
    USAGE;

  evtsize = atoi (argv[1]);
  repeat  = atoi (argv[2]);

  npages = evtsize / pageSize + 1;
  //length = (evtsize % sizeof (int) == 0 ? 0 : 1)+ evtsize / sizeof (int);
  length = evtsize;

  memoryPool = new MemoryPool_malloc (npages, pageSize);
  EventFragment *fragment = new HWFragment (memoryPool, length);

  /* Open the circular buffer */

  RCDNullDataOut *nullout = new RCDNullDataOut ();

  DFCountedPointer<Config> conf = Config::New ();

  conf->set ("outputDelay", 0);

  nullout->setup (conf);
  nullout->configure ();

  for (i = 0; i < repeat; i++) {
    nullout->sendData (fragment, 0);
    sched_yield ();
  }

  DFCountedPointer<Config> outconf = nullout->getInfo ();

  cout << "testOutNull : sent " << outconf->getInt ("FragmentsSent") <<
          " fragments" << endl;

  delete nullout;
  delete fragment;
  delete memoryPool;

  return 0;
}
