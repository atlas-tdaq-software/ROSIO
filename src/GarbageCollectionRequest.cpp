// $Id$
// //////////////////////////////////////////////////////////////////////
//  
//
//  Author:  G.J.Crone
//
//  $Log$
//  Revision 1.3  2008/01/28 18:38:20  gcrone
//  Use new getISInfo method, remove getInfo. Plus DataChannel interface is now in ROSCore
//
//  Revision 1.2  2007/11/07 16:36:48  gcrone
//  Fix problesm with missing std:: namespace qualifiers in front of string, vector etc.
//
//  Revision 1.1  2005/11/15 15:13:37  gcrone
//  New Request type for garbage collection
//
//
// //////////////////////////////////////////////////////////////////////
#include <iostream>
#include <unistd.h>

#include "rcc_time_stamp/tstamp.h"

#include "ROSIO/GarbageCollectionRequest.h"
#include "ROSCore/DataChannel.h"
#include "DFDebug/DFDebug.h"

using namespace ROS;


GarbageCollectionRequest::GarbageCollectionRequest(const unsigned int oldestLevel1Id,
                                                   std::vector<DataChannel*> * dataChannels) :
  Request(dataChannels), m_oldestLevel1Id(oldestLevel1Id)
{
}


int GarbageCollectionRequest::execute(void)
{
   DEBUG_TEXT(DFDB_ROSIO, 20, "GarbageCollectionRequest::execute   function entered");

   TS_RECORD(TS_H1,2410);

   for (std::vector<DataChannel*>::iterator iter=m_dataChannels->begin();
        iter!=m_dataChannels->end(); iter++) {
      TS_RECORD(TS_H1,2420);
      (*iter)->collectGarbage(m_oldestLevel1Id) ;
      TS_RECORD(TS_H1,2430);
   }

   TS_RECORD(TS_H1,2440);
   return(REQUEST_OK);
}

std::string GarbageCollectionRequest::what()
{
   std::string description="GarbageCollectionRequest";
   return (description);
}

std::ostream& GarbageCollectionRequest::put(std::ostream& stream) const
{
   stream << "GarbageCollectionRequest - oldest valid L1 Id " << m_oldestLevel1Id;
   return stream;
}
