// $Id$
// //////////////////////////////////////////////////////////////////////
//  Base class for a hardware driven TriggerIn
//
// $Log$
// Revision 1.12  2008/02/15 17:29:39  gcrone
// Adapt HardwareTriggerIn to new Fragment Rquest/Builder scheme.
//
// Revision 1.11  2008/01/28 18:38:22  gcrone
// Use new getISInfo method, remove getInfo. Plus DataChannel interface is now in ROSCore
//
// Revision 1.10  2007/04/23 11:53:07  gcrone
// Make all error messages use IOException
//
// Revision 1.9  2006/11/01 12:57:38  gcrone
// Use new ROSErrorReporting macros instead of dfout
//
// Revision 1.8  2006/03/09 19:04:35  gcrone
// IOMPlugin now uses new FSM methods
//
// Revision 1.7  2005/08/04 17:34:06  gcrone
// Remove obsolete Requests and add HWTriggeredRemoteRequest
//
// Revision 1.6  2005/07/27 17:49:50  gcrone
// Add debug and fix includes
//
// Revision 1.5  2005/07/01 11:48:07  gcrone
// Use HWTriggeredROSDataRequest and HWTriggeredRODEmulationRequest instead of HardwareTriggeredRequest and HardwareTriggeredRODEmulationRequest
//
// Revision 1.4  2005/07/01 11:36:48  gcrone
// Use HWTriggeredROSDataRequest and HWTriggeredRODEmulationRequest instead of HardwareTriggeredRequest and HardwareTriggeredRODEmulationRequest
//
// Revision 1.3  2005/06/22 11:12:26  gcrone
// Debug output added.
//
// Revision 1.2  2005/05/29 14:08:27  gcrone
// Use conventional getFragment() with L1 ID generated in HardwareTriggerIn.
//
// Revision 1.1  2005/05/27 09:03:26  gcrone
// Added HardwareTriggerIn and related Requests
//
//
//
// //////////////////////////////////////////////////////////////////////
#include <ostream>                             // for ostringstream, basic_o...
#include <string>                              // for operator==, allocator

#include "ROSIO/HardwareTriggerIn.h"
#include "ROSIO/HWTriggeredFragmentRequest.h"
#include "ROSIO/RODFragmentBuilder.h"
#include "ROSIO/ROSFragmentBuilder.h"
#include "ROSIO/HWTriggeredRemoteRequest.h"
#include "ROSIO/IOException.h"
#include "ROSCore/IOManager.h"                 // for IOManager
#include "DFThreads/DFThread.h"                // for DFThread
#include "DFSubSystemItem/Config.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "DFDebug/DFDebug.h"
#include "DFThreads/DFCountedPointer.h"        // for DFCountedPointer
#include "ROSInfo/TriggerInInfo.h"             // for TriggerInInfo

class ISInfo;
namespace ROS { class Request; }
namespace daq { namespace rc { class TransitionCmd; } }

using namespace ROS;

HardwareTriggerIn::HardwareTriggerIn() : m_requestType(RT_NULL), m_builder(0) {
   DEBUG_TEXT (DFDB_ROSIO, 10, "HardwareTriggerIn::constructor entered");
  m_triggerActive=false; 
  m_level1Id=0;
}
HardwareTriggerIn::~HardwareTriggerIn() noexcept{
   DEBUG_TEXT (DFDB_ROSIO, 10, "HardwareTriggerIn::destructor entered");
}

void HardwareTriggerIn::setup(IOManager* iomanager, DFCountedPointer<Config> configuration)
{
   DEBUG_TEXT (DFDB_ROSIO, 10, "HardwareTriggerIn::setup entered");
   initialise(iomanager);

   std::string requestName=configuration->getString("RequestType");
   if (requestName=="HWTriggeredROSDataRequest") {
      DEBUG_TEXT (DFDB_ROSIO, 15, "HardwareTriggerIn::setup requestType is ROS");
      m_requestType=RT_ROS;
      m_builder=new ROSFragmentBuilder;
   }
   else if (requestName=="HWTriggeredRODEmulationRequest") {
      DEBUG_TEXT (DFDB_ROSIO, 15, "HardwareTriggerIn::setup requestType is ROD");
      m_requestType=RT_ROD;
      m_builder=new RODFragmentBuilder;
   }
   else if (requestName=="HWTriggeredRemoteRequest") {
      DEBUG_TEXT (DFDB_ROSIO, 15, "HardwareTriggerIn::setup requestType is Null Data");
      m_requestType=RT_NULL;
   }
   else {
      CREATE_ROS_EXCEPTION(ex, IOException, UNKNOWN_REQ, "");
      throw(ex);
   }
}

void HardwareTriggerIn::prepareForRun(const daq::rc::TransitionCmd&){ 
   DEBUG_TEXT (DFDB_ROSIO, 10, "HardwareTriggerIn::prepareForRun entered");
   m_triggerActive=true; 
   m_level1Id=0;
   m_stats.numberOfLevel1=0;
   startExecution();
}

void HardwareTriggerIn::stopGathering(const daq::rc::TransitionCmd&){
   DEBUG_TEXT (DFDB_ROSIO, 10, "HardwareTriggerIn::stopGathering entered");
   m_triggerActive=false; 

  stopExecution();
}

void HardwareTriggerIn::run()
{
   DEBUG_TEXT(DFDB_ROSIO, 10, "HardwareTriggerIn::run entered");
   while (m_triggerActive) {
      DFThread::cancellationPoint();
      if (waitForTrigger()) {
         Request* myRequest;
         DEBUG_TEXT(DFDB_ROSIO, 19, "HardwareTriggerIn::run got trigger, generating Request");
         switch (m_requestType) {
         case  RT_ROS:
         case RT_ROD:
            myRequest=new HWTriggeredFragmentRequest(m_level1Id, m_builder, this);
            break;
         case RT_NULL:
         default:
            myRequest=new HWTriggeredRemoteRequest(m_level1Id, this);
            break;
         }

         // Pass it to IOManager
         DEBUG_TEXT(DFDB_ROSIO, 19, "HardwareTriggerIn::run queueing Request");
         m_ioManager->queueRequest(myRequest);
         m_level1Id++;
         m_stats.numberOfLevel1++;
      }
      else {
         DEBUG_TEXT(DFDB_ROSIO, 19, "HardwareTriggerIn::run waitForTrigger() returned not ready");
         DFThread::yieldOrCancel();
      }
   }
   DEBUG_TEXT(DFDB_ROSIO, 10, "HardwareTriggerIn::run done");
}

void HardwareTriggerIn::cleanup(){
   DEBUG_TEXT(DFDB_ROSIO, 10, "HardwareTriggerIn::cleanup");
}

ISInfo* HardwareTriggerIn::getISInfo()
{
   return &m_stats;
}

