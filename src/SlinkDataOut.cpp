//*****************************************************************************/
// file: SlinkDataOut.cpp
// desc: DataOut using S-Link, e.g. for ROD emulation
// auth: 07/02/03 R. Spiwoks
//*****************************************************************************/

// $Id$

#include <iostream>
#include <sys/types.h>
#include <stdio.h>                          // for stderr
#include <string.h>                         // for memcpy
#include <string>                           // for allocator, basic_string
#include "DFThreads/DFCountedPointer.h"     // for DFCountedPointer
#include "DFThreads/DFFastMutex.h"          // for DFFastMutex
#include "ROSCore/DataOut.h"                // for DataOut (ptr only), NodeID
#include "ROSInfo/DataOutInfo.h"            // for DataOutInfo
#include "ROSMemoryPool/MemoryPage.h"       // for MemoryPage
#include "ROSMemoryPool/MemoryPool.h"       // for MemoryPool
#include "ROSMemoryPool/MemoryPool_CMEM.h"  // for MemoryPool_CMEM

#include "DFSubSystemItem/Config.h"
#include "ROSBufferManagement/Buffer.h"
#include "rcc_error/rcc_error.h"            // for rcc_error_print

#include "ROSIO/SlinkDataOut.h"
#include "ROSslink/slink.h"                 // for SLINK_parameters, SLINK_C...

class ISInfo;
namespace daq { namespace rc { class TransitionCmd; } }


using namespace ROS;

//------------------------------------------------------------------------------

SlinkDataOut::SlinkDataOut() : m_sendMutex(0) {

#ifdef DEBUG
    std::cout << "SlinkDataOut::SlinkDataOut: DEBUG" << std::endl;
#endif

    // open S-Link library	 
    if((m_status = SLINK_Open()) != 0) {
	    std::cerr << "SlinkDataOut::SlinkDataOut: ERROR - opening S-Link library"
	     << std::endl;
	rcc_error_print(stderr,m_status);
        return;
    }
    std::cout << "SlinkDataOut::SlinkDataOut: INFO - S-Link library opened" << std::endl;

    m_statistics.fragmentsOutput = 0;
    m_statistics.dataOutput = 0;
    m_memoryPool = new MemoryPool_CMEM(1,65536); //REMEMBER TO MAKE IT PROGRAMMABLE!!!!!!!
}

//------------------------------------------------------------------------------

SlinkDataOut::~SlinkDataOut() noexcept {

#ifdef DEBUG
    std::cout << "SlinkDataOut::~SlinkDataOut: DEBUG" << std::endl;
#endif

    // close S-Link library
    SLINK_Close();
    std::cout << "SlinkDataOut::~SlinkDataOut: INFO - S-Link library closed" << std::endl;

    m_sendMutex->destroy();
    delete m_memoryPool;
}

//------------------------------------------------------------------------------

void SlinkDataOut::setup(DFCountedPointer<Config> cfg) {

    m_occurrence = cfg->getInt("occurrence");
    std::ostringstream str;
    str << "_SLINKDATAOUTSENDMUTEX" << m_occurrence ;
    char * mutexName = const_cast<char *>(str.str().c_str());
    m_sendMutex = DFFastMutex::Create(mutexName);

    std::cout << "SlinKDataOut::setup: INFO - occurrence = " << m_occurrence << std::endl;
}

//------------------------------------------------------------------------------

void SlinkDataOut::sendData(const Buffer* buffer,
                            NodeID /*dst*/,
                            u_int /*tid*/,
                            unsigned int /*fragStatus*/) {

    int		status;

#ifdef DEBUG
    std::printf("SlinkDataOut::sendData: INFO - sending buffer, dst = %d, tid = %d\n",dst,tid);
    //    buffer->print();
#endif

    // This is where we should actually write out data
    
    m_sendMutex->lock();

    //Take a page 
    MemoryPage *page = m_memoryPool->getPage();

    //Now copy the data (SLINK wants contiguous fragments!)
    for (Buffer::page_iterator p=buffer->begin(); p!=buffer->end(); p++) {
 	const void *src = (*p)->address();
	int size = (*p)->usedSize();
	void *dest = page->reserve(size);
	memcpy(dest,src,size);
    }

    char *phys = (char *) page->physicalAddress();
    int totalSize = page->usedSize();

    // send S-Link packet
    if((m_status = SPS_InitWrite(m_dev,(char*)phys,totalSize,SLINK_BOTH_CONTROL_WORDS,SLINK_ENABLE)) != 0) {
      std::cerr << "SlinkDataOut::sendData: ERROR received from call to SPS_InitWrite" << std::endl;
      rcc_error_print(stderr,m_status);
      return;
    }
    
    // wait for transfer to finish
    do {
      if((m_status = SPS_ControlAndStatus(m_dev,&status)) != 0) {
	std::cerr << "SlinkDataOut::sendData: ERROR - waiting for transfer to finish" << std::endl;
	rcc_error_print(stderr,m_status);
	return;
      }
    } while (status != SLINK_FINISHED);
    
    // update statistics
    m_statistics.dataOutput += totalSize / (float)(1024*1024);
    m_statistics.fragmentsOutput++;

    page->free();

    m_sendMutex->unlock();
}

//------------------------------------------------------------------------------

void SlinkDataOut::configure(const daq::rc::TransitionCmd&) {

    // initialize S-Link parameters
    SLINK_InitParameters(&m_par);        

    // override some parameters
    m_par.occurence = m_occurrence;
    m_par.byte_swapping = SLINK_ENABLE; //MAKE IT A PARAMETER IN DATABASE!!!!!!!!!! 
    m_par.start_word = 0x0000f0b0;
    m_par.stop_word  = 0x0000f0e0;

#ifdef DEBUG
    // dump S-Link parameters
    std::cout << "SlinkDataOut::connect: DEBUG - S-Link parameters:" << std::endl;
    std::printf("    data width    = %8d\n",m_par.data_width);
    std::printf("    occurrence    = %8d\n",m_par.occurence);
    std::printf("    time-out      = %8d\n",m_par.timeout);
    std::printf("    byte swapping = %8d\n",m_par.byte_swapping);
    std::printf("    start word    = %08x\n",m_par.start_word);
    std::printf("    stop word     = %08x\n",m_par.stop_word);
#endif

    // open S-Link device
    if((m_status = SLINK_SPSOpen(&m_par,&m_dev)) != 0) {
	    std::cerr << "SlinkDataOut::connect: ERROR - opening S-Link device" << std::endl;
	rcc_error_print(stderr,m_status);
	return;
    }

#ifdef DEBUG 
    // dump S-Link device status
    SLINK_PrintState(m_dev);
#endif

    std::cout << "SlinkDataOut::connect: INFO - S-Link connection created" << std::endl;
}

//------------------------------------------------------------------------------

void SlinkDataOut::unconfigure(const daq::rc::TransitionCmd&) {

    // close S-Link device
    if((m_status = SLINK_SPSClose(m_dev)) != 0) {
	    std::cerr << "SlinkDataOut::disconnect: ERROR - closing S-Link device"
	     << std::endl;
	rcc_error_print(stderr,m_status);
    }

    std::cout << "SlinkDataOut::disconnect: INFO - S-Link connection closed" << std::endl;
}


//------------------------------------------------------------------------------

ISInfo* SlinkDataOut::getISInfo() {
   return &m_statistics;
}

//------------------------------------------------------------------------------
// shared library entry point

extern "C" {
  extern DataOut* createSLinkDataOut();
}

DataOut* createSLinkDataOut() {
  return (new SlinkDataOut());
}
