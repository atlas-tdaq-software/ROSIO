// $Id$
// //////////////////////////////////////////////////////////////////////
//
// B.Gorini, G. Crone, M.Joos, J.Petersen, G.Unel CERN/PH/ATD
//
// $Log$
// Revision 1.26  2008/03/20 12:49:37  jorgen
//  add NIKHEF code for testing MRODs
//
// Revision 1.25  2007/11/13 12:38:23  jorgen
//  move code for testing of GC from generate() to generateRequestsForOneL1ID()
//
// Revision 1.24  2007/11/07 16:36:48  gcrone
// Fix problesm with missing std:: namespace qualifiers in front of string, vector etc.
//
// Revision 1.23  2006/11/30 13:11:12  gcrone
// Use CREATE_ROS_EXCEPTION for instantiating all exceptions
//
// Revision 1.22  2006/11/01 12:57:38  gcrone
// Use new ROSErrorReporting macros instead of dfout
//
// Revision 1.21  2006/04/05 13:41:36  gcrone
// Report warning and disable trigger generation if numberOfChannels is
// 0, same as when meanNumberOfRolsPerL2Request is 0.
//
// Revision 1.20  2006/01/23 07:37:52  jorgen
//  remove test on number of channels
//
// Revision 1.19  2005/12/12 15:41:22  gcrone
// Include <algorithm> for min_element with icc8
//
// Revision 1.18  2005/12/01 13:16:55  jorgen
//  add emulation & testing of garbage collection
//
// Revision 1.17  2005/11/03 16:48:55  gcrone
// Updated config item names to be as given in the database so that ROSDBConfig doesn't need to translate
//
// Revision 1.16  2005/10/27 08:32:22  jorgen
//  restructure for use with DDT/ECR
//
// Revision 1.15  2005/09/03 13:55:50  jorgen
//  fix problems with SYNC queues in trigger generator
//
// Revision 1.14  2005/08/25 11:34:40  gcrone
// TriggerGenerator is the sync version now
//
//
// //////////////////////////////////////////////////////////////////////

#include <vector>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <pthread.h>                         // for pthread_testcancel
#include <cstdlib>                          // for RAND_MAX, rand, srand
#include <queue>                             // for queue
#include <set>                               // for set, operator!=, operator==
#include "DFThreads/DFCountedPointer.h"      // for DFCountedPointer
#include "ers/ers.h"                         // for error


#include "rcc_time_stamp/tstamp.h"

#include "ROSIO/TriggerGenerator.h"

#include "ROSIO/IOException.h"
#include "DFDebug/DFDebug.h"
#include "DFSubSystemItem/Config.h"

#include "ROSUtilities/ROSErrorReporting.h"

using namespace ROS;



TriggerGenerator::TriggerGenerator(const std::vector<unsigned int>& channelList,
                                   const std::vector<unsigned int>& roiDistribution,
                                   int deleteGrouping,
                                   float l2RequestFraction,
                                   float ebRequestFraction,
                                   unsigned int maxL1,
                                   unsigned int eventsPerEcr)
   : m_deleteGrouping(deleteGrouping),
     m_testGarbageCollection(false),
     m_syncDeletesWithEBRequests(false),
     m_level1Id(0),
     m_disabled(false),
     m_channelIds(channelList),
     m_l2puGroup(roiDistribution),
     m_inputDelay(0),
     m_maxL1Index(maxL1),
     m_useList(false),
     m_eventsPerEcr(eventsPerEcr),
     m_ecrCount(0) {
   DEBUG_TEXT(DFDB_ROSTG, 15, "TriggerGenerator: constructor:  called");

   m_instance = 0;		// may be overwritten by ROSTestDC  FIXME
   m_totalInstances = 1;

   m_numberOfChannels=channelList.size();
   if (m_numberOfChannels==0) {
      CREATE_ROS_EXCEPTION (tException, IOException, IOException::DIVBY0,
                            "Number of channels is zero. All triggers will be disabled. ");
      ers::error( tException );
      m_disabled=true; 
   }

   m_l2ap_i = (int) ((ebRequestFraction / 100.0) * (double)RAND_MAX);
   m_l2puGroup.insert(m_l2puGroup.begin(),0);
   m_maxRolsInRoI=roiDistribution.size();
   configure(l2RequestFraction,ebRequestFraction);
}



/********************************************************************************/
TriggerGenerator::TriggerGenerator(DFCountedPointer<Config> configuration) {
/********************************************************************************/
  DEBUG_TEXT(DFDB_ROSTG, 15, "TriggerGenerator: constructor:  called");

   m_maxL1Index=0xffffffff;

   m_level1Id=0;
   m_instance = 0;		// may be overwritten by ROSTestDC  FIXME
   m_totalInstances = 1;
   m_useList=false;
  m_disabled=false;

  // garbage collection
  m_testGarbageCollection = false;
  m_oldestL1id = 0;

  m_next = 0;
  m_transactionId = 1;


  //Delete grouping
  m_deleteGrouping = configuration->getInt("DeleteGrouping");
  DEBUG_TEXT(DFDB_ROSTG, 20, "TriggerGenerator: constructor:  delete grouping = " << m_deleteGrouping);

  //Reserve space for the vector of l1Ids for deletes
  m_l1Ids.reserve(m_deleteGrouping);

  //Max size of ROI requests
  m_maxRolsInRoI = configuration->getInt("maxRolsInRoI");

  //Number of ROLs
  m_numberOfChannels =  configuration->getInt("numberOfChannels");
  if (m_numberOfChannels==0) {
     CREATE_ROS_EXCEPTION (tException, IOException, IOException::DIVBY0,
                           "Number of channels is zero. All triggers will be disabled. ");
     ers::error( tException );
     m_disabled=true; 
  }
  for (int i=0; i<m_numberOfChannels; i++) 
  {
    m_channelIds.push_back(configuration->getInt("channelId", i));
    DEBUG_TEXT(DFDB_ROSTG, 20, "TriggerGenerator: constructor:  found channel # " << m_channelIds[i]);
  }

  //Reserve space for vector of rols for L2 requests
  m_rols.reserve(m_numberOfChannels);  

  //Probability of an L2PU request
  float l2RequestFraction = configuration->getFloat("L2RequestFraction");
   
  //Probability of an EB request
  float ebRequestFraction = (configuration->getFloat("EBRequestFraction"));
  m_l2ap_i = (int) ((ebRequestFraction / 100.0) * (double)RAND_MAX);
  DEBUG_TEXT(DFDB_ROSTG, 20, "TriggerGenerator: constructor:   L2A % scaled up = " << m_l2ap_i);
  DEBUG_TEXT(DFDB_ROSTG, 20, "TriggerGenerator: constructor:   RAND_MAX = " << RAND_MAX);

  //Parameters for DC simulation
  m_inputDelay = configuration->getInt("InputDelay");
  m_destination = configuration->getInt("destinationNodeId");
  
  //Parameters to control the number of ROLs involved in a L2PU request    
  m_l2puGroup.reserve(m_maxRolsInRoI + 1);
  for(int i = 0; i< m_maxRolsInRoI; i++) 
  {
    m_l2puGroup[i + 1] = configuration->getInt("probRols", i + 1);
  }

// NIKHEF parameter ..

  m_syncDeletesWithEBRequests = configuration->getBool("SyncDeletesWithEBRequests");
  DEBUG_TEXT(DFDB_ROSTG, 20, "TriggerGenerator: constructor: m_syncDeletesWithEBRequests = " << m_syncDeletesWithEBRequests);

// garbage collection parameters
  m_testGarbageCollection = configuration->getBool("TestGarbageCollection");

  configure(l2RequestFraction,ebRequestFraction);
}


void TriggerGenerator::clearStatistics() {
   m_numberOfL1ids = 0;
   m_numberOfL2A = 0;
   m_totalNumberOfROI=0;
   m_numberOfL2req=0;
   m_numberOfDeleteGroup = 0;

   // Make sure there's an entry for every channel
   m_roihist.resize(m_channelIds.size()+1); // +1 for range 0 - n channels
   std::fill(m_roihist.begin(),m_roihist.end(),0);
   m_numberOfROI.resize(m_channelIds.size());
   std::fill(m_numberOfROI.begin(),m_numberOfROI.end(),0);

// Why are these vectors if they're of constant size???
   m_qEBhist.resize(c_maxEBbins);
   for (int i = 0; i < c_maxEBbins; i++) {
      m_qEBhist[i]=0;
   }

   m_qCLEARhist.resize(c_maxCLEARbins);
   for (int i = 0; i < c_maxCLEARbins; i++) {
      m_qCLEARhist[i]=0;
   }

   m_setINPROCESShist.resize(c_maxINPROCESSbins);
   for (int i = 0; i < c_maxINPROCESSbins; i++) {
      m_setINPROCESShist[i]=0;
   }
}


void TriggerGenerator::configure(float l2RequestFraction,
                                 float ebRequestFraction) {
   clearStatistics();

   //Configuration dump
   std::cout << "TriggerGenerator: L2Request fraction " << l2RequestFraction
             << "% per ROL (" << std::dec << m_numberOfChannels << ")"  << std::endl;
   std::cout << " EBRequest fraction " << ebRequestFraction << "%" << std::endl;
   std::cout << " deletes grouped in " << std::dec << m_deleteGrouping << " s" << std::endl;
   std::cout << " input delay " << std::dec << m_inputDelay << " microseconds" << std::endl;
   for(int i=0; i< m_maxRolsInRoI; i++) {
    std::cout << "TriggerGenerator: m_l2puGroup[" << i+1 << "] = " << m_l2puGroup[i+1] << std::endl;
   }

   std::cout << "TriggerGenerator: m_testGarbageCollection   = " << m_testGarbageCollection << std::endl;

   int sum = 0;
   for(int loop = 1; loop < (m_maxRolsInRoI + 1); loop++) {
      sum += m_l2puGroup[loop];
      DEBUG_TEXT(DFDB_ROSTG, 20, "TriggerGenerator: constructor:   sum = " << sum);
   }
  
   if (sum != 100 && m_maxRolsInRoI) {
      std::cout << "The sum of the probuXXRols parameters is " << sum << " instead of 100" << std::endl;
      CREATE_ROS_EXCEPTION(tException, IOException, IOException::TGSUM,
                           "The sum of the probuXXRols parameters is " << sum << " instead of 100");
      throw tException;
   }


   //Vector of random numbers
   srand(3);	// the seed, why not ..
   for(int loop = 0; loop < c_aleasize; loop++) {
      m_alea[loop] = rand();
   }


   //Compute some constants for the creation of L2PU requests
   float meanNumberOfRolsPerL2Request = 0;
   for(int loop = 1; loop < (m_maxRolsInRoI + 1); loop++) {
      meanNumberOfRolsPerL2Request += (float)m_l2puGroup[loop] / 100.0 * loop;
   }

   if (meanNumberOfRolsPerL2Request == 0) {
      CREATE_ROS_EXCEPTION(tException, IOException, IOException::DIVBY0,
                           "\n meanNumberOfRolsPerL2Request is zero. All triggers will be disabled");
      ers::error(tException);
      m_disabled=true; 
   }

   float l2FractionPerRos = (float)m_numberOfChannels * l2RequestFraction / meanNumberOfRolsPerL2Request;
   if (l2FractionPerRos > 100.)  {
      std::cout << "The fraction of ROI requests per ROS exceeds 100%: please increase the mean l2 grouping or decrease the l2 fraction!" << std::endl ;
      std::cout << "l2FractionPerRos             = " << l2FractionPerRos << std::endl;
      std::cout << "m_numberOfChannels           = " << std::dec << m_numberOfChannels << std::endl;
      std::cout << "l2RequestFraction            = " << l2RequestFraction << std::endl;
      std::cout << "meanNumberOfRolsPerL2Request = " << meanNumberOfRolsPerL2Request << std::endl;
      CREATE_ROS_EXCEPTION (tException, IOException, IOException::TGL2F, "");
      throw tException;  
   }
	
   std::vector<float> absoluteL2puGroup(m_maxRolsInRoI + 1);
   absoluteL2puGroup[0] = 100.0;
   for(int loop = 1; loop < (m_maxRolsInRoI + 1); loop++) {
      absoluteL2puGroup[loop] = m_l2puGroup[loop] / 100.0 * l2FractionPerRos;
      absoluteL2puGroup[0] -= absoluteL2puGroup[loop];
   }

   m_l2puLimit.reserve(m_maxRolsInRoI);  
   float integratedL2puGroup = 0.;
   for(int loop = 0; loop < m_maxRolsInRoI; loop++) {
      integratedL2puGroup += absoluteL2puGroup[loop];
      m_l2puLimit[loop] = (unsigned int)(integratedL2puGroup / 100.0 * (float)RAND_MAX);
   }
}

void TriggerGenerator::setL1List(std::vector<unsigned int>& l1List){
   m_l1List=l1List;
   m_useList=true;
   m_maxL1Index=m_l1List.size();
}

void TriggerGenerator::resetL1(){
   if (m_useList) {
      m_l1Index=m_instance;
      m_level1Id=m_l1List[m_l1Index];
   }
   else {
      m_l1Index=m_instance;
      m_level1Id = m_instance;
   }
}
void TriggerGenerator::nextL1(){
   // make sure we can run w/ multiple instances of ROSTester and
   // *not* use the same events.

   if (m_l1Index+m_totalInstances<m_maxL1Index) {
      m_l1Index+=m_totalInstances;
   }
   else {
      m_l1Index=m_instance;
   }

   if (m_useList) {
      m_level1Id=m_l1List[m_l1Index];
   }
   else {
      if (m_eventsPerEcr) {
         if (m_l1Index>=m_eventsPerEcr) {
            m_l1Index=m_l1Index%m_eventsPerEcr;
            m_ecrCount++;
         }
         m_level1Id=((m_ecrCount&0xff)<<24)|m_l1Index;
      }
      else {
         m_level1Id=m_l1Index;
      }
   }
   m_numberOfL1ids++; //= (m_level1Id-m_instance)/m_totalInstances;
}

/**
  Generates L2 requests, EB requests and release requests in proportion controlled by the configuration.
*/

/**************************************************************/
int TriggerGenerator::generate(int nRequests, int mode)
/**************************************************************/
{
  //Definition of mode:
  //0 = continue with old values (e.g. L1ID)
  //1 = re-start from zero
  
  TS_RECORD(TS_H1, 1000);	// request handle start
  DEBUG_TEXT(DFDB_ROSTG, 15, "TriggerGenerator: generate:  called");
 
  if(m_disabled) {
    DEBUG_TEXT(DFDB_ROSTG, 20, "TriggerGenerator: generate:  m_disabled -> nothing to do -> return(0)");
    return(0);
  }
 
  if (mode==1) {
    DEBUG_TEXT(DFDB_ROSTG, 20, "TriggerGenerator: generate:  mode = 1: Restarting from L1ID = 0");
    m_next = 0;
    resetL1();
    m_transactionId = 1;
  }
    
  bool forever = false;
  if (nRequests == 0) forever = true;
  int requestsGenerated = 0;

  while ((forever || (requestsGenerated < nRequests)) && condition()) { 
    requestsGenerated += generateRequestsForOneL1ID(m_level1Id);
    nextL1();
 //    TS_RECORD(TS_H1, 1490);  
  }
  pthread_testcancel(); //MJ: how would the loop be terminated if forever==true?
  DEBUG_TEXT(DFDB_ROSTG, 20, "TriggerGenerator: generate:  # data requests generated  = " << requestsGenerated);
  return(requestsGenerated);
}

/****************************************************************************************/
int TriggerGenerator::generateL1idBunch(unsigned int firstL1id, unsigned int lastL1id)
/****************************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSTG, 15, "TriggerGenerator: generateL1idBunch:  entered");
 
  int requestsGenerated = 0;

  for (unsigned l1id = firstL1id; l1id <=lastL1id; l1id++) {
    requestsGenerated += generateRequestsForOneL1ID(l1id);
    m_numberOfL1ids++;
  }

  pthread_testcancel(); //MJ: how would the loop be terminated if forever==true?
  DEBUG_TEXT(DFDB_ROSTG, 20, "generateL1idBunch:  # data requests generated  = " << requestsGenerated);
  return(requestsGenerated);
}



/******************************************************/
void TriggerGenerator::set_level1Id(int /*l1id*/) {
/******************************************************/
   resetL1();


    m_l1Ids.clear();
    m_l1IdsInProcess.clear();
    while (!m_queue_ebrequest_ids.empty()) {
       m_queue_ebrequest_ids.pop();
    }
    while (!m_queue_all_ids.empty()) {
       m_queue_all_ids.pop();
    }
    while (!m_queue_ebfinished_ids.empty()) {
       m_queue_ebfinished_ids.pop();
    }

}

/*******************************************************************/
int TriggerGenerator::generateRequestsForOneL1ID(unsigned int l1id) 
/*******************************************************************/
{
  int requestsGenerated = 0;

  TS_RECORD(TS_H1, 1100);

  int rolsInThisRoI = noRolsInThisRoI();
  DEBUG_TEXT(DFDB_ROSTG, 15, "generateRequestForOneL1ID: L1id = 0x" << std::hex << l1id
                             << " # Rols in ROI = " << std::dec << rolsInThisRoI);
    
//    TS_RECORD(TS_H1,1110);
  bool L1id_L2ed = false;
  if (rolsInThisRoI > 0) {
    m_numberOfL2req++;
    generateL2Request(l1id);
    requestsGenerated++;
    L1id_L2ed=true;

    if (m_testGarbageCollection) {
      m_l1IdsInProcess.insert(l1id);	// L1ID "busy" with Level2
      DEBUG_TEXT(DFDB_ROSTG, 20, "TriggerGenerator: generate: inserted L1id = " <<
                                 l1id << " in busy set " << " of size " << m_l1IdsInProcess.size());
   
      // update  set size histogram
      int setIPsize = m_l1IdsInProcess.size();
      if (setIPsize < (c_maxINPROCESSbins -1)) {
        m_setINPROCESShist[setIPsize]++;
      }
      else {				// overflow
        m_setINPROCESShist[c_maxINPROCESSbins -1]++;
      }
    }
  }

//    TS_RECORD(TS_H1, 1190);
  // we are done with this L1ID, if not sent to ROS for q request we put it into a Q.

  if (!L1id_L2ed) {
    DEBUG_TEXT(DFDB_ROSTG, 20, "generateRequestForOneL1ID:  if no l2Request, l1id = " <<
                               std::hex << l1id << " on EB Q of size " << std::dec << ebQueueSize());
    ebQueuePush(l1id);
  }

  //EB Requests ---------------------------------------

  // handle ONE L1id from the EB Q
  // the L1ids "without" L2 request will push ONE request onto the EB Q (code above)
  // and remove ONE immediately afterwards: NOT necessarily the same ONE if Q is non-empty!!
  // If L1ids are pushed onto the Q from getLvlId(), the Q size may increase.
  // If (several) L2 requests are generated, the Q will be emptied since NO request is put
  // on the Q in the trigger generator (code above); still, if the Q is non-empty, we go
  // through the code below and remove one L1id

  if ( ! ebQueueEmpty() ) 
  {
//      TS_RECORD(TS_H1, 1200);

    // update EB Q size histogram
    int qEBsize = ebQueueSize();
    if (qEBsize < (c_maxEBbins -1)) {
      m_qEBhist[qEBsize]++;
    }
    else {				// overflow
      m_qEBhist[c_maxEBbins -1]++;
    }

    int level1IdEB = ebQueuePop(); // get L1 id to decide on its fate.

    DEBUG_TEXT(DFDB_ROSTG, 20, "generateRequestForOneL1ID:  EB request, popped l1id = " <<
                               std::hex << level1IdEB << " from EB Q of size " << std::dec << ebQueueSize());

    bool L1id_EBed = false;
    if (m_alea[m_next] <= m_l2ap_i) 
    {   
//	TS_RECORD(TS_H1, 1210);
      m_numberOfL2A++;
      generateEBRequest(level1IdEB);
//	TS_RECORD(TS_H1, 1290);
      L1id_EBed=true;
      requestsGenerated++;

      if (m_syncDeletesWithEBRequests) {
        m_queue_ebrequest_ids.push(level1IdEB);
      }

      if (m_testGarbageCollection) {
        m_l1IdsInProcess.insert(level1IdEB);	// L1ID "busy" with EB
        DEBUG_TEXT(DFDB_ROSTG, 20, "TriggerGenerator: generate: inserted L1id = " <<
                                   level1IdEB << " in busy set " << " of size " << m_l1IdsInProcess.size());
   
        // update  set size histogram
        int setIPsize = m_l1IdsInProcess.size();
        if (setIPsize < (c_maxINPROCESSbins -1)) {
          m_setINPROCESShist[setIPsize]++;
        }
        else {				// overflow
          m_setINPROCESShist[c_maxINPROCESSbins -1]++;
        }
      }

    }

    if (++m_next == c_aleasize) m_next = 0;

    if (!m_syncDeletesWithEBRequests) {
      if (!L1id_EBed) {
        DEBUG_TEXT(DFDB_ROSTG, 20, "generateRequestForOneL1ID: if no EB request, l1id = "
                                   << std::hex << level1IdEB << " on CLEAR Q of size "
                                   << std::dec << releaseQueueSize());
        releaseQueuePush(level1IdEB);
      }
    }
    else {
      m_queue_all_ids.push(level1IdEB);
      if (m_queue_ebfinished_ids.size() > 0)
      {
          if (m_queue_ebfinished_ids.front() != m_queue_all_ids.front() )
          {
            releaseQueuePush(m_queue_all_ids.front());
          }
          else
          {
                  m_queue_ebfinished_ids.pop();
          }
          m_queue_all_ids.pop();
      }
    }
  } 
  // end-of-EB-queue -------------------
  //    

  processReleaseQueue(false) ;		// empty the release Q

  return requestsGenerated;

}

/******************************************************/
void TriggerGenerator::processReleaseQueue(bool flush) 
/******************************************************/
{
  // the CLEAR Queue may grow quite large: in EB mode, EB requests will be generated while the 
  // release Q is empty. The request Q may be filled: <~ 100 requests
  // The request handlers then start processing the requests and pushing CLEAR L1ids on the
  // release sync Q. The trigger generator (thread) may only resume when the watermark is hit:
  // 10 elements left. We may expect the Q to grow to ~ 90 elements.
  // careful to keep request queue to 100 as ALWAYS ..

  if (m_deleteGrouping && !releaseQueueEmpty() ) {	// possibly several delete groups
    while  ( (m_l1Ids.size() < m_deleteGrouping) && ( !releaseQueueEmpty() ) )	// up to one delete group
    {

      // update CLEAR Q size histogram
      int qCLEARsize = releaseQueueSize();
      if (qCLEARsize < (c_maxCLEARbins -1)) {
        m_qCLEARhist[qCLEARsize]++;
      }
      else {				// overflow
        m_qCLEARhist[c_maxCLEARbins -1]++;
      }

      int level1Id = releaseQueuePop();

      if (m_syncDeletesWithEBRequests) {
        if (m_queue_ebrequest_ids.size() > 0)
        {
          if (level1Id == (int) m_queue_ebrequest_ids.front() )
          {
            m_queue_ebfinished_ids.push(level1Id);
            m_queue_ebrequest_ids.pop();
          }
        }
      }

      if (m_testGarbageCollection) {
        m_l1IdsInProcess.erase(level1Id);
        DEBUG_TEXT(DFDB_ROSTG, 20, "processReleaseQueue: erased L1ID " << level1Id <<
                                   " from L1ID busy set of size " << m_l1IdsInProcess.size());
      }

      m_l1Ids.push_back(level1Id);
      DEBUG_TEXT(DFDB_ROSTG, 20, "processReleaseQueue: The L1ID to be deleted is:"
                                 << std::hex << level1Id);
    } 
 
    if ( m_l1Ids.size() == m_deleteGrouping ) {

      m_numberOfDeleteGroup++;		// # delete groups: NOT # release requests

//        TS_RECORD(TS_H1, 1300);
      if (m_testGarbageCollection) {
        std::set<unsigned int>::iterator it_minL1id = min_element(m_l1IdsInProcess.begin(), m_l1IdsInProcess.end());
        m_oldestL1id = *it_minL1id;
        DEBUG_TEXT(DFDB_ROSTG, 10, "processReleaseQueue: m_oldestL1id = " << std::dec << m_oldestL1id);
      }
      else {
        m_oldestL1id = 0;
      }

      generateReleaseRequest(m_oldestL1id);
      //requestsGenerated += m_deleteGrouping;   //You need this line if you want to run with deletes only

//        TS_RECORD(TS_H1, 1390);

      m_l1Ids.clear();		// forget the accumulated ones
    }
  }

  // queue is now empty but L1ID vector may not be.
  if (flush) {
    DEBUG_TEXT(DFDB_ROSTG, 20, "processReleaseQueue: flush L1ID vector of size " << std::dec << m_l1Ids.size());
    if (m_l1Ids.size() > 0) {
      generateReleaseRequest(m_oldestL1id);
      m_l1Ids.clear();		// forget the accumulated ones
      m_numberOfDeleteGroup++;
//        TS_RECORD(TS_H1, 1390);
      //requestsGenerated += m_deleteGrouping;   //You need this line if you want to run with deletes only
    }
  }


}

/******************************************************/
int TriggerGenerator::noRolsInThisRoI(void) 
/******************************************************/
{

  int rolsInThisRoI = m_maxRolsInRoI;
  for(int loop = 0; loop < m_maxRolsInRoI; loop++)
  {
    if(m_alea[m_next] < m_l2puLimit[loop]) 
    {
      rolsInThisRoI = loop;
      DEBUG_TEXT(DFDB_ROSTG, 20, "noRolsInThisRoI:  loop = " << loop << "  alea = " << m_alea[m_next] <<
                                 "  limit = " << m_l2puLimit[loop]);
      break;
    }
  }
  if (++m_next == c_aleasize) m_next = 0;
 
  //define the ROLs involved for this event
  int rol = m_alea[m_next] % m_numberOfChannels;
  if (++m_next == c_aleasize) m_next = 0;
  for(int loop2 = 0; loop2 < rolsInThisRoI; loop2++)
  {
    DEBUG_TEXT(DFDB_ROSTG, 20, "noRolsInThisRoI:  L2 request on (logical) rol = " << rol
                               << " rol ID = " << m_channelIds[loop2]);
    m_rols.push_back(m_channelIds[rol]);  // the Channel ID
    m_numberOfROI[rol]++;		        // count per ROL
    m_totalNumberOfROI++;
    rol++;
    if (rol == m_numberOfChannels) rol = 0;
  } 
  m_roihist[rolsInThisRoI]++;			// each L1id?   FIXME  

  return rolsInThisRoI;

}

/*************************************************************/
void TriggerGenerator::generateL2Request(unsigned int l1id) 
/*************************************************************/
{
  // emulate DC input
  if (m_inputDelay > 0) 
  {
    unsigned int nyield = 0;
    ts_wait(m_inputDelay, &nyield);
  }
 
     
//      TS_RECORD(TS_H1,1150);
  DEBUG_TEXT(DFDB_ROSTG, 15, "generateL2Request:  L2 request for L1id = 0x" << std::hex <<
                              l1id << " generated, ROL size = " << std::dec << m_rols.size());
  m_transactionId++;

  l2Request(l1id, &m_rols, m_destination, m_transactionId);
//      TS_RECORD(TS_H1, 1160);
      
  m_rols.clear();	// size = 0

}

/****************************************************************/
void TriggerGenerator::generateEBRequest(unsigned int level1Id) 
/****************************************************************/
{
  // emulate DC input
  if (m_inputDelay > 0) 
  {
    unsigned int nyield = 0;
    ts_wait(m_inputDelay, &nyield);
  }

  DEBUG_TEXT(DFDB_ROSTG, 15, "generateEBRequest:  EB request generated for L1id = 0x"
                             << std::hex << level1Id );
  ebRequest(level1Id, m_destination);

}

/*****************************************************************************/
void TriggerGenerator::generateReleaseRequest(unsigned int oldestLevelId) 
/*****************************************************************************/
{
  // emulate DC input
  if (m_inputDelay > 0) 
  {
    unsigned int nyield = 0;
    ts_wait(m_inputDelay, &nyield);
  }

//        TS_RECORD(TS_H1, 1310);
  DEBUG_TEXT(DFDB_ROSTG, 15, "generateReleaseRequest:  release request to be sent, last L1id  = 0x"
                             << std::hex << m_l1Ids[m_l1Ids.size()-1]
                             << " oldest L1ID = " << oldestLevelId);

//   std::cout << "TriggerGenerator " << m_instance << " clear request for events ";
//   for (auto id: m_l1Ids) {
//      std:: cout << id << " ";
//   }
//   std::cout << std::endl;
  releaseRequest(&m_l1Ids, oldestLevelId);
//  m_l1Ids.clear();		// moved to processReleaseQueue

}

/********************************************************************************/
void TriggerGenerator::printStatistics(float elapsedSeconds,std::ostream &output) 
/********************************************************************************/
{

  output << std::endl;
  output << " EB Queue histogram " << std::endl;
  output << " Q size  # elements " << std::endl;
  int nEBgroup = 5;					// ad hoc
  for (int i=0; i<(c_maxEBbins/nEBgroup); i++) {
    int bin_contents = 0;
    for (int j=0; j<nEBgroup; j++) {
      bin_contents+= m_qEBhist[nEBgroup*i+j];
    }
    output << std::setw(7) << std::dec << nEBgroup*i << std::setw(12) << bin_contents << std::endl;
  }
  output << std::endl;

  output << " CLEAR Queue histogram " << std::endl;
  output << " Q size  # elements " << std::endl;
  int nCLEARgroup = 5;
  for (int i=0; i<(c_maxCLEARbins/nCLEARgroup); i++) {
    int bin_contents = 0;
    for (int j=0; j<nCLEARgroup; j++) {
      bin_contents+= m_qCLEARhist[nCLEARgroup*i+j];
    }
    output << std::setw(7) << std::dec << nCLEARgroup*i << std::setw(12) << bin_contents << std::endl;
  }
  output << std::endl;

  if (m_testGarbageCollection) {
    output << " In Process set size histogram " << std::endl;
    output << " set size  # elements " << std::endl;
    int nIPgroup = 10;
    for (int i=0; i<(c_maxINPROCESSbins/nIPgroup); i++) {
      int bin_contents = 0;
      for (int j=0; j<nIPgroup; j++) {
        bin_contents+= m_setINPROCESShist[nIPgroup*i+j];
      }
      output << std::setw(9) << std::dec << nIPgroup*i << std::setw(12) << bin_contents << std::endl;
    }
    output << std::endl;
  }

  output << " Number of events = " << std::dec << m_numberOfL1ids << std::endl;

  for (int rol = 0; rol <= m_numberOfChannels; rol++) {
    output << " Number of events with " << rol << " ROLs per ROI = " << m_roihist[rol] << std::endl;
  }
  
  for (int rol = 0; rol < m_numberOfChannels; rol++) {
    float reqPerRobin = 100.0 * ((float)m_numberOfROI[rol] / m_numberOfL1ids);
    output << " Number of ROI requests for ROL ID" 
	   << m_channelIds[rol] << " = " 
	   << m_numberOfROI[rol] << " in % = " << reqPerRobin << std::endl;
  }
  float L2APer = 100.0 * ((float)m_numberOfL2A /  m_numberOfL1ids);
  output << " Number of events with L2A = " << m_numberOfL2A << " in % = " << L2APer << std::endl;
  output << " Number of delete Groups   = " << m_numberOfDeleteGroup << std::endl;
  output << std::endl;

  output << " Last L1 ID generated  = " << m_level1Id << std::endl;
  output << std::endl;

  if (elapsedSeconds>0.) {
    output << "--------------------------------------------" << std::endl ;
    output << "Elapsed time (sec): " << elapsedSeconds << std::endl; 
    output << "L1 rate (kHz): " << m_numberOfL1ids/(1000.*elapsedSeconds) << std::endl ;
    output << "Total data requests rate (kHz): "
           << (m_numberOfL2req+m_numberOfL2A)/(1000.*elapsedSeconds) << std::endl ;
    output << "L2 requests rate (kHz): " << m_numberOfL2req/(1000.*elapsedSeconds) << std::endl ;
    output << "EB requests rate (kHz): " << m_numberOfL2A/(1000.*elapsedSeconds) << std::endl ;
    output << "Delete requests rate (kHz): "
           << m_numberOfDeleteGroup/(1000.*elapsedSeconds) << std::endl ;
  }
  
}
