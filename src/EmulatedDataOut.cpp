// $Id$
// //////////////////////////////////////////////////////////////////////
//  Debug implementation of DataOut.  Does nothing but write data to
// screen/file
//
//  Author:  G.J.Crone, University College London
//
//  $Log$
//  Revision 1.35  2008/07/18 13:11:42  gcrone
//  Specify namespace std:: where necessary
//
//  Revision 1.34  2008/02/19 13:58:08  gcrone
//  Split file writing and IO emulation DataOuts
//
//  Revision 1.33  2008/02/08 08:39:13  gcrone
//  Update dataOutput stats
//
//  Revision 1.32  2008/02/07 11:11:23  gcrone
//  Use new sendData with status argument
//
//  Revision 1.31  2007/10/22 12:41:18  gcrone
//  Use correct type of get method for getting run params fron the
//  runParams Config object (detector mask is now 64 bit).
//
//  Revision 1.30  2007/09/26 15:52:01  gcrone
//  Adapt EmulatedDataOut to new IS publishing scheme
//
//  Revision 1.29  2007/04/23 11:53:07  gcrone
//  Make all error messages use IOException
//
//  Revision 1.28  2007/04/20 12:55:41  gcrone
//  Use AgreedFilename for the name of the output file when writing to disk.
//
//  Revision 1.27  2007/04/12 15:04:01  gcrone
//  Construct the base filename for DataWriter from appName, run number
//  etc. since the new dataWriter with tdaq-common-01-06-00 doesn't do
//  that anymore.
//
//  Revision 1.26  2007/03/08 18:59:26  gcrone
//  Hack to make it compile with DataWriter from tdaq-common-01-06-00
//
//  Revision 1.25  2007/02/27 14:36:34  gcrone
//  Add local StorageCallback instead of using the one from DFInfo
//
//  Revision 1.24  2006/12/08 12:17:59  gcrone
//  Mutex protect deletion of DataWriter and check it hasn't been deleted
//  before trying to access it in sendData().
//
//  Revision 1.23  2006/11/30 16:20:01  gcrone
//  Use RC::MAX_EVT_DONE Issue for max events message.
//
//  Revision 1.22  2006/11/30 14:29:05  gcrone
//  Send "Max events reached" message as a warning for now.
//
//  Revision 1.21  2006/11/30 13:11:12  gcrone
//  Use CREATE_ROS_EXCEPTION for instantiating all exceptions
//
//  Revision 1.20  2006/11/29 16:35:26  gcrone
//  Try and give reason why DataWriter open fails.  Include run number in
//  file name for 'unformatted' output.
//
//  Revision 1.19  2006/11/01 12:57:38  gcrone
//  Use new ROSErrorReporting macros instead of dfout
//
//  Revision 1.18  2006/08/21 09:48:25  gcrone
//  Don't refer to destination in DEBUG_TEXT since commented in arg list.
//
//  Revision 1.17  2006/08/02 15:31:13  gcrone
//  Suppress some warnings by commenting out unused args.
//  Add some debug (disabled).
//
//  Revision 1.16  2006/05/02 16:11:44  gcrone
//  Initialise m_outputFile to 0 in constructor.  Add some DEBUG_TEXT.
//
//  Revision 1.15  2006/04/25 17:56:26  gcrone
//  Change message from WARNING to FATAL if DataWriter opend fails.  Also
//  check m_dataWriter->good() before calling m_dataWriter->putData()
//
//  Revision 1.14  2006/03/30 18:07:16  gcrone
//  Get UserMetaData and pass it to DataWriter constructor if it's available.
//
//  Revision 1.13  2006/03/09 19:04:35  gcrone
//  IOMPlugin now uses new FSM methods
//
//  Revision 1.12  2006/03/08 16:40:49  gcrone
//  New sendData API with Buffer* instead of Fragment*
//
//  Revision 1.11  2005/11/03 16:48:55  gcrone
//  Updated config item names to be as given in the database so that ROSDBConfig doesn't need to translate
//
//  Revision 1.10  2005/07/26 16:07:42  gcrone
//  Use DEBUG_TEXT instead of m_trace and cout.  Don't create DataWriter if sampling_gap=0
//
//  Revision 1.9  2004/08/17 17:10:58  gorini
//  EmulatedDataOut fixed sampling algorithm
//
//  Revision 1.8  2004/07/09 12:21:26  glehmann
//  delete storageCallback at stop
//
//  Revision 1.7  2004/06/30 11:52:54  glehmann
//  updated ROSIO to store data in a file containing also the application name and to publish storage information via the StorageCallback function
//
//  Revision 1.6  2004/04/14 16:24:21  gcrone
//  Use DFOutputStream instead of DFErrorCatcher.
//
//  Revision 1.5  2004/04/13 10:20:38  gcrone
//  Make TriggerIn and DataOut inherit from Controllable.  The
//  Configuration is now passed in a new method called setup.
//
//  Revision 1.4  2004/02/19 18:28:58  gcrone
//  Pick up extra info from runParams Config and pass to DataWriter.
//
//  Revision 1.3  2004/02/19 14:29:19  gcrone
//  Protect our thread from being cancelled during writing (and leaving
//  locked MUTEX) by calling pthread_setcancelstate() around mutex
//  protected section.
//
//  Revision 1.2  2004/02/18 19:03:54  gcrone
//  Mutex protect writing.
//  Set maxEventsPerFile and maxMegaBytesPerFile in DataWriter.
//  Use writingPath instead of outputFileName.
//  Change error message if m_dataWriter->good() returns false.
//
//  Revision 1.1.1.1  2004/02/05 18:31:17  akazarov
//  imported from nightly 05.02.2004
//
//  Revision 1.10  2004/01/21 10:35:08  gcrone
//  Updated for new version of DataWriter.
//
//  Revision 1.9  2004/01/20 19:51:28  gcrone
//  Support for EventStorage lib data writing
//
//  Revision 1.8  2004/01/17 14:44:48  jorgen
//   DFDebug instead of ROSDebug
//
//  Revision 1.7  2003/12/03 15:09:35  gcrone
//  Config constructors now protected, use static method New to enforce use of DFCountedPointer
//
//  Revision 1.6  2003/11/11 10:56:06  gcrone
//  Use DFCountedPointer<Config> in place of Config*.
//
//  Revision 1.5  2003/08/11 16:28:04  gcrone
//  Use GlobalDebug settings instead of trace config item.
//
//  Revision 1.4  2003/03/11 15:33:21  pyannis
//  Changes to compile with gcc 3.2 too...
//
//  Revision 1.3  2003/02/19 18:38:49  gcrone
//  Use EventFragment* instead of ROSFragment*
//
//  Revision 1.2  2003/02/04 14:31:05  gcrone
//  Removed logFile stuff.
//
//  Revision 1.1  2002/11/27 16:27:40  gcrone
//  Rationalised naming of classes
//
//  Revision 1.6  2002/11/27 09:22:36  gcrone
//  Pass transaction ID from original DC request message
//
//  Revision 1.5  2002/11/03 18:56:15  gcrone
//  Set modes when opening output file!
//
//  Revision 1.4  2002/11/03 18:27:15  gcrone
//  Support for output file and log file.  Actually write binary data file.
//
//  Revision 1.3  2002/11/02 08:28:23  glehmann
//  Config.h is now taken from DFSubSystemItem
//
//  Revision 1.2  2002/10/31 14:10:52  jorgen
//   added input & output delays and improved time stamping
//
//  Revision 1.1.1.1  2002/10/07 09:50:07  pyannis
//  Initial version of DataFlow ROS packages tree...
//
//  Revision 1.2  2002/09/17 21:43:19  gcrone
//  Don't cast fragment to ROSFragment
//
//  Revision 1.1  2002/09/13 10:31:46  gcrone
//  New test implementation of DataOut for debugging.
//
//
// //////////////////////////////////////////////////////////////////////
#include <iostream>                      // for operator<<, basic_ostream::o...
#include <memory>                        // for allocator

#include "DFThreads/DFCountedPointer.h"  // for DFCountedPointer
#include "ROSCore/DataOut.h"             // for DataOut (ptr only), NodeID
#include "ROSInfo/DataOutInfo.h"         // for DataOutInfo

#include "DFSubSystemItem/Config.h"
#include "ROSIO/EmulatedDataOut.h"
#include "rcc_time_stamp/tstamp.h"

#include "DFDebug/DFDebug.h"


class ISInfo;
namespace ROS { class Buffer; }
namespace daq { namespace rc { class TransitionCmd; } }

using namespace ROS;

EmulatedDataOut::EmulatedDataOut() : m_outputDelay(0) {
   DEBUG_TEXT(DFDB_ROSIO, 15, "EmulatedDataOut::constructor:  called");

   m_statistics.fragmentsOutput=0;
   m_statistics.dataOutput=0.0;
}

EmulatedDataOut::~EmulatedDataOut() noexcept
{
   DEBUG_TEXT(DFDB_ROSIO, 15, "EmulatedDataOut::destructor:  called");
   DEBUG_TEXT(DFDB_ROSIO, 15, "EmulatedDataOut::destructor:  finished");
}

void EmulatedDataOut::sendData(const Buffer* /*buffer*/, NodeID /*destination*/,
                               unsigned int /*transactionId*/,
                               unsigned int /*status*/)
{
   DEBUG_TEXT(DFDB_ROSIO, 20,  "\"Sending\" buffer ");

   // emulate DC output
   unsigned int nyield = 0;
   if (m_outputDelay > 0) {
      ts_wait(m_outputDelay, &nyield);
   }
   m_statistics.fragmentsOutput++;
}

void EmulatedDataOut::prepareForRun(const daq::rc::TransitionCmd&)
{
   DEBUG_TEXT(DFDB_ROSIO, 15, "EmulatedDataOut::prepareForRun() entered");

   m_statistics.fragmentsOutput=0;
}


void EmulatedDataOut::setup(DFCountedPointer<Config> configuration)
{
   DEBUG_TEXT(DFDB_ROSIO, 15, "EmulatedDataOut::setup() entered");


   configuration->dump();

   m_configuration=configuration;

   m_outputDelay=configuration->getInt("OutputDelay");

   std::cout << "EmulatedDataOut::"
        << ", output delay " << m_outputDelay
        << std::endl;
}

ISInfo* EmulatedDataOut::getISInfo()
{
   return &m_statistics;
}

extern "C" {
   extern DataOut* createEmulatedDataOut();
}
DataOut* createEmulatedDataOut()
{
   return (new EmulatedDataOut());
}

