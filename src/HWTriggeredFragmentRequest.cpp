// $Id$
// //////////////////////////////////////////////////////////////////////
//  
//
//  Author:  G.J.Crone, University College London
//
//  $Log$
//  Revision 1.3  2008/05/06 13:16:56  gcrone
//  Set monitoring flag to true in the FragmentRequest constructor!
//
//  Revision 1.2  2008/02/19 10:15:22  gcrone
//  Add destructor.
//
//  Revision 1.1  2008/02/15 17:29:39  gcrone
//  Adapt HardwareTriggerIn to new Fragment Rquest/Builder scheme.
//
//
// //////////////////////////////////////////////////////////////////////
#include <ostream>
#include <string>                              // for string, allocator

#include "ROSIO/HWTriggeredFragmentRequest.h"
#include "ROSCore/DataChannel.h"
#include "ROSCore/Request.h"
#include "ROSCore/FragmentRequest.h"           // for FragmentRequest
#include "ROSCore/TriggerIn.h"
#include "DFDebug/DFDebug.h"

namespace ROS { class FragmentBuilder; }

using namespace ROS;

HWTriggeredFragmentRequest::HWTriggeredFragmentRequest (unsigned int level1Id,
                                                        FragmentBuilder* builder,
                                                        TriggerIn* trigger) :
   FragmentRequest(level1Id,DataChannel::channels(),false, true, false, builder),
   m_trigger(trigger)
{
   DEBUG_TEXT(DFDB_ROSIO, 20, "HWTriggeredFragmentRequest:: constructor entered level1Id=" << level1Id);
}

HWTriggeredFragmentRequest::~HWTriggeredFragmentRequest() {
}

int HWTriggeredFragmentRequest::execute(void)
{
   DEBUG_TEXT(DFDB_ROSIO, 10, "HWTriggeredFragmentRequest::execute entered");

   FragmentRequest::execute();

   m_trigger->clear();
   DEBUG_TEXT(DFDB_ROSIO, 15, "HWTriggeredFragmentRequest::execute done");
   return(REQUEST_OK);
}


std::ostream& HWTriggeredFragmentRequest::put(::ostream& stream) const
{
   stream << "Hardware triggered Fragment request";
   return stream;
}

std::string HWTriggeredFragmentRequest::what()
{
   std::string description="Hardware triggered Fragment request";
   return (description);
}
