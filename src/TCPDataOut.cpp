//
// //////////////////////////////////////////////////////////////////////
#include <iostream>

#include <errno.h>                           // for errno, EAGAIN, EINTR
#include <netdb.h>                           // for gethostbyname, getprotob...
#include <netinet/in.h>                      // for sockaddr_in, htonl, htons
#include <netinet/tcp.h>                     // for TCP_NODELAY
#include <stdio.h>                           // for perror
#include <string.h>                          // for bcopy, memcpy
#include <sys/select.h>                      // for select, FD_SET, FD_ZERO
#include <sys/socket.h>                      // for setsockopt, SOL_SOCKET
#include <sys/time.h>                        // for timeval
#include <unistd.h>                          // for close
#include <mutex>                             // for lock_guard, mutex
#include <string>                            // for string, operator<<, allo...
#include <vector>                            // for vector, vector<>::const_...

#include "DFSubSystemItem/Config.h"
#include "ROSIO/TCPDataOut.h"
#include "ROSBufferManagement/Buffer.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSIO/IOException.h"

#include "DFDebug/DFDebug.h"                 // for DEBUG_TEXT
#include "DFThreads/DFCountedPointer.h"      // for DFCountedPointer
#include "ROSCore/DataOut.h"                 // for DataOut (ptr only), NodeID
#include "ROSInfo/TCPDataOutInfo.h"          // for TCPDataOutInfo
#include "ROSMemoryPool/MemoryPage.h"        // for MemoryPage

class ISInfo;
namespace daq { namespace rc { class TransitionCmd; } }


using namespace ROS;


/**********************/
TCPDataOut::TCPDataOut() : m_fragmentsInput(0), m_samplingGap(1),
                           m_TCPbufSize(0),m_throwIfUnavailable(false),
                           m_errno(0), m_runActive(false)
/**********************/
{
  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPDataOut::constructor:  called");
  struct timeval tout = TCP_WRTIMEOUT;

  m_stats.dataOutput=0.0;
  m_stats.fragmentsOutput=0;
  m_stats.timeouts = 0;

  m_port = 0;

  bcopy (&tout, &m_wrtimeout, sizeof (struct timeval));

  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPDataOut::constructor:  done");
}


/***********************/
TCPDataOut::~TCPDataOut() noexcept
/***********************/
{
  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPDataOut::destructor:  called");
//  unconfigure();
  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPDataOut::destructor:  done");
}


/********************************************************/
bool TCPDataOut::TCPSafeSend (const char *data, int length, int socketNumber,bool quitIfUnavailable)
/********************************************************/
{
  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPDataOut::TCPSafeSend:  called");
  int data_sent = 0;

  while (data_sent < length && m_runActive)
  {
    fd_set fdset;
    struct timeval timeout;

    FD_ZERO (&fdset);
    FD_SET  (m_socket[socketNumber], &fdset);
    bcopy (&m_wrtimeout, &timeout, sizeof (struct timeval));

    while (select (m_socket[socketNumber] + 1, 0, &fdset, 0, &timeout) <= 0){

       ++m_stats.timeouts;
       if (quitIfUnavailable || !m_runActive){
          return true;
       }

       FD_ZERO (&fdset);
       FD_SET  (m_socket[socketNumber], &fdset);
       bcopy (&m_wrtimeout, &timeout, sizeof (struct timeval));
    }

    int result = ::send (m_socket[socketNumber],
                         &data[data_sent],
                         length-data_sent,
                         MSG_DONTWAIT);
    if (result < 0) {
       if( errno != EINTR && errno != EAGAIN) {
          m_errno = errno;
          DEBUG_TEXT(DFDB_ROSIO, 5, "TCPDataOut::TCPSafeSend:  Error in ::send (errno = " << errno << ")");
         return false;
       }
    }
    else{
      data_sent += result;
    }
  }

  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPDataOut::TCPSafeSend:  done");
  return true;
}


/********************************************************************/
void TCPDataOut::sendData(const Buffer* buffer, NodeID /*destination*/,
		 	  unsigned int /*transactionId*/,
                          unsigned int /*fragStatus*/)
/********************************************************************/
{
  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPDataOut::sendData:  called");

  if (m_samplingGap && m_fragmentsInput%m_samplingGap == 0) {
     std::lock_guard<std::mutex> lock(m_mutex);

     // Choose destination socket
     int socketNumber=m_stats.fragmentsOutput%m_socket.size();


    unsigned int bytes = buffer->size();
    unsigned int n_size = htonl (bytes);

    DEBUG_TEXT(DFDB_ROSIO, 20, "TCPDataOut::sendData:  The ROS fragment has a size of " << bytes << " bytes");

    if (!TCPSafeSend ((const char *) &n_size, (int) sizeof (int), socketNumber,m_throwIfUnavailable))
    {
       if (m_runActive) {
          CREATE_ROS_EXCEPTION( tException, IOException, IOException::TCP_SEND_ERROR,
                                "a call to send returned " << m_errno);
          throw tException;
       }
       else {
          return;
       }
    }

    for (Buffer::page_iterator page=buffer->begin(); page!=buffer->end(); page++) 
    {
      int s = (*page)->usedSize();
      bool sentOK=TCPSafeSend ((const char *) (*page)->address(), s, socketNumber, false);
      if (!sentOK)
      {
         if (m_runActive) {
            CREATE_ROS_EXCEPTION( tException, IOException, IOException::TCP_SEND_ERROR,
                                  "a call to send returned errno=" << m_errno);
            throw tException;
         }
         else {
            return;
         }
      }
    }
    m_stats.fragmentsOutput++;
    m_stats.dataOutput+=bytes/(float)(1024*1024);
  }

  m_fragmentsInput++;
  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPDataOut::sendData:  done");
}


/***************************/
void TCPDataOut::connect (const daq::rc::TransitionCmd&)
/***************************/
{
  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPDataOut::connect:  called");
  /* open a socket and connect to the destination node */

  for (std::vector<std::string>::const_iterator destIter=m_destinationNode.begin();
       destIter!=m_destinationNode.end(); destIter++) {

     int option = 1;
     struct sockaddr_in saddr;

     int newSocket;
     if ((newSocket = ::socket (AF_INET, SOCK_STREAM, 0)) < 0)
        {
           CREATE_ROS_EXCEPTION(tException, IOException, IOException::SOCKET_CREATE_ERROR, "");
           throw tException;
        }

     if (setsockopt (newSocket, SOL_SOCKET, SO_KEEPALIVE,
                     &option, sizeof (option)))
        {
           close (newSocket);
           CREATE_ROS_EXCEPTION(tException, IOException, IOException::SOCKET_OPTION_ERROR, "option KEEPALIVE");
           throw tException;
        }

     if (setsockopt (newSocket, SOL_SOCKET, SO_REUSEADDR,
                     &option, sizeof (option)))
        {
           close (newSocket);
           CREATE_ROS_EXCEPTION(tException, IOException, IOException::SOCKET_OPTION_ERROR, "option REUSEADDR");
           throw tException;
        }

     option = m_TCPbufSize;

     if (setsockopt (newSocket, SOL_SOCKET, SO_SNDBUF,
                     &option, sizeof (option)))
        {
           close (newSocket);
           CREATE_ROS_EXCEPTION(tException, IOException, IOException::SOCKET_OPTION_ERROR, "option SNDBUF");
           throw tException;
        }

     if (setsockopt (newSocket, SOL_SOCKET, SO_RCVBUF,
                     &option, sizeof (option)))
        {
           close (newSocket);
           CREATE_ROS_EXCEPTION(tException, IOException, IOException::SOCKET_OPTION_ERROR, "option RCVBUF");
           throw tException;
        }

     option = 0; /* 0 to enable Nagle algorithm */

     if (setsockopt (newSocket,
                     getprotobyname ("tcp")->p_proto,
                     TCP_NODELAY,
                     &option, sizeof (option)))
        {
           close (newSocket);
           CREATE_ROS_EXCEPTION(tException, IOException, IOException::SOCKET_OPTION_ERROR, "option NODELAY");
           throw tException;
        }

     struct hostent *hptr = gethostbyname (destIter->c_str ());
     std::cout << "m_destinationNode=<" << destIter->c_str() << ">\n";
     if (hptr == 0)
        {
           close (newSocket);
           CREATE_ROS_EXCEPTION(tException, IOException, IOException::ADDRESS_TRANSLATION_ERROR, *destIter);
           throw tException;
        }

     saddr.sin_family      = AF_INET;
     saddr.sin_port        = htons (m_port);
     memcpy (&saddr.sin_addr.s_addr, hptr->h_addr, hptr->h_length);
     DEBUG_TEXT(DFDB_ROSIO, 20, "TCPDataOut::connect:  m_port = " << m_port);

     if (::connect (newSocket, (struct sockaddr *) &saddr,
                    sizeof (saddr)))
        {
           perror ("connect error");
           close (newSocket);
           CREATE_ROS_EXCEPTION(tException, IOException, IOException::TCP_CONNECT_ERROR, "Cannot connect to "
                                << *destIter << " port " << m_port);
           throw tException;
        }

     std::cout << "TCPDataOut : connected to " << *destIter
          << " port " << m_port << std::endl;

     m_socket.push_back(newSocket);
  }
  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPDataOut::connect:  done");
}


/****************************/
void TCPDataOut::disconnect(const daq::rc::TransitionCmd&)
/****************************/
{
  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPDataOut::disconnect:  called");
  /* close TCP connection */


   while (!m_socket.empty()) {
      int socket=m_socket.back();
      m_socket.pop_back();
      close(socket);
   }
  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPDataOut::disconnect:  done");
}


/*******************************/
void TCPDataOut::prepareForRun (const daq::rc::TransitionCmd&)
/*******************************/
{
  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPDataOut::prepareForRun:  called");
  m_stats.dataOutput = 0.0;
  m_stats.fragmentsOutput = 0;
  m_fragmentsInput = 0;
  m_stats.timeouts = 0;
  m_runActive=true;
  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPDataOut::prepareForRun:  done");
}

void TCPDataOut::stopHLT(const daq::rc::TransitionCmd&) {
   m_runActive=false;
}

/*************************************************************/
void TCPDataOut::setup (DFCountedPointer<Config> configuration)
/*************************************************************/
{
  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPDataOut::setup:  called");
  m_TCPbufSize         = configuration->getInt ("TCPBufferSize");

  m_destinationNode    = configuration->getVector<std::string> ("DestinationNode");
  m_port = (unsigned short) configuration->getInt ("DestinationPort");

  m_samplingGap        = configuration->getInt ("SamplingGap");
  m_throwIfUnavailable = configuration->getBool ("ThrowIfUnavailable");

  std::cout << "TCPDataOut::"
       << " sending data to ";
  for (std::vector<std::string>::const_iterator destIter=m_destinationNode.begin();
       destIter!=m_destinationNode.end(); destIter++) {
     std::cout << *destIter << ",";
  }
  std::cout << " destination port " << m_port
       << " every " << m_samplingGap << " calls to sendData"
       << std::endl;
  DEBUG_TEXT(DFDB_ROSIO, 15, "TCPDataOut::setup:  done");
}


/********************************************/
ISInfo* TCPDataOut::getISInfo()
/********************************************/
{
   return &m_stats;
}


extern "C" 
{
   extern DataOut* createTCPDataOut();
}

DataOut* createTCPDataOut()
{
   return (new TCPDataOut());
}

