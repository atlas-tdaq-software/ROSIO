// -*- c++ -*-
//
// ///////////////////////////////////////////////////////////////////////////

#ifndef CIRCDATAOUT_H
#define CIRCDATAOUT_H

#include <string>

#include "circ/Circ.h"
#include "circ/Circ_params.h"
 
#include "ROSCore/DataOut.h"
#include "ROSInfo/CircDataOutInfo.h"

namespace ROS {
   /**
      Implementation of DataOut for writing to a circular buffer.
   */

   class CircDataOut : public DataOut
   {
   private:
     int m_fragNum;
     int m_outputDelay;
     bool m_doNothing;
     std::string m_outputCircName;
     int m_outputCirc;
     int m_samplingGap;
     int m_bufSize;
     bool m_throwIfFull;
     CircDataOutInfo m_stats;

   public:
      CircDataOut ();
      virtual ~CircDataOut () noexcept;

      virtual void setup (DFCountedPointer<Config> configuration);

      /** Opens output stream specified by configuration. */
      virtual void configure (const daq::rc::TransitionCmd&);
      /** Closes output stream */
      virtual void unconfigure (const daq::rc::TransitionCmd&);

      virtual void prepareForRun (const daq::rc::TransitionCmd&);

      virtual void sendData (const Buffer* buffer, const NodeID,
			     const unsigned int transactionId=1,
                             const unsigned int fragStatus=0);

      virtual ISInfo* getISInfo ();
   };
}
#endif
