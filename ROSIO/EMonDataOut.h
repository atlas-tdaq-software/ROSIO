// -*- c++ -*-
//  $Id$
#ifndef EMONDATAOUT_H
#define EMONDATAOUT_H

//******************************************************************************
// file: EMonDataOut.h
// desc: DataOut using ROS Monitoring Handler, e.g. for ROD monitoring
// auth: 05/04/03 R. Spiwoks
//******************************************************************************
#include <string>

#include "ROSCore/DataOut.h"
#include "ROSInfo/DataOutInfo.h"
#include "DFDebug/DFDebug.h"

#include <emon/EventSampler.h>
#include <DFThreads/DFThread.h>
#include <DFThreads/DFFastMutex.h>

namespace ROS {
	
  class EMonDataOut : public DataOut {

  public:
    EMonDataOut(const Type myType=SAMPLED); 

    EMonDataOut(const EMonDataOut&)=delete;
    EMonDataOut& operator=(const EMonDataOut&)=delete;

    virtual ~EMonDataOut() noexcept;

    void setup(DFCountedPointer<Config> configuration);

    void sendData(const Buffer* buffer, const NodeID,
                  const u_int tid=1,const unsigned int status=0);
    ISInfo* getISInfo();

  private:
     class Sampling : public emon::PullSampling {
     public:
        Sampling(const emon::SelectionCriteria &sc, 
                 DFFastMutex* mutex,
                 char** buffer,
                 size_t* sizePtr);
        virtual ~Sampling();
        virtual void sampleEvent(emon::EventChannel& channel);
     private:
        // output channel 
        // parameters
        emon::SelectionCriteria m_selectionCriteria;

        DFFastMutex* m_mutex;

        char** m_buffer;
        size_t* m_sizePtr;
     } ;


     class SamplingFactory : public emon::PullSamplingFactory {
     public:
        SamplingFactory(DFFastMutex* mutex, const DataOut::Type type, EMonDataOut* dataOut,
                        char** buffer, size_t* sizePtr);
        ~SamplingFactory();
        emon::PullSampling * startSampling(const emon::SelectionCriteria & criteria);
           /* throw (emon::BadCriteria, emon::NoResources); */
     private:
        DFFastMutex* m_mutex;
        DataOut::Type m_type;
        emon::SelectionCriteria* m_criteria;
        char** m_buffer;
        size_t* m_sizePtr;
     };

     void copyBuffer(const Buffer* buffer);

     SamplingFactory* m_samplingFactory;
     Sampling* m_sampling;
     emon::EventSampler* m_eventSampler;

     // temporary buffer
     char * m_dataStorage;
     unsigned int m_dataStorageSize;

     // multi-threading protection
     DFFastMutex *m_accessMutex;

    // statistics
     int m_sendCalled;
     DataOutInfo m_stats;

     // Keep track of our type
     Type m_type;
     size_t m_bufferedEventSize;
  };

}	// namespace ROS

#endif	// EMONDATAOUT
