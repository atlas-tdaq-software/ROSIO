// -*- c++ -*-
// $Id:$
//
#ifndef METFRAGMENTBUILDER_H
#define METFRAGMENTBUILDER_H

#include "ROSCore/FragmentBuilder.h"
#include "ROSEventFragment/EventFragment.h"
#include "ROSEventFragment/ROBFragment.h"


class DFFastMutex;
namespace ROS {
   class MemoryPool;
   class MemoryPage;

   class MEtFragment : public EventFragment {
   public:
      MEtFragment(MemoryPool* pool, unsigned int sourceId);
      virtual ~MEtFragment();
      virtual u_int size();
      virtual u_int level1Id();
      virtual u_int status();
      virtual void setStatus(u_int status_word);
      virtual u_int runNumber();
      virtual bool fragmentReady();
      virtual Buffer* buffer(void) const;
      virtual void print(std::ostream& out = std::cout) const;
      void append(ROBFragment* robFragment);
      void close();
      static const u_int s_nStatusElements=1;
   private:
      bool processFeb(unsigned int* dataPtr);
      void addEmptyBlock();
      bool m_firstAppend;
      unsigned int* m_robCount;
      std::vector<unsigned int> m_statusWords;
      unsigned int m_sourceId;
      Buffer* m_buffer;
      ROBFragment::ROBHeader* m_header;
      RODFragment::RODHeader* m_rodHeader;
      unsigned int* m_status;
      MemoryPage* m_writePage;
      u_int* m_writePtr;
      bool m_ready;
      bool m_needHeader;
   };


   class MEtFragmentBuilder : public FragmentBuilder {
   public:
      MEtFragmentBuilder(unsigned int source);
      virtual EventFragment* createFragment(unsigned int level1Id, unsigned int nChannels);
      virtual void appendFragment(EventFragment* parent, EventFragment* child);
      virtual void closeFragment(EventFragment* parent);
   private:
      DFFastMutex* m_requestMutex;
      unsigned int m_sourceId;
   };
}


#endif
