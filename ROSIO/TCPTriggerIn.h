// -*- c++ -*-
// $Id$ 
//
// $Log$
// Revision 1.2  2006/03/09 19:04:34  gcrone
// IOMPlugin now uses new FSM methods
//
// Revision 1.1  2005/08/08 14:40:26  gcrone
// Added TCPTriggerIn
//
//
#ifndef TCPTRIGGERIN_H
#define TCPTRIGGERIN_H
#include "ROSIO/HardwareTriggerIn.h"

#include <string>

namespace ROS{

   class TCPTriggerIn : public HardwareTriggerIn {
   public:
      virtual void setup(IOManager* iomanager, DFCountedPointer<Config> configuration);
      virtual void connect(const daq::rc::TransitionCmd&);
      virtual void disconnect(const daq::rc::TransitionCmd&);

      //! Get operational statistics 
      //      virtual DFCountedPointer<Config> getInfo();

      virtual void clear();


   protected:
      virtual bool waitForTrigger();
   private:
      int m_socket;
      std::string m_destinationNode;
      int m_port;
      int m_connectTimeout;
   };
}


#endif
