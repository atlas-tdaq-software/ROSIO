// -*- c++ -*- $Id$ 
// ///////////////////////////////////////////////////////////////////////////
//
// $Log$
// Revision 1.18  2008/02/19 13:58:08  gcrone
// Split file writing and IO emulation DataOuts
//
// Revision 1.17  2008/02/07 11:14:24  gcrone
// Use new sendData with status argument
//
// Revision 1.16  2008/02/01 17:22:07  gcrone
// Include ISInfo headers from ROSInfo
//
// Revision 1.15  2007/09/26 15:52:00  gcrone
// Adapt EmulatedDataOut to new IS publishing scheme
//
// Revision 1.14  2007/03/08 18:59:26  gcrone
// Hack to make it compile with DataWriter from tdaq-common-01-06-00
//
// Revision 1.13  2007/02/27 14:36:33  gcrone
// Add local StorageCallback instead of using the one from DFInfo
//
// Revision 1.12  2006/12/05 18:12:51  gcrone
// Remove forward refs to classes that are never used
//
// Revision 1.11  2006/08/18 11:24:10  gcrone
// Remove declaration of Config with no namespace
//
// Revision 1.10  2006/03/09 19:04:34  gcrone
// IOMPlugin now uses new FSM methods
//
// Revision 1.9  2006/03/08 16:40:48  gcrone
// New sendData API with Buffer* instead of Fragment*
//
// Revision 1.8  2005/11/24 17:24:19  gcrone
// Remove spurious ;s to make gcc344 happy
//
// Revision 1.7  2005/07/26 16:07:42  gcrone
// Use DEBUG_TEXT instead of m_trace and cout.  Don't create DataWriter if sampling_gap=0
//
// Revision 1.6  2005/04/28 20:55:27  gorini
// Adapted MonitorinDataOut to new emon package. Moved Request implementation from ROSCore to here. Moved abstract classes from here to ROSCore to avoid cross dependencies between packages.
//
// Revision 1.5  2004/07/09 12:21:25  glehmann
// delete storageCallback at stop
//
// Revision 1.4  2004/04/14 16:24:20  gcrone
// Use DFOutputStream instead of DFErrorCatcher.
//
// Revision 1.3  2004/04/13 10:20:35  gcrone
// Make TriggerIn and DataOut inherit from Controllable.  The
// Configuration is now passed in a new method called setup.
//
// Revision 1.2  2004/02/18 18:55:02  gcrone
// Add mutex for protecting write.
//
// Revision 1.1.1.1  2004/02/05 18:31:17  akazarov
// imported from nightly 05.02.2004
//
// Revision 1.5  2004/01/20 19:51:27  gcrone
// Support for EventStorage lib data writing
//
// Revision 1.4  2003/11/11 10:56:03  gcrone
// Use DFCountedPointer<Config> in place of Config*.
//
// Revision 1.3  2003/02/19 18:38:47  gcrone
// Use EventFragment* instead of ROSFragment*
//
// Revision 1.2  2003/02/04 14:31:03  gcrone
// Removed logFile stuff.
//
// Revision 1.1  2002/11/27 16:27:31  gcrone
// Rationalised naming of classes
//
// Revision 1.4  2002/11/27 09:03:52  gcrone
// Initial check in of IOException class
//
// Revision 1.3  2002/11/03 18:24:56  gcrone
// Support for output file and log file.  Also, sendData() now takes ROSFragment* instead of EventFragment*
//
// Revision 1.2  2002/10/31 14:10:49  jorgen
//  added input & output delays and improved time stamping
//
// Revision 1.1.1.1  2002/10/07 09:50:07  pyannis
// Initial version of DataFlow ROS packages tree...
//
// Revision 1.2  2002/09/17 21:44:46  gcrone
// Document code only - No functional changes
//
//
// ///////////////////////////////////////////////////////////////////////////
#ifndef EMULATEDDATAOUT_H
#define EMULATEDDATAOUT_H

#include "ROSCore/DataOut.h"
#include "ROSInfo/DataOutInfo.h"


namespace ROS {
   /**
      Dummy implementation of DataOut for debug/testing.
   */
   class EmulatedDataOut : public DataOut
   {
   public:
      EmulatedDataOut();
      virtual ~EmulatedDataOut() noexcept;
      void setup(DFCountedPointer<Config> configuration);

      void prepareForRun(const daq::rc::TransitionCmd&);

      void sendData(const Buffer* buffer, const NodeID,
                    const unsigned int transactionId=1,
                    const unsigned int status=0);

      ISInfo* getISInfo();
   private:
      DataOutInfo m_statistics;
      int m_outputDelay;

      DFCountedPointer<Config> m_configuration;

   };
}
#endif
