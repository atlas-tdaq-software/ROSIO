// -*- c++ -*-
// $Id$
//
// $Log$
// Revision 1.3  2005/11/29 16:07:10  gcrone
// Removed more spurious ;s to make gcc344 happy
//
// Revision 1.2  2005/11/24 17:24:19  gcrone
// Remove spurious ;s to make gcc344 happy
//
// Revision 1.1  2005/11/15 15:13:37  gcrone
// New Request type for garbage collection
//
//
//
#ifndef GARBAGECOLLECTIONREQUEST_H
#define GARBAGECOLLECTIONREQUEST_H

#include <vector>
#include <ostream>
#include <string>

//#include "ROSObjectAllocation/FastObjectPool.h"
#include "ROSObjectAllocation/DoubleEndedObjectPool.h"
//#include "ROSObjectAllocation/ThreadSafeObjectPool.h"

#include "ROSCore/Request.h"

namespace ROS {
   class DataChannel;
   /** Request class to inform DataChannel objects that they may release
       buffered event fragments.
   */
   class GarbageCollectionRequest: public Request
   {
   private:
      const unsigned int m_oldestLevel1Id;
   public:
      GarbageCollectionRequest(const unsigned int oldestLevel1Id,
                               std::vector<DataChannel*> * dataChannels);

      virtual ~GarbageCollectionRequest();

      int execute(void);
      virtual std::ostream& put(std::ostream& stream) const;
      virtual std::string what();
      void * operator new(size_t) ;
      void operator delete(void *memory) ;
   };
  
   inline GarbageCollectionRequest::~GarbageCollectionRequest() {
   }

   inline void * GarbageCollectionRequest::operator new(size_t) {
      //return ThreadSafeObjectPool<GarbageCollectionRequest>::allocate() ;
      return DoubleEndedObjectPool<GarbageCollectionRequest>::allocate() ;
      //return FastObjectPool<GarbageCollectionRequest>::allocate() ;
   }
  
   inline void GarbageCollectionRequest::operator delete(void * memory) {
      //ThreadSafeObjectPool<GarbageCollectionRequest>::deallocate(memory) ;
      DoubleEndedObjectPool<GarbageCollectionRequest>::deallocate(memory) ;
      //FastObjectPool<GarbageCollectionRequest>::deallocate(memory) ;
   }

}

#endif
