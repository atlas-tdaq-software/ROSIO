// -*- c++ -*-
// $Id$ 
//
// $Log$
// Revision 1.1  2008/02/15 17:29:40  gcrone
// Adapt HardwareTriggerIn to new Fragment Rquest/Builder scheme.
//
//
#ifndef HWTRIGGEREDFRAGMENTREQUEST_H
#define HWTRIGGEREDFRAGMENTREQUEST_H

#include <iosfwd>                     // for ostream
#include <string>                     // for string

#include "ROSCore/FragmentRequest.h"


namespace ROS {
   class TriggerIn;
   class FragmentBuilder;

   class HWTriggeredFragmentRequest : public FragmentRequest {
   public:
      HWTriggeredFragmentRequest(unsigned int level1Id,
                                 FragmentBuilder* builder,
                                 TriggerIn* trigger);
      virtual ~HWTriggeredFragmentRequest();
      virtual int execute(void);
      virtual std::ostream& put(std::ostream& stream) const;
      virtual std::string what();
   private:
      TriggerIn* m_trigger;
   };
}
#endif
