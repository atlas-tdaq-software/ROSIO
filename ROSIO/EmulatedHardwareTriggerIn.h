// -*- c++ -*-
// $Id$ 
//
#ifndef EMULATEDHARDWARETRIGGERIN_H
#define EMULATEDHARDWARETRIGGERIN_H

#include "ROSIO/HardwareTriggerIn.h"
#include "DFThreads/DFCountedPointer.h"  // for DFCountedPointer

namespace daq { namespace rc { class TransitionCmd; } }

namespace ROS{
   class Config;
   class IOManager;

   class EmulatedHardwareTriggerIn : public HardwareTriggerIn {
   public:
      EmulatedHardwareTriggerIn() : m_busy(false){};

      virtual void setup(IOManager* iomanager, DFCountedPointer<Config> configuration);
      virtual void prepareForRun(const daq::rc::TransitionCmd&);
      virtual void stopGathering(const daq::rc::TransitionCmd&);
      virtual void clear();
   protected:
      virtual bool waitForTrigger();
   private:
      bool m_busy;
   };
}
#endif
