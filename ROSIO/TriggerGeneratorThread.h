// -*- c++ -*- 
// ///////////////////////////////////////////////////////////////////////////
//
// Trigger generating thread
//
// ///////////////////////////////////////////////////////////////////////////
#ifndef TRIGGERGENERATORTHREAD_H
#define TRIGGERGENERATORTHREAD_H

#include "DFThreads/DFThread.h"
#include "DFSubSystemItem/Config.h"

namespace ROS {

#define ALEASIZE 500000
#define MAX_ROLS 100

#ifdef DEBUG_TRIGGERGENERATORTHREAD
  #define EMDEBUG(x) cout << x << endl;
#else
  #define EMDEBUG(x)
#endif

   class TriggerGeneratorThread : public DFThread {
   public:
      typedef void (*l2Request_callback)(const unsigned int level1Id,
					 const std::vector<unsigned int> * rols,
					 const unsigned int destination);
      typedef void (*ebRequest_callback)(const unsigned int l1Id,
					 const unsigned int destination);
      typedef void (*releaseRequest_callback)(const std::vector<unsigned int>* level1Ids);    
      TriggerGeneratorThread(l2Request_callback l2Request,
			     ebRequest_callback ebRequest,
			     releaseRequest_callback releaseRequest);
      //! Load configuration
      void configure(DFCountedPointer<Config> configuration);
      void start();
      void stop();
   private:
      DFCountedPointer<Config> m_config;
      int m_numberOfRols;
      int m_numberOfL1ids;
      int m_numberOfL2A ;
      int m_numberOfDeleteGroup;
      int m_numberOfROI[MAX_ROLS];    // # ROIs per ROL
      int m_roihist[MAX_ROLS];        // # ROLs per ROI

     //Callbacks
     l2Request_callback m_l2Request; 
     ebRequest_callback m_ebRequest;
     releaseRequest_callback m_releaseRequest;
   protected:
      virtual void run();
      virtual void cleanup();
   };
}


#endif
