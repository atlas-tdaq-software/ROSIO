// -*- c++ -*-
// $Id$ 
//
// $Log$
// Revision 1.1  2008/02/14 08:54:30  gcrone
// New file
//
//
#ifndef FULLFRAGMENTBUILDER_H
#define FULLFRAGMENTBUILDER_H

#include "ROSCore/FragmentBuilder.h"

namespace ROS {
   class EventFragment;
   class FullFragmentBuilder : public FragmentBuilder {
   public:
      virtual EventFragment* createFragment(unsigned int level1Id, unsigned int nChannels);
      virtual void appendFragment(EventFragment* parent, EventFragment* child);
   };
}

#endif
