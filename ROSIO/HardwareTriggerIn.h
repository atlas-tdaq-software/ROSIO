// -*- c++ -*- $Id$ 
// ///////////////////////////////////////////////////////////////////////////
//  Base class for a hardware driven TriggerIn
//
// $Log$
// Revision 1.8  2008/02/15 17:41:17  gcrone
// Update to new Fragment Request/Builder scheme
//
// Revision 1.7  2008/02/01 17:22:07  gcrone
// Include ISInfo headers from ROSInfo
//
// Revision 1.6  2008/01/28 18:36:48  gcrone
// Use new getISInfo method, remove getInfo
//
// Revision 1.5  2006/03/09 19:04:34  gcrone
// IOMPlugin now uses new FSM methods
//
// Revision 1.4  2005/11/25 14:01:53  gcrone
// Removed spurious ;s to make gcc344 happy
//
// Revision 1.3  2005/08/04 17:34:05  gcrone
// Remove obsolete Requests and add HWTriggeredRemoteRequest
//
// Revision 1.2  2005/05/29 14:08:26  gcrone
// Use conventional getFragment() with L1 ID generated in HardwareTriggerIn.
//
// Revision 1.1  2005/05/27 09:03:25  gcrone
// Added HardwareTriggerIn and related Requests
//
//
// ///////////////////////////////////////////////////////////////////////////
#ifndef HARDWARETRIGGERIN_H
#define HARDWARETRIGGERIN_H

#include "ROSCore/TriggerIn.h"
#include "ROSInfo/TriggerInInfo.h"
#include "DFSubSystemItem/Config.h"      // for Config
#include "DFThreads/DFCountedPointer.h"  // for DFCountedPointer

class ISInfo;
namespace daq { namespace rc { class TransitionCmd; } }

namespace ROS {
   class FragmentBuilder;
   class IOManager;
   //! A TriggerIn class that waits for a hardware trigger of some kind
   class HardwareTriggerIn : public TriggerIn {
   public:
      HardwareTriggerIn();
      virtual ~HardwareTriggerIn() noexcept;

      //! Load configuration
      virtual void setup(IOManager* iomanager, DFCountedPointer<Config> configuration);

      virtual void prepareForRun(const daq::rc::TransitionCmd&);
      virtual void stopGathering(const daq::rc::TransitionCmd&);
      //! Get operational statistics 
      virtual ISInfo* getISInfo();

      //! Callback function to clear the source of the trigger after
      //  the Request has dealt with it
      virtual void clear()=0;
   protected:
      enum RequestTypes {RT_ROS, RT_ROD, RT_NULL};
      virtual void run();
      virtual void cleanup();

      //! Method that actually waits for the hardware trigger 
      virtual bool waitForTrigger()=0;

      DFCountedPointer<Config> m_config;
      int m_level1Id;
      bool m_triggerActive;
      RequestTypes m_requestType;
      TriggerInInfo m_stats;
      FragmentBuilder* m_builder;
   };
}
#endif
