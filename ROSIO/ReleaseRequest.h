// -*- c++ -*-
// $Id$
//
// $Log$
// Revision 1.4  2008/07/18 13:11:41  gcrone
// Specify namespace std:: where necessary
//
// Revision 1.3  2005/11/29 16:07:10  gcrone
// Removed more spurious ;s to make gcc344 happy
//
// Revision 1.2  2005/11/25 14:01:53  gcrone
// Removed spurious ;s to make gcc344 happy
//
// Revision 1.1  2005/04/28 20:55:27  gorini
// Adapted MonitorinDataOut to new emon package. Moved Request implementation from ROSCore to here. Moved abstract classes from here to ROSCore to avoid cross dependencies between packages.
//
// Revision 1.7  2004/03/24 14:07:24  gorini
// Merged NewArchitecture branch.
//
// Revision 1.6.2.1  2004/03/16 23:54:05  gorini
// Adapted IOManager and Request classes to new ReadouModule/DataChannel architecture
//
// Revision 1.6  2004/01/07 17:07:10  gcrone
// Added virtual destructor
//
// Revision 1.5  2003/04/02 09:57:56  joos
// Time-out added for Data Requests
//
// Revision 1.4  2003/02/05 09:25:59  gcrone
// Rearranged Request class hierarchy.  All data requests
// L2,TestBeam... now subclassed from DataRequest which has a
// buildROSFragment method.
// Request::execute now returns bool for success/(temporary) failure. Now
// retry data requests where Request::execute returns false;
//
// Revision 1.3  2002/11/27 14:29:13  gcrone
// Pass level1Ids vector to constructor by reference
//
// Revision 1.2  2002/11/08 15:30:58  gcrone
// New error/exception handling
//
// Revision 1.1.1.1  2002/10/07 09:50:07  pyannis
// Initial version of DataFlow ROS packages tree...
//
// Revision 1.2  2002/09/17 21:39:52  gcrone
// Document code only - No functional changes
//
//
#ifndef RELEASEREQUEST_H
#define RELEASEREQUEST_H

#include <vector>

#include "ROSCore/Request.h"

namespace ROS {
   class DataChannel;
   /** Request class to inform DataChannel objects that they may release
       buffered event fragments.
   */
   class ReleaseRequest: public Request
   {
   private:
      const std::vector<unsigned int> m_level1Ids;
   public:
      ReleaseRequest(const std::vector<unsigned int>& level1Ids,
                     std::vector<DataChannel*> * dataChannels);

      virtual ~ReleaseRequest();

      int execute(void);
      virtual std::ostream& put(std::ostream& stream) const;
      virtual std::string what();
   };
  
   inline ReleaseRequest::~ReleaseRequest() {
   }
}
#endif
