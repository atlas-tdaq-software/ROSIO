// -*- c++ -*- $Id$ 
// ///////////////////////////////////////////////////////////////////////////
//
//  $Log$
//  Revision 1.16  2009/01/27 19:00:10  gcrone
//  Added Generator::resetQueues() method.
//
//  Revision 1.15  2008/02/13 17:47:43  gcrone
//  Use new FragmentRequest/FragmentBuilder architecture
//
//  Revision 1.14  2008/02/01 17:22:07  gcrone
//  Include ISInfo headers from ROSInfo
//
//  Revision 1.13  2008/01/28 18:36:48  gcrone
//  Use new getISInfo method, remove getInfo
//
//  Revision 1.12  2006/03/09 19:04:34  gcrone
//  IOMPlugin now uses new FSM methods
//
//  Revision 1.11  2005/12/01 13:16:17  jorgen
//   add emulation & testing of garbage collection
//
//  Revision 1.10  2005/10/27 08:24:07  jorgen
//   added destructor; remove use of startTrigger and stopTrigger; add flushReleaseQueue
//
//  Revision 1.9  2005/09/03 13:55:50  jorgen
//   fix problems with SYNC queues in trigger generator
//
//  Revision 1.8  2005/08/25 11:42:21  gcrone
//  Adapt to sync version of trigger generator
//
//
// ///////////////////////////////////////////////////////////////////////////
#ifndef EMULATEDTRIGGERIN_H
#define EMULATEDTRIGGERIN_H

#include "ROSCore/TriggerIn.h"
#include "ROSIO/ROSFragmentBuilder.h"
#include "ROSIO/TriggerGenerator.h"
#include "ROSInfo/DcTriggerInInfo.h"

#include "DFThreads/DFFastQueue.h"

namespace ROS {

#define ALEASIZE 500000
#define MAX_ROLS 100

#ifdef DEBUG_EMULATEDTRIGGERIN
  #define EMDEBUG(x) cout << x << endl;
#else
  #define EMDEBUG(x)
#endif

   //! An emulated TriggerIn class that internally generates Requests.
   class EmulatedTriggerIn : public TriggerIn {
   public:
      EmulatedTriggerIn();
      virtual ~EmulatedTriggerIn() noexcept;

      //! Load configuration
      virtual void setup(IOManager* iomanager,
                         DFCountedPointer<Config> configuration) override;

      // From Controllable
      virtual void configure(const daq::rc::TransitionCmd&) override;
      virtual void unconfigure(const daq::rc::TransitionCmd&) override;
      virtual void prepareForRun(const daq::rc::TransitionCmd&) override;
      virtual void stopGathering(const daq::rc::TransitionCmd&) override;
      //! Get operational statistics 
      ISInfo* getISInfo() override;
   protected:
      virtual void run() override;
      virtual void cleanup() override;
   private:
      class Generator : public TriggerGenerator {
      public:
	Generator(DFCountedPointer<Config> config,
                  IOManager* ioManager,
                  DFFastQueue<unsigned int>* ebQueue,
                  DFFastQueue<unsigned int>* releaseQueue);
        virtual ~Generator(void);

	virtual void l2Request(const unsigned int level1Id,
			       const vector<unsigned int> * rols,
			       const unsigned int destination,
			       const unsigned int transactionId=1);
	virtual void ebRequest(const unsigned int l1Id,
			       const unsigned int destination);
        virtual void releaseRequest(const vector<unsigned int>* level1Ids,
                                    const unsigned int oldestLevel1Id);

        virtual void flushReleaseQueue(void);

	virtual bool condition() { 
	  DFThread::cancellationPoint(); 
	  return  m_runActive;
	}

         virtual void ebQueuePush(unsigned int lvl1id) override;
         virtual unsigned int  ebQueuePop(void) override;
         virtual int  ebQueueSize(void) override;
         virtual bool ebQueueEmpty(void) override;

         virtual void releaseQueuePush(unsigned int lvl1id) override;
         virtual unsigned int  releaseQueuePop(void) override;
         virtual int  releaseQueueSize(void) override;
         virtual bool releaseQueueEmpty(void) override;

	 int releaseRequestsReceived();
         void clearCounters();
         ISInfo* getISInfo();
         void resetQueues();
         void start();
         void stop();
      private:
         IOManager * m_ioManager;
         DFFastQueue<unsigned int>* m_ebQueue;
         DFFastQueue<unsigned int>* m_releaseQueue;

         DcTriggerInInfo m_stats;

         int m_releaseRequestsReceived;

         // for garbage collection
         bool m_testGarbageCollection;
         int m_deltaNumberOfLostClears;		// distance between lost clears
         int m_clearLossThreshold;		// distance between calls to garbage collection

         ROSFragmentBuilder m_builder;
         bool m_runActive;
      }; 

      protected:
      Generator * m_generator;

      private:

      DFCountedPointer<Config> m_configuration;

      DFFastQueue<unsigned int>* m_ebQueue;
      DFFastQueue<unsigned int>* m_releaseQueue;

      int m_numberOfLevel1Last;
      // time stamps
      tstamp m_tsStart;
      tstamp m_tsStop;
      tstamp m_tsStopLast;
   };

   inline ISInfo* EmulatedTriggerIn::Generator::getISInfo() {
      return &m_stats;
   }

   inline void EmulatedTriggerIn::Generator::ebQueuePush(unsigned int lvl1id) {
      m_ebQueue->push(lvl1id);
   }
   inline unsigned int EmulatedTriggerIn::Generator::ebQueuePop() {
      return(m_ebQueue->pop());
   }
   inline int EmulatedTriggerIn::Generator::ebQueueSize(){
      return(m_ebQueue->numberOfElements());
   }
   inline bool EmulatedTriggerIn::Generator::ebQueueEmpty() {
      return(m_ebQueue->empty());
   }

   inline void EmulatedTriggerIn::Generator::releaseQueuePush(unsigned int lvl1id) {
      m_releaseQueue->push(lvl1id);
   }
   inline unsigned int EmulatedTriggerIn::Generator::releaseQueuePop() {
      return(m_releaseQueue->pop());
   }
   inline int EmulatedTriggerIn::Generator::releaseQueueSize(){
      return(m_releaseQueue->numberOfElements());
   }
   inline bool EmulatedTriggerIn::Generator::releaseQueueEmpty() {
      return(m_releaseQueue->empty());
   }
}

#endif
