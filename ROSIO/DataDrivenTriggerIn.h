// -*- c++ -*- $Id$ 
// ///////////////////////////////////////////////////////////////////////////
//
// $Log$
// Revision 1.10  2008/02/27 14:33:52  jorgen
//  add SingleFragmentMode for Data Driven Trigger
//
// Revision 1.9  2008/02/13 17:47:43  gcrone
// Use new FragmentRequest/FragmentBuilder architecture
//
// Revision 1.8  2008/01/28 18:36:47  gcrone
// Use new getISInfo method, remove getInfo
//
// Revision 1.7  2006/03/09 19:04:34  gcrone
// IOMPlugin now uses new FSM methods
//
// Revision 1.6  2005/11/25 14:01:53  gcrone
// Removed spurious ;s to make gcc344 happy
//
// Revision 1.5  2005/04/28 20:55:27  gorini
// Adapted MonitorinDataOut to new emon package. Moved Request implementation from ROSCore to here. Moved abstract classes from here to ROSCore to avoid cross dependencies between packages.
//
// Revision 1.4  2005/01/20 19:38:17  gcrone
// New TriggerIn/Request scheme: TriggerIns linked directly with their
// appropriate Requests.  Requests now instantiated by TriggerIn and
// passed to IOManager via IOManager::queueRequest method.
//
// Revision 1.3  2004/10/19 13:08:10  glehmann
// added missing libraries to test programs
//
// Revision 1.2  2004/04/13 10:20:35  gcrone
// Make TriggerIn and DataOut inherit from Controllable.  The
// Configuration is now passed in a new method called setup.
//
// Revision 1.1.1.1  2004/02/05 18:31:17  akazarov
// imported from nightly 05.02.2004
//
// Revision 1.3  2003/11/11 10:56:03  gcrone
// Use DFCountedPointer<Config> in place of Config*.
//
// Revision 1.2  2003/04/08 16:06:05  jorgen
//  Q from int to unsigned
//
// Revision 1.1  2003/01/22 09:20:38  jorgen
//  renamed SWRobin to DataDriven
//
// Revision 1.2  2003/01/16 17:01:16  jorgen
//  using Fast Q
//
// Revision 1.1  2002/12/20 13:53:29  jorgen
//  added SWRobinTriggerIn.h
//
//
//
// ///////////////////////////////////////////////////////////////////////////
#ifndef DATADRIVENTRIGGERIN_H
#define DATADRIVENTRIGGERIN_H

#include "ROSCore/TriggerIn.h"
#include "ROSCore/TriggerInputQueue.h"

namespace ROS {
   class FragmentBuilder;
   //! An emulated TriggerIn class that internally generates Requests.
   class DataDrivenTriggerIn : public TriggerIn {
   private:
      FragmentBuilder* m_builder;

      DFCountedPointer<Config> m_config;
      int m_numberOfRequests;

      TriggerInputQueue* m_inputToTriggerQueue;

      bool m_singleFragmentMode;
      bool m_runActive;
   protected:
      virtual void run();
      virtual void cleanup();
   public:
      DataDrivenTriggerIn();
      virtual ~DataDrivenTriggerIn() noexcept;
      //! Load configuration
      void setup(IOManager* iomanager, DFCountedPointer<Config> configuration);
      void configure(const daq::rc::TransitionCmd&);
      void unconfigure(const daq::rc::TransitionCmd&);
      void prepareForRun(const daq::rc::TransitionCmd&);
      void stopGathering(const daq::rc::TransitionCmd&);
   };
}
#endif
