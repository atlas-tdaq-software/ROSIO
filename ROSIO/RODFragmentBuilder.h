// -*- c++ -*-
// $Id$ 
//
// $Log$
// Revision 1.2  2008/04/03 11:28:22  gcrone
// Add closeFragment method to add ROD trailer
//
// Revision 1.1  2008/02/15 17:29:40  gcrone
// Adapt HardwareTriggerIn to new Fragment Rquest/Builder scheme.
//
//
#ifndef RODFRAGMENTBUILDER_H
#define RODFRAGMENTBUILDER_H


#include "ROSCore/FragmentBuilder.h"

namespace ROS {
   class EventFragment;
   class RODFragmentBuilder : public FragmentBuilder {
   public:
      virtual EventFragment* createFragment(unsigned int level1Id, unsigned int nChannels);
      virtual void appendFragment(EventFragment* parent, EventFragment* child);
      virtual void closeFragment(EventFragment* fragment);
   };
}

#endif
