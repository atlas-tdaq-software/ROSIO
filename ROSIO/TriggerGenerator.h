// -*- c++ -*-  
// ///////////////////////////////////////////////////////////////////////////
//
// Trigger Generator class
//
// $Log$
// Revision 1.20  2008/05/30 09:42:30  jorgen
//  increase EB Q size
//
// Revision 1.19  2008/03/20 12:49:36  jorgen
//  add NIKHEF code for testing MRODs
//
// Revision 1.18  2007/11/07 16:36:48  gcrone
// Fix problesm with missing std:: namespace qualifiers in front of string, vector etc.
//
// Revision 1.17  2005/12/01 13:16:17  jorgen
//  add emulation & testing of garbage collection
//
// Revision 1.16  2005/11/24 17:24:19  gcrone
// Remove spurious ;s to make gcc344 happy
//
// Revision 1.15  2005/10/27 08:25:47  jorgen
//  restructure for use with DDT/ECR
//
// Revision 1.14  2005/09/03 13:55:50  jorgen
//  fix problems with SYNC queues in trigger generator
//
// Revision 1.13  2005/08/25 16:53:53  gcrone
// Removed redundant include of <queue>
//
// Revision 1.12  2005/08/25 11:40:37  gcrone
// Tidy up: Remove #ifdef SYNC_TRIGGER,
//   #include DFSubSystemItem/Config.h instead of ROSCore/TriggerIn.h
//   rename m_tot_instances to m_totalInstances
//   make m_deleteGrouping unsigned
//
//
// ///////////////////////////////////////////////////////////////////////////
#ifndef TRIGGERGENERATOR_H
#define TRIGGERGENERATOR_H

#include <iostream>
#include <set>
#include <queue>

#include "DFSubSystemItem/Config.h"

namespace ROS {
   class TriggerGenerator {
   public:
      TriggerGenerator(DFCountedPointer<Config> configuration);
      TriggerGenerator(const std::vector<unsigned int>& channelList,
                       const std::vector<unsigned int>& roiDistribution,
                       int deleteGrouping,
                       float l2RequestFraction,
                       float ebRequestFraction,
                       unsigned int maxL1=0xffffffff,
                       unsigned int eventsPerEcr=0);

      virtual ~TriggerGenerator() {}
      int generate(int nRequests = 0, int mode = 0);  //Generate next trigger
      // generate a bunch of consecutive L1IDs
      int generateL1idBunch(unsigned int firstL1id, unsigned int lastL1id);
      void printStatistics(float elapsedSeconds=0.,std::ostream &output=std::cout);  // Get operational statistics 
      virtual bool condition() { return true; }

      void set_level1Id(int l1id);
      void setL1List(std::vector<unsigned int>& l1List);
    protected:

      void processReleaseQueue(bool flush);

      //Callbacks
      virtual void l2Request(const unsigned int level1Id,
			     const std::vector<unsigned int> * rols,
			     const unsigned int destination,
			     const unsigned int transactionId=1) = 0;
      virtual void ebRequest(const unsigned int l1Id,
			     const unsigned int destination) = 0;
      virtual void releaseRequest(const std::vector<unsigned int>* level1Ids,
                                  const  unsigned int oldestLevelId) = 0;

     virtual void ebQueuePush(unsigned int lvl1id)=0;
     virtual unsigned int  ebQueuePop(void)=0;
     virtual int  ebQueueSize(void)=0;
     virtual bool ebQueueEmpty(void)=0;

     virtual void releaseQueuePush(unsigned int lvl1id)=0;
     virtual unsigned int  releaseQueuePop(void)=0;
     virtual int  releaseQueueSize(void)=0;
     virtual bool releaseQueueEmpty(void)=0;


     void configure(float l2RequestFraction, float ebRequestFraction);
     void clearStatistics();
      void resetL1();

      unsigned int m_deleteGrouping;

      int m_instance;
      int m_totalInstances;

      bool m_testGarbageCollection;
      bool m_syncDeletesWithEBRequests;

      std::queue<unsigned int> m_queue_ebrequest_ids;
      std::queue<unsigned int> m_queue_all_ids;
      std::queue<unsigned int> m_queue_ebfinished_ids;

    int m_level1Id;

  protected:
    virtual void nextL1();

  private:
    int generateRequestsForOneL1ID(unsigned int l1Id);
    int noRolsInThisRoI(void);
    void generateL2Request(unsigned int l1Id);
    void generateEBRequest(unsigned int l1Id);
    void generateReleaseRequest(unsigned int oldestLevelId);

    //Constants
    static const int c_aleasize = 500000;
    static const int c_maxRolsPerL2PU = 12;	// do not touch. See TriggerGenerator.cpp
    static const int c_maxEBbins = 220;		// goes with EB request Q size !!
    static const int c_maxCLEARbins = 220;
 
    // "static" loop variables
    int m_next;
    int m_transactionId;
   
    //Statistical counters
    int m_numberOfL1ids;
    int m_numberOfL2A ;
    int m_numberOfDeleteGroup;
      std::vector<int> m_numberOfROI;    // # ROIs per ROL
    int m_totalNumberOfROI;
    int m_numberOfL2req;
      std::vector<int> m_roihist;        // # ROLs per ROI
    int m_maxRolsInRoI;

    std::vector<int> m_qEBhist;
    std::vector<int> m_qCLEARhist;

    unsigned int m_alea[c_aleasize];
    unsigned int m_l2ap_i;			// L2A % scaled up
    bool m_disabled;
    int m_numberOfChannels;
    std::vector<unsigned int> m_channelIds;
    std::vector<unsigned int> m_l1Ids;
    std::vector<unsigned int> m_rols;
    std::vector<unsigned int> m_l2puGroup;
    std::vector<unsigned int> m_l2puLimit;
    unsigned int m_destination;
    unsigned int m_inputDelay;
    unsigned int m_maxL1Index;

    // for garbage collection
    std::set<unsigned int> m_l1IdsInProcess;		// set of L1IDs busy with level2 and EB
    unsigned int m_oldestL1id;			// TG will never request L1IDs older(less) than this
    static const int c_maxINPROCESSbins = 200;
    std::vector<int> m_setINPROCESShist;

      std::vector<unsigned int> m_l1List;
      unsigned int m_l1Index;
   protected:
      bool m_useList;
      unsigned int m_eventsPerEcr;
      unsigned int m_ecrCount;
  };
}

#endif  //TRIGGERGENERATOR_H
