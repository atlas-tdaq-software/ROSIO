// -*- c++ -*-
// $Id$ 

#ifndef IOEXCEPTION_H
#define IOEXCEPTION_H

#include "DFExceptions/ROSException.h"
#include <string>

class IOException : public ROSException {

public:
  enum ErrorCode {UNKNOWN_TRANSPORT_ERROR,
                  PROVIDER_CONFIGURE_ERROR,
                  PORT_CREATE_ERROR,
                  SEND_ERROR,
                  DESTINATION_ERROR,
                  UNEXPECTED_DC_MESSAGE,
                  TCP_SEND_ERROR,
                  SOCKET_CREATE_ERROR,
                  SOCKET_OPTION_ERROR,
                  TCP_CONNECT_ERROR,
                  ADDRESS_TRANSLATION_ERROR,
		  TGMAX_ROLS,
		  TGMAX_ROLS2,
		  TGSUM,
		  TGL2F,
                  CIRC_OPEN_ERROR,
                  DIVBY0,
                  UNKNOWN_REQ,
                  SOCKET_BIND_ERROR,
                  SOCKET_ACCEPT_ERROR,
                  SOCKET_LISTEN_ERROR,
                  TCP_READ_ERROR,
                  BAD_DATA_REQUEST,
                  DATAWRITER_OPEN_ERROR,
                  DATAWRITER_SEND_ERROR,
                  RECV_FAIL,
                  UNEXPECTED_DATA,
                  BAD_HEADER,
                  TIMEOUT
  } ;

  explicit IOException(ErrorCode error) ;
  IOException(ErrorCode error, const std::string& description) ;
  IOException(ErrorCode error, const ers::Context& context) ;
  IOException(ErrorCode error, const std::string& description,
              const ers::Context& context) ;
  IOException(const std::exception& cause, ErrorCode error,
              const std::string& description, const ers::Context& context);

protected:
  virtual ers::Issue * clone() const override { return new IOException( *this ); }
  static const char * get_uid() { return "ROS::IOException"; }
  virtual const char* get_class_name() const override {return get_uid();}
  std::string getErrorString(unsigned int errorId) const;

};

inline
IOException::IOException(IOException::ErrorCode error) 
   : ROSException("IOPackage",error,getErrorString(error)) { }

inline
IOException::IOException(IOException::ErrorCode error,
		const std::string& description) 
   : ROSException("IOPackage",error,getErrorString(error),description) { }
inline
IOException::IOException(IOException::ErrorCode error,
                         const ers::Context& context) 
   : ROSException("IOPackage",error,getErrorString(error),context) { }

inline
IOException::IOException(IOException::ErrorCode error,
		const std::string& description, const ers::Context& context) 
   : ROSException("IOPackage",error,getErrorString(error),description,context) { }

inline IOException::IOException(const std::exception& cause, ErrorCode error,
                                const std::string& description,
                                const ers::Context& context) 
     : ROSException(cause, "IOPackage", error, getErrorString(error), description, context) {}

inline std::string IOException::getErrorString(unsigned int errorId) const
{
	std::string result;    
   switch (errorId) {
   case UNKNOWN_TRANSPORT_ERROR:
      result = "Error: unknown transport: ";
      break;
   case PROVIDER_CONFIGURE_ERROR:
      result = "Error from Provider::configure_all: ";
      break;
   case PORT_CREATE_ERROR:
      result = "Error from Port::create";
      break;
   case SEND_ERROR:
      result = "Error from Port::send";
      break;
   case DESTINATION_ERROR:
      result = "Unknown destination";
      break;
   case UNEXPECTED_DC_MESSAGE:
      result = "Unexpected request message received from DC";
      break;
   case TCP_SEND_ERROR:
      result = "Error from send on a TCP connection ";
      break;
   case SOCKET_CREATE_ERROR:
      result = "Couldn't open a socket ";
      break;
   case SOCKET_OPTION_ERROR:
      result = "Couldn't set a socket option ";
      break;
   case ADDRESS_TRANSLATION_ERROR:
      result = "Couldn't translate IP address ";
      break;
   case TGMAX_ROLS:
      result = "parameter m_numberOfChannels it too large ";
      break;   
   case TGMAX_ROLS2:
      result = "parameter m_maxRolsInRoI it too large ";
      break;   
   case TGSUM:
      result = " The sum of the probuXXRols parameters is not 100";
      break;     
   case TGL2F:
      result = " The fraction of ROI requests per ROS exceeds 100%: please increase the mean l2 grouping or decrease the l2 fraction!";
      break;  
   case TCP_CONNECT_ERROR:
      result = "Error in opening connection ";
      break;
   case CIRC_OPEN_ERROR:
      result = "Error opening output buffer ";
      break;
  case DIVBY0:
      result = "Potential division by zero ";
      break;
   case UNKNOWN_REQ:
      result = "Unknown Request type";
      break;
   case SOCKET_BIND_ERROR:
      result = "Couldn't bind a socket ";
      break;
   case SOCKET_ACCEPT_ERROR:
      result = "Couldn't accept a socket ";
      break;
   case SOCKET_LISTEN_ERROR:
      result = "Couldn't set a socket to listen ";
      break;
   case TCP_READ_ERROR:
      result = "Error reading from a TCP connection ";
      break;
   case BAD_DATA_REQUEST:
      result="Bad data Request ";
      break;
   case DATAWRITER_OPEN_ERROR:
      result="DataWriter open failed ";
      break;
   case DATAWRITER_SEND_ERROR:
      result="DataWriter put failed ";
      break;
   case RECV_FAIL:
      result="Failed to receive message ";
      break;
   case UNEXPECTED_DATA:
      result="Unexpectedly received some data ";
      break;
   case BAD_HEADER:
      result="bad DC message header";
      break;
   case TIMEOUT:
      result="Timeout waiting for thread:";
      break;
   default:
      result = "Unspecified error";
      break;
   }
   return(result);
}


#endif
