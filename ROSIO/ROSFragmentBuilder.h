// -*- c++ -*-
// $Id$ 
//
// $Log$
// Revision 1.1  2008/02/07 11:38:06  gcrone
// New fragment building support
//
//
#ifndef ROSFRAGMENTBUILDER_H
#define ROSFRAGMENTBUILDER_H

#include "ROSCore/FragmentBuilder.h"

namespace ROS {
   class EventFragment;
   class ROSFragmentBuilder : public FragmentBuilder {
   public:
      virtual EventFragment* createFragment(unsigned int level1Id,
                                            unsigned int nChannels) override;
      virtual void appendFragment(EventFragment* parent,
                                  EventFragment* child) override;
   };
}


#endif
