// $Id$
// //////////////////////////////////////////////////////////////////////
//   Hardware triggered Null Data Request: Something that acts like a
// data Request but doesn't expect to get any data.
//
//  Author:  G.J.Crone
//
//  $Log$
//  Revision 1.2  2005/11/25 14:01:53  gcrone
//  Removed spurious ;s to make gcc344 happy
//
//  Revision 1.1  2005/08/04 17:34:05  gcrone
//  Remove obsolete Requests and add HWTriggeredRemoteRequest
//
//
// //////////////////////////////////////////////////////////////////////
#ifndef HWTRIGGEREDREMOTEREQUEST_H
#define HWTRIGGEREDREMOTEREQUEST_H

#include <iostream>

#include "ROSCore/DataRequest.h"

//#include "ROSObjectAllocation/FastObjectPool.h"
#include "ROSObjectAllocation/DoubleEndedObjectPool.h"
//#include "ROSObjectAllocation/ThreadSafeObjectPool.h"

namespace ROS {
   class TriggerIn;
   class HWTriggeredRemoteRequest: public DataRequest
   {
   public:
      HWTriggeredRemoteRequest(const unsigned int level1Id,
                                 TriggerIn* trigger);

      virtual ~HWTriggeredRemoteRequest () {};

      int execute(void);
      virtual ::ostream& put(::ostream& stream) const;
      virtual string what();
      void *operator new(size_t) ;
      void operator delete(void *memory) ;
   private:
      bool buildFragment();
      TriggerIn* m_trigger;
   };
  
   inline void *HWTriggeredRemoteRequest::operator new(size_t) {
      //return ThreadSafeObjectPool<L2Request>::allocate() ;
      return DoubleEndedObjectPool<HWTriggeredRemoteRequest>::allocate() ;
      //return FastObjectPool<L2Request>::allocate() ;
   }
  
   inline void HWTriggeredRemoteRequest::operator delete(void * memory) {
      //ThreadSafeObjectPool<L2Request>::deallocate(memory) ;
      DoubleEndedObjectPool<HWTriggeredRemoteRequest>::deallocate(memory) ;
      //return FastObjectPool<L2Request>::deallocate() ;
   }
}
#endif
