#ifndef SLINKDATAOUT_H
#define SLINKDATAOUT_H

//******************************************************************************
// file: SlinkDataOut.h
// desc: DataOut using S-Link, e.g. for ROD emulation
// auth: 07/02/03 R. Spiwoks
//******************************************************************************

// $Id$

#include <string>

#include "ROSCore/DataOut.h"
#include "ROSInfo/DataOutInfo.h"
#include "ROSslink/slink.h"
#include "ROSMemoryPool/MemoryPool_CMEM.h"
#include "DFThreads/DFFastMutex.h"

namespace ROS {
	
// SLinkDataOut class ----------------------------------------------------------

class SlinkDataOut : public DataOut {

  public:
    SlinkDataOut();
    virtual ~SlinkDataOut() noexcept;

    void setup(DFCountedPointer<Config> configuration);

    void configure(const daq::rc::TransitionCmd&);
    void unconfigure(const daq::rc::TransitionCmd&);
    void sendData(const Buffer* buffer, const NodeID, const u_int tid=1,
                  const unsigned int fragStatus=0);

    ISInfo* getISInfo();

  private:
    SLINK_device*	m_dev;
    SLINK_parameters	m_par;
    u_int		m_status;
    MemoryPool *        m_memoryPool;
    DFFastMutex *       m_sendMutex;

    int			m_occurrence;
    DataOutInfo         m_statistics;
};

}	// namespace ROS

#endif	// SLINKDATAOUT
