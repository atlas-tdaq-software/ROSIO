// -*- c++ -*-
// $Id: IOException.h 60078 2009-07-03 15:15:19Z gcrone $ 

#ifndef IOEXCEPTION_H
#define IOEXCEPTION_H

#include "DFExceptions/ROSException.h"
#include <string>

class MEtException : public ROSException {

public:
   enum ErrorCode {BAD_TYPE,
                   BAD_NSAMPLES,
                   MISSING_DETECTOR,
                   BAD_LENGTH,
                   PAGE_SIZE
   } ;

  MEtException(ErrorCode error) ;
  MEtException(ErrorCode error, std::string description) ;
  MEtException(ErrorCode error, const ers::Context& context) ;
  MEtException(ErrorCode error, std::string description, const ers::Context& context) ;
  MEtException(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context);

protected:
  virtual ers::Issue * clone() const { return new MEtException( *this ); }
  static const char * get_uid() { return "ROS::MEtException"; }
  virtual const char* get_class_name() const {return get_uid();}
  virtual std::string getErrorString(unsigned int errorId) const;

};

inline
MEtException::MEtException(MEtException::ErrorCode error) 
   : ROSException("IOPackage",error,getErrorString(error)) { }

inline
MEtException::MEtException(MEtException::ErrorCode error,
		std::string description) 
   : ROSException("IOPackage",error,getErrorString(error),description) { }
inline
MEtException::MEtException(MEtException::ErrorCode error, const ers::Context& context) 
   : ROSException("IOPackage",error,getErrorString(error),context) { }

inline
MEtException::MEtException(MEtException::ErrorCode error,
		std::string description, const ers::Context& context) 
   : ROSException("IOPackage",error,getErrorString(error),description,context) { }

inline MEtException::MEtException(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context) 
     : ROSException(cause, "IOPackage", error, getErrorString(error), description, context) {}

inline std::string MEtException::getErrorString(unsigned int errorId) const
{
	std::string result;    
   switch (errorId) {
   case BAD_TYPE:
      result = "Bad event type";
      break;
   case BAD_NSAMPLES:
      result = "Bad number of samples";
      break;
   case MISSING_DETECTOR:
      result="Missing detector Id -- Update ROSDBConfig";
      break;
   case BAD_LENGTH:
      result="Bad length";
      break;
   case PAGE_SIZE:
      result="MemoryPool pages are too small for the MEt fragment";
      break;
   default:
      result = "Unspecified error";
      break;
   }
   return(result);
}


#endif
