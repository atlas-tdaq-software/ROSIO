// -*- c++ -*- $Id$ 
// ///////////////////////////////////////////////////////////////////////////
//
//  $Log$
//  Revision 1.3  2008/02/14 13:48:19  gcrone
//  Implement getISInfo instead of getInfo
//
//  Revision 1.2  2006/03/09 19:04:34  gcrone
//  IOMPlugin now uses new FSM methods
//
//  Revision 1.1  2005/10/27 13:06:00  jorgen
//  change name DDTECR to Robin
//
//  Revision 1.1  2005/10/27 08:28:17  jorgen
//   data driven trigger for the ROBIN
//
//
// ///////////////////////////////////////////////////////////////////////////
#ifndef ROBINDATADRIVENTRIGGERIN_H
#define ROBINDATADRIVENTRIGGERIN_H

#include "ROSIO/EmulatedTriggerIn.h"
#include "DFSubSystemItem/Config.h"      // for Config
#include "DFThreads/DFCountedPointer.h"  // for DFCountedPointer

namespace daq { namespace rc { class TransitionCmd; } }

class ISInfo;

namespace ROS {
   class IOManager;
   class TriggerInputQueue;
class RobinDataDrivenTriggerIn : public EmulatedTriggerIn {
  public:
    RobinDataDrivenTriggerIn();
    virtual ~RobinDataDrivenTriggerIn() noexcept;
    //! Load configuration
    virtual void setup(IOManager* iomanager,
                       DFCountedPointer<Config> configuration) override;

    // From Controllable
    virtual void configure(const daq::rc::TransitionCmd&) override;
    virtual void unconfigure(const daq::rc::TransitionCmd&) override;

    virtual void stopGathering(const daq::rc::TransitionCmd&) override;

    //! Get operational statistics 
    virtual ISInfo* getISInfo() override;
  protected:
    virtual void run() override;
    virtual void cleanup() override;
  private:
    DFCountedPointer<Config> m_configuration;
    TriggerInputQueue* m_inputToTriggerQueue;
   bool m_runActive;
 }; 

}

#endif
